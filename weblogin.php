<?php
// weblogin.php  called from ..showprod.php login..
// Does login, change password, forgot pswd forms...
// 1/23/2010 added username retreival
//06/21/2012 changed function to fxn -- forgot.. buttons not working --  missing >  fixed.
// 8/5/21 cmt block here and trcheckout
// too many redirects
// if ($_SERVER["SERVER_NAME"]=="127.0.0.1"  || $_SERVER["SERVER_NAME"]=="localhost")
// {
//  	// working locally so don't do anything
// }
// else
// {
//   // Switch to SSL
// 	if ($_SERVER["SERVER_PORT"] != "443")
//   		header("location:https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
// }

  include ('book_sc_fns.php');
  session_start();
  if (!isset($_SESSION['searchstr']))
   		include('set_vars.php');   //Initialize variables
   		
  //Find out which form to display: login (default); change password; 
  //forgot password; ask_Question; get username
  if (!isset($_REQUEST['function']))
    $function='Login';
  else
    $function=$_REQUEST['function'];      
    // can be: change, forgot, ask, user
  
  /*  5/30/2017 -- disable old 
    OnLoad in <body... in tr_header.php
    now use autofocus  Was:    
  //2/4/2010: Set up OnLoad focus prior to 
  //calling tr_header.php <body onload...
  
  switch ($function)
		  {
        case 'change' :  
            $g_onload = 'OnLoad="document.change.old_passwd.focus();"';
             break;
        case 'forgot' :  
            $g_onload = 'OnLoad="document.forgot.username.focus();"';
            break;
        case 'ask' :  
            $g_onload = 'OnLoad="document.ask.answer.focus();"';
            break;
        case 'user' :  
            $g_onload = 'OnLoad="document.user.email.focus();"';
            break;
        default :  
            $g_onload = 'OnLoad="document.login.f_user.focus();"';
        }
	 //echo $_SERVER["SERVER_NAME"].'  '.$g_onload;
     // end of OnLoad code def...
   */
   
   include('tr_header.php');   //Show header with menu

  //start display table
	echo "<table class=login border=0 align=$g_table1_align width=$g_table1_width>
		  <tr >
      <td class=login ><div align=left><br/>&nbsp;<b>Secure web communication.</b> <br/><br/> </div>";
      
//      &nbsp;&nbsp;<br>&nbsp;&nbsp;$g_ssl_img</div></td>
//		  <td class=login>";
//	echo '<center>';
	//Select which function to display
	switch ($function)
		  {
        case 'change' :
          //Called from Profile Form.
          //make sure not being called from outside the program -- user must be logged in
          if (check_web_user())  //(isset($_SESSION['UNAME']))
            display_password_form();    //ouput_fns.php
          else
            echo 'Unable to process.';
            // breechlog('Change pswd')    //future..
          break;
        case 'forgot' :
          // Called from weblogon()
          display_forgot_password_form();  //ouput_fns.php
          // breechlog('Change pswd')    //future..
          break;
        case 'ask' :
          // Called from forgot_password.php,  get question and ask it.
          if (isset($_GET['question']))
              display_forgot_question_form($_GET['question']);    //output_fns.php
          else
            echo 'Unable to process.';
          break;

        case 'user' :
          // Called from weblogon()  -- added 1/23/2010
          display_forgot_username_form();  //ouput_fns.php
          break;

        default :
		      weblogon();  //action_fns.php
		      break;
      }
//		  echo '</center>';
    echo "<div align=left>&nbsp;&nbsp;<br/>&nbsp;&nbsp;$g_ssl_img</div></td></tr></table>";
   //silk_footer($g_table1_align,$g_table1_width);
?>
