<?php
header("Content-type: application/json");
// ziplookupjson.php : zip5 lookup gets array and returns json (in Silk/Twin)
include ('book_sc_fns.php');

if (isset($_POST["this_zip"]))
	$zip = $_POST["this_zip"];
else
	$zip = '99999';
    
$location = ziplookup($zip);
//echo "{\"zip\": \"$zip\" }";
echo json_encode($location); 

?>