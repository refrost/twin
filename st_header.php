<?php
   // st_header.php file   ref&a 12/22/03
   // 5/31/2016 responsive meta..

  include ('head.php'); //responsive meta..
  echo '
		<title>Twinrocker Handmade Invitations</title>
    <meta name="keywords" content="Twinrocker, twinrocker, fine arts, handmade
    paper, watercolor paper, artists paper, art supplies, calligraphy, invitations, stationery,
    wedding invitations, book arts, papermaking supplies, drawing paper, printmaking, custom cotton
    rag, crafts supplies, letterhead">
		<meta name="DESCRIPTION" content="The finest handmade stationery, invitations, and artist paper for watercolor, calligraphy, printmaking, book arts, etc. Papermaking supplies for making your own paper at home, at school, and for fine artists">
		<meta name="AUTHOR" content="Howard Clark ">';
    st_style(); //Links stylesheet to tr.css (previously was a function)

    if (isset($jscript))  //10.19.2005 add JS validation code
    // 11/6/2015 Added passing in array (or one string).
    {
      if(is_array($jscript))   {
        foreach($jscript as $jcode){
            echo insert_javascript($jcode);
        }
      }
      else
        echo insert_javascript($jscript);  
        //Just one script --action_fns.php
    }


	 echo '</head>
         <body marginheight=0 margin=0 bgcolor=#2f4f4f>';
   
   // Show header & menu bar  for Stationery:

   // Define departments and order shown in this array. This defines top menu bar
   // Department names are used in the Products table so any change here must also
   // be made in the dept field in the products table. Grp_default is 
   // the group that shows first once a deparment is selected.


	display_header("Twinrocker Handmade Invitations",$g_table1_width,$g_headerbgcolor);		//Hardcoded for Twinrocker in tr_fns.php

  if (isset($show_help_menu))
  {
  
      // show help menu
      echo "<table border=1 width=$g_table1_width align=center><tr>";
      echo "<td>Help</td>";
      echo "</tr> </table>";

  }
  else
  {
      // Changing dept_array will require updating the 'dept' and maybe 'groupname' fields
      // in the products table to reflect the correspnding changes:
      // Ex.  UPDATE products SET dept='newvalue' WHERE dept='oldvalue';  in phpadmin
      //
      // The grp_default value is the first group to show when clicking on the dept
      
      $dept_array=array('Cards','Invitation Sets','Stationery & Notes','All Sizes and Prices','Leaves & Liners','Specials');  // 'Printing',  
      $grp_default=array('Reply','Small','Personal Note','Selector','Bo Leaves','LetterPress','Current');

	    echo "<table border=1 width=$g_table1_width align=center><tr height=23>";
			
			//When st_header is used on "special" pages (viewcart), pulp, etc.
			//then must pass the "$dept"
			
			if (!isset($dept))
				if (isset($_GET['dept']))
					$dept = $_GET['dept'];
				else
					$dept = '';
					
			// Display Department menu below header
			for ($i=0; $i < count($dept_array);$i++)
			{
				if ($dept==$dept_array[$i])
					$topmenu2class = 'topmenu2_on';
				else
					$topmenu2class = 'topmenu2';
				
				echo "<td class=$topmenu2class > 
					  <a class=$topmenu2class href=st_main.php?dept=".urlencode($dept_array[$i])
					     ."&grp=".urlencode($grp_default[$i]).">".$dept_array[$i]."</a></td>";
			}
						 
	    echo "</tr> </table>";
	    
  }

?>
