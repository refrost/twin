<?php
// set_shipping.php   (c) REF&A 2002
// Called from show_cart Go passed the destination State (session_var), and a ship_type
// 
  include ('book_sc_fns.php');
  session_start();
  
  // post var were here..
  
  if (isset($_POST["this_state"]))
  	$ship_state = $_POST["this_state"];	// Assign value from state dropdown to session_var registers in show_cart
  else
  	$ship_state = '==';

  if (isset($_POST["this_type"]))
  	$ship_type  = $_POST["this_type"];
  else
  	$ship_type  = 'ups';	
  if (isset($_POST["this_special_charge"]))	
  	$ship_special = $_POST["this_special_charge"];
  else
  	$ship_special = '';
	

  if (isset($_POST["this_rush"]))	
  	$ship_rush  = 'ON';
  else
    $ship_rush  = 'OFF';

  if (isset($_POST["this_cod"]))	
  	$ship_cod  = 'ON';
  else
    $ship_cod  = 'OFF';
 
 if (isset($_POST["returnto"]))	// CONTROLS RETURN TO CHECKOUT OR DEFAULT OF SHOW_CART
  	$returnto  = $_POST["returnto"];
  else
    $returnto  = ''; 
 
  $s_catid  = $_POST["catid"];
  
  //$call = $_POST["call"];	//returns to the program it was called from
  
  
  if ($ship_state!= "==") 
  {
	  // 6/5/2010 Added papercart due to addition of count_oversize() in  	
	  calc_shipping($ship_state,$ship_type,$_SESSION["fibercart"],$_SESSION["cart"],$_SESSION["papercart"],$_SESSION["pulp_shpitems"],$s_catid,$ship_special,$ship_rush,$ship_cod);
	  
  }
  else
  {
	  $ship_string = "Please pick a customer or a state.";
 	  $ship_charge = 0.00;
  }
  
  $_SESSION["ship_state"] = $ship_state;
  $_SESSION["ship_type"] = $ship_type;
  $_SESSION["ship_string"] = $ship_string;
  $_SESSION["ship_charge"] = $ship_charge;
  $_SESSION["ship_rush"] = $ship_rush;
  $_SESSION["ship_cod"] = $ship_cod;
  $_SESSION["ship_special"] = $ship_special;
  
  //echo "<a href=show_cart.php?catid>continue</a>";
  	  if ($returnto=='checkout')
	  	header("location:tr_checkout.php");
	  elseif ($returnto=='showcart')
	  	header("location:showcart.php");
  	  elseif ($returnto=='tr_checkout')
	  	header("location:tr_checkout.php");
	  else
  	  	header("location:showcart.php");  //header("location:show_cart.php?catid=".$catid);

	  exit;
?>
