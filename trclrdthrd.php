<?php
// 06/27/2015 php 5.3  (Fixed missing end '>' in waxed_linen... form so items added to cart)
// Processed form -- Called from waxed_linen_colored_thread() in tr_fns.php
include("book_sc_fns.php"); 
session_start();

// trclrdthrd.php  
// Receives item prefix,color code,item suffix to build item number
// (Originally for colored thread), and a qty.  
// Inserts item to cart n times..

// Works for french wax


    if ($_POST["itemsuffix"]=='_')  // _ represents a blank in colored thread
		$suffix = '';
	else
		$suffix = $_POST["itemsuffix"];
	
    $item = $_POST["itemprefix"].$_POST["color"].$suffix;
	$qty = $_POST["qty"];
	$returnto = urldecode($_POST["returnto"]);
	
	//add_qty_to_cart($itemno,$qty);
	// Adds qty of one item to the cart  1/6/03	
	if ($qty > 0)
	{	
		$xcart = $_SESSION["cart"];

		// Pass the itemno. If item is in array, increment qty, 
		// else add item and qty=1 to array.
		for ($i=0; $i < $qty; $i++)
		{
			if(@$xcart[$item])
      			$xcart[$item]++;
		    else 
		      $xcart[$item] = 1;
		}
  
		//calculate_cart_totals($cart);
	    $_SESSION['total_price'] = calculate_price($xcart);  
        $_SESSION['total_weight'] = calculate_weight($xcart);
        $_SESSION['items'] = calculate_items($xcart);

	    $_SESSION['cart'] = $xcart;	
}
	
	//echo $returnto;
    //exit;
	header("location:".$returnto);
	
?>