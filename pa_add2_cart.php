<?php
// php 5.30
// 7/22/2015 chk $onhand - $qtyoncart
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

  include ('book_sc_fns.php');
  // pa_add2_cart.php: 6/10/04 ref&a: made from st_add2_cart
  session_start();

    $itemno = $_POST['itemno'];   //7/30/04 Pass the itemno, then arrange for added cp/hp price add-on elsewhere
    $descrip = $_POST['descrip']; // Descrip from invt; may need to be modified for \CP \HP
    $price = $_POST['price'];
    $surface = $_POST['surface']; // Can be  /CP, /HP, /BP, hp, r
    $qty = $_POST['qty'];

    if (isset($_POST['color']))
        $color = $_POST['color'];     // Color ex. off-white;  [Not used yet...
    else
        $color = '';

    $anchor = $_POST['anchor'];   // Use to return to page/item

    $onhand = $_POST['onhand'];   //  1/26/2007 Added check preventing buying more than onhand
    
    // 7/22/2015  prevent oversells 
    // subtract item qty already oncart
   
    $available = $onhand - paperqtyoncart($itemno);
    
    if ($qty > $available)
    {

      js_alert_back("Note: Quantity ordered exceeds onhand qty less any quantity already on the cart. Please call the office to special order.");
      echo $qty.' .gt. '.$available;
      exit;
      
    }

    // 7/30/04 Paper sheets are not like supplies in that most sheets can be
    // ordered with CP,HP,Both surface treatments which causes an add-on in price
    // For this reason, I added a fourth "cart"
    
    //  OLD CODE ---  added to plain $cart[]   From add2_cart.php

   // Hot Process has an add_on to price of 10%  /HP;hp   /BP is 5%
   // Price changes must also be in ordermemory.php (item4)
   $add_on = 0.00;
   switch ($surface)
   {
      case '/HP':
          $add_on = 0.10;      // 10% add-on
          break;
      case '/B':
          $add_on = 0.05;      // Half price - do two at a time
          break;
      case 'hp':
          $add_on = 0.10;
          break;
      default:
          $add_on = 0.00;
          break;
   }
   
   $surfaceprice=round($add_on*$price,2);
   $thisprice = ($price+$surfaceprice);
   $totalamount =$qty*$thisprice;
   
   // Create this paper line item  -- Keep Price and surfaceprice separate so recalcpaper.php can work well
   // display_tr_papercart() shows sum as the Price
	 $paperitem = array("item"=>$itemno,"description"=>$descrip,"price"=>$price,
						          "surface"=>$surface,"surfaceprice"=>$surfaceprice,"qty"=>$qty,
                      "totalamount"=>$totalamount);

    $papercart = $_SESSION["papercart"];
    
    //Insert this line item into papercart
		$papercart[] = $paperitem;

		// save papercart into SESSION var
		$xcart = $papercart;
		$_SESSION["papercart"] = $xcart;


		//Calculate paper items...
   	    $count = 0;
  		$val = 0.00;
		reset($xcart);
    	foreach ($xcart as $key => $row)
		  {
	 	  if ($xcart[$key]['qty']>0)
	 		{
  	 		   $count = $count + $xcart[$key]['qty'];
	   		$val= $val + $xcart[$key]['totalamount'];
		  	}
		  }
		  //Store into session_vars

  		$_SESSION["paper_items"]= $count;
  		$_SESSION["paper_price"]= $val;

  //return to calling script: pass back anchor which reactivates the re-query with anchor added
/*
  $url = parse_url($_SERVER["HTTP_REFERER"]);
  $script = strrchr($url['path'],'/');
  $script = strtolower(substr($script,1));
  $qry =  $url['query'];

  //echo $_SERVER["HTTP_REFERER"].'<br />'.$script.'<br />'.$qry.'<br />';
  //exit;
*/

  header("location:pa_main.php?anchor=yes#$anchor");
  exit;

?>
