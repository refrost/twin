<?php
// encapsulation. Instead, it�s better to use old style �getters� and �setters�, e.g.:

class Config
{
    private $values = [];
    
    public function setValue($key, $value) {
        $this->values[$key] = $value;
    }
    
    public function getValue($key) {
        return $this->values[$key];
    }
}

$config = new Config();

$config->setValue('testKey', 'testValue');
echo $config->getValue('testKey');    // echos 'testValue'

//--------
$data = [];
if (count($ids)) {
    $result = $connection->query("SELECT `x`, `y` FROM `values` WHERE `id` IN (" . implode(',', $ids));
    while ($row = $result->fetch_row()) {
        $data[] = $row;
    }
}
//--------------


require '/path/to/mustache/src/Mustache/Autoloader.php';
Mustache_Autoloader::register();

//-----------------

// http://www.tungsten-network.com/us/en/schneider/
?>