<?php
// trs_shipping.php   (c) REF&A 2002
// Called from show_cart passed the destination State (session_var), and a ship_type
// 
  include ('book_sc_fns.php');
  session_start();
  
  $ship_state = $this_state;	// Assign value from state dropwdown to session_var registers in show_cart
  if (this_state=='--')
/*
*   	{
  		//no state has been selected yet so send back a message
		$ship_string = 'No State selected';
	}
*/	
  

	
  //get the cart_weight
  $cart_weight = calculate_weight($cart);
  
  //get the fiber_weight
  //$fiber_weight = calculate_fiber_weight($fibercart,'$wt');
  //$bales = calculate_bales($fibercart);
  //function calculate_fiber_weight($fcart)
//{  
    $fiberwt = 0;
	$balewt = 0;
	$bales = 0;

	reset($fibercart);
    
    foreach ($fibercart as $key => $row)
	{
	 if ($fibercart[$key]['totalweight']>0)
	 	{
			if (strtoupper(substr($fibercart[$key]['isbn'],-2)) == '-B')
			{
				//it's a bale
				$bales = $bales + 1;
				$balewt = $balewt + $fibercart[$key]['totalweight'];
			}
			else
	 			$fiberwt = $fiberwt + $fibercart[$key]['totalweight'];
		}
	} 
	//RETURN ??;
//}

  //get the pulp_weight
  
  // Begin with plan for 'ups' and 'rush' ship_types
  switch ($ship_type)
  {
  	case "ups" :
		{
			//echo 'xx'.$ship_state.'   </br>';
			
			$rates = get_ups_rate($ship_state);	//$ship_state in session _var??
			foreach ($rates as $key => $value)
				{
				//echo "<br>Key:  $key    Value:  $value[0] $value[1] $value[2]  $value[3]  <br>";
				$this_state =$value[0];
				$first5 = $value[1];
				$over5 = $value[2];
				$pulprate = $value[3];
				}
			
			$total_wt = ($cart_weight+$fiberwt);
			$wt_over5 = $total_wt - 5;
			if ($wt_over5 < 0)
				$wt_over5 = 0;
			
			$ret = '';
			/*
			echo $this_state."   Standard Items: $cart_weight  <br>
	                     		 Fiber: $fiberwt <br>
	   				     		 Total: $total_wt lbs<br><br>";
			
			echo			     "<h5>UPS Ground = $$first5 for 1st 5 lbs + $wt_over5 lbs over 5 lbs x $$over5/lb =  $".number_format(($first5+($wt_over5*$over5)),2).'</h5>';
			*/
			$ret =	"UPS Ground to ".$this_state." = $".$first5." for 1st 5 lbs + ".$wt_over5." lbs over 5 lbs x $".$over5."/lb =  $".number_format(($first5+($wt_over5*$over5)),2).'</br>' ;	
			$ship_charge = ($first5+($wt_over5*$over5)); 			 
		
			if ($bales > 0)
			{
                if ($bales ==1))
                    $strname = 'bale';
                else
                    $strname = 'bales';
				$ret1 = $bales."  $strname to be sent Freight Collect weighing: ".$balewt." lbs" ;
				$ret = $ret.$ret1;
			}
			//echo $ret;   
			$_SESSION["ship_string"] = $ret;
			
			header("location:show_cart.php?catid=$catid");
			
		
		}
  }
?>