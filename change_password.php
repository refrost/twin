<?php
 // Change password: receive old and new password(s); compare, reset_password; Message
 // php5.3
 
 include ('book_sc_fns.php');
 session_start();

 if (!check_web_user())  //If there is no user logged in...
  pass_msg('Unable to process request.');  // prevent access from browser

 
 if (!filled_out($_POST))
 {
   pass_msg("You have not filled out the form completely.
         Please try again. <a href=weblogin.php?function=change>Change Password</a>");
 }
 else 
 {
    // Get posted vars
    if (isset($_POST['old_passwd']))
        $old_passwd = $_POST['old_passwd'];
    else
        $old_passwd = '';

    if (isset($_POST['new_passwd']))
        $new_passwd = $_POST['new_passwd'];
    else
        $new_passwd = '';
        
    if (isset($_POST['new_passwd2']))
        $new_passwd2 = $_POST['new_passwd2'];
    else
        $new_passwd2 = '';

    //Test entries....
    if ($new_passwd != $new_passwd2)
    {
       pass_msg("Passwords entered were not the same.
            No change made.<a href=weblogin.php?function=change>Try Again</a>");
    }
    else
    {
      	// Check to make sure the password is long enough and of the right format.
	      //if (!xregi ("^[[:alnum:]]{8,16}$", $new_passwd)) // 8/19/2015
          if (preg_match("/^[a-z0-9]{6,16}+$/i", $new_passwd)!== 1) 
           pass_msg("New password must be between 6 and 16 characters, letters and numbers.
                  <a href=weblogin.php?function=change>Change Password</a>");
        else
        {
            $valid_user = get_username();
            // attempt update
            if ($valid_user && change_password($valid_user, $old_passwd, $new_passwd))
              pass_msg("Password changed.");
            else
            {
              pass_msg("$valid_user, $old_passwd, $new_passwd  Password could not be changed. If problem persists, email the webmaster. <a href=weblogin.php?function=change>Change Password</a>");
            }
        }
     }
 }

