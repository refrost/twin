<?php
include ('book_sc_fns.php');
//bboardcost.php   calculates price break for 1/2 sheets of binder board 
//                 and then adds to cart.
// 01/05/2015 See recalccart.php
// 03/22/2015 php 5.3 upgrd
session_start();

//$size = array('59','79','82','98');		// These are the 'correct' numbers in the SBT item number
$size = array('59','74','82','98');   // changed 79 to 74 3/5/07

$qty[0] = (integer)$_POST['qty059'];
$qty[1] = (integer)$_POST['qty074'];  // was $_POST['qty079']
$qty[2] = (integer)$_POST['qty082'];
$qty[3] = (integer)$_POST['qty098'];
$gotouri = $_POST['gotouri'];

$total = $qty[0]+$qty[1]+$qty[2]+$qty[3];	//Add qty up

ba_style();
echo '<table class=descrip width=100% align=center border=1><tr><td  valign=top>';

if ($total < 2)
{ 
	echo "You must order at least 2 half sheets of Binder's Board.<br>";
	echo "<a href='".$gotouri."'>Return</a>";

}
else
{
	// Figure which price level applies for sum of items
	switch ($total)
	{
		case $total>25 :
			$level = 'C';
			break;
		case $total>9 :
			$level = 'B';
			break;
		default :
			$level = 'A';
			break;
	}	
	echo "Price level $level<br>";
	//Now add each qty to cart using the right item number.

	$xcart = $_SESSION["cart"];
	
	for ($j=0;$j<4;$j++)
	{
		if ($qty[$j]>0)
		{
			$item = 'BDH'.$size[$j].$level;
			echo $item.' '.$qty[$j].'<br>';
				
				
			// Adds qty of one item to the cart  1/6/03	
			if ($qty[$j] > 0)
			{	
				// Pass the itemno. If item is in array, increment qty, 
				// else add item and qty=1 to array.
				for ($i=0; $i < $qty[$j]; $i++)
				{
					if(@$xcart[$item])
      					$xcart[$item]++;
		    		else 
		      			$xcart[$item] = 1;
				}
			}
		}
	}

	$_SESSION['total_price'] = calculate_price($xcart);  
	$_SESSION['total_weight'] = calculate_weight($xcart);
    $_SESSION['items'] = calculate_items($xcart);

	$_SESSION["cart"] = $xcart;	
    
	header("location:".$gotouri);

}

echo '</td></tr></table>';

?>
