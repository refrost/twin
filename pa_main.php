<?php
//pa_main.php ref&a 12/25/03  Main Page of 3rd section (Paper)
//Adapted ftom st_main.php
//rev. 1/25/2010 Excluded GEL's from allpurpose 'NOT FIND_IN_SET'  
//rev. 7/8/2011  (bug) from sum(onhand-aloc) in mysql to count rows for (onhand-aloc)>0
//rev. 3/26/2012  Change Water color sampler, header text
//rev. 6/21/2012  Changed water color sampler test (removed  'tinted')
//rev. 7/18/2012  Added 'per sheet'  in 'Be Aware' and  '/sheet' in table header
//rev. 5/7/2013   Gouache and watercolor radio button changes
//                Added uses=watercolor in papergrroups table at end of enum set.
// php 5.30
// 6/13/2017 Remove decorative swatch..
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

include_once ('book_sc_fns.php');
  session_start();
  // Define Netscape Table colors... matches w/ css
  $leftmenubg='#ffcc99';		// flesh colored background color of leftmenu table
  if (!isset($_SESSION['searchstr']))
      include('set_vars.php');
   // find out which dept,groupname, & product
   // Set defaults:

   if (isset($_GET['dept']))
  	 $dept = $_GET['dept'];
   else
   	 $dept = 'pa_main.php';
   if (isset($_GET['grp']))
  	 $groupname = $_GET['grp'];
   else
     $groupname = '-';
   if (isset($_GET['prod']))
  	 $prod = $_GET['prod'];
   else
   {
   if ($dept=='pa_main.php')
	 	  $prod = 'Intro'; 	// Coming to page no or missing dept
	 else
	 	  $prod = ''; 		// Set blank and then use as a switch below to get first product
   }

   // Get products in this group to list out on the left menu


   $conn = db_connect();

   if (isset($_SESSION["SESSION_ADNAME"]))
   {
	   $query = "select product,display from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' ORDER BY displayorder ";   //
   }
   else
   {
	   $query = "select product,display from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and display = 'y' ORDER BY displayorder ";   //
   }

   //echo $query;
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   $theseproducts = db_result_to_array($result);
   @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

   // Get the available groupnames for this dept (that get listed horizonally..
   $query = "select DISTINCT groupname  from products where dept='".addslashes($dept)."'";   //
   //echo $query;
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   $thesegroups = db_result_to_array($result);
   @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);


   // Get one product
   // $conn = db_connect();
   if (isset($_SESSION["SESSION_ADNAME"]))
   {
   		if ($prod)  //if passed via ...
	   		$query = "select * from products where product='$prod'  and dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."'";
   		else
   	   		$query = "select * from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' ORDER BY displayorder LIMIT 1";   // get the first one in this dept+groupmname
   }
   else
   {
   		if ($prod)  //if passed via ...
	   		$query = "select * from products where product='$prod'  and dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and display = 'y'";
   		else
   	   		$query = "select * from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and display = 'y' ORDER BY displayorder LIMIT 1";   // get the first one in this dept+groupmname
   }

    //echo $query;
   $result = @mysqli_query($conn, $query);
   //echo $result;
   if (!$result)
   		// No product name so see if can display the first product in this dept/group
     return false;
   $num_invt = @mysqli_num_rows($result);
   //if ($num_invt ==0)
   //   return false;
   $thisproduct = db_result_to_array($result);
   @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   if (!$prod && $thisproduct)
   	  $prod = $thisproduct[0][0];  // if first time on group, set $prod...

   //------------------- done getting the data, now display...

   // Display Header and "sections" menu menubar
   //echo $_SESSION['ship_state'];
   //echo $GLOBALS['ship_state'];

  $jscript = "validpaperform";  //1/26/2007 added check on qty > onhand
   include ('pa_header.php');

  // Show "Groups" menubar
	$cnt = count($thesegroups);
	if ($cnt>1)   // Show groups menu if more than one subgroup
	{
		echo "<table class=leftmenu  border=1 align=$g_table1_align width=$g_table1_width >
			<tr height=20><td class=topmenu3 width=100 ><div align=center><font color=silver>Subgroup >>>&nbsp</font></div></td><td class=topmenu3>";
		for ($i=0; $i < $cnt; $i++)
		{
			if ($groupname==$thesegroups[$i][0])
				$topmenu3class = 'topmenu3_on';
			else
				$topmenu3class ='topmenu3';

			echo "<a class=$topmenu3class href=pa_main.php?dept=".urlencode($dept)."&grp=".urlencode($thesegroups[$i][0]).">".$thesegroups[$i][0]."</a> | ";
		}

		echo "</td></tr></table>";

  }


	$ht = '';
   	$wd = '';

   	if  (isset($thisproduct[0]['product']) )  //if there is a product name defined...
   	{

		// Display the main table, starting with the left menu
		// Netscape demands that background color -- css doesn't seem to work
		echo "<table  width=$g_table1_width border=1 align=center bgcolor=#ffffff><tr>";
    
    if (count($thisproduct)>0)   // More than one item
		{ 
        echo "<td  width=100  valign=top ><table border=0>";

		    //List out the products in this group down the leftmenu
		    for ($i=0;$i<count($theseproducts);$i++)
		    {
			     if ($theseproducts[$i]['display']=='n')
				    $mdisplay = '*';  //Indicates that product is set to NOT display -- shows to Admin users
			     else
				    $mdisplay = '';

			     if ($prod==stripslashes($theseproducts[$i][0]))  //Controls highlite
				      $leftmenuclass = 'leftmenu_on';  //'leftmenu_on';
			     else
				      $leftmenuclass = 'leftmenu';
          
			     echo "<tr><td width=108 bgcolor=$leftmenubg class=$leftmenuclass>"
              .product_URL_no_br("pa_main.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode($theseproducts[$i][0]),stripslashes($theseproducts[$i][0].$mdisplay),"class=$leftmenuclass").'</td></tr>';
		    }
		    echo '</table></td>';

		}
      


      echo '<td valign=top>';


    	if ($thisproduct[0]['height'])
   			$ht = " height=".$thisproduct[0]['height'];
	    if ($thisproduct[0]['width'])
   			$wd = " width=".$thisproduct[0]['width'];
	    if ($thisproduct[0]['align'])
   			$align = " align=".$thisproduct[0]['align'];
	    else
	   		$align = '';


 	    echo '<table class=descrip border=0><tr><td width=480>
   			<div class=product>'.stripslashes($thisproduct[0]['name']).'</div>';
      // Show subheader, if any...
      if ($thisproduct[0]['subheader'])
          echo '<div class=product_subheader>'. $thisproduct[0]['subheader'].'</div>';

        // Show ALERT, if any...
        if ($thisproduct[0]['alert'])
          echo '<br><div class=product_alert >'. $thisproduct[0]['alert'].'</div><br>';

        echo '</td>';
		//Place 'Next' button in a place that doesn't move  (leftmenu)
		echo "<td  align=right >";
		if (check_admin_user())
		{
			echo product_admin($dept,$groupname,$prod).'  '.product_URL_no_br("pa_main.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu");
		}
		else
		    echo product_URL_no_br("pa_main.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu");
		echo '<td></tr></table>';


	  echo '<table border=0 class=descrip width=100% ><tr><td  valign=top>';

		//set flag to exclude empty pictures or items
		$mpics = trim($thisproduct[0]["smallimg"]);
		$mitems = trim($thisproduct[0]["items"]);
		$mpricetable = trim($thisproduct[0]["pricetable"]);
		$comments = trim($thisproduct[0]["comments"]);
		$moreinfo = trim($thisproduct[0]["moreinfo"]);

      
		// Display the product picture and the text description
		if (!empty($mpics))
			echo '<a href='.$thisproduct[0]['bigimg'].
				'><img src='.$thisproduct[0]['smallimg']." alt='".$thisproduct[0]['alt']."' $ht $wd $align".
			'></a>';
			
	//3/5/2011 Add EVAL as code if begins with php::
	$pos = strpos(stripslashes(trim($thisproduct[0]['descrip2'])),'php::');
	if ($pos=== false)
    {
   		echo '</td><td  valign=top><b>'.trim(stripslashes($thisproduct[0]['descrip1']).'</b> '.
   			stripslashes($thisproduct[0]['descrip2'])).'</td></tr>
   			</table>';
   	}
   	else
   	{
			echo '</td><td  valign=top>';
			$code = substr(trim($thisproduct[0]['descrip2']),5);
			EVAL("$code");
			echo '</td></tr></table>';
    }


		// Display the "More Info" line/separator, if prices are displayed...
		if (!empty($mitems)||!empty($mpricetable))
		{
		    echo '<table border=1 width=480 align=center>
				<tr class=info>';
			if ($moreinfo)
			{
				echo '<td >';
				$url = 'prodinfo.php?str='.urlencode(addslashes($moreinfo)).'&item='.urlencode($prod);
				moreinfo($url, 'More Info (in separate window)', $aclass='');
				echo '</td>';
			}

			echo '<td>Click picture to enlarge; Click price to Add-to-Cart.</td></tr>
				</table>';
		}

		//Items listed  as  SBTITEMNUMBER,short description.  Code looks up price and creates url to add-to-cart
		if (!empty($mitems)||!empty($mpricetable))
		{
			// Start to display the pricing info (from product.items and/or product.pricetable
			echo "<table class=pricetable border=0 width=480><tr><td>";
		
		  if (!empty($mitems))
		  {
			  $itempair = explode("\n",trim(stripslashes($thisproduct[0]["items"])));

	   		//echo "<table class=pricetable border=1 width=480>";
		    for ($i=0; $i<count($itempair); $i++)
   			{
   				$thisone = explode("|",$itempair[$i]);
				  $item = $thisone[0];
				  $itemname = $thisone[1];
				  if ($item)
				  {
					   $thisprice = silk_buy($item);
	   				 echo  "<tr><td align=right>$itemname</td><td>&nbsp;&nbsp;&nbsp;</td><td width=100>&nbsp;".$thisprice."</td><td>&nbsp;</td></tr>";
				  }
		    }
		   }

		  // If there is code in the product.pricetable field,
		  // EVALUATE it (usually a function in inc/tr_fns.php)
		  if ($mpricetable)	// This inserts HTML code, forms, etc, to call special functions.
		  {
			   echo  "<tr><td align=left colspan=4>";
			   EVAL("$mpricetable");
			   echo '</td></tr>';
		  }
    
      echo '</table>';
    }


		// If there are comments, add these next.
		if ($comments)
		{
			echo "<table class=comment border=1 width=480><tr><td>";
			echo '<tr><td  colspan=4 align=left>';
			EVAL("$comments");
			echo '</td></tr></table>';
		}


		## If in admin mode, allow access to Add/edit product records:
		if (check_admin_user())
		{
      echo '<tr> <td colspan=4 align=right>'.
            product_admin($dept,$groupname,$prod).'  '.product_URL_no_br("pa_main.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu").'</td></tr>';
		}
		else
			echo '<tr><td colspan=4 align=right>'.product_URL_no_br("pa_main.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu").'</td></tr>';;

		echo '</table>';
	}
	
  
  else  //No specific product to show so SHOW THE PAPER SELECTOR TABLES,
	
  
  {
    if (!isset($_POST['submit']) && !isset($_GET['anchor']) && !isset($_POST['find_one']) && !isset($_POST['one_watercolor']) )
    {
      //Not called from paper selector form  so
      //show picture and terms and ship next day message.
	  echo '<table class=supply  border="0" cellpadding="1" cellspacing="0" width='.$g_table1_width.'  align=center>
      <tr>
      <td class=supply align=center><img src="images/main/kathy3.jpg" width="100" height="100" border="1"></td>
      <td class=supply colspan="5">
      <font color=red><b>&nbsp;&nbsp;Welcome to our dynamic web site!
              <br>&nbsp;&nbsp;Order handmade paper and pay on-line anytime, day 
              <br>&nbsp;&nbsp;or night. Stocked orders received by 3 pm EST ship
              <br>&nbsp;&nbsp;UPS Ground the next business day. 
              <br>
              <br>&nbsp;&nbsp;For BEST service on RUSH ORDERS,  call us 
              <br>&nbsp;&nbsp;at 1-800-757-8946.
              </b></font> <br>
      </td>
      <td class=supply  style="text-align:center">&nbsp;<img src="graphics/visa.gif" height="16" width="100" alt="[Visa, Mastercard, etc.]">
              <br> <small> <a href=showprod.php?dept=MyMenu&grp=Company&prod=Terms>Terms</a> <a href=showprod.php?dept=MyMenu&grp=Company&prod=Privacy>Privacy</a></small>
          </td></tr>
      <tr><td class=supply></td>
      <td class=supply align="center" colspan="5"></td><td class=supply><br />';
         
      $dcodes =   $_SESSION['discount_codes'];
      show_discount_form($dcodes,'pa_main.php',true);  // Get discount codes   5/10/2013
      
      echo '</td></tr></table>';

      
      
      //SHOW THE PAPER SELECTOR Followed by the RADIO BUTTONS FORM 
      //paper_radios() is in inc/tr_fns.php and contains selects for watercolors,
      //               all-purpose and the select by usage/color/weight


      echo '<table class=pa_main_page  border="1" cellpadding="1" cellspacing="0" width='.$g_table1_width.'  align=center>

        <tr height="57"> <td colspan="7">';
        
      echo '<h3>Select Twinrocker Handmade Papers for your project:</h3>

          <p class=pa_text>We make many types of paper in a range of 
          colors, sizes, and weights. Most items can be provided 
          with cold pressed or hot pressed on both sides, or 
          cold pressed surface on one side and hot-pressed
          on the other surface -- which we call "Both".
             Use the Selectors below to find the right paper for your 
             project, or <u>if you prefer, call '.  $g_payment_phone .' to talk with 
             Travis or Fran</u>. 
             </p>';
                     
         ?>
         <div class=product><a href="pa_main.php?dept=Specials&grp=Current"><strong>Check our current Specials!</strong></a></div>
   			<br />
        <table class=descrip border=0><tr><td width=<?php echo $g_table1_width; ?> >
        
   			<div class=product>New to using our papers? Select one of these paper swatch sets:</div>
         </td><td  align=right ><td></tr></table>
         <table border=0 class=descrip width="100%" ><tr><td  valign=top>
         <a href=images/bookarts/baswatch.jpg>
         <img src=images/bookarts/xbaswatch.jpg alt='Twinrocker Handmade Paper Swatch Set:' height="100"  ></a></td>
         <td  valign=top><b></b>          About 40 labeled samples with information on inventory, prices, thickness and surface.
         <br /><br /> Twinrocker Handmade Paper Swatch Set:<br /> <?php echo silk_buy("SWATCH"); ?> Postpaid (Click price to Add to Cart)</td></tr>

			   </table>
			   <br />
        <!-- 6/13/2017 cmt dec swatch 
        <table class=descrip border=0><tr><td width=<?php echo $g_table1_width; ?> >
         </td><td  align=right ><td></tr></table>
         <table border=0 class=descrip width="100%" ><tr><td  valign=top>
         <a href=images/bookarts/baswatch3.jpg>
         <img src=images/bookarts/xbaswatch3.jpg alt='Twinrocker and Decorative Paper Swatch Set:' height="100"  ></a></td>
         <td  valign=top><b></b>           Labeled samples of both Twinrocker and Decorative Papers with information on inventory, prices, thickness and surface.
         <br /><br /> Twinrocker and Decorative Paper Swatch Set:<br /> <?php echo silk_buy("SWATCHTO"); ?> Postpaid (Click price to Add to Cart)
         
         </td></tr>

			   </table>	
        -->
         <br />
        
      
        <table class=descrip border=0><tr><td width=<?php echo $g_table1_width; ?> >
         </td><td  align=right ><td></tr></table>
         <table border=0 class=descrip width="100%" ><tr><td  valign=top>
         <a href=images/paper/wcsampler.jpg>
         <img src=images/paper/xwcsampler.jpg alt='Watercolor Paper Swatch Set:' height="100"  ></a></td>
         <td  valign=top>  Three sheets of  8" x 10" White Watercolor paper, one of each surface: Cold
 Press, Hot Press and Rough. <br /><br /> 
Watercolor Sampler Set 
<?php // 6/21/12 edited: One sheet each of the four tinted watercolor papers in an 8" x 10" size.
 echo silk_buy("SWAT-W"); ?> Postpaid (Click price to Add to Cart)</td></tr>

			   </table>	
	      
         <br />	
         	
         <div align="left"><a href="showprod.php?dept=Book+Arts&grp=Samplers">More Swatch Sets</a></div>
         <br />
        <?php

      //  Show the water colors  and all papers select menus and the radio button selectors.
      echo '<div align=center><a name="wcpaper"></a>'.
        paper_radios().'
        </div>
        </td></tr></table>';

    }
    else // returning to this page to show results of a lookup..
    {
      // Coming from Post OR  returning from pa_add2cart.php
      // which means  need detect and GET and proceed accordingly...FIX 7/30/04
      // ...
      // 2/7/05:  First, find out if coming from watercolor form
      // The watercolor form passes the first  five char of the item name
      // and the submit button is named 'one_watercolor'.  
      // If that submit button is set then we reuse the code from the prior development
      // but, when time comes, select only the item name that the person selects.
      // I've left the weight selector code in here in case must add later.
          
      $watercolor = isset($_POST['one_watercolor']); 
      //if coming from watercolor selector
      //AND for the specific item number fragment
      // ZWHWC, other.. defined in tr_fns.select_one_watercolor_paper_type()
      if ($watercolor)
          $thiswatercolor = $_POST['watercolor']; //Passed from selector box
      else 
          $thiswatercolor = '';
          
      //echo 'xx '.$thiswatercolor;
          
      
      if (isset($_GET['anchor']))
      {
          // Returning from pa_add2_cart so recover last $query
          $query = $_SESSION["paper_query"];
          $paperuses = $_SESSION["paper_uses"];
          $papercolor = $_SESSION["paper_color"];
          $papervalue = $_SESSION["paper_value"];
          $gmweight = $_SESSION["paper_gmwt"];
          $paperweight = $_SESSION["paper_weight"];
          $watercolor = $_SESSION["watercolor"];
          $thiswatercolor = $_SESSION["thiswatercolor"];
      }
      else
      {
        if (isset($_POST['submit']))
        {
          // Coming from Paper Selector, so build new query  
          //  5/7/2013 - added uses=watercolor in table at end of enum set.
          $paperuses=$_POST['paperuses'];
          $papercolor=$_POST['papercolor'];
          $papervalue=$_POST['papervalue'];
          $paperweight=$_POST['paperweight'];
          $gmweight = '--';
          $query = "select * from papergroups where ";

          if ($paperuses!='--')  //if not all  
          {
            $query .= "uses LIKE '%$paperuses%' ";
          } 
   		  else
   	   	    $query .= "1 ";
   	   	    
          if ($papercolor!='--')  //if not all
	   		    $query .= " and color='$papercolor' ";
          if ($papervalue!='--')  //if not all
	   		    $query .= " and value='$papervalue' ";

          $query .= " AND active='y' ORDER BY displayorder ";    // 5/7/13 added active='y'
        }
        else
        {
          //Must be coming from <select> one papercode
          //or, from selecting one watercolor:  Added 2/7/05
          //Kludged in below...
          // The watercolor form passes the papercode of the item name
          // and the submit button is named 'one_watercolor'.  
          // If that submit button is set then we reuse the code from the prior development
          // but, when time comes, select only the item name that the person selects.
          // I've left the weigth selector code in here in case must add later.
     
          if ($watercolor)
          {
            $query = "select * from papergroups where papercode='$thiswatercolor'";
          }
          else
          {
            $one_type = $_POST['papertype'];
            //echo 'vv'.$one_type;
            $query = "select * from papergroups where papercode='$one_type' ";
          }
          
          $paperuses='--';
          $papercolor='--';
          $papervalue='--';
          $gmweight='--';
          $paperweight='--';
        }
        // Now save the present query to re-use when returning from pa_add2_cart.php
        // via _GET ~anchor (above)
        $_SESSION["paper_query"] = $query;
        $_SESSION["paper_uses"] = $paperuses;
        $_SESSION["paper_color"] = $papercolor;
        $_SESSION["paper_value"] = $papervalue;
        $_SESSION["paper_gmwt"] = $gmweight;
        $_SESSION["paper_weight"] = $paperweight;
        $_SESSION["watercolor"] =$watercolor;
        $_SESSION["thiswatercolor"] = $thiswatercolor;

      }
      
      //echo $query;  //XXX
      
      // Run the query and display the selected papers
   	  $result = @mysqli_query($conn, $query);
      $num_types = @mysqli_num_rows($result);
      
      if ($num_types == 1)
        $type_str='This paper type fits ';
      else
        $type_str='These paper types and items fit ';

      //Retrieve the gm/sq m weight to show in the header of the list
      $a_paperweight=array(array('-Any weight-','--'),
                    array(' 80g/sq m Thin Text ','TT'),
                    array('115g/sq m Text','T'),
                    array('200g/sq m Heavy Text','HT'),
                    array('255g/sq m Light Art','LA'),
                    array('410g/sq m Art','A'),
                    array('460g/sq m Heavy Art','HA'),
                    array('650g/sq m Board','B')
                  );
       $w_cnt = count($a_paperweight);
       $thisweight = ' '.$paperweight.'/';
       for ($n=0; $n<$w_cnt; $n++)
       {
          if ($paperweight == $a_paperweight[$n][1])
          {
              $gmweight = $a_paperweight[$n][0];
              break;
          }
       }

      /*if (!$result)
        return false;
      $num_invt = @mysqlx_num_rows($result);
      $thisgroup = db_result_to_array($result);
      @mysqlx_free_result($result);
      */
      // 5/7/2013 retain uses spelled wrong as Guache in table enumorater and query, 
      // but fix spelling (Gouache) in the page display:
      $p_uses  = ucwords($paperuses);
      $p_uses  = str_replace('Water Media/Guache','Gouache',$p_uses);
      echo '<table class=pa_main_page  border="1" cellpadding="1" cellspacing="0" width="'.$g_table1_width.'"  align="center">'.
           '<tr><td colspan="4"><p class=pa_criteria>Criteria => &nbsp;&nbsp;
            &nbsp;  '.$p_uses .'  '.ucwords($papercolor).'  '.
            ucwords($papervalue).'  '.$gmweight.' ('.$paperweight.') 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=pa_main.php>
            Select Again</a></p>
           Please be aware: 
           <br />1. On-hand quantity numbers shown below (left of price) are approximate.                          more swatch
           <br />2. Quantities only update after order checkout.
           <br />3. Hot Pressed surfacing adds 10% to cost; Both (one side hot, one cold) adds 5%. 
           Cold pressed surface both sides is free. 
           <br />4. Price is per sheet.
           <br /><hr>
           Browse, or select your favorites from the these paper types: 
           (Only instock items will respond and show)<br /> ';
          // Build list of links of papertypes... 10/15/05  
          while ($row=mysqli_fetch_array($result))
   	      {
              echo '<a href="#'.$row['papercode'].'">'.$row['papername'].'</a>&nbsp;&nbsp; ';
          }
          echo '</td></tr>';
          //'.$type_str.'
      
      $onhand_gt_zero = 0;    //  10/10/2005 -ref- add logic to show "presently out of stock.."

      $result = @mysqli_query($conn, $query);  // redo the query
      while ($row=mysqli_fetch_array($result))
   	  {
        $thistype = $row['papercode'];
        
        if ($watercolor)    //if searching for one water color...
        {
            //Get the detail, for this papercode
            //AND for the specific item number fragment
            // ZWHWC, other.. defined in tr_fns.select_one_watercolor_paper_type()
            // $thiswatercolor Set above,  Passed from selector box
            
            if ($paperweight == '--')
            {
              //show all weights  // Watercolors have an H-N in the next to last position of the ItemNo  11/19/05 -ref-
              $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thiswatercolor%' 
                      AND (onhand-aloc) > 0";
            }
            else
            {
               //Show only the selected weight...
               $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thiswatercolor%' 
                      AND descrip LIKE '%$thisweight%'
                      AND (onhand-aloc) > 0";
            }
        }
        else
        {        
            //Get the detail, for this papercode  -- chose an All Purpose name
            if ($paperweight == '--')
            {
              //show all weights
              $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thistype%'
                      AND (onhand-aloc) > 0";
            }
            else
            {
               //Show only the selected weight...
               $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thistype%' 
                      AND descrip LIKE '%$thisweight%'";
            }
        }
        //Figure out if there ARE items in this group to display
        // 7/8/2011 Previously did it on: sum(onhand-aloc) as sum_onhand,
        // but that failed as a negative sum if LARGE aloc on a large special order.
        // change to just count the rows where (onhand-aloc) > 0 -- ref  
     	  $summed = @mysqli_query($conn, $query);
        //$sum_items = @mysqlx_fetch_array($summed); // Rev 7/8/2011
        // Count rows that have positive (onhand-aloc):
        $theseitems = @mysqli_num_rows($summed); //$sum_items['sum_onhand'];

        //echo $query.'  '.$theseitems; //XXX
        //Get the detail.
        
        if ($watercolor)    //if searching for one water color...
        {
            if ($paperweight == '--')
            {
              $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thiswatercolor%' AND FIND_IN_SET(LEFT(RIGHT(item,2),1),'H,I,J,K,L,M,N')";
            }
            else
            {
              $query = "select * from invt where  left(item,1)='Z'
                      AND item LIKE '%$thiswatercolor%' AND FIND_IN_SET(LEFT(RIGHT(item,2),1),'H,I,J,K,L,M,N')
                      AND descrip LIKE '%$thisweight%'";
            }
        }
        else
        {
            if ($paperweight == '--')
            {
              if (isset($_POST['find_one']))  //Added 1/25/2010  GEL's
              {
                  // These are allpurpose so Exclude GEL's
                  $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thistype%' 
                      AND  NOT FIND_IN_SET(LEFT(RIGHT(item,2),1),'H,I,J,K,L,M,N')";
               }
               else
               {
                  // These are from the "Purposes" selector  & '--' so don't exclude GEL's
                  $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thistype%'";
               }
            }
            else
            {
              //Paperweight specified so from "Purposes" selector so don't exclude GEL's
              $query = "select * from invt where left(item,1)='Z'
                      AND item LIKE '%$thistype%' 
                      AND descrip LIKE '%$thisweight%'";
            }
        }

     	  $detail = @mysqli_query($conn, $query);
        $num_items = @mysqli_num_rows($detail);

        
        If ($theseitems>0)  //only show types that have detail w/ onhand qty > 0 to show...
        {
          
          //print the papergroup info       //'.$thistype.$num_items .'
          echo '<tr><td rowspan="2" class="pa_type_line"><a href="'.$row['largeimg'].'" alt="'.$row['largeimg'].'" target="Paperimage">
               <img src="'.$row['smallimg'].'"></a></td>
               <td class="pa_type_line"><a name="'.$thistype.'">
               </a>'.
               '<b>'.stripslashes($row['papername']).'</b> -- '.$row['fiber'].':<br>'.
               $row['uses'].'</td>
               <td class="pa_type_line">'.$row['color'].'</td>
               <td class="pa_type_line">'.$row['value'].
               '</td></tr>';
          echo '<tr><td colspan=3>
                <table border="1" width="100%" cellspacing="0">';
          $c = 0;
          while ($line=mysqli_fetch_array($detail))
   	      {
            //print the paper item detail
            if (($line['onhand']-$line['aloc'])>0)      //don't show items not in stock
            {
              $onhand_gt_zero = $onhand_gt_zero+1;  // 10/10/05 control "out of stock message"
              //First row shows col headings
              if ($c==0)
              {
                echo "<tr><td class=pa_detail_line><b>Sheet name and size</b></td>
                          <td class=pa_detail_line><b>Qty</b></td>
                          <td class=pa_detail_line><b>Price/sheet</b></td>
                          <td class=pa_detail_line colspan=3><b>Select surface and order quantity</b></td>
                      </tr>";
                $c++;
              }
            
            
              $thisitem = $line['item'];
              $thisdescrip = stripslashes($line['descrip']);
              $thisprice = $line['price'];

              if (strstr($thisdescrip,'/CP'))
                $thiscp = 'CP';
              if (strstr($thisdescrip,'/HP'))
                $thiscp = 'HP';
              if (strstr($thisdescrip,'/R'))
                $thiscp = 'R';

              $onhand = $line['onhand']-$line['aloc'];  //1/26/2007 Added chk qty !> onhand
              echo '
                <tr><td class="pa_detail_line">'.
                $thisdescrip.
                '</td>
                <td class="pa_detail_line" align="right" >'.number_format($onhand,0).'
                </td> <td class="pa_detail_line" nowrap>$ '.
                number_format($thisprice,2).
                '</td><form method=post action=pa_add2_cart.php onSubmit="return validpaperform(this)">
                 <td align="left" class="pa_detail_line">
                 <input type=hidden name=itemno value="'.$thisitem.'">
                 <input type=hidden name=descrip value="'.$thisdescrip.'">
                 <input type=hidden name=anchor value="'.$thistype.'">
                 <input type=hidden name=price value="'.$line['price'].'">
                 <input type=hidden name=onhand value="'.$onhand.'">
                 ';

                 // If paper is marked r or hp that is the way it goes
                 // If /CP then can be changed  to HP or B  with extra charge
                 switch ($thiscp)
                 {
                    case 'CP':
                     echo  radio_suface_prep($thisitem);   // 9/20/2013 adjust fnc for mica rose (see tr_fns.php)
                     // 10/14/05 changed to radio from selects that didn't show on IEX with large lists.
                    /*
                      echo "<select style='font:8pt Arial' name=surface size=1>
                              <option style='font:8pt Arial' selected value='/CP'>Cold Pressed</option>
                              <option style='font:8pt Arial' value='/HP'>Hot Pressed</option>
                              <option style='font:8pt Arial' value='/B'>Both</option>
                          </select>";
                    */
                          break;
                    case 'HP':
                      echo 'Hot Pressed
                      <input type=hidden name=surface value="'.$thiscp.'">';
                      break;
                    case 'R':
                      echo 'Rough
                           <input type=hidden name=surface value="'.$thiscp.'">';
                      break;

                 }
                 echo "
                      </td><td class=st_order_form valign=Middle><input style='font:8pt Arial' type=text name=qty value=0 size=6 maxsize=4>
                      </td><td class=st_order_form valign=Middle><input style='font:8pt Arial' type=submit name=submit value='Add'>
                      </form>
                      </td></tr>";
            }
          }
          echo '</table></td></tr>';
        }
      }

      if ($onhand_gt_zero==0)  // 10/10/05  Show this if no items were found.
      {
               echo "<tr><td class=pa_detail_line >
                    <br />Not available or presently not in stock.
                    Please call for additional information.<br /><br />
                    </td></tr>";
      }
      
      echo '</table>';

      // Display the radio button selector...
      echo '<table class=pa_main_page  border="1" cellpadding="1" cellspacing="0" width="'.$g_table1_width.'"  align=center>
        <tr height="57"> <td colspan="7">
        <div align=center>'.
        paper_selects($paperuses,$papercolor,$papervalue,$paperweight).'</div>';
        // 10/07/04 Add form to let user pick one papertype by name then display results...
        ?>
        <br />
        <form method="POST" action='pa_main.php'>
          &nbsp;&nbsp;Select one paper by name: &nbsp; 
          <?php  echo select_one_paper_type(); ?>
          &nbsp;<input type='SUBMIT' name='find_one' value='Show'>
        </form> 
        <br />
        
        </td></tr></table>
        
        <?php


    }

  }
	silk_footer($g_table1_align,$g_table1_width);
  //ob_end_flush();
  //page_footer();
?>
