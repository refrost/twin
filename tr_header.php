<?php   // tr_header  include file   <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

// 5/31/2016 viewport and bootstrap

  include ('head.php'); //responsive meta..
  
  echo '
	<title>Twinrocker Handmade Papermaking Supplies</title>
    <meta name="keywords" content="Twinrocker, twinrocker, fine arts, handmade
    paper, watercolor paper, artists paper, art supplies, calligraphy, invitations, stationery,
    wedding invitations, book arts, papermaking supplies, drawing paper, printmaking, custom cotton
    rag, crafts supplies, letterhead">
		<meta name="DESCRIPTION" content="The finest handmade stationery, invitations, and artist paper for watercolor, calligraphy, printmaking, book arts, etc. Papermaking supplies for making your own paper at home, at school, and for fine artists">
		<meta name="AUTHOR" content="Howard Clark ">';
    ba_style(); //Links stylesheet to tr.css (previously was a function)
    
    if (isset($jscript))  //10.19.2005 add JS validation code
    // 11/6/2015 Added passing in array (or one string).
    {
      if(is_array($jscript))   {
        foreach($jscript as $jcode){
            echo insert_javascript($jcode);
        }
      }
      else
        echo insert_javascript($jscript);  
        //Just one script --action_fns.php
    }
           
    //2/4/2010 Add OnLoad clause first in weblogin.php: set focus to input field.
   //Ex.  $g_onload = 'OnLoad="document.login.f_user.focus();"';
   if (!isset($g_onload))   
      $g_onload = '';
    // 05/30/2017 refa   Note:
    // Disabled OnLoad in body; switched 
    // to autofocus in weblogin.php forms
    // removed: '.$g_onload.' from <body...
    // (Cmted the Switch code in weblogin.php) 
   
   echo '</head>
         <body  bgcolor="'.$g_body_bg_color.'">';
   
   // Show header & menu bar  for SUPPLIES:

   // Define departments and order shown in this array. This defines top menu bar
   // Department names are used in the Products table so any change here must also
   // be made in the dept field in the products table. Grp_default is 
   // the group that shows first once a deparment is selected.


	display_header($g_headername,$g_table1_width,$g_headerbgcolor);		//Hardcoded for Twinrocker in tr_fns.php

  if (isset($show_help_menu))
  {
  
      // show help menu
      echo "<table border=1 width='$g_table1_width' align=center><tr>";
      echo "<td>Help</td>";
      echo "</tr> </table>";

  }
  else
  {
      // Changing dept_array will require updating the 'dept' and maybe 'groupname' fields
      // in the products table to reflect the correspnding changes:
      // Ex.  UPDATE products SET dept='newvalue' WHERE dept='oldvalue';  in phpadmin
      //
      // The grp_default value is the first group to show when clicking on the dept
      
      // Main Menu Definition
      $dept_array=array('Book Arts','Pulp','Fibers','Additives-Colorants','Forming','Paper Storage','Safety','Books','Kits','Specials');     //'Extras','MyMenu'  ,'Specials'
      $grp_default=array('Tools','Twinrocker Pulp','Twinrocker Fibers','Additives','Moulds','Clear Holders','Safety Products','Books','Papermaking Kits','Current');  //

	    echo "<table border=1 width='$g_table1_width' align=center><tr >";
			
			//When tr_headers is used on "special" pages (viewcart), pulp, etc.
			//then must pass the "$dept"
			
			if (!isset($dept))
				if (isset($_GET['dept']))
					$dept = $_GET['dept'];
				else
					$dept = '';
					
			// Display Department menu below header
            $cnt = count($dept_array);
			for ($i=0; $i < $cnt ;$i++)
			{
				if ($dept==$dept_array[$i])
					$topmenu2class = 'topmenu2_on';
				else
					$topmenu2class = 'topmenu2';
				
				echo "<td class=$topmenu2class > 
					  <a class=$topmenu2class href=\"$g_app?dept=".urlencode($dept_array[$i])
					     ."&amp;grp=".urlencode($grp_default[$i])."\">".$dept_array[$i]."</a></td>";
			}
						 
	    echo "</tr> </table>";
	    
  }

?>
