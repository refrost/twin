<?php
// rev 1/24/2010: added forgot_username*.php
// rev 3/27/2013:  inserted google analtyics in output_fns.php-silk_footer()
//                 improve redirect if initial entry to site:
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

session_start();
include ('book_sc_fns.php');
  //echo 'xxx'; 
 
  // Define Netscape Table colors... matches w/ css
  $leftmenubg='#ffcc99';		// flesh colored background color of leftmenu table 
 
  if (!isset($_SESSION['ship_state']))
  {
      //12/05/2010 handle calls from twinrocker.com smoothly
      //$ref = getenv("HTTP_REFERER");
      //echo $ref;
      //exit;
      include('set_vars.php');
  } 
   // find out which dept,groupname, & product  
   //Set defaults:
    
   if (isset($_GET['dept']))
  	 $dept = $_GET['dept'];
   else
   	 $dept = 'Supplies';
   if (isset($_GET['grp']))
  	 $groupname = $_GET['grp'];
   else 
     $groupname = '-';
   if (isset($_GET['prod']))
  	 $prod = $_GET['prod'];
   else
   {
   if ($dept=='Supplies')
	 	  $prod = 'Intro'; 	// Coming to page no or missing dept
	 else
	 	  $prod = ''; 		// Set blank and then use as a switch below to get first product
   }   		
   
   // Get products in this group to list out on the left menu


   $conn = db_connect();

   if (isset($_SESSION["SESSION_ADNAME"]))
   {
	   $query = "select product,display from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' ORDER BY displayorder ";   //
   }
   else
   {
	   $query = "select product,display from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and display = 'y' ORDER BY displayorder ";   //
   }
   
   //echo $query;
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   $theseproducts = db_result_to_array($result);
   @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

   // Get the available groupnames for this dept (that get listed horizonally..
   $query = "select DISTINCT groupname  from products where dept='".addslashes($dept)."'";   //
   //echo $query;
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   $thesegroups = db_result_to_array($result);
   @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   
   
   // Get one product  
   // $conn = db_connect();
   if (isset($_SESSION["SESSION_ADNAME"]))
   {
   		if ($prod)  //if passed via http...
	   		$query = "select * from products where product='$prod'  and dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."'";  
   		else
   	   		$query = "select * from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' ORDER BY displayorder LIMIT 1";   // get the first one in this dept+groupmname
   }
   else
   {
   		if ($prod)  //if passed via http...
	   		$query = "select * from products where product='$prod'  and dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and display = 'y'";  
   		else
   	   		$query = "select * from products where dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and display = 'y' ORDER BY displayorder LIMIT 1";   // get the first one in this dept+groupmname
   }

   //echo $query; 
   $result = @mysqli_query($conn, $query);
   //echo $result;
   if (!$result)
   		// No product name so see if can display the first product in this dept/group 
     return false;
   $num_invt = @mysqli_num_rows($result);
   //if ($num_invt ==0)
   //   return false;
   $thisproduct = db_result_to_array($result);
   @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   if (!$prod && $thisproduct)
   	  $prod = $thisproduct[0][0];  // if first time on group, set $prod...
	  
   //------------------- done getting the data, now display...
   
   // Display Header and "sections" menu menubar

   include ('tr_header.php');
   
   // Show "Groups" menubar 
		 
			
	$cnt = count($thesegroups);
	if ($cnt>1)  //Show subgroups if more than 1
	{
		echo "<table class=leftmenu border=1 align=$g_table1_align width=$g_table1_width >
			<tr valign=middle height=23><td class=topmenu3 width=100 ><div align=center><font color=silver>Subgroup >>>&nbsp</font></div></td><td class=topmenu3>";
		for ($i=0; $i < $cnt; $i++)
		{
			if ($groupname==$thesegroups[$i][0])
				$topmenu3class = 'topmenu3_on';
			else
				$topmenu3class ='topmenu3';
					
			echo "<a class=$topmenu3class href=showprod.php?dept=".urlencode($dept)."&grp=".urlencode($thesegroups[$i][0]).">".$thesegroups[$i][0]."</a> | ";
		}
	 
		echo "</td></tr></table>";
		
    }
   
   
	  $ht = '';
   	$wd = '';
   
   	if  (isset($thisproduct[0]['product']))  //if there is a product name defined...
   	{
	
		// Display the main table, starting with the left menu
		// Netscape demands that background color -- css doesn't seem to work 
		echo "<table  width=$g_table1_width border=1 cellspacing=0 align=center bgcolor=#ffffff><tr>
		<td  width=100  valign=top >
		<table border=0>";
		
		//List out the products in this group down the leftmenu
		for ($i=0;$i<count($theseproducts);$i++)
		{
			if ($theseproducts[$i]['display']=='n')
				$mdisplay = '*';  //Indicates that product is set to NOT display -- shows to Admin users
			else
				$mdisplay = '';
					
			if ($prod==stripslashes($theseproducts[$i][0]))  //Controls highlite
				$leftmenuclass = 'leftmenu_on';  //'leftmenu_on';
			else
				$leftmenuclass = 'leftmenu';	

			echo "<tr><td width=108 bgcolor=$leftmenubg class=$leftmenuclass>"
           .product_URL_no_br("showprod.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode($theseproducts[$i][0]),stripslashes($theseproducts[$i][0].$mdisplay),"class=$leftmenuclass").'</td></tr>';
		}

		echo '</table>
		      </td>
			  <td valign=top>';

   
    	if ($thisproduct[0]['height'])
   			$ht = " height=".$thisproduct[0]['height'];
	    if ($thisproduct[0]['width'])
   			$wd = " width=".$thisproduct[0]['width'];
	    if ($thisproduct[0]['align'])
   			$align = " align=".$thisproduct[0]['align'];
	    else 
	   		$align = '';
	

 	    echo '<table class=descrip border=0><tr><td width="100%">
   			<div class=product>'.stripslashes($thisproduct[0]['name']).'</div>';
      // Show subheader, if any...
        if ($thisproduct[0]['subheader'])
          echo '<div class=product_subheader>'. $thisproduct[0]['subheader'].'</div>';

        // Show ALERT, if any...
        if ($thisproduct[0]['alert'])
          echo '<br><div class=product_alert >'. $thisproduct[0]['alert'].'</div><br>';

        echo '</td>';
		//Place 'Next' button in a place that doesn't move  (leftmenu)
		echo "<td  align=right >";
		if (check_admin_user())
		{
			echo product_admin($dept,$groupname,$prod).'  '.product_URL_no_br("showprod.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu");
		}
		else
		    echo product_URL_no_br("showprod.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu");
		echo '</td></tr></table>';
	  
	   	
	   	echo '<table border=0 class=descrip width="100%" ><tr><td  valign=top>';

		//set flag to exclude empty pictures or items
		$mpics = trim($thisproduct[0]["smallimg"]);
		$mitems = trim($thisproduct[0]["items"]);
		$mpricetable = trim($thisproduct[0]["pricetable"]);
		$comments = trim($thisproduct[0]["comments"]);
		$moreinfo = trim($thisproduct[0]["moreinfo"]);
		
		// Display the product picture and the text description (or eval(php:: code)
		
   	    if (!empty($mpics))
   		   echo '<a href='.$thisproduct[0]['bigimg'].
   			'><img src='.$thisproduct[0]['smallimg']." alt='".$thisproduct[0]['alt']."' $ht $wd $align".
   	   	'></a>';
		
		//3/5/2011 Add EVAL as code if begins with php::
		$pos = strpos(stripslashes(trim($thisproduct[0]['descrip2'])),'php::');
		if ($pos=== false)
        {
          //</td><td  valign=top>
   		  echo '<p><b>'.trim(stripslashes($thisproduct[0]['descrip1']).'</b> '.
   			stripslashes($thisproduct[0]['descrip2'])).'</p></td></tr>
   			</table>';
   	    }
   	    else
   	    {
			// 4/19/2017 echo '</td><td  valign=top>';
			$code = substr(trim($thisproduct[0]['descrip2']),5);
			EVAL("$code");
			echo '</td></tr></table>';
      }
		
		// Display the "More Info" line/separator, if prices are displayed...
		if (!empty($mitems)||!empty($mpricetable))
		{
		    echo '<table border=1 width="100%" align=center>
				<tr class=info>';
			if ($moreinfo)
			{
				echo '<td >';
				$url = 'prodinfo.php?str='.urlencode(addslashes($moreinfo)).'&item='.urlencode($prod);
				moreinfo($url, 'More Info (in separate window)', $aclass=''); 
				echo '</td>';
			}
			
			echo '<td>Click picture to enlarge; Click price to Add-to-Cart.</td></tr>
				</table>';
		}		
        br();
		//Items listed  as  SBTITEMNUMBER,short description.  Code looks up price and creates url to add-to-cart
		// Start table correct/revised structure 4/21/2017
        // for record with items or without (myOrderMemory) )
        echo "<table class=pricetable border=0 width=100%>";
		if (!empty($mitems))
		{
			$itempair = explode("\n",trim(stripslashes($thisproduct[0]["items"])));

	   		//echo "<table class=pricetable border=1 width=480>";
		    for ($i=0; $i<count($itempair); $i++)
   			{
   				$thisone = explode("|",$itempair[$i]);
				$item = $thisone[0];
				$itemname = $thisone[1];
				if ($item)
				{
					$thisprice =silk_buy($item);
	   				echo  "<tr><td align=left>$itemname</td><td>&nbsp;&nbsp;&nbsp;</td><td width=100>&nbsp;".$thisprice."</td><td>&nbsp;</td></tr>";
				}
		    }
		}
		
		// If there is code in the product.pricetable field, 
		// EVALUATE it (usually a function in inc/tr_fns.php)
		if ($mpricetable)	// This inserts HTML code, forms, etc, to call special functions.
		{
			echo  "<tr><td align=left colspan=4>";
			EVAL("$mpricetable");
			echo '</td></tr>';		
		}
			
		
		
		// If there are comments, add these next.
		if ($comments)
		{
			echo "<tr><td><table class=comment border=2 >";
			echo '<tr><td align=left>';
			EVAL("$comments");
			echo '</td></tr></table>';
		}
        echo '</td></tr></table>';
	    br();
        // Edit, Add, Next
		echo '<table border=0>';
		## If in admin mode, allow access to Add/edit product records: 
		if (check_admin_user())
		{
           echo '<tr> <td  align=right>'.
            product_admin($dept,$groupname,$prod).'  '.product_URL_no_br("showprod.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu").'</td></tr>';
		}
		else
			echo '<tr><td align=right>'.product_URL_no_br("showprod.php?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode(show_next($theseproducts,$prod)),'Next',"class=cartmenu").'</td></tr>';;
		
		echo '</table>';
	}
	else
	{
		switch ($dept)    //Special departments...
		{
			case 'Fibers' :
				//Display pricebreak/order table  -- Price Data is in fiber table
				header("location:tr_fiber.php?clearfiber=N");
				break;

			default :
			//'Welcome to our dynamic web site!'
			// Come to the page with incomplete specifications so assume at "Supplies home"
			echo "<table  width=$g_table1_width border=1  align=center><tr>
			<td class=supply valign=top>";
			include('supply.php');  // Which is mostly plain html code in this case..
			echo '</td></tr></table>';
			break;
		
		} //end of 'Supplies' fullpage options
		
	}
	echo '</td></tr></table>';
	silk_footer($g_table1_align,$g_table1_width);

?>
