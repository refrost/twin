<?php
  // st_pricelist.php  adapted from st_pricelist.html
  // 12/22/03 php by Ralph Frost
  // In this version, we do 1 sql select in page and get all values as an associative array.
  // Then we build the page in a similar manner as in st_pricelist.html
  // which does ~71 calls to the DB
  // 5/26/2011  Added '008b' natural deckel 5-7/8x4-1/8: Must copy and hand edit the html tr below
  // 9/20/2011 51 natural, not feather
  // 12/17/2012 Added 42a -oval; edited 42. Also fixed stat_price() call for '048A' -- was '048_'
  // 12/18/2012 Added Select menu (~free)
  // 12/19/2012 Removed vellum
  // 05/31/2021 Added $dim array to echo flat and folded dimensions
  //            here and in st product records. (Only for 6 and 12 now)
  //            Pulls vales from statdims table
  
  include ('book_sc_fns.php');
  session_start();
  if (!isset($_SESSION['ship_state']))
    include('set_vars.php');

// 12/18/2012 Added Select menu

if (isset($_GET['submit'])) { // redirect to anchor
	$anchor = $_GET['bookmark'];
	header("location:st_pricelist.php#$anchor");
	exit;
}
$stat_types = array(
    array('Place/Escort Cards','PLACE'),
    array('Reply Cards','REPLY'),
    array('Small Invitation Sets (with i/o envelopes)','SMALLINVTSET'),
    array('Large Invitation Sets (with i/o envelopes)','LARGEINVIT'),
    array('Heavyweight Rectangular Card & Envelope Sets (no i envelope)','HEAVYRECTCARDS'),
    array('Tall Heavyweight Card & Envelope Sets (no i envelope)','TALLHEAVYCARDS'),
    array('Personal Cards and Letters','PERSONALCARDS'),
    array('Business Letters','BUSNLTRS'),
    array('Square Place/Escort Cards','SQUAREHEAVYCARDS'),
    array('Square Reply Cards & Envelopes','REPLYCARDSANDENVEL'),
    array('Square 6" Cards & Envelopes','SIXCARDSANDENVEL'),
    array('Square 6-1/2" Cards & Envelopes','SIXHALFCARDSANDENVEL'),
    array('Square 7" Cards & Envelopes','SEVENCARDSANDENVEL'),
    array('Portfolios','OTHER')
);

// In this version, we do 1 sql select and get all values as an associative array.
// Then we build the page in a similar manner as in st_pricelist.html build an array of all

$jscript = 'validstatform';  // 01/20/2007 validate qty < onhand
include('st_header.php');

//$stat = get_all_styles();
  $conn = db_connect();
  $query = "select * from stationery";
  $result = @mysqli_query($conn, $query) or die;
  //if ($result)
    $num_styles = @mysqli_num_rows($result);
    $stat = db_result_to_array($result);
  ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
  //return $thisitem;
  
// 5/31/2021 build array from 'statdims' table
// to be inserted in st_pricelist.php and in st_main.php
// stationery pages. Currently only for items 6 and 12
 
$conn = db_connect();

$qry="select * from `statdims` order by `stylecode`";
$result=mysqli_query($conn,$qry);

// Build $dim array.
$dim = [];
while ($row = mysqli_fetch_array($result,MYSQLI_BOTH)) {
	
	$dim[$row[0]]=$row;
	
}

//echo $dim['006_']['flat']; //[2];

?>
  
  <!-- refa 12/20/03 Added nowrap and <br>'s below to try to fit into width=100% -->
			<table align=center border="2" cellpadding="0" cellspacing="2"  width=<?php echo $g_table1_width; ?> bgcolor="silver" >
				<tr>
					<th align="center"  colspan="6">
						<h1><font size="5" color="#ffffff"><b>INVITATION&nbsp;AND&nbsp;STATIONERY&nbsp;PRICE LIST </b></font></h1>
                        <?php
                        // 12/18/2012  Added select menu...
                        echo "<form action='st_pricelist.php' method='GET'>
                              Select type: ";
                        build_select($stat_types,'bookmark','');
                        echo "<input type=submit name=submit value='Jump'>";
                        echo '</form>';
                        ?>
					</th>
				</tr>
                <tr>
					<th align="center" valign="middle" colspan="6">
						<h3><font size=5 color="White">Rectangular Cards and Envelopes</font></h3>
					</th>
				</tr>
				<tr >
					<td align="center" valign="middle" bgcolor="#ffffcc" colspan="6"><a NAME="PLACE"></a><font size=3><b>Place Cards / Escort Cards</b></font></td>
				</tr>
				<tr bgcolor="#a7a7a7">
					<td align="center" valign="middle" bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td align="center" valign="middle" bgcolor="#ffffff" ><font size="2"><i>Style</i></font></td>
					<td align="center" valign="middle" bgcolor="#ffffff" width="20"><font size="2"><i>Edge</i></font></td>
					<td align="center" valign="middle" bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td align="center" valign="middle" bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td align="center" valign="middle" nowrap bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">1</font></td>
					<td ><font size="2">Escort Card, Folded</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-1/2&quot; x<br>
						3-1/4&quot;</font></td>
					<td><font size="2">{2-1/4&quot;x<br>
						3-1/4}</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'001_','PLACE'); ?>  </td>
				</tr>
<!--
				<tr>
					<td colspan="6"><font size="2">(Note: the Folded Escort is the same card as Flat Reply, see Flat Reply listing in the Inventory )</font></td>
				</tr>
-->
				<tr align="center">
					<td><font size="2">2</font></td>
					<td><font size="2">Escort Card, Flat</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">2-1/4&quot; x<br>
						3-5/8&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'002_','PLACE'); ?>  </td>
				</tr>
				<tr align="center">
					<td><font size="2">2a</font></td>
					<td><font size="2">Escort Card, Flat</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">2-1/4&quot; x<br>
						3-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=center nowrap width=50><?php echo stat_price($stat,$num_styles,'002A','PLACE'); ?>  </td>
				</tr>
				
				<tr align="center">
					<td><font size="2">3</font></td>
					<td><font size="2">Escort Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">2-1/2&quot; x<br>
						3-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'003_','PLACE'); ?>  </td>
				</tr>
				<tr>
					<td align="center" valign="middle" bgcolor="#ffffcc" colspan="6"><a NAME="REPLY"></a><font size=3><b>Reply Cards</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">4</font></td>
					<td><font size="2">Flat Reply Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">3-1/4&quot; x<br>
						4-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'004_','REPLY'); ?>  </td>
				</tr>
				<tr align="center">
					<td><font size="2">5</font></td>
					<td><font size="2">Flat Reply Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">3-1/4&quot; x<br>
						4-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'005_','REPLY'); ?>  </td>
				</tr>
				<tr align="center">
					<td><font size="2">6</font></td>
					<td><font size="2">Folded Reply Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2"><?php echo $dim['006_']['flat']; ?></font></td>
					<td><font size="2">{<?php echo $dim['006_']['fold']; ?>}</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'006_','REPLY'); ?>  </td>
				</tr>
				<tr align="center">
					<td><font size="2">7</font></td>
					<td><font size="2">Reply Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">3-1/2&quot; x<br>
						5&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'007_','REPLY'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="SMALLINVTSET"></a><font size=3><b>Small Invitation Sets (with inner &amp; outer envelopes)</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">8</font></td>
					<td><font size="2">Small Flat Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">5-7/8&quot; x<br>
						4-1/8&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'008_','SMALLINVTSET'); ?> </td>
				</tr>
					<tr align="center">
						<td><font size="2">8a</font></td>
						<td><font size="2">Small Flat Card</font></td>
						<td><font size="2">feather<br>
								deckle</font></td>
						<td><font size="2">5-1/4&quot; x<br>
								4&quot;</font></td>
						<td><font size="2">n/a</font></td>
						<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'008A','SMALLINVTSET'); ?> </td>
					</tr>
               	<tr align="center">
						<td><font size="2">8b</font></td>
						<td><font size="2">Small Flat Card</font></td>
						<td><font size="2">natural<br>
								deckle</font></td>
						<td><font size="2">5-3/4&quot; x<br>
								4&quot;</font></td>
						<td><font size="2">n/a</font></td>
						<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'008B','SMALLINVTSET'); ?> </td>
					</tr>

					<tr align="center">
					<td><font size="2">9</font></td>
					<td><font size="2">Small Folded Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-3/4&quot; x<br>
						8-1/2&quot;</font></td>
					<td><font size="2">{5-3/4&quot; x<br>
						4-1/4&quot;}</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'009_','SMALLINVTSET'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">10</font></td>
					<td><font size="2">Small Inner Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-1/2&quot; x<br>
						6-1/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'010_','SMALLINVTSET'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">11</font></td>
					<td><font size="2">Small Outer Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-3/4&quot; x<br>
						6-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'011_','SMALLINVTSET'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">12</font></td>
					<td ><font size="2">Flat Card for the Outer Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2"><?php echo $dim['012_']['flat']; ?></font></td>
					<td><font size="2"><?php echo $dim['012_']['fold']; ?></font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'012_','SMALLINVTSET'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">13</font></td>
					<td><font size="2">Flat Card for the Outer Envelope</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">6-1/2&quot; x<br>
						4-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'013_','SMALLINVTSET'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="LARGEINVIT"></a><font size=3><b>Large Invitation Sets (with inner &amp; outer envelopes)</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">14</font></td>
					<td><font size="2">
                    Large Flat Card<br />
                    For Large Inner Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-1/2&quot; x<br>
						7-1/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
					<td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'014_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">15</font></td>
					<td><font size="2">
                    Large Flat Card<br />
                    For Large Inner Envelope</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">5-5/8&quot; x<br>
						7-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'015_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">16</font></td>
					<td><font size="2">
                    Large Folded Card <br/>with Natural Deckled Edge for Large Inner Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7-3/8&quot; x<br>
						10-3/4&quot;</font></td>
					<td><font size="2">{7-3/8&quot; x<br>
						5-3/8&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'016_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">17</font></td>
					<td><font size="2">Large Inner Envelope for the 3 above</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-3/4&quot; x<br>
						7-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'017_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">18</font></td>
					<td><font size="2">Large Outer Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6-1/4&quot; x<br>
						8-1/8&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'018_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">19</font></td>
					<td><font size="2">Large Flat Card for Outer Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6&quot; x<br>
						7-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'019_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">20</font></td>
					<td><font size="2">Lrg Flat Card for Outer Envelope</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">6-1/8&quot; x<br>
						8&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'020_','LARGEINVIT'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">20a</font></td>
					<td><font size="2">Folded Card for Outer</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7-1/2&quot; x<br>
						11-3/4&quot;</font></td>
					<td><font size="2">{7-1/2&quot; x<br>
						5-7/8&quot;}</font></td>
          <td align=center nowrap width=50><?php echo stat_price($stat,$num_styles,'020A','LARGEINVIT'); ?> </td>
				</tr>

				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="IMPINVITSETS"></a><font size=3><b>Imperial Invitation Sets (large, heavy weight with inner &amp; outer envelopes)</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">21</font></td>
					<td><font size="2">Flat Imperial Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">9-3/4&quot; x<br>
						6-3/8&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'021_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">22</font></td>
					<td><font size="2">Flat Imperial Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">9-3/4&quot; x<br>
						6-1/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'022_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">23</font></td>
					<td><font size="2">Folded Imperial Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">9-3/4&quot; x<br>
						13&quot;</font></td>
					<td><font size="2">{9-3/4&quot; x<br>
						6-1/2&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'023_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">24</font></td>
					<td><font size="2">Imperial Inner Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">10-1/4&quot; x<br>
						6-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'024_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">25</font></td>
					<td><font size="2">Imperial Outer Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7-1/4&quot; x<br>
						10-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'025_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">26</font></td>
					<td><font size="2">Flat Impr Card for Outer Envlp</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">10-3/8&quot; x<br>
						6-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'026_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">27</font></td>
					<td><font size="2">Flat Impr Card for Outer Envlp</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">10-3/8&quot; x<br>
						6-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'027_','IMPINVITSETS'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="HEAVYRECTCARDS"></a><font size=3><b>Heavyweight Rectangular Card &amp; Envelope Sets (no inner envelope)</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">28</font></td>
					<td><font size="2">Rectangular Card</font></td>
					<td><font size="2">feather deckle</font></td>
					<td><font size="2">6&quot; x<br>
						8&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'028_','HEAVYRECTCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">29</font></td>
					<td><font size="2">Rectangular Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6-1/2&quot; x<br>
						8-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'029_','HEAVYRECTCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">30</font></td>
					<td><font size="2">Rectangular Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">6-5/8&quot; x<br>
						8-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'030_','HEAVYRECTCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">31</font></td>
					<td><font size="2">Rectangular Card Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6-3/4&quot; x<br>
						9&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'031_','HEAVYRECTCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="TALLHEAVYCARDS"></a><font size=3><b>Tall Heavyweight Card &amp; Envelope Sets (no inner envelope)</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">32</font></td>
					<td><font size="2">Tall Flat Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-1/4&quot; x<br>
						9-1/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'032_','TALLHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">33</font></td>
					<td><font size="2">Tall Flat Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">9-5/8&quot; x<br>
						4-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'033_','TALLHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">34</font></td>
					<td><font size="2">Tall Folded Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">9-1/4&quot; x<br>
						8-1/2&quot;</font></td>
					<td><font size="2">{9-1/4&quot; x<br>
						4-1/4&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'034_','TALLHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">35</font></td>
					<td><font size="2">Tall Folded Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">9-3/4&quot; x<br>
						8-1/4&quot;</font></td>
					<td><font size="2">{9-3/4&quot; x<br>
						4-1/8}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'035_','TALLHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">36</font></td>
					<td><font size="2">Tall Card Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-1/2&quot; x<br>
						9-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'036_','TALLHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="PERSONALCARDS"></a><font size=3><b>Personal Cards and Letters</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">37</font></td>
					<td><font size="2">Personal Scalloped Note Card</font></td>
					<td><font size="2">special deckle</font></td>
					<td><font size="2">8-1/2&quot; x<br>
						5-1/2&quot;</font></td>
					<td><font size="2">{4-1/4&quot;x 5-1/2&quot;}</font></td>
     <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'037_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">38</font></td>
					<td><font size="2">Personal Scalloped Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-1/2&quot; x<br>
						5-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'038_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">39</font></td>
					<td><font size="2">Personal Large Scalloped Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7-1/2&quot; x<br>
						10-1/2&quot;</font></td>
					<td><font size="2">{7-1/2&quot;x 5-1/4&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'039_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">40</font></td>
					<td><font size="2">Personal Large Scalloped Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-3/4&quot; x<br>
						7-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'040_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">41</font></td>
					<td><font size="2">Personal Letter</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7&quot; x<br>
						10&quot;</font></td>
					<td><font size="2">{7&quot;x 3-1/3&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'041_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">42</font></td>
					<td><font size="2">Personal Envelope<br> (w/ Square Flap)</font></td>
					<td></td>
					<td><font size="2">4&quot; x<br>
						7-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
                    <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'042_','PERSONALCARDS'); ?> </td>
				</tr>
                <!--// 12/17/2012 added 42a -->
				<tr align="center">
					<td><font size="2">42a</font></td>
					<td><font size="2">Personal Envelope<br> (w/ Oval Flap)</font></td>
					<td></td>
					<td><font size="2">4&quot; x<br>
						7-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
                    <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'042A','PERSONALCARDS'); ?> </td>
				</tr>
                
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="BUSNLTRS"></a><font size=3><b>Business Letters</b></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">43</font></td>
					<td><font size="2">Business Letter 3 cut edges<br> &amp; one natural deckle</font></td>
					<td></td>
					<td><font size="2">8-1/2&quot; x<br>
						11&quot;</font></td>
					<td><font size="2">{8-1/2&quot;x 3-2/3&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'043_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">44</font></td>
					<td><font size="2">Business (#10) Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-1/8&quot; x<br>
						9-1/2&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'044_','PERSONALCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6"><font size=5 color=white><b>Square Cards and Envelopes</b></font></td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="SQUAREHEAVYCARDS"></a><font size=3><b>Square Place Cards / Escort Cards</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">45</font></td>
					<td><font size="2">Escort Card, Folded</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-3/4&quot; x<br>
						2-3/4&quot;</font></td>
					<td><font size="2">{2-7/8&quot;x 2-3/4&quot;}</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'045_','SQUAREHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">46</font></td>
					<td><font size="2">Escort Card, Flat</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">2-3/4&quot; x<br>
						2-3/4&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'046_','SQUAREHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">47</font></td>
					<td><font size="2">Escort Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">3&quot; x<br>
						3&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'047_','SQUAREHEAVYCARDS'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="REPLYCARDSANDENVEL"></a><font size=3><b>Square Reply Cards and Envelopes</b></font></td>
				</tr>
				<tr align="center" bgcolor="#a7a7a7">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximatequantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">48</font></td>
					<td><font size="2">Flat Reply Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">4-3/4&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'048_','REPLYCARDSANDENVEL'); ?> </td>
				</tr>
					<tr align="center">
						<td><font size="2">48a</font></td>
						<td><font size="2">Circle Reply Card</font></td>
						<td><font size="2">natural deckle</font></td>
						<td><font size="2">4-3/4&quot; dia</font></td>
						<td><font size="2">n/a</font></td>
            <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'048A','REPLYCARDSANDENVEL'); ?> </td>
				</tr>
					<tr align="center">
					<td><font size="2">49</font></td>
					<td><font size="2">Flat Reply Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">4-3/4&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'049_','REPLYCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">50</font></td>
					<td><font size="2">Reply Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'050_','REPLYCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="SIXCARDSANDENVEL"></a><font size=3><b>Square 6&quot; Cards and Envelopes</b></font></td>
				</tr>
				<tr align="center">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
          <td bgcolor=white  ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">51</font></td>
					<td><font size="2">Flat Heart Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6&quot; height</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'051_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">52</font></td>
					<td><font size="2">Heart Card w/Pocket</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'052_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">53</font></td>
					<td><font size="2">Telescoping Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'053_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
	<tr align="center">
					<td><font size="2">53a</font></td>
					<td><font size="2">Square Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'053A','SIXCARDSANDENVEL'); ?> </td>
				</tr>
	<tr align="center">
					<td><font size="2">53b</font></td>
					<td><font size="2">Square Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">5-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'053B','SIXCARDSANDENVEL'); ?> </td>
				</tr>                
				<tr align="center">
					<td><font size="2">54</font></td>
					<td><font size="2">6&quot; Square Flat Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'054_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">54a</font></td>
					<td><font size="2">Circle Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6&quot; dia</font></td>
					<td><font size="2">n/a</font></td>
          <td align=center nowrap width=50><?php echo stat_price($stat,$num_styles,'054A','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				
				<tr align="center">
					<td><font size="2">55</font></td>
					<td><font size="2">6&quot; Square Flat Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">6&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'055_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">56</font></td>
					<td><font size="2">6&quot; Square Folded Card</font></td>
					<td><font size="2">feather deckle</font></td>
					<td><font size="2">6&quot; x<br>
						12&quot;</font></td>
					<td><font size="2">(6&quot; square)</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'056_','SIXCARDSANDENVEL'); ?> </td>
				</tr>

         	<tr align="center">
			<td><font size="2">56a</font></td>
			<td><font size="2">6&quot; Square Folded Card</font></td>
			<td><font size="2">natural deckle</font></td>
			<td><font size="2">6&quot; x<br>
						12&quot;</font></td>
			<td><font size="2">(6&quot; square)</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'056A','SIXCARDSANDENVEL'); ?> </td>
				</tr>


				<tr align="center">
					<td><font size="2">57</font></td>
					<td><font size="2">6-1/2&quot; Square Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'057_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">58</font></td>
					<td><font size="2">7&quot; Square Envelope (for outer)</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'058_','SIXCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="SIXHALFCARDSANDENVEL"></a><font size=3><b>Square 6-1/2&quot; Cards and Envelopes</b></font></td>
				</tr>
				<tr align="center">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td nowrap bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">59</font></td>
					<td><font size="2">Circle Card</font></td>
					<td><font size="2">feather deckle</font></td>
					<td><font size="2">6-1/2&quot; dia</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'059_','SIXHALFCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">60</font></td>
					<td><font size="2">6-1/2&quot; Square Cards</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'060_','SIXHALFCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">61</font></td>
					<td><font size="2">6-1/2&quot; Square Cards</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">6-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'061_','SIXHALFCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">62</font></td>
					<td><font size="2">6-1/2&quot; Square Folded Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">6-1/2&quot; x<br>
						13&quot;</font></td>
					<td><font size="2">(6-1/2&quot; square)</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'062_','SIXHALFCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">58</font></td>
					<td><font size="2">7&quot; Square Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'058_','SIXHALFCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">63</font></td>
					<td><font size="2">7-1/2&quot; Square Envelope (for outer)</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'063_','SIXHALFCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center" bgcolor="#ffffcc">
					<td colspan="6"><font size="2"><a NAME="SEVENCARDSANDENVEL"></a></font><font size=3><b>Square 7&quot; Cards and Envelopes</b></font></td>
				</tr>
				<tr align="center">
					<td bgcolor="#ffffff"><font size="2"><i>Item #</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Edge</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>{Folded Dim.}</i></font></td>
					<td bgcolor="#ffffff" ><font size="2"><i>Price, Approximate quantities each color, and Order Form </i></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">64</font></td>
					<td><font size="2">7&quot; Flat Card</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'064_','SEVENCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">65</font></td>
					<td><font size="2">7&quot; Flat Card</font></td>
					<td><font size="2">die cut</font></td>
					<td><font size="2">7&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'065_','SEVENCARDSANDENVEL'); ?> </td>
				</tr>
				<tr align="center">
					<td><font size="2">63</font></td>
					<td><font size="2">7-1/2&quot; Square Envelope</font></td>
					<td><font size="2">natural deckle</font></td>
					<td><font size="2">7-1/2&quot; square</font></td>
					<td><font size="2">n/a</font></td>
          <td align=left nowrap width=60><?php echo stat_price($stat,$num_styles,'063_','SEVENCARDSANDENVEL'); ?> </td>
				</tr>
	<tr align=center bgcolor=#ffffcc>
		<td colspan=6 bgcolor=silver><a NAME="OTHER"></a><font size=5 color=white><b>Other Items</b></font></td>
	</tr>
<!--
	<tr align="center" bgcolor="#ffffcc">
					<td colspan="6" bgcolor="#ffffcc"><a NAME="VELLUM"></a><font size=3><b>Translucent Vellum </b></font></td>
				</tr>
				<tr align="center">
					<td><font size="2">70</font></td>
					<td colspan="2"><font size="2">Translucent Vellum<br> for invitations</font></td>
					<td><font size="2">23&quot; x<br>
						35&quot;</font></td>
					<td><font size="2">n/a</font></td>
          <td align=center ><font size=2><?php echo silk_buy('YYVEL070_','VELLUM'); ?></font></td>
				</tr>
-->
				<tr align="center" bgcolor="#ffffcc">
					<td colspan="6"><font size=3><b>Stationery Gift Portfolios</b></font></td>
				</tr>
				<tr align="center" bgcolor="#888888">
					<td colspan="3" bgcolor="#ffffff"><font size="2"><i>Style</i></font></td>
					<td bgcolor="#ffffff"><font size="2"><i>Dimensions</i></font></td>
			<td bgcolor=white><font size="2"><i>Price </i></font></td>
			<td bgcolor="#ffffff" ><spacer size=2 type=horizontal></td>
				</tr>
				<tr align="center">
					<td colspan="3"><font size="2">Pouchette Portfolio<br> with 6 Reply notes &amp; env</font></td>
					<td align="center"><font size="2">7&quot; x<br>
						9&quot;</font></td>
			<td><font size=2><?php echo silk_show('XPE7'); ?></font></td>
			<td rowspan=6 width=60><font size="2">Call to </font><br>
				<font size=2>order </font><br>
				<font size=2>and </font><br>
				<font size=2>choose</font><br>
				<font size=2> color</font></td>
				</tr>
				<tr align="center">
					<td colspan="3"><font size="2">Pouchette Portfolio only</font></td>
			<td align="center"><font size="2">7&quot; x<br>
						9&quot;</font></td>
			<td><font size=2><?php echo silk_show('XMSC7'); ?></font></td>
		</tr>
				<tr align="center">
					<td colspan="3"><font size="2">Pouchette Portfolio<br> with 8 notes &amp; envelope</font></td>
			<td align="center"><font size="2">8&quot; x<br>
						10&quot;</font></td>
			<td><font size=2><?php echo silk_show('XRU8'); ?></font></td>
		</tr>
				<tr align="center">
					<td colspan="3"><font size="2">Pouchette Portfolio only</font></td>
			<td align="center"><font size="2">8&quot; x<br>
						10&quot;</font></td>
			<td><font size=2><?php echo silk_show('XMSC8'); ?></font></td>
		</tr>
				<tr align="center">
					<td colspan="3"><font size="2">Pouchette Portfolio<br> with 10 letters &amp; envelope</font></td>
			<td align="center"><font size="2">9&quot; x<br>
						12&quot;</font></td>
			<td><font size=2><?php echo silk_show('XHE9'); ?></font></td>
		</tr>
				<tr align="center">
					<td colspan="3"><font size="2">Pouchette Portfolio only</font></td>
			<td align="center"><font size="2">9&quot; x<br>
						12&quot;</font></td>
			<td><font size=2><?php echo silk_show('XMSC9'); ?></font></td>
		</tr>
			</table>
			<br>
			</h3>
		</center>
	</body>

	</center>

</html></body>
