<?php
  include ('book_sc_fns.php');

  // error.php -  returns to login form
  // 5.30
  session_start();
  // session_destroy();      // disable 7/30/03  allow usr to make mistake in login
                             // and NOT lose contents of cart.  Destroy all session variables
	
  if (!isset($_SESSION['searchstr']))
 	include('set_vars.php');
	
  include('tr_header.php');
			
  echo "<table class=descrip border=$g_table1_border align=$g_table1_align width=$g_table1_width>
		  <tr><td><p class=product>Error</td></tr><tr height=70><td class=descrip>";
		  
	// check the error code and generate an appropriate error message 
	$e = $_GET["e"];
	switch ($e) {
	case -3:
		$message = "User name already in use, try again.";
		break;

	case -1:
		$message = "No such user.";
		break;
	
	case 0:
		$message = "Invalid username and/or password.";
		break;
	
	case 2:
		$message = "Unauthorized access.";
		break;

	default:
		$message = "An unspecified error occurred.";
		break;
	}
 
	echo '<center>'.$message.'<br>'; 
	echo "Please <a href=weblogin.php>log in</a> again.</center>";

	echo  '</td></tr></table>';
   silk_footer($g_table1_align,$g_table1_width);
?>
