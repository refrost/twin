<?php
 // 1/24/2010  -- Forgot  username: 
 // Receives email from weblogin.php.display_forgot_password(),
 // If one record matches email, email the username to the address.
 // else if more than one record is found for the email,
 // send an email that sends them to the user which CAN start the merge_id.php process.
 // selection/consolidation page.
 // If the  username is found, present lostpassword question (back on the weblogin.php?function=question,
 // which posts to forgot_password2.php
 // If username & answer matches, change the password. . receive old and new password(s); compare, reset_password; Message
 // php5.30
 // 8/29/2015 replace mail_() w/ phpmailer
 // 9/15/2015: started w/ mailjet.com/phpmailer also w/ user_auth_fns notify_password()
 
 include ('book_sc_fns.php');
 session_start();

 if (!filled_out($_POST) || !valid_email($_POST['email']) )
 {
   pass_msg("You have not filled out the form completely.
          <a href=weblogin.php?function=user>Please try again.</a>",1);
 }
 else
 {
      // check for one or more matches in customers table
      if (!($conn = db_connect()))
         return false;
      $email = $_POST['email'];
      $email = @mysqli_real_escape_string($conn, $email);
      
      //Get username..
      $query = "select username,firstname,lastname,entered from customers where email='$email'";
      $result = @mysqli_query( $conn, $query);
      if (!$result || mysqli_num_rows($result)==0)
      {
          pass_msg("No username found for this email.  Try again or email Webmaster for assistance.
                    <br /><a href=weblogin.php?function=user>Please try again.</a>",1);
          // failed or no matches
      }
      else
      {
         // One or more cust records so, create message and send email...
         $from = "admin@$g_emaildomain";
         $fromname = 'Webadmin';
          
         $msg  = "Dear $g_storename Customer: \r\n\r\n"; 
         $count =  mysqli_num_rows($result);
         
         if ($count==1)
         {
    	       $row = mysqli_fetch_array($result);
    	       $username = stripslashes($row['username']);
             $msg .= "Your  login username is <b>$username</b>. \r\n";
         }
         else
         {
            // More than one record. Handle it.
            $msg .= "Our records show you have $count login usernames at $g_companyurl.\r\n These are listed below:  \r\n\r\n";
            while ($row = mysqli_fetch_array($result,  MYSQLI_ASSOC)) 
            {
               $msg .= stripslashes($row['username']). "\tcreated on ".$row['entered']."\r\n";
            }
            
            $msg .=  "\r\nYou can continue to use any of these, or if you would like to consolidate "
                    ."\r\norder histories and shipping address under one username in the future, "
                    ."\r\nplease reply or send an email containing your preferred username "
                    ."\r\nand request consolidation.";                           
            //username($email);
         }
         
         $msg .= "\r\n\r\n"
                 ."--Web Admin and Support \r\n"
                 .$from."\r\n"
                 .$g_companyurl."\r\n"
                 .$g_payment_phone;
            
            $subject = "$g_storename Login Information";                      
            // -f $from was in mail() replaces the default  mailed-by as fall.phpwebhosting.com; Use ->AddReplyTo($from) in phpmailer.

           //On local or production site, so have phpmailer/mailjet send email

           // phpmailer 8/29/2015
           
           include("mail_fns.php");
           $mail = new mailer;

           $mail->SMTPDebug = 0;
           $mail->addReplyTo($from,$fromname); //Set before ->setFrom
           $mail->setFrom($from,$fromname);
           $mail->Subject = $subject;
           $mail->Body    = $msg;
           $mail->addAddress($email);
           
          //if (mail($email, "$g_storename Login Information", $msg, "From: $from","-f $from"))
            
          if($mail->Send())
          {
              // Log info in a text file
              sys_log("ForgotUserName: $email");  //book_sc_fns.php
              pass_msg("Your username has been emailed to $email.".msgcontinue(),1);
              return true; // (true) //echo nl2br($msg);
           }  
           else
           {
              sys_log("Failed to send email: Forgot_UserName: $email");  //book_sc_fns.php
              pass_msg("There was a problem sending the email. Please try again or call the office.".msgcontinue(),1);               
              return false;
           }
      } // End One or more records so, create message and send email...

} // End valid email; check for one or more matches in customers table
?>
