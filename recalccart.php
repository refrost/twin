<?php
// php 5.3

  include ('book_sc_fns.php');
  // recalccart.php  5/20/03  (from recalc_cart.php) redirects to showcart for tr/css/web version
  // 01/05/2015 Exclude/handle recalc of itemno "BDH..." -- halfstuff binderboard
  
  	session_start();
	
	$catid = $_POST["catid"];      //$catid = $_SESSION["catid"];
    //$zcart = $_SESSION["cart"];  //this is the old list.
	$xcart = $_POST;				//this is the edited list

//This next mess  goes throught the _POST and saves only the item/qty pairs 
//into a local array (acart). Thus, this excludes the hidden vars (catid) and the (submit),
//plus it "deletes" zero values and prevents user from entering a negative number.  
//May NEED to validate data here and prevent bad data....


foreach ($xcart as $key => $value)  
  {
	if ($key != 'save'|| $key != 'catid')  //Don't want these hidden/submit vars
	{
      if ($value+ 12 == 12) 			//if 0, we don't want it. Delete from list.
        true;  // this doesn't work as of 3/26/02 ver 4.12 ==> 'unset($xcart[$key]);
      else
	    {
	    if ($value < 0)
		  $acart[$key] = ($value * -1) ;  //Force to be positive & save in new array
		else
		  $acart[$key] = $value;		  //Save in new array.
		}
	}
	
  }
    
// 01/05/2015 Halfstuff revision (see bboardcost.php - recalc previously failed...)
// If any items  like BDH... figure qty and set price level
reset($acart);
$bdhqty = 0;

foreach ($acart as $key => $value)   
{
	if (left($key,3) === 'BDH')  
	{
        $bdhqty = $bdhqty + $value;
    }
}

if ($bdhqty > 0)     // Set price level
{
    switch ($bdhqty)
	{
		case $bdhqty>25 :
			$level = 'C';
			break;
		case $bdhqty>9 :
			$level = 'B';
			break;
		default :
			$level = 'A';
			break;
	}
    reset($acart);
    
    foreach ($acart as $key => $value)  
    {
    	if (left($key,3) === 'BDH')  // Revise itemno for price code
    	{
            $newkey = substr_replace($key,$level,-1);
            $bcart[$newkey] = $value ;
        }  
    	else
    		$bcart[$key] = $value;
    }
    $acart = $bcart;
} // end if $bdhqty > 0
    
    //back to original recalccart...
    reset($acart);

	//calculate_cart_totals($cart);
	
	$_SESSION['total_price'] = calculate_price($acart);  
    $_SESSION['total_weight'] = calculate_weight($acart);
    $_SESSION['items'] = calculate_items($acart);

	$_SESSION['cart'] = $acart;
	
	//shipping is automatically recalc'ed in show_cart.

    //echo "<a href=show_cart.php?catid=$catid>Continue</a>"
  
  	header("location:showcart.php?catid=".$catid);
    exit;

?>
