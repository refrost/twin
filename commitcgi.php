#!/usr/local/bin/php
<?php
  // # Search and edit: $body2 to change text of emails to user.
  //
  // 09/26/2015 php5.5  phpmailer added/uploaded. 
  // This is production  version.
  // For localhost  running
  // edit this original in cgi-bin and then save copy to twin, and cmt hashtag
  
  // #!/usr/local/bin/php
  // replaces mail()
  
  // 09/27/2015 Encryption overview and context:
  // Encryption is set in c:\gnupg on \\graphic
  // (old xp) may also be on Win7 \\newggail.
  // Public key is copied to ~/.gnupg on website
  // Original webapp in php4.48 ran as user 
  // nobody requiring script in cgi-bin 
  // to create temp file. Upgrade (9/15)to 
  // php5.5-cgi would allow run the script 
  // as user from the www directory.
  // This script emails the encrypted order 
  // to tr@gmail.com where signal is copy-
  // pasted on \\newgail into Foxpro 'Dropkick
  // Decryptor' which decrypts and then 
  // re-parses the order fields and values and
  // prints the order -- which is then hand-
  // entered into SBT accounting.
  // Possible upgrade would be to
  // pass/parse as JSON. Move encrypt to main www.
    
  // php5.3 9/27/2014  6/23/2015 include path
  // 8/9/2012  blind copy emails to twinrocker@gmail.com; stopped sending to rf@gm
  // 6/5/2010 Edited and uploaded [Fixed eregi]
  // 4/1/2010 Added "Card not charged till order ships...
  // NOTE:  10/10/2008  Doc:   This routine sends an encrypted email to orders@twinrockerhandmadepaper.com
  //        and a order confirmation email to the customer and to postmaster@twinrockerhandmadepaper.com.
  //        Blind Copies of each are sent to ralph.frost@gmail.com for test/monitoring purposes.
  //        At Twinrocker, emails are filtered to put the encrypted ordered into the Online Sales
  //        directory and the Confirmations into the Order Confirmations.  The Encrypted Order is copied 
  //        and pasted into a custom VFP program (Dropkick) which decrypts the ordered and then prints.
  //        Order is then re-entered into their SBT Sales Order Module. 
  //        postmaster@twinrockerhandmadepaper.com is accessed via the dominant personality in Eudora    bod
  //        (and the onlinesales personality is not used since it results in a duplicate confirmation).
  
  
  // CGI php Script to encrypt the order info and email it.  .old.4.3.6
  
  // December 15, 2004 -- ISP upgraded to php 4.3.10 LOSS gpg and email
  // January 14, 2005 -- ISP Suggested shebang line of #!/usr/local/bin/php.old.4.3.6
     //Outcome: failed
  // January 26/2005:  ISP suggested copy /usr/local/bin/php to cgi-bin
  //    and add following lines to .htaccess
  //    AddHandler custom-php .php .phtml
  //    Action custom-php /cgi-bin/php
  // This force the script to run as user and then the encrypting worked again.
  
  // Set include_path to nclude our functions
  if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost" )
{
    ini_set("include_path","c:/wamp/www/twin/inc"); // win7-darth 2014
    // Copy emails to r.f. in dev
    $bcc = 'ralph.frost@gmail.com';
    $dev = true;
  } else {
      ini_set("include_path","/home/twinrocker/inc");
      // Copy emails to tr@gmail in production
      // which is where all processing occurs. 
      $bcc = 'twinrocker@gmail.com';
      $dev = false; 
  }
  include ('book_sc_fns.php');
  include ('mail_fns.php');
  
  // Get the info passed via _GET
  // Tokens/keys and ccnum relating to the order...
	if (isset($_GET['a']))  {
    $soid   = $_GET['a'];
    $webid  = $_GET['b'];
    $ccnum    = unserialize(base64_decode($_GET['c']));
    $shipinfo = unserialize(base64_decode($_GET['d']));
    $confirmation = $_GET['e'];
    $sono =  $_GET['f'];
  } else {
    //pass_msg('Error committing data.');
  }
  //br();
  //echo $soid.' '.$webid.' '.$ccnum;
  //echo;
  //br();
  
  // Get this order from the Orders table
  $conn = db_connect();
  $query = "select * from orders where order_token='$soid'";
  $queryresult = @mysqli_query($conn, $query); // or die("Error in updating logindate"); 
  $contents = '';
  $delimited = '';
  
  // January 13, 2005 -- Encryption problem workaround
  // edited 9/27/2014 php5.3
  if (stristr($shipinfo,'rush charge') === FALSE)  
    $is_rush = '0';
  else
    $is_rush = '1';
  
  //Add credit card number AND shipinfo to email contents below.
 
  $finalstring = $confirmation." commit.001 -- Data access Problem.";
  if ($queryresult)  {

      // Added delimited list 11/20/03 to ease import into VFP decrypter
      // Moved cc# to front 11/23/03
      // Added fieldname delimiters to ease memo processing in Dropkick (the VFP decrypter) 12/03/03
     
      // Use next info in body of raw email to office if/when gpg fails..
      $raw_email =  qd_delimited_list($queryresult). "<shipinfo>\nshipinfo:$shipinfo<br>\n</shipinfo>\n</end>";

      $delimited = "creditcard:$ccnum<br>\n".$raw_email;  // this is for gpg and encryped mail
     
  
      //Get name from delimited  so can add to subject line
      $line = explode('<br>',$delimited);
      $cnt = count($line);
      $str = explode(':',$line[7]);
      $company = $str[1];                //company
      $str = explode(':',$line[8]);
      $person = $str[1];                //name
      $str = explode(':',$line[9]);
      $email = $str[1];                //email
      $name = $email.'  '.$person.' - '.$company;    // Company and FL Name
      
      $str1 = explode(':',$line[19]);
      $meth = $str1[1];                // payment method: cconline or ccphone
      $str = explode(':',$line[20]);
      $safeccnumber = $str[1];         // first and last four digit of ccnumber

      //echo "email $email  person $person name $name <br>" ;
      //echo $delimited;
      //exit;
      
      // Variable lines require searching to find the total_order value
      for ($j = 32; $j < $cnt ; $j++)
      {
          if (stristr($line[$j],'total_order')!==FALSE)
          {
            $str2 = explode(':',$line[$j]);
            $totalsale = number_format($str2[1],2);
            break;
          }
      }

      // from send_private_mail.php ---------
      // TRHMP forwards to POP3 at the site, downloaded on demand or into separate mailreader.
      // twinrocker stilts49 -- 9/26/03   <-- password
      // Initially set up to send a copy to ref&a while developing/debugging. Serves as crash backup.
      
      $body = $delimited;    // 11/20/03 was   $contents;

      // Now do the encryption --------------
      // Tell gpg where to find the key ring
      $oldhome = getenv("GNUPGHOME");
      putenv("GNUPGHOME=/home/twinrocker/.gnupg");
      //echo getenv("HOME");
  
      //create a unique file name
      $infile = tempnam("/home/twinrocker/tmp/","kq13"); //.uniqid(microtime(),1);   // world writeable //
      $outfile = $infile.".asc";
      //echo $infile.' '. $outfile,'<br>';
       
      //write the user's text to the file
      $fp = fopen($infile, "w");
      fwrite($fp, $body);
      fclose($fp);
      
      //print  $fp;
      //set up our command
      //(The recipient in quotes is the name of the public key
      //created on the receiving box and exported/imported
      //then edit:trust/sign/save on unix box.)
      // Changed 9/23/03  to 'twinrocker online'
      // Changed 1/27/05 to 'twinrocker ecommerce' w/ keyword of 'Leadership'
      $recipient = 'Leadership';
      
      //Form the gpg cmd
      $command =  "/usr/bin/gpg -a  --recipient $recipient --encrypt -o $outfile $infile";
      
      // execute our gpg command
      passthru($command,$result);
      //passthru("dir",$result);

      //delete the unencrypted temp file
      unlink($infile);
  
      //echo '<br>Resultcode='.$result;
      //exit;
      $finalstring = " commit.002 -- Command Problem.";
      
      if($result==0 || $dev)  // Encrypted worked okay   (or developing)
      {
        $fp = fopen($outfile, "r");
        if((!$fp||filesize ($outfile)==0) && !$dev)
        {
          //file open failed -- not encrypt/mailed
          $result = '98';  
        }
        else
        {
          if (!$dev) {
              //read the encrypted file; send it
              $contents = fread ($fp, filesize ($outfile));
              //delete the encrypted temp file
              unlink($outfile);
              $to_email = "orders@twinrockerhandmadepaper.com";
          } else {
              // $dev = true 
              $contents = $raw_email;
              $to_email = "ralph@frostscientific.com";
          }
                   
          //MAIL IT....  (1st -- encrypted..)
          // was $title:
          $subject = "Ref#: $sono $name $$totalsale $meth";
          $from = "onlinesales@twinrockerhandmadepaper.com";
          $fromname = 'TR OnlineSales';
          
          //OLD: mail($to_email, $title, $contents, "From: $from\n");
          //$finalstring = $confirmation;
                  
        // phpmailer 09/19/2015     1st email
        $mail = new mailer;

        $mail->SMTPDebug = 0;
        $mail->addReplyTo($from,$fromname); 
        $mail->setFrom($from,$fromname);
        $mail->Subject = $subject;
        $mail->Body    = $contents;
        $mail->addAddress($to_email);
        $mail->addBCC($bcc);        
        if($mail->Send()) {
            //echo $bcc; //success
        } else {
            //echo 'failed'; //failure
        }

          //Now send the customer a brief order confirmation with a blind copy to tr 
        
          $body2 = ucwords(strtolower($person))."\n".ucwords(strtolower($company))."\n$email\n\n";
          if ($meth=='cconline')
          {
            $left = left($safeccnumber,1);
            switch ($left)
            {
              case '4' :
                $card = 'Visa Card';
                break;
              case '5' :
                $card = 'Master Card';
                break;
              default :
                $card = 'credit card';
                break;
            }
            $body2 .= "Thank you for web order, reference number $sono, in the amount of $$totalsale charged to the $card "
                     ." ending: ".right($safeccnumber,4).".\n\n Your card will not be charged until your order ships.\n\n";


          }
          else {
            $body2 .= "We have received  web order, reference number $sono in the amount of $$totalsale.  "
                     ."Unless you have an up-to-date credit card payment agreement or are paying via an institutional purchase order "
                     ."on file with us covering on-line orders, please call 1-800-757-TWIN (8946) "
                     ."as soon as possible to provide your credit card or other payment information. "
                     ."No activity will take place on this order until payment arrangements are made.\n\n";
         }

          $body2 .="Please note that you can re-call this web order for editing and/or re-ordering at our dynamic website <http://www.twinrockerhandmadepaper.com>. "
                ."After logging in with your username and password, click on MyMenu and then MyOrderHistory.\n\n"
                ."If you have any questions about this order, reply to this email or call or fax us.\n\n\n"
                ."Sincerely,\nJoni Phebus\nOffice Manager\nTwinrocker, Inc.\nPhone 1-800-757-TWIN (8946)\nFAX 1-800-466-1633\n<http://www.twinrocker.com>  Home Page\n"
                ."<https://www.twinrockerhandmadepaper.com> Papermaking Supplies\ninfo@twinrockerhandmadepaper.com  Email\n\n"
                ."(Please remember, do not send any sensitive information such as credit card information via unsecure email.)";

          $title2 = "Twinrocker Order Confirmation - Ref# $sono Amount: $$totalsale";
          $from2 = "twinrocker@twinrockerhandmadepaper.com";
          $fromname2 = 'Twinrocker Online Sales';
          
          // user's email   is $email; 

          // Mail it... (2nd - Confirm to user and tr)
          //OLD: mail($to_email2, $title2, $body2, "From: $from2\n");

          //phpmailer()  -- Order Confirmation
          if (!isset($mail))
               $mail = new mailer;
          else {    
               //clear addresses all types
               $mail->clearAllRecipients();
               $mail->clearBCCs();
          }

           $mail->addReplyTo($from2,$fromname2); 
           $mail->setFrom($from2,$fromname2);
           $mail->Subject = $title2;
           $mail->Body    = $body2;
           $mail->addAddress($email);
           $mail->addBCC($bcc);
          
           if($mail->Send()) {
                //success
           } else {
                //failure
           }

        }  //Done - Reading the encrypted file and emailing it and order conf
        
      } // Done processing encryption worked!
      else
      {
        // Encryption failed  outright
        $result = '99';  //1.14.05  gpg/encryption failed -- have person CALL in order_conf.php 
        $finalstring = "commit.003 -- encryption failed.";
      }
  } // Found the file in orders table
  else
  {
      //Query failed, no data
      $result = '44';
  }

  if ($result=='44')  //EXITS:  Query failed, there is a mysql problem
      pass_msg('Problem encountered. Try again, or call in your order. ');  //exits
  
  if ($result > '90') 
  {
        //The email didn't encrypt so send a msg, the order_conf.php will call error
        //to display asking user to call the office. 
        //This emails detail of order in crude delimited form to twinrocker... no CC#
        
        $body = $raw_email;    // 01/14/05
        $subject = "RAW $sono $name $$totalsale ";
        $from = "onlinesales@twinrockerhandmadepaper.com";
        $to_email = "onlinesales@twinrockerhandmadepaper.com";
         
        //OLD: mail($to_email, $title, $body, "From: $from\n");
        
        // phpmailer ...(3rd -- Raw to tr)
        if (!isset($mail))
            $mail = new mailer;
        else {    
            // clear addresses of all types
            $mail->clearAllRecipients();
            $mail->clearBCCs();
        }
        //$mail->addReplyTo($from,'Raw Order'); 
        $mail->setFrom($from,'Raw Order Ecryption Failed');
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->addAddress($to_email);
        $mail->addBCC($bcc);  // tr@gmail
          
        if($mail->Send()) {
            //success
        } else {
            //failure
        }

  } // Resultcode  above 90
  
  //Okay, so have sent the encrypted email to the office
  //we send the "Sono # .... totaling $,,,, when ships, will be changed to card number: 5-2665",
  
  //Now  display the order confirmation page.
  if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost" )
  {
      header("location:order_conf.php?res=$result&str=$finalstring&rush=$is_rush");
      exit;
  }
  else
  {
      header("location:https://".$_SERVER["SERVER_NAME"]."/order_conf.php?res=$result&str=$finalstring&rush=$is_rush");
      exit;
  }


?>
