<?php
 // rev 1/24/2010 -ref-
 // forgot_password2.php
 // Second part of Forgot  password checking routine:
 // First part gets username and checks it, then asks the secret answer.
 // which posts here to forgot_password2.php
 // If username & answer matches, change the password.
 // php5.30 
 
 include ('book_sc_fns.php');
 session_start();

 if (!filled_out($_POST))
 {
   pass_msg("You have not filled out the form completely.
          <a href=weblogin.php?function=forgot>Please try again.</a>",1);
 }
 else
 {
    $user_answer = $_POST['answer'];
    $username = $_SESSION['tempuser'];
    $real_answer = get_user_answer($username);

 
    //9/6/03 crypt the answer so only user knows, saved as uppercase
    // salt  (initially set in register.php.)
    if (crypt(STRTOUPPER($user_answer),'si')==$real_answer)
    {
        //User has answered the question so give a new password.

        if ($password = reset_password($username) )
        {
            // Next: see user_auth_fns.php
            if (notify_password($username,$password,$g_companyurl))
               pass_msg("Your new password has been emailed to you.".msgcontinue(),1);
            else
               pass_msg("Your password  was changed to $password but there was a problem sending email . Make a note of it now.".msgcontinue(),1);
        }
    }
    else
        pass_msg("Your password could not be reset. Your answer is not correct. <a href=weblogin.php?function=forgot>Please try again.</a>",1);

 }
