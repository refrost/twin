<?php
  include ('book_sc_fns.php');
//Get/add/edit customer record  ** for intranet **
//This script displays Query form to get a customer by  zip, phone, Lastname, etc.
// called ftom "new customer" from the menubar, and calls to cust_scrn_form.php

  session_start();
  include_once('tr_header.php');
  
  echo "<table class=profile border=1 align=$g_table1_align width=$g_table1_width>
		  <tr><td><p class=intro>Admin=>Find a Customer</td></tr><tr height=200><td class=descrip>";
  echo '<center>';

  if (isset($_GET["menu"]))  //if coming from browse off the do_menu()
  {	
  	$action = $_GET["menu"];
  	if ($action == 'new')
	  {
      //If adding new rcord, unset shipid and payarray
		  if (isset($_SESSION["SESSION_SHIPID"]))
		  {
			 unset($_SESSION["SESSION_SHIPID"]);
			 unset($_SESSION["payarray"]);
		  }
		  //Display the form and get search criteria
		  get_customer();
	  }
  	exit;   //Leave here, with return with _get OR _post vars
            //Get, if coming from get_customer()
            //Post, if coming from List (created below) of potential matches
  }


  $exact = false;  // controls display of id =  or id like;
  if (isset($_POST["cb"]))		//if returning from custlist; cb is for checked box array.
  {
      $checked = $_POST["cb"];
	   $searchtype  = 'id';
	   //find which record was selected
	   
	   foreach($checked as $key => $value) {
		    //echo $key.'  val '.$value;
		    if ($value)
		    {
			     $searchterm = $value;	//id of checked customer
			     $exact = true;
		    }
	   }
  }
  else
  { 
     //Coming from the get_customer() function 
  	  if (isset($_GET["cust_search_string"]))
	  {
	    $searchterm = $_GET["cust_search_string"];
    	 $searchtype  = $_GET["cust_search_field"];
	  }
	  else
	  {
		    if (!isset($_GET["cust_action"]))  //called from selected customer get_cust_action())
		    {
			     echo "<br> <a href=customer.php?menu=new>No search details. Please try again.</a>";
			     exit;
		    }
		    else
		    {
			     $searchterm = $_GET["this_id"];
    	        $searchtype  = 'id';
			     $exact = true;
		    }
	   }
  }
  
  //If have search terms, do the query...
	
  if (!$searchtype || !$searchterm)
  {
     echo "<br> <a href=customer.php?menu=new>You have not entered search details.  Please go back and try again.</a>";
     exit;
  }
  
  $searchtype = addslashes(trim($searchtype));
  $searchterm = addslashes(trim($searchterm));

  
 if ($searchtype == 'phone')  //parse the phone number and  search
 {
 	$p1 = ''; $p2 = ''; $p3 = '';
	$p1 = substr($searchterm,0,3);
	$p2 = substr($searchterm,3,3);
	$p3 = substr($searchterm,6,4);
	
	if (empty($p2))
		$mfilter = " where phn_area = '".$p1."' ";
	else 
	{
		if (empty($p3))
			$mfilter = " where phn_area = '".$p1."' and phn_prefix = '".$p2."'";
		else	
			$mfilter =  "where phn_area = '".$p1."' and phn_prefix = '".$p2."' and phn_suffix LIKE '".$p3."%'";
	}
	
	
	$query = "select *  from customers".$mfilter;
	
 }
 else 
 {
 	//Otherwise, if not searching on phone, look up field equal $value, or field LIKE $value...
 	if ($exact)
		$query = "select * from customers where ".$searchtype." = '".$searchterm."' LIMIT 150";
	else
		$query = "select * from customers where ".$searchtype." LIKE '".$searchterm."%' LIMIT 150";
 }

 //echo $query.'  xx';

 
 //Do the query...
 $conn = db_connect();
 $result = @mysqli_query( $conn, $query) or die ("Error in query: $query. " . ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
 $num_rows = mysqli_num_rows($result);
 
 if ($num_rows > 0)
 {
		// have found one or more value; display; verify; use
		{
			//have a list: show list; select one, display and use. 
		}

  	
	if ($num_rows == 1)	
	{
		br();
		// post to cust_scrn_form from customer.php whereas losts or other acesses
		// use GET
		
		// 10/23/02 -ref- Streamline feature -- skip showing  the echo "<form...
		//                just call the cust_scrn_form with right values. xx

		// xx    echo "<form method=post action=cust_scrn_form.php >";
		
	}
	else
	{
		if ($_SESSION["SESSION"] == "Admin")  //Only Admin users can see the multiple matches
		{
			display_space_head("$num_rows Matches");
			echo "<FORM METHOD=POST ACTION=customer.php>";
			echo "<input type=submit name=Go value=Select>";
		}
		else
		{
			//regular user, so don't show list of other people
			echo "No data found.";
			exit;
		}
	}
	
	// if made it to here then there are more than one matches
	// show the table and let user click on the selected one... 
	// which returns to .this. script where $numrows WILL be 1
	// .. show table...
	echo"<table class=profile border=1 align=center>";
  	for ($i=0; $i <$num_rows; $i++)
  	{
     	$row = mysqli_fetch_array($result);
		$phn = stripslashes($row["phn_area"])."/"
				.stripslashes($row["phn_prefix"])."-"
				.stripslashes($row["phn_suffix"]);
				
      echo  "<tr><td bgcolor=#ffcc00>";
	  if ($num_rows == 1)
	  {
	  	// if there is only one in the list then display pulldown menu of actions..
	  	
		// xx  10/23/02 when have one id, set vars and return to home page
		// xx  get_cust_action();	//  cust_action
		// xx  echo  "<input type=hidden name=this_id value="
	   	// xx  	.stripslashes($row["id"]).">";
		// xx  echo  "<input type=submit value=Go></td><td>";
		
		header("location:cust_scrn_form.php?cust_action=continue&this_id=".$row["webid"]);
		exit;
		
	  }
	  else
	  {
	  	//if many records, display a checkbox for each record. The cb array value 
		// passes back the record id the user selects.
		
	  	echo  "<input type=checkbox name=cb[$i] value="
	   		.stripslashes($row["id"])."></td><td>";
	  }
	  //Display other variables
      echo   stripslashes($row["name"])."</td><td>"
     	    .stripslashes($row["address1"])."</td><td>"
			.stripslashes($row["city"]).' '.stripslashes($row["state"])."</td><td>&nbsp;"
			.stripslashes($row["ldate"])."</td><td>$"
			.stripslashes(number_format($row["lsale"],2))."</td><td>"		
			.$phn."</td><td>"										
     		.stripslashes($row["custno"])."</td></tr>";
  	}
	//name,address1,city,zip,id,custno,type,ldate,lsale
	//	.stripslashes($row["type"])."</td><td>"
	echo "</table>";
	if ($num_rows == 1)
 	{
		true;
		//Now, the single customer is listed at the top of the page and this is where 
		//all the action of get_cust_action() takes place...
		
	}
	else
	{
		//Display the submit button which passes the cb[] back to this script
		echo "<input type=submit name=Go value=Select>";
	}
	echo "</form>";
	
	    
 }
 else
 {
		//No records found
		
		echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;  <a href=customer.php?menu=new>No record found. Try Again</a>";
		
 }

echo '</center>';
echo  '</td></tr></table>';
?>
