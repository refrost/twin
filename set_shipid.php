<?php
require_once("book_sc_fns.php"); 
session_start();

// set_ship_id.php
  if (isset($_POST["cb"]))		//if returning from custlist; cb is for checked box array.
  {
  	 //$_POST["cb"];
	   $shipid = -1 ;
	
	   
       foreach ($_POST["cb"] as $key => $value)
	   {
		    echo $key.'  val '.$value;
		    if ($value)
		    {
			     $shipid = $value;	//id of  shipaddrid
		    }
	   }
	   if (isset($_POST["delete"]))
	   {
		    //want to delete his record if an adminstrator (or later, the user.)
		    delete_shipaddress($shipid);
		    header("location:shipaddr.php");  //Go back to list...
  	   	exit;
	   }

  }
  else 
  {
  	 if (isset($_GET["s_addr"]))		//if returning from custlist; cb is for checked box array.
  	 {
  		  $shipid = $_GET["s_addr"];
	 }
	 else
		  $shipid = -1 ;
  } 

  // 6/5/2010 :  Added reset ship_state on change
  if ($shipid != -1)
  {
      $ship = get_shiparray($shipid); 
      $_SESSION["ship_state"] = $ship['state'];
  }
  else
  {
      //Ship_state is set to billto state
      $ship = get_custarray($_SESSION["SESSION_UACCT"]);
      $_SESSION["ship_state"] = $ship['state'];
  }

  $_SESSION["SESSION_SHIPID"] = $shipid;
  
  header("location:tr_checkout.php");  //Now re-display the cart
  exit;

  //echo $_SESSION["SESSION_SHIPID"];
  //echo "<a href='checkout.php?catid=home'>Go</a>";
?>
