<?php
include ('book_sc_fns.php');
// paper.php  shows a set of selects and then shows the selected types of paper.
// -ref- 7/14-18/02  Adds items to local cart by cliking  on price. Has Americat code 
//                   that would allow entering qty and hit ADD later.

// Note that click on price version gets display data from the PAPER table but then gets 
// price (cart functions) from the invt table (invt list).  Update BOTH or not at all. 
// PAPER then invt.

// 10/10/02  
// Changed behavior so when first run Silk displays the "Top 40 selling items"
// Added $pinstock checkbox so user can control seeing items that QOH > 0 only, or
// else get option to add QOH <= 0 to 'wishlist'
// php 5.30


  session_start();
  if (!isset($_SESSION['ship_state']))
      include('set_vars.php');

	if (isset($_GET['catid']))
	   $catid = $_GET['catid'];
  else
     $catid = '';

	include ('pa_header.php');


  echo "<table width=100% align=center bgcolor=white border=0><tr><td>";
  
	// Display header	
	display_space_head("Paper Selector");
	
	
	// Initialize variables and arrays that popular form variables
	$pinstock = 0;
	
	$ptype = array (  array('PPW','Watercolor', "CATID = 'PPW'" ),
					  array('Any','All types', "(CATID = 'PPZ' OR CATID = 'PPW')" )
				   );
				   
				   # array('PPZ','All purpose', "CATID = 'PPZ'"),
				   
				   
	$pweight = array (  array('Any weight','',''),
						array('Thin Text','A','H'),	
		  				array('Text','B','I'),
		  				array('Heavy Text','C','J'),
		  				array('Light Art','D','K'),
		  				array('Art','E','L'),	
		  				array('Heavy Art','F','M'),	
		  				array('Board','G','N')
					 );
					 
	$psize  =  array (  array('Any size','',''), 
		  				array('Small  - 12 inch or less','S',"SIZEGROUP='S'"),
		  				array('Medium - 12 to 21 inch','M',"SIZEGROUP='M'"),
		  				array('Large  - 21 or more inches','L',"SIZEGROUP='L'"),
		  				array('Round shapes','x',"(LENGTHCODE = 'DIA')"),
						array('Heart shapes','x',"(LENGTHCODE = 'HRT')")    
					  );

	
	if (isset($_GET['papertype']))	// If form has been submitted...
	{
	
		// Checkbox controlling display or not or wishlist items (QOH <= 0)
		if (isset($_GET['pinstock']))
			$pinstock = $_GET['pinstock'];
		else
			$pinstock = 0;
		
		
		$type = $_GET['papertype'];
		$weight = $_GET['paperweight'];
		$color = $_GET['papercolor'];
		$size = $_GET['papersizegroup'];
		
		// Next set values from the arrays
		$mtype = $ptype[$type][1];
		$mtypecode = $ptype[$type][0];
		$mtypewhere = $ptype[$type][2];

		$msize = $psize[$size][0];
		$msizecode = $psize[$size][1];
		$msizewhere = $psize[$size][2];
		
		
		$mweight = $pweight[$weight][0];
		
		// Set value of paperweight filter
		if ($weight>0)
		{
			//if not Any Weights
			if ($mtypecode=="PPZ")
				$mweightwhere = "WEIGHTCODE='".$pweight[$weight][1]."'";
			else
			{
				if ($mtypecode=="PPW")
					$mweightwhere = "WEIGHTCODE='".$pweight[$weight][2]."'";
				else
					$mweightwhere = "(WEIGHTCODE='".$pweight[$weight][1]."' OR WEIGHTCODE='".$pweight[$weight][2]."')";
			}	
		}
		else
			$mweightwhere = '';
		
		//Set color filter...	
		if ($color == 'Any color')
		{
			$colorwhere = '';
		}
		else
		{
			$colorwhere = "COLORGROUP='$color'";
		}
	
		// build the sql filter
		$msqlfilter =  $mtypewhere;

		if ($mweightwhere<>'')
				$msqlfilter .= " AND $mweightwhere";
		if ($colorwhere <>'')
				$msqlfilter .= " AND $colorwhere";
		if ($msizecode<>'')
				$msqlfilter .= " AND $msizewhere";
				
		if ($pinstock == 1) // if box is checked only show items with QOH > 0
				$msqlfilter .= " AND ((QOH>0) OR (QOH=0 AND length(trim(item))<6))";		//Show	headings 
		//else 
		//	$msqlfilter .= "OR ((QOH=0 AND (length(trim(item))<6))";
	
		// if the form has been submitted, don't  shop the Top 40
		$showtop40 = false;
	}
	else
	{
		//This is the first time in the form so initialize defaults in selects 
		$pinstock = 1;
		$type = 1;
		$weight = 'Any weight';
		$color = 'Any color';
		$size = 'Any size';
		
		// and set flag to show top 40
		$showtop40 = true;
		$msqlfilter = '';
	}
	
    
	
	//make selection  --  get_paper_selects();
	$colorarray =get_papercolorcodes(); //Gets distinct colors from PAPER table
	$end = sizeof($colorarray);
	
	//Display the table containing the select form elements...
	echo "<table border=0>
	<form action=paper.php method=get>
	<input type=hidden name=catid value=PAO>
	<tr valign=top>
	<th>&nbsp;&nbsp;&nbsp;<u>Paper type:</u></th>
	<th valign=top>&nbsp;&nbsp;&nbsp;<u>Weight/usage:</u></th>
  <th>&nbsp;&nbsp;&nbsp;<u>Paper color:</u></th>
	<th>&nbsp;&nbsp;&nbsp;<u>Size/Shapes:</u></th>
	</tr><tr valign=top>";
	
  //	<th>&nbsp;&nbsp;&nbsp;<u>'Wishlist' items:</u></th>

	// Show radio buttons for papertype from array
	echo '<td >';
	for ($row = 0; $row < count($ptype); $row++)
	{
		echo "&nbsp;
		      <input type=radio name=papertype value=$row ";
		if ($row==$type)
			echo 'CHECKED';
		echo '>'.$ptype[$row][1]."<br>";
	}
	
	echo "</td><td>";

	// weightcodes vary by whether watercolor or all purpose
	echo "<select name=paperweight>";
				
	for ($row = 0; $row < count($pweight); $row++)
	{
		echo "<option value=$row ";
		if ($row==$weight)
			echo ' SELECTED';
		echo '>'.$pweight[$row][0];
	}

	echo "</select>	
		  </td><td>";
	     
	echo "<select name=papercolor>			  
		  <option value='Any color' SELECTED>Any color";
		 // <option value=lights >Light colors
		 // <option value=darks >Dark colors
		 // <option value=brights >Bright colors
	for ($j = 0; $j < $end; $j++ )
		 {
		  echo "<option value='".($colorarray[$j]['colorgroup']);
		  if ($colorarray[$j]['colorgroup']==$color)
		  	echo "' SELECTED>".($colorarray[$j]['colorgroup']); 
		  else
		    echo "'>".($colorarray[$j]['colorgroup']); 
         }
	echo "</select>";

	echo "</td><td>";
    echo "<select name=papersizegroup>";
	
	for ($row = 0; $row < count($psize); $row++)
	{
		echo "<option value=$row ";
		if ($row==$size)
			echo ' SELECTED';
		echo '>'.$psize[$row][0];
	}	
	echo "</select></td>";

  /*
	// Show the 'Wishlist' checkbox
	echo '<td width=120 align=center><br>';
	echo "<input type=checkbox name=pinstock value=1";
		if ($pinstock == 1)
			  echo ' CHECKED';
		echo ">Don't Show";
	  
	echo "</td><td><br>";
	*/
	
	// Show the submit button
	echo "</tr><tr><td colspan=4 align=center>
        <input type=submit value=Go>
	      </td></tr>
		  </form>";
	echo "</table>";
	
	
	
	//echo $msqlfilter; //TEST

	// Get and list out the items in the current selection
	
	if (isset($_GET['papertype']) OR $showtop40)
	{
		if ($showtop40) // if first time,  show the top 40. (FUNCTIONS in book_fns.php)
		{
			$paperlist = get_bestpaperitems();
		}
		else
			$paperlist = get_paperitems($msqlfilter);
		
		// Do some figuring on items in or not in stock (QOH > 0)	
		$totalpaperitems = count($paperlist);
		$mrecsinstock = 0;
		$mrecsoutofstock = 0;
		for ($row = 0;$row < $totalpaperitems; $row++)
		{
			if ($paperlist[$row]['QOH']>0)
				$mrecsinstock++;
			else
				$mrecsoutofstock++;
		}
		
		//echo $msqlfilter;
		
		// Show summary info...
		if (!$paperlist && !$showtop40)
			echo "<h4>No  records  - $mtype $mweight papers; $color; $msize</h4>";
		else
		{
			if (!$showtop40)
			{
				echo "<h4>$mrecsinstock ";
				if ($pinstock == 0)
					echo "of $totalpaperitems";
				echo " items in stock - $mtype $mweight papers; $color; $msize</h4>";
			}
			if ($mrecsinstock>0)
			{
				if ($showtop40)
						echo '<h3>Top 40 - most popular paper items</h3>';
				//else
					//echo "<a href=paper.php?catid=PAO>Top 40</a>";

// Show legend from original site.. Use in online version
/*
?>				<P>
<CENTER><FONT FACE="Times New Roman"><FONT COLOR="000000"><FONT SIZE=-1><B>Weight:&nbsp;&nbsp;<FONT COLOR="#990000">TT</FONT> = Thin Text;&nbsp;&nbsp;<FONT COLOR="#990000">T</FONT> = Text;&nbsp;&nbsp;<FONT COLOR="#990000">HT</FONT> = Heavy Text;&nbsp;&nbsp;<FONT COLOR="#990000">LA</FONT> = Light Art;&nbsp;&nbsp;<FONT COLOR="#990000">A</FONT> = Art;&nbsp;&nbsp; </B></FONT></FONT></CENTER>
<CENTER><FONT FACE="Times New Roman"><FONT COLOR="000000"><FONT SIZE=-1><B><FONT COLOR="#990000">HA</FONT> = Heavy Art;&nbsp;&nbsp;<FONT COLOR="#990000">B</FONT> = Board.&nbsp;&nbsp;</B></FONT></FONT></CENTER>
<CENTER><FONT FACE="Times New Roman"><FONT COLOR="000000"><FONT SIZE=-1><B>Finish:&nbsp;&nbsp;<FONT COLOR="#003333">CP</FONT> = Cold Pressed;&nbsp;&nbsp;<FONT COLOR="#003333">R</FONT> = Rough;&nbsp;&nbsp;<FONT COLOR="#003333">HP</FONT> = Hot Pressed.&nbsp;&nbsp;</B></FONT></FONT>
<CENTER><FONT FACE="Times New Roman"><FONT COLOR="#000000"><FONT SIZE=-1><B>"<FONT COLOR="#660000">GEL</FONT>" indicates Twinrocker All-Purpose Paper with gelatin sizing.</B></FONT></FONT>&nbsp;&nbsp;&nbsp;&nbsp;<FONT FACE="Times New Roman"><FONT COLOR="000000">
<br></b><FONT SIZE=-1><B>Click Description for picture & more info.</B><FONT SIZE=+0><FONT COLOR="#000066"></FONT></FONT></CENTER>
</CENTER>
<?php				
*/				
				echo "<h5>(Click 'Price' to add a unit to the shopping cart";
				if ($pinstock == 0 && !$showtop40)
					echo " or click 'Wish' to add to the Wishlist/Production Schedule.";
				echo ")</h5>";

				// Start the data table with header...
				echo "<table border=1 bgcolor=#ffce9c><tr valign=top bgcolor=#e0e0e0>
					<th align=left>Description <FONT SIZE=-2>(click for more info)</FONT></th>
					<th>Price</th>
					<th>On-hand</th>
					<th>Color</th>
					<th>Item code</th>
					</tr>";
					//<th>Order Qty</th>
					// <td>&nbsp;</td>    // used as last cell in papertype header
				for ($row = 0; $row < count($paperlist); $row++)
				{
					// if the first 4 characters of diescrption are caps,
					// treat as a header and make bold.
					$teststr = substr($paperlist[$row]['DESCRIP'],0,4);
					if ($teststr== STRTOUPPER($teststr) && $teststr <> '100%') 
						echo "<tr valign=top><td><a href='pzdescrp.htm#".substr($paperlist[$row]['ITEM'],0,5).
                 "' TARGET='TWINROCKER ALL-PURPOSE PAPER INVENTORY' ><b>".
                  $paperlist[$row]['DESCRIP']."</b></a></td><td>
                  &nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
					else
						if ($paperlist[$row]['QOH']>0 && $paperlist[$row]['PRICE']>0)  // if QOH > 0 then make Price a link
						{
              // Original link creation...
							//$priceurl = add_items_to_cart($paperlist[$row]['ITEM'],
              //                              $paperlist[$row]['PRICE'],$catid);
              
              //Call to pa_add_one_form.php 8/23/04
              
              $priceurl = show_link_add_paperform($paperlist[$row]['ITEM'],
                            $paperlist[$row]['PRICE'],
                            $paperlist[$row]['QOH'],
                            $paperlist[$row]['DESCRIP']
                          );

              //echo $paperlist[$row]['PRICE'];
              //exit;

              /*
              $priceurl = "<a href-pa_add_one_form.php?itemno=".$paperlist[$row]['ITEM'].
                              "&descrip=".$paperlist[$row]['DESCRIP'].
                              "&price=".$paperlist[$row]['PRICE'].">".$paperlist[$row]['PRICE']."</a>";
							*/
							
							// Enabled 'click description' to see more info and pictures of papers
							echo "<tr valign=top><td>".$paperlist[$row]['DESCRIP'].
                   "</td><td >&nbsp;$&nbsp;$priceurl&nbsp;&nbsp;</td>".
                   "<td align=right>&nbsp;&nbsp;&nbsp;".
                    number_format($paperlist[$row]['QOH'],0).
                    "</td><td align=center>".
                    $paperlist[$row]['COLORGROUP']."</td><td align=left>&nbsp;".
                    $paperlist[$row]['ITEM']."</td>";
							
							# optional items to display
							//if ($showtop40)
							//	echo "<td align=center>".$paperlist[$row]['YTDSLS']."</td>";
							//echo "<td>".americart_form($paperlist[$row])."</td></tr>";
						}
						else
						{
							// if zero QOH, change layout add to wish list
							
							//$wishurl = add_items_to_wishlist($paperlist[$row]['ITEM'],$paperlist[$row]['PRICE'],$catid);
							$wishurl = '';  //'Wish';
							
							if ($paperlist[$row]['PRICE']==0)
								echo '<tr valign=top><td><a href=pzdescrp.htm#'.substr($paperlist[$row]['ITEM'],0,5)." TARGET='TWINROCKER ALL-PURPOSE PAPER INVENTORY'>".$paperlist[$row]['DESCRIP'].
                     "</a></td><td >&nbsp;$&nbsp;".$paperlist[$row]['PRICE']."&nbsp;&nbsp;</td><td align=right>&nbsp;0&nbsp;&nbsp;$wishurl</td><td align=center>".$paperlist[$row]['COLORGROUP']."</td><td align=left>&nbsp;".$paperlist[$row]['ITEM']."</td>";
							else
								echo "<tr valign=top><td>".$paperlist[$row]['DESCRIP'].
                     "</td><td >".$paperlist[$row]['PRICE'].
                     "</td><td align=right>&nbsp;0&nbsp;&nbsp;$wishurl</td>
                     <td align=center>".$paperlist[$row]['COLORGROUP']."</td>
                     <td align=left>&nbsp;".
                     $paperlist[$row]['ITEM']."</td>";

						}
				}
				echo "</tr></table>";
				echo "<a href=paper.php?catid=PAO>Display Top 40</a>";
			}
		}	
	}
	
	echo '</td></tr></table>';
	echo '</center>';
	//do_html_footer();
	
		//$XX = ARRAY(XX=>'23');
		//ECHO COUNT($XX);
		
function americart_form($thisitem)		
{

// initially made for PAPER  7/16/02 -ref-
 
	//$description = str_replace(" ","+",trim($thisitem["DESCRIP"]));
	//$description = str_replace("++","+",trim($description));
	//	  .$description."^"
	//echo $description;
	$ac_price = number_format($thisitem["PRICE"], 2);
	$ac_form_op2 = "p-2914^"
	      .$thisitem["ITEM"]."^"
		  .trim($thisitem["DESCRIP"])."^"  
		  .$ac_price."^"
		  ."op2";   //."^"."^"."^";
		  //.number_format($thisitem["weight"], 3);
		  
	$mret  = "<form action='http://www.cartserver.com/sc/cart.cgi' method=POST>"
	 ."<input type=hidden name=item value='$ac_form_op2'>"
	 ."<input type=text name=op2 size=3 value='1'>"
	 ."<input type=image name=nvadd src='sc_images/gw_rd_add.gif'>"
	 ."</form>";

	 //echo $ac_form_op2;
	RETURN $mret;
}
?>
