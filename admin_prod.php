<?php

include('book_sc_fns.php');
//prod_admin.php  1/12/03 add/edd product records
session_Start();

if (!check_admin_user())    // 1/27/2007  prevent access from non-admin through url
{
  go_showprod();
}
// $_GET -> $dept,$groupname,$prod
// add and/or look up the record 
// pass the array to the following function
// save results
// confirm returns to show_prod from the calling point.

// 12/26/03 - Adding sections requires knowing which page to return to...
$gotourl = $_SESSION['SECTION_MAINFILE'];   // this is set when changing sections...
//echo $gotourl;

//Deal with situation if called from Form after add/edit with user pressing update (or delete)...

if (isset($_POST['action']) )	
{
	$action = $_POST['action'];
	if (isset($_POST['dept']))
		$dept = $_POST['dept'];
	else
		$dept = '';	
	if (isset($_POST['groupname']))
		$groupname = $_POST['groupname'];
	else
		$groupname = '';
	$grp = $groupname;	//Used in returning to right product
	if (isset($_POST['product']))
		$product = $_POST['product'];
	else
		$product = '';


  $gotourl = $_SESSION['SECTION_MAINFILE'];

  if ($action=='Cancel Delete')
  {
    header("location:$gotourl?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode($product));
    exit;
  }
  
  
	if ($action=='Delete Record' )   // Coming from Admin form
	{
      //If user clicked delete button, get user to confirm
      
      confirm_delete($dept,$grp,$product);  //action_fns.php
      
      // If no, return to display_prod_form
      // If Yes, return here w/ action='Delete Confirmed'
  }
	if ($action=='Delete Confirmed' )
	{
		//want to delete the record 
		$conn = db_connect();
		
		$query = "DELETE FROM products WHERE dept='".addslashes($dept)."' and groupname='".addslashes($groupname)."' and product='".addslashes($product)."' LIMIT 1";
		 
		echo $query.'<br>';
		
		$result = @mysqli_query($conn, $query);
		if (!$result)
	    	echo "Failed deleting data  ";
		else
			echo "Record deleted from product data table ";
			
   		@((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
		
		// Go back to same dept and prod after deleting product
		//header("location:showprod.php?dept=".urlencode($dept)."&grp=".urlencode($groupname));
		//exit;
		echo "<a href=$gotourl?dept=".urlencode($dept)."&grp=".urlencode($groupname).">Continue from delete</a>";
		
	}
	elseif ($action=='Save Record' )
	{
		
		$product = addslashes($_POST['product']);
		$name = addslashes($_POST['name']);
		$subheader = addslashes($_POST['subheader']);
		if ($_POST['display']=='on')
			$display = 'y';
		else
			$display = 'n';
		$alert = $_POST['alert'];
		$bigimg = addslashes($_POST['bigimg']);
		$alt = addslashes($_POST['alt']);
		$smallimg = addslashes($_POST['smallimg']);
		$height = addslashes($_POST['height']);
		$width = addslashes($_POST['width']);
		$align = addslashes($_POST['align']);
		$descrip1 = addslashes($_POST['descrip1']);
		$descrip2 = addslashes($_POST['descrip2']);
		$items = addslashes($_POST['items']);
		$pricetable = addslashes($_POST['pricetable']);
		$comments = addslashes($_POST['comments']);
		$keywords = addslashes($_POST['keywords']);
		$moreinfo = addslashes($_POST['moreinfo']);
		$dept = addslashes($_POST['dept']);
		$groupname = addslashes($_POST['groupname']);
		$grp = $groupname; // usef in returning to diplay, below
		$catid = addslashes($_POST['catid']);
		$displayorder = addslashes($_POST['displayorder']);
		$entered = $_POST['entered'];
			
			
		$conn = db_connect();
		
		$query = "REPLACE products (product, name, subheader,display, alert, bigimg,".
		         " alt, smallimg, height, width, align, descrip1, descrip2,"
				 ." items, pricetable, comments, keywords, moreinfo, "
				 ."	dept, groupname, catid, displayorder, entered)"
				 ."VALUES ('$product', '$name', '$subheader', '$display', '$alert', '$bigimg',"
		 		 ."'$alt', '$smallimg', $height, $width, '$align', '$descrip1', '$descrip2',"
		 		 ."'$items', '$pricetable', '$comments', '$keywords', '$moreinfo',"
				 ."'$dept', '$groupname', '$catid', $displayorder, '$entered')"; 
		
		echo $_POST['entered'];
		br(); 
		echo $query;
		
		$result = @mysqli_query($conn, $query);
		if (!$result)
     	{
			echo "Failed to add new product. Try later.";
			//exit;
		}	
		@((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
			
		header("location:$gotourl?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode($product));
		exit;
		//echo "<a  //href=showprod.php?dept=".urlencode($dept)
//    ."&grp=".urlencode($groupname)."&prod=".urlencode($product).">Continue //from update</a>";

	}
	elseif ($action=='Cancel')
	{
		//cancelled from form
		header("location:$gotourl?dept=".urlencode($dept)."&grp=".urlencode($groupname)."&prod=".urlencode($product));
		exit;
	}		
}


// if called from show_prod.php, as GET, from the edit/add links
if (isset($_GET['action']))	
{
	if (isset($_GET['dept']))
		$dept = $_GET['dept'];
	else
		$dept = '';	
	if (isset($_GET['grp']))
		$grp = $_GET['grp'];
	else
		$grp = '';
	if (isset($_GET['product']))
		$product = $_GET['product'];
	else
		$product = '';

	$action = $_GET['action'];
	if ($action=='add')
	{
		$conn = db_connect();
		
		// make a blank record in this dept+grp, then edit it (below..)
		$query = "REPLACE INTO products (product, name, subheader, display, alert, bigimg,".
		         " alt, smallimg, height, width, align, descrip1, descrip2,"
				 ." items, pricetable, comments, keywords, moreinfo, "
				 ."	dept, groupname, catid, displayorder, entered)"
				 ."VALUES ('', '', '', 'y', '', '',"
		 		 ."'', '', '0', '0', '', '', '',"
		 		 ."'', '', '', '', '',"
				 ."'$dept', '$grp', '', '0', NOW())"; 
		 
		//echo $query;
		
		$result = @mysqli_query($conn, $query);
		if (!$result)
     	{
			echo "Failed adding new blank record";
			//exit;
		}	
   		@((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
		
	}
	//GET the desired record
	$conn = db_connect();
	$query = "select * from products where dept='".addslashes($dept)."' and groupname='".addslashes($grp)."' and product='".addslashes($product)."' LIMIT 1";   // get the first product
    //echo $query; 
    $result = @mysqli_query($conn, $query);
    //echo $result;
    if (!$result)
   		// No product name so see if can display the first product in this dept/group 
     return false;
    $num_invt = @mysqli_num_rows($result);
    //if ($num_invt ==0)
    //   return false;
    $prod = db_result_to_array($result);
    @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
	
	
	//Do the add/edit form.. which POSTs back to this script
	display_prod_form($prod);
	
	
	
	// echo "<a href=showprod.php?dept=".urlencode($dept)."&grp=".urlencode($grp)."&prod=".urlencode($product).">Hmm Continue</a>";

}


//echo "<a href=showprod.php?dept=".urlencode($dept)."&grp=".urlencode($grp)."&prod=".urlencode($product).">Continue</a>";

//make sure there are no blank records...
$conn = db_connect();
$query = "delete FROM `products` WHERE product=''"; 
$result = @mysqli_query($conn, $query);
@((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);


?>
