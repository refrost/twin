<?php
// set_discounts.php --  called from show_discounts_form()
// cleans user input, set var and return to showcart or tr_checkout
include ('book_sc_fns.php');
session_start();
$returnto = $_POST['returnto'];   // gets hidden var

if (empty($_POST['discountcode']))  {
    $_SESSION["discount_codes"] = '';
    $_SESSION["total_discount"] = 0.00;
    $_SESSION["dc"] = array();
    header("location:$returnto");
    exit;
}

// Clean and validates user input of discount codes. Save in sessvar
$rawcode = str_replace(' ',',',$_POST['discountcode']);
$rawcode = str_replace(';',',',$rawcode);
$rawcode = str_replace(':',',',$rawcode);
$rawcode = preg_replace("/[^a-zA-Z0-9,]/", "", $rawcode);

$dcodes = strtoupper($rawcode);
$dcodes = validate_discounts($dcodes);  // trn_fns.php  - make sure active and not expired

$_SESSION["discount_codes"] = $dcodes;  // Saves valid codes in a session var
    
// if empty give alert  'no valid code entered'
if (trim($dcodes)=='') {
    $message = urlencode ("The discount code you entered is not recognized or is no longer active. "."<br><br><a href=$returnto>Continue</a>");
			header ("Location: message.php?message=$message"); 
			exit;
}
    
header("location:$returnto");
exit;

?>