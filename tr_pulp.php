<?php
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

include ('book_sc_fns.php');
// tr_pulp.php web version of prior program  -ref- 2/14/03
// pulp.php  shows a table for available ready-to-use pulps  (per pail and five pail)
//           Also allows a person to specify beating etc.
// -ref- 7/23-18/02    10/18/02
// -ref- 7/2/2010   format page width
// 8/31/2011 Get pulpitems from table (changed from tr_pulp_price)  
//rev. 6/21/2012 Changed pails to Sealed Bags/Containers

// IMPORTANT
//    Edit '..inc/tr_pulp_price.php' to modify ready-to-use
//    Pulp pricing (11/21/03) And also, edit  records in 'pulpitems' table in Database to
//    change pulp fiber prices and availability (8/31/2011)    

// Displays form on first entry, then adds pulp items to pulpcart -- recalculating 
// on each pass.
// 06/27/2015 php 5.3

//  EDIT CODE WITH GREAT CARE!!  10/26/02

    session_start();
   // include('set_vars.php');
	//Set flag to clear the pulpcart  or to delete one line from the list...
    	
	if (isset($_GET['clearpulp']))
    	$clearpulp = true;
	else
		$clearpulp = false;		
	
	if (isset($_GET['del']))
	{
		$del_one = true;
		$k = $_GET['del'];   //this is the array index of the line to erase
	}
	else
		$del_one = false;
		
	
		
  	if ((isset($_SESSION["pulpcart"]) && count($_SESSION["pulpcart"]) > 0) )
  	{
  		$pulpcart = $_SESSION["pulpcart"];
		
/*		foreach ($pulpcart as $n => $value)
  		{
			//if (isset($n)) 
				//echo $n.'  '.$value.'<br>';
			//$row = $value[$n];
			echo $n.' :: '.$$value.'   '.$value['shpqty'];
		}		
*/	
		
		//clear the pulpcart if flag is set
		if ($clearpulp)
		{
  		  unset($pulpcart);				//erase the pulpcart
		  unset($_SESSION['pulpcart']);	//erase the session copy
		}
	
		// delete a specific pulpitem in the pulpcart (called from below)
		if ($del_one)
		{
			unset($pulpcart[$k]);  //delete the array item $k
			// resave pulpcart into SESSION var
			$xcart = $pulpcart;
			$_SESSION["pulpcart"] = $xcart;		
		}
		
		//Calculate pulp items... and show in header on each pass through
  		$shpcount = 0;
  		$count = 0;
  		$val = 0.00;		
		reset($pulpcart);
    	
        foreach ($pulpcart as $key => $row)
		  {
	 	  if ($pulpcart[$key]['shpqty']>0)
	 		  {
  	 		$shpcount = $shpcount + $pulpcart[$key]['shpqty'];
	  		$count= $count + 1;
	   		$val= $val + $pulpcart[$key]['totalamount'];
		  	}
		  }
		  //Store into session
		
  		$_SESSION["pulp_shpitems"]= $shpcount;
  		$_SESSION["pulp_items"]= $count;
  		$_SESSION["pulp_price"]= $val;		
		
		//Now show the header so that any changes in qty will show up...
		
		$dept='Pulp';  // Pass the department we are in to show on menubar, in this case , pulp
		$grp ='Pulp+Order+Form';
		$jscript = 'validpulpform';  // Add javascript validation to header
		include('tr_header.php');	
		
		// Show the current pulp items that have been ordered
		display_tr_pulpcart($pulpcart);
		
		if ($clearpulp)		//if user pressed clearpulp, then must redisplay page to reset the session vars (I think)
		{
			header("location:tr_pulp.php");	
			exit;
		}
	}
	else   //this is first pass, now show the header 
	{
		$dept='Pulp';
		$grp ='Pulp+Order+Form';
		$jscript = 'validpulpform';  // Add javascript validation to header
		include('tr_header.php');
	}
	
	// Now we start to display the pulp form....
	
	if (isset($_POST['go']))	// Has the form been run once?
	{
		// Get the these $m___  forms vars  that were POSTED
		$mfiber = $_POST['mfiber'] ;
		$mbeating = $_POST['mbeating'] ;
		$mfiveinfour = $_POST['mfiveinfour'] ;
		$mcolorant = $_POST['mcolorant'] ;
		$mpails = $_POST['mpails'] ;
		$mpulptext = $_POST['mpulptext'] ;

		if (!isset($_POST['msizing']))
			$msizing = '0' ;
		else
			$msizing = $_POST['msizing'] ;
			
		if (!isset($_POST['mcmc']))
			$mcmc = '' ;
		else
			$mcmc = $_POST['mcmc'] ;
		if (!isset($_POST['mclay']))
			$mclay = '' ;
		else
			$mclay = $_POST['mclay'] ;
		if (!isset($_POST['mcalcium']))
			$mcalcium = '' ;
		else
			$mcalcium = $_POST['mcalcium'] ;

		if (!isset($_POST['mpurpose']))
			$mpurpose = 'All' ;
		else		
			$mpurpose =  $_POST['mpurpose'] ;
		if (!isset($_POST['mixproportions']))
			$mixproportions = '' ;
		else		
			$mixproportions =  $_POST['mixproportions'] ;
			
	}
	else  //We just landed on this page and this is the first display
	      //OR we ARE redisplaying 
	{
		// See bottom of page -- passes formvars as a string in the header...
		// This routine here gets the vars back so that  the form "remembers" the last
		// state even though the pulp.php form is called a couple times to get the session
		// vars refreshed.
		
		if (isset($_GET['formvars']))
		{
			$formvars = $_GET['formvars'];
			//echo $formvars.'<br>';
			$mstr = explode("|",$formvars,12);
/*			echo $mstr[0];br();
			echo $mstr[1];br();
			echo $mstr[2];br();
			echo $mstr[3];br();
			echo $mstr[4];br();
			echo $mstr[5];br();
			//echo $mstr[6];br();
*/			
			$mfiber = explode(':',$mstr[0]);   //make the string into an array (for mixtures)
			$mbeating = $mstr[1];
			$mfiveinfour = $mstr[2];
			$mcolorant = $mstr[3];
			$mpails = intval($mstr[4]);
			$mpulptext = $mstr[5];
			$msizing = $mstr[6];
			$mcmc = '';  //leave radio unset  $mstr[7];
			$mclay = $mstr[8];
			$mcalcium = $mstr[9];
			$mpurpose = $mstr[10];
			$mixproportions = $mstr[11];
		}
		else  //Okay, it really IS the first time the form is displayed!!
		{
			$mfiber = 'PHCL27';
			$mbeating = 'MC';
			$mfiveinfour = '1'; //12/1/03 change from chkbox to radio
			$mcolorant = '';
			$mpails = 1;
			$mpulptext = '--->';
			$msizing = '0' ;
			$mcmc = '' ;
			$mclay = '' ;
			$mcalcium = '' ;
			$mpurpose = 'All';
			$mixproportions = '';
		}
	}	
	//echo 'xx'.$mfiber;		
/*	//Debug list 
    if (is_array($mfiber))
	{
		reset($mfiber);
		foreach ($mfiber as $key => value) {
			echo $value; br();
		}
	}
*/	

	// !!!!!!!!!!!!  SET PULP PRICES HERE!    !!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	include('tr_pulp_price.php'); 	//fiber, beating, colorants, usage and additives

	// NOW we show the Pulp Selection form...
	
	echo "<table  border=1 cellspacing=2 cellpadding=2 bgcolor=white width=$g_table1_width align=$g_table1_align>
	<tr valign=top>
	<td align=left colspan=3 class=pulp><h3>WET, READY-TO-USE-PULP ORDER FORM: &nbsp;&nbsp;&nbsp;&nbsp;
     STEP-BY-STEP</h3></td>";

 ?>

	<form onSubmit="return validpulpform(this)" action="tr_pulp.php" method="POST">
	<input type="hidden" name="catid" value="PUO">
<?php
	// Select pulp fibers:  can select mixtures  width=120
	echo "</tr><tr valign=top><td class=pulp width=160>
		  1. Select Fiber(s)  <a href=showprod.php?dept=Pulp&grp=Twinrocker+Pulp&prod=Pulp+Prices>&nbsp;Check Pricing</a><br /><br />
		  
		  <select name=mfiber[] SIZE=6 MULTIPLE>";
		  
	$halfstuff = pulpitems();   // 8/31/2011 Get pulpitems from table as an array  8/31/2011
	
	for ($row = 0; $row < count($halfstuff); $row++) //list all the halfstuff array
	{
		echo '<option value='.$halfstuff[$row][3];
		//Need to loop throught the mfiber array on each $row to see if .this. item 
		//was selected on last pass and thus needs to be 'selected'.
		if (is_array($mfiber))
		{
			reset($mfiber);
			
            foreach ($mfiber as $key => $value)
			{
				//echo $value; br();
				if ($value==$halfstuff[$row][3])	// 10/30/02 Mixture item matches
				{
					echo ' SELECTED';   //mark to show as highlighted
			
					//Save aspects of the selected item in this mixture in these arrays
					$fiberitem[]= $halfstuff[$row][3];        
					$fiberdesc[] = $halfstuff[$row][0];
					$fiberprice[] = $halfstuff[$row][1];
					$fiberdiscount[] = $halfstuff[$row][2];
				}
			}
		}
		echo '>'.$halfstuff[$row][0];
	}

	echo "</select><br /><br />[For mixtures: hold Ctrl key and click multiple fibers]
	      <br /><br />
		  <u>Mix Proportions:</u> <input type=text name=mixproportions value='".$mixproportions."' size=5>
		     <br /><br />(Example: 1:2:1 -- listed in the same fiber order as shown above.)
      </td>";
	
	
	// Show Beating selection 
	echo "<td class=pulp width=160>2. Select Beating:<br><SELECT name=mbeating SIZE=1>";
	for ($row = 0; $row < count($beatopt); $row++)
	{
		echo "<option value='".$beatopt[$row][0]."'";
		if ($beatopt[$row][0]==$mbeating)
		{
			echo ' SELECTED';
			$beatstr = '/'.$beatopt[$row][0];
			$beatprice = $beatopt[$row][2];
		}			
		echo '>'.$beatopt[$row][1];
	}
	echo '</SELECT>';
		
	//echo '</td><td>';
	br(3);
	//Additives radio buttons...	  
	echo "3. Select Sizing(s):<br><input type=radio name=msizing value='1' ";
	if ($msizing =='1')
	{
		echo 'CHECKED';
		$sizeprice = 1*$sizecharge;
		$sizestr ='/Sz';
	}
	echo '>1x';
	echo "<input type=radio name=msizing value='2' ";
	if ($msizing =='2')
	{
		echo 'CHECKED';
		$sizeprice = 2*$sizecharge;
		$sizestr ='/Sz2';
	}
	echo '>2x';
	echo "<input type=radio name=msizing value='3' ";
	if ($msizing =='3')
	{
		echo 'CHECKED';
		$sizeprice = 3*$sizecharge;
		$sizestr ='/Sz3';
	}
	echo '>3x';

	echo '<br><br>';


  //Initialize cmc vars
	$cmcprice = 0;
	$cmcstr ='';

	echo " CMC: <input type=radio name=mcmc value='1' ";
	if (isset($mcmc) && $mcmc=='1')
	{
		echo 'CHECKED';
		$cmcprice = 1*$cmccharge;
		$cmcstr ='/CMC';
	}
	echo '>1x';
	echo "<input type=radio name=mcmc value='2' ";
	if (isset($mcmc) && $mcmc=='2')
	{
		echo 'CHECKED';
		$cmcprice = 2*$cmccharge;
		$cmcstr ='/CMC2';
	}
	echo '>2x<br /><br />';


	echo "<input type=checkbox name=mclay value='on' ";
	if (isset($mclay) && !empty($mclay))
	{
		echo 'CHECKED';
		$clayprice = 1*$claycharge;
		$claystr ='/KLN';
	}
	else
	{
		$clayprice = 0;
		$claystr ='';
	}
	echo '>Clay	<br>';
	echo "<input type=checkbox name=mcalcium value='on' ";
	if (isset($mcalcium) && !empty($mcalcium))
	{
		echo 'CHECKED';
		$calciumprice = 1*$calciumcharge;
		$calciumstr ='/CC';
	}
	else
	{
		$calciumprice = 0;
		$calciumstr ='';
	}
	echo ">Calcium carbonate";
	
		echo "</td><td class=pulp>";

	// Select custom colorant..
	//br(3);
	
	echo "4. Select Colorant:<br /> <SELECT name=mcolorant SIZE=1>";
	for ($row = 0; $row < count($colorants); $row++)
	{
		echo "<option value='".$colorants[$row][0]."'";
		if ($colorants[$row][0]==$mcolorant)
		{
			echo ' SELECTED';
			$colorstr = $colorants[$row][2];
			$colorprice = $colorants[$row][1];
		}			
		echo '>'.$colorants[$row][0];
	}
	echo '</SELECT><br />(Specify color in comments below)';
	Br(2);

	echo "5. Select your purpose(s):<br><SELECT name=mpurpose[] MULTIPLE>";

	for ($row = 0; $row < count($pulpusage); $row++)
	{
		echo "<option value='".$pulpusage[$row][0]."'";

/*
This section doesn't work. See mfiber for system which 'remembers'.

		//Need to loop throught the purpose array on each $row to see if .this. item
		//was selected on last pass and thus needs to be 'selected'.
		if (is_array($mpurpose) )
		{
      if (array_key_exists($pulpusage[$row][0],$mpurpose) )
			   echo ' SELECTED';
		}
*/
		echo '>'.$pulpusage[$row][1];
	}
	echo "</SELECT>";

	
	//removed $mpulptext from value in next line.. Doesn't remeber text string... Micxture	
	echo "</td></tr><tr>
		 <td  class=pulp valign=top align=left colspan=2 >6. Special Instructions/Custom Color:<br> <textarea name=mpulptext  rows=4 cols=45 WRAP>".$mpulptext."</textarea></td>";

	// Get the number of pails or five-in-four batches of pulp being ordered...
	// <input type=checkbox name=mfiveinfour value='on'
	echo "</td><td class=pulp>
	
        <table border=0><tr><td class=pulp colspan=2>
        7. Quantity & Batch Size: &nbsp;&nbsp;
        <br><br></td></tr>
        <tr><td class=pulp>
        <input type=text name=mpails value='$mpails' size=3 maxlength=4>
        </td><td class=pulp>";
  // rev. 6/21/12:   <a href='showprod.php?dept=Pulp&grp=Twinrocker+Pulp&prod=Pulp+Prices#pulpinfo'>        More Info</a>
  //Set five-in-four option through radio button (12/1/03 per HClark)
  echo "<input type=radio name=mfiveinfour value='1' ";
  if ($mfiveinfour =='1')
	{
		echo 'CHECKED';
		//$sizeprice = 1*$sizecharge;
		$unitstr ='/ones';
	}
	echo '>Single Containers (Sealed Bags)<br>';
	echo "<input type=radio name=mfiveinfour value='2' ";
	if ($mfiveinfour =='2')
	{
		echo 'CHECKED';
		$unitstr ='/5in4';
	}
	echo '>5-in-4 Batches </td></tr>';
	
	// Submit the form  on bottom row so user see all the fields 'Go'
	echo "<tr><td>&nbsp;</td><td class=pulp>&nbsp;&nbsp;<input type=submit style='font-family:Arial;
			font-weight:bold;
		  	font-size:10px;' name=go value='Add to cart'>
        </td></tr></table>

     </td>
		 </tr>
		 </form>";
	echo "</table>";
	//silk_footer($g_table1_align,$g_table1_width);
	//page_footer();
	//End of displaying the pulp selector form...
	
	
	//Start Calculating:
	//User pressed 'Go' so add item to pulpcart array and save as session var.
	if (isset($_POST['go']))
	{
		//$pails = $mpails;
		
		//12/1/03 changed to single pails of batches of 5in4
    //Must readjust $pails back to prior logic
		if (isset($mfiveinfour) && $mfiveinfour=='2')
		{
      $pails = $mpails*5;
      
   		// Calc the # of batches
  		$batchesfives = $mpails;
	   	$batchesones = 0;

			$shippails = $batchesfives*4;	//Number of pails to ship if dewatered
    }
		else
		{
      $pails = $mpails;
    	$shippails = $pails;		//else, if not dewatered, ship the same number of pails.
   		// Calc the # of batches
  		$batchesfives = 0;
	   	$batchesones = $mpails;
    }

		//Deal with mixtures of fiber... 10/30/02
		//Must set this_item to PMIX if a mix; concat individual item's in comment
		//figure maxfiberprice and associated discount (charge on most expensive fiber
		// array('Cotton Linters #27',37.00,10.00,'PHCL27'),
		
		$maxprice = 0.00;
		$discount = 0.00;
		$itemlist = '';		//this is the list of fiber items selected in the mixture
		$mixturemsg = '';   //this is the string of items in the mixture to be 
							//added to long comment
			
		reset($mfiber);	
		for ($j=0 ; $j < count($mfiber); $j++)
		{ 
			if ($fiberprice[$j] > $maxprice)
			{
				$maxprice = $fiberprice[$j];
				$discount = $fiberdiscount[$j];
			}
			$itemlist .= $fiberitem[$j].':';
			$mixturemsg .=$fiberdesc[$j].':';
		}	

		if (count($mfiber)==1)		//If there is more than one item, item = PMIX
		{
			$mixstr = '';
			$mitem = $fiberitem[0];
			$itemlist = $mitem;
			$comment = ' ';
			$mixproportions = ' ';
			
		}
		else
		{
			$mixstr = '('.$mixproportions.')';
			$mitem = 'PMIX';
			$comment = $itemlist.$mixstr;
		}
				
		$fiberprice = $maxprice;
		$fiberdiscount = $discount;
		$fiberdesc = $mitem.$mixstr;
		//set the prices
		if ($pails > 4)   //Batches of five are discounted whether dewatered or not.
			$unit5price = $fiberprice - $fiberdiscount;
		else
			$unit5price = $fiberprice;

		// Calc the # of batches
		//$batchesfives = intval($pails/5);
		//$batchesones = $pails%5;

		//calc the different prices
		$ext5price = $batchesfives*5*$unit5price;	//discounted price for beating groups of 5 pails and 
		$ext1price = $batchesones*1*$fiberprice;	//regular price for beating one pail batches
		
		//if (isset($mfiveinfour) && $mfiveinfour=='on')
		//	$shippails = $batchesfives*4+$batchesones;	//Number of pails to ship if dewatered
		//else
		//	$shippails = $pails;		//else, if not dewatered, ship the same number of pails.
			
			
		$beat5price = $batchesfives*2*$beatprice;	//Beating add-on is 2x for batches of 5 
		$beat1price = $batchesones*1*$beatprice;
		
		$additivesprice = ($sizeprice+$calciumprice+$cmcprice)*$pails; //additives price
		$clayprice =	$batchesfives*5*$clayprice; // Clay added only to groups of 5
		
		if ($mcolorant != '')
		{
			$coloramt = $pails*$colorprice;
		}
		else
		{
			$coloramt = 0.00;
		}
			
		//Show the item and price...
		
		echo '</center>';
	
		if ($claystr)
		{
			if ($batchesones > 0)
			{
				$comment .= 'Clay only in groups of 5 pails';
				if ($batchesfives == 0)
				{
					$mclay = '';		
					$claystr = '';	//Don't add KLN to description  //2/14/03 -ref-
					//$clayprice = 0.00; // Handled in bactchesfive above...
				}
			}
		}
		 
		$pulpdescrip = $fiberdesc.$beatstr.$calciumstr.$claystr.$sizestr.$cmcstr.$colorstr; 
		//echo $pulp['fiber']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$pulpdescrip."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$shippails." pails to ship of ".$pails." ";
		
/*		br();
		echo 'batchesfives = '.$batchesfives.' x $'.$unit5price.' x 5'; br(); 
		echo 'batchesones = '.$batchesones.' x $'.$fiberprice; br();
		echo 'ext5price = '.$ext5price; br();
		echo 'ext1price = '.$ext1price; br();
		echo 'beat5price = '.$beat5price; br();
		echo 'beat1price = '.$beat1price; br();	

		echo '$additivesprice = '.$additivesprice; br();
		echo '$clayprice = '.$clayprice;	br();
		echo '$colorprice = '.$coloramt.$pulp['colorant'];
	*/	
		// Figure totals and values to store to array.
		$totalamount = 0.00+$ext5price+$ext1price+$beat5price+$beat1price+$additivesprice+$clayprice+$coloramt;
		$avgprice = $totalamount/$pails;
		
		if ($mcolorant != 'None')
			$comment .= ": CustomColor";
		
		// fix text string remove prior "Purpose"..
		
		//echo "$mpulptext";
		
		$s = strpos($mpulptext,">");
		
		$mpulptext = '-->'.substr($mpulptext,$s+1).' '.$mixturemsg.$mixproportions;
		if ($mpurpose[0] <> 'A')   // If NOS, first char is 'U'
		{	
			$mstr = '';
			for ($j=0;$j < count($mpurpose);$j++)
			{
				$mstr .= $mpurpose[$j].'/ ';
			}
		}
		else
    {
      		$mstr .= 'All/ ';
    }
		$mpulptext = trim($mstr.$mpulptext);

		
		
		// Create the line item
		$pulpitem = array("catid"=>"PU","item"=>$mitem,
		                  "description"=>$pulpdescrip,"price"=>$avgprice,
						  "colorant"=>$mcolorant,"shpqty"=>$shippails,
						  "qty"=>$pails,"totalamount"=>$totalamount,
						  "comment"=>$comment,
						  "pulptext"=>$mpulptext,'fives'=>$batchesfives,
						  'ones'=>$batchesones);
		//Insert into pulpcart
		$pulpcart[] = $pulpitem; 
		
		// save pulpcart into SESSION var
		$xcart = $pulpcart;
		$_SESSION["pulpcart"] = $xcart;	
		
		//echo $mfiber[0][0];
		//save formvars as a string to pass as a GET for when page shows again..
		$mpulptext = '-->';
		$pulpform = $itemlist.'|'.$mbeating.'|'.$mfiveinfour.'|'.$mcolorant.'|'.strval($mpails).'|'.$mpulptext.'|'.$msizing.'|'.$mcmc.'|'.$mclay.'|'.$mcalcium.'|'.$mpurpose.'|'.$mixproportions;
		header("location:tr_pulp.php?formvars=".$pulpform);			
		exit;
	}
silk_footer($g_table1_align,$g_table1_width);
?>
