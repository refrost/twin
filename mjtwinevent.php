<?php
/* 5/2/2016  mjtwinevent.php ralph@refrost.com 765-637-7998
   Receives Events posted from Mailjet.com
   trigger and sends notice to FromAddress.
   - Requires php > 5.4; Mailjet API php lib
     is in mailjet subdir below this script
   - Mailjet creds coded below; Sender's
     Events trigger is set at Mailjet.com
     to call this script.
   - In production: industrialplatinginc.com/mjevent.php
     Mailjet trigger calls: 
          https://twinrockerhandmadepaper.com/mjtwinevent.php
*/

// Verify the post comes from mailjet.com
// 4/2/16  REMOTE_ADDR = 176.31.236.72

// Allow only from Mailjet.com, or dev on localhost
$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
if ('mailjet.com' != substr($host, -11)  AND $_SERVER["SERVER_NAME"] != "localhost")  {
  return false;
  exit;
}

// Read the posted Event json object/string from Mailjet
$mjevent = file_get_contents("php://input");
// Strip out any line breaks from string
$linechars = array("\r\n", "\n", "\r");
$mjevent = str_replace($linechars,'',$mjevent);

// and convert json object to a php array
$event =   json_decode($mjevent, TRUE);

if ($event['email'] == '') {
  // If this is a mj test, process test data to developer's email
  $test = true;
  $mjevent = '{"event":"bounce","time":1459628813,"MessageID":18295912046852826,"email":"service@isco.com","mj_campaign_id":5390969334,"mj_contact_id":1577905012,"customcampaign":"","blocked":false,"hard_bounce":false,"error_related_to":"domain","error":"invalid domain"}';
  $mjevent = str_replace($linechars,'',$mjevent);
  $event =   json_decode($mjevent, TRUE);
} 
// response: 200 OK
header("HTTP/1.1 200 ok"); // 6/13/2016 

// Provides...
$failedemail = $event["email"];
$status  = $event["event"];
$errorrelatedto = $event["error_related_to"];
$error = $event["error"];

// First, set Twin  Mailjet Creds and vars:
$apikey = '58400a108ed12fd92150f5b990747e6d';
$apisecret = 'e0ad37741414c0f0425be08cf5759167';
$domain = 'twinrockerhandmadepaper.com';
$company = 'Twin';
$now = date('Y-m-d H:i:s');
$failnotice = '';

// Load MJ php library and create the mj instance
require 'mailjet/vendor/autoload.php';// lib in mailjet subdir
use \Mailjet\Resources;
$mj = new \Mailjet\Client($apikey, $apisecret);

// Set resource filter to get /campaign on mj_campaign_id
// WAS: /messagesentstatistics ID:MessageID,Style:full
$filters = [
'ID'=>$event['mj_campaign_id']
];

// Query the MJ API /campaign resource was: /messagesentstatistics
$response = $mj->get(Resources::$Campaign,['filters' => $filters]);

if ($response->success()) {
  $msg = $response->getData();
  $fromaddress = $msg[0]["FromEmail"];
  $fromname = $msg[0]["FromName"];
  $subject = $msg[0]["Subject"];
  $datesent = $msg[0]["CreatedAt"];

} else {
  $failnotice = 'Fail1';
  $fromaddress = 'ralph.frost@gmail.com';
  $fromname = 'BounceCodeFailed';
  $subject = 'MessageID = '.$event['MessageID'];
  $status = $errorrelatedto = $error = $datesent = '';
}

$notice = <<<EOT
Hey, $fromname,

Your recent email to $failedemail did
not go through.

Email subject:  $subject
    Date sent:  $datesent
       Status:  $status
   Related to:  $errorrelatedto
 Likely issue:  $error

Please verify and correct the email address
and then send your message again.

Thank you.

admin@

twinrockerhandmadepaper.com
Brookston IN 47923
765-637-7998
EOT;

// if $test then redirect email to dev;
if ($test == true) {
  $fromaddress = 'ralph.frost@gmail.com';
  $fromname = 'Bounce Code Test';
}
// Save this event info in a file.
$finalevent = $now.' '.$fromaddress.' '.$mjevent."\r\n";
file_put_contents("outputfile.txt", $finalevent ,FILE_APPEND);

// Email the notice via mailjet api w/ copy to dev
$body = [
    'FromEmail' => "admin@twinrockerhandmadepaper.com",
    'FromName' => "Ralph Frost",
    'Subject' => "Twin/Mailjet Email Event",
    'Text-part' => $notice,
    'Html-part' => "<h3>Attention:</h3>".
    nl2br($notice),
    'Recipients' => [['Email' => $fromaddress,'Name'=> $fromname],
                     ['Email' => "ralph.frost@gmail.com",'Name'=> "Ralph"]
                   ]
];

$response = $mj->post(Resources::$Email, ['body' => $body]);

// done
 ?>
