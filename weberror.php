<?php
// weberror:  1/14/05  gpg/email prob
// if cntrl=1 then STAY in SSL mode (called from register (or login)
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

  include ('book_sc_fns.php');
  session_start();
	if (isset($_GET['message']))
		$message = $_GET['message'];
	else
	{
	  // The gpg/email failed
    $message =      "
    <font color='red' font size='4'> 
    ATTENTION:<br />

    <b>You need to take <u>one</u> of the steps below <br />to allow us 
    to complete your order.*</b></font>
 <div align=left> 
 <pre>    <font face=arial>
                   
 We need your name, the credit card number, expiration date and 
 CVV number on the back of your card. 
 
 Do not re-send your order. Please do one of the following:
                  
 <b><font color=red>Call Twinrocker at 1-800-757-8946.</b>
 
    If you leave your information after hours (M-F 8-5 EST) on our answering
    machine, please speak slowly and clearly.  Also leave a phone number
    and absolute best time to reach you should that be necessary.
                  
 <b>Or, FAX your order to Twinrocker at 1-800-466-1633.</b>  
 
    Click <a href=showprod.php?dept=MyMenu&grp=MyMenu&prod=MyOrderMemory>here</a> to re-display your order, print it and hand write the
    credit card info. Then FAX the order to 1-800-466-1633.

 <b>For best service with RUSH ORDERS, call the Office.</font></b> 

                

We apologize for any inconvenience this extra step causes you, 
and we appreciate your patience while we work this matter out. 
                  
                                  Thank you. 
                                  
                                  <a href=showprod.php>Click to continue</a>
                  </font>
                  </pre>
                  </div>
                  ";
  }
		
  if (!isset($_SESSION['searchstr']))
   		include('set_vars.php');
	include('tr_header.php');
	
	echo "<table class=message width=$g_table1_width align=center border=1 ><tr><td class=message>";
	br();
	Echo $message;
	br(2);
	echo '</td></tr></table>';
	
	if (isset($_GET['duration']))
	{
		sleep($_GET['duration']);
		go_showprod();
	}
	silk_footer($g_table1_align,$g_table1_width);
?>
