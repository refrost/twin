<?php
// 6/5/2010 Edited
// 03/08/2012 rev. changed /BP to /B in item4 so  recalled order calcs right.
// 6/29/2012  rev. properly define $halfstuff to get right pulp pricing:
// rev. 7/31/13 Prevent adding paper if out of stock
// php 5.30

  include ('book_sc_fns.php');
  
  // Called from:    showprod.php?dept=MyMenu&grp=Shopping+Functions&prod=MyOrderMemory
  //
  session_start();
  
  $action = $_GET['action'];
  $sono   = $_GET['sono'];         // use this to communicate back to MyOrderMemory
  $order_token   = $_GET['token']; // use this to specify the record..
  if (isset($_SESSION['SESSION_UACCT']))
    $webid  = $_SESSION['SESSION_UACCT'];  // Use to specify record belongs to .THIS. user.
  else
  {
    header("location:weblogin.php");
    exit;
  }
  
  switch ($action)
  {
    case "delete" :
      $conn = db_connect();
      $query = "update orders  SET order_name = 'deleted' where webid='$webid' and order_token ='$order_token' ";
      $result = mysqli_query($conn, $query);
      if ($result)
         $message = "Order $sono deleted from MyOrderMemory list.";
      else
         $message = "Unable to delete order number $sono from list.";
      break;

    case "delete_all" :
      $conn = db_connect();
      $query = "update orders  SET order_name = 'deleted' where webid='$webid' ";
      $result = mysqli_query($conn, $query);
      if ($result)
         $message = "Orders deleted from MyOrderMemory list.";
      else
         $message = "Deletion effort failed. Try again.";
      break;

      
    case "reorder" :
      // When customer hits reorder we will
      // 1. Check to make sure this is the customer's order_token, not spoofed by _GET
      $conn = db_connect();
      $query = "select *  from orders where order_token ='$order_token' and order_name != 'deleted' and webid='$webid' ";
      $result = mysqli_query($conn, $query);
      
      if ($result)
      {
         //Clear sessions vars  //6/3/2010 -rev-
         $xstate = $_SESSION["ship_state"];  //Retain ship_state after clear
         include('clearcartcode.php');  //Clears cart and session vars
         $_SESSION["ship_state"] =$xstate;

        	unset($_SESSION['SESSION_SHIPID']);  //handle multiple shipto's  5/19/02
        	unset($_SESSION['payarray']);		   //handle pay terms each order in Intranet 5/19/02

        	//_unregister("SESSION_UNAME");	// 7/9/03 allow user to clear cart // 10/7/02 when clear cart; release current user
        	//_unregister("SESSION_UID");
        	//_unregister("SESSION_UACCT");
        	//_unregister("ship_state");     //08/13/03 Don't clear ship_state - let user login

          $xcart = $_SESSION["cart"];
	        
          $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
          $item1 = $row['items1'];
          $item2 = $row['items2'];
          $item3 = $row['items3'];
          $item4 = $row['items4'];    //8/20/04 added paper
          
          if ($item1)
          {
              // Add the items qty pairs into the cart
              $list = explode('<br>',$item1);

              //echo qd_list($list);

              for ($j = 0; $j < count($list)-1; $j++)
              {
                  $thisitem[$j] = explode(',',$list[$j]);
                  //echo $thisitem[$j];
                  // $thisitem[0] == itmeno & $thisitem[1] is the qty to add.
                  
                  //echo $thisitem[$j][1];
                  for ($i=0; $i<$thisitem[$j][1]; $i++)
				          {
            		       $itemno = $thisitem[$j][0];
    				           if(@$xcart[$itemno])
      					         $xcart[$itemno]++;
    				          else
      					         $xcart[$itemno] = 1;
				          }
              }
              


          }  //if item1
            $cart = $xcart;
  	         $_SESSION["cart"] = $cart;

  	         //calculate_cart_totals($cart);

             $_SESSION["total_price"] = calculate_price($cart);  //_SESSION["
             $_SESSION["total_weight"] = calculate_weight($cart);
             $_SESSION["items"] = calculate_items($cart);

           if ($item2)   //fiber
           {

              $list = explode('<br>',$item2);   // Get each line

              //echo qd_list($list).'xx<br>';
              for ($j = 0; $j < count($list)-1; $j++)   //for each lineitem
              {
                  $thisitem[$j] = explode(',',$list[$j]);

                  //echo $list[$j].'<br>'; //$thisitem[$j][0].'  '.$thisitem[$j][1].'  '.$thisitem[$j][2].'  '.$thisitem[$j][3].'  '.$thisitem[$j][4].'<br>';
                  $item = $thisitem[$j][1];
                  $qty    = $thisitem[$j][5];
                  //echo $item;
                  if (preg_match("%BALE%",$thisitem[$j][2]==true)) 
                  // if BALE is in the Description field -- ereg('BALE'
                  {

                      $bale=true;
                      $itemno = preg_replace("/-B/",'',$item);   //Strip off the -B for looking up in fiber table below
                      $qty = preg_replace("/b/","",$qty);
                      //Strip the leading b off to get the nqty of bales
                      $count = 'b'.$qty;  //This shows in the bale count.
                      //echo 'xxbale'.$count;
                  }
                  else
                  {
                      $bale=false;
                      $itemno = $item;
                      $count = '';
                  }
                  // get current price from table

                  $conn = db_connect();
                  $query = "select * from fiber where active = 'Y'  and item ='$itemno'";   //$filename
                  $result = @mysqli_query($conn, $query);
                  if ($result)
                  {
                    $fiberdata = mysqli_fetch_array($result);
                    //if it's a bale we need to get the baleweight
                    //echo $fiberdata[0].'   '.$fiberdata[1].'  '.$fiberdata[2].'  '.$fiberdata[3].'  '.$fiberdata[4].'  '.$fiberdata[5].'  '.$fiberdata[6].'  '.$fiberdata[7].'  '.$fiberdata[8].'  '.$fiberdata[9].'  '.count($fiberdata);
                    if ($bale)
                      $unitprice = $fiberdata[6];
                    else//figure price break  HARDCODED!!
			              {

				             if ($qty < 11 or $fiberdata[3]==0.00)
				  	           $unitprice = $fiberdata[2];
				  	           elseif ($qty < 26 or $fiberdata[4]==0.00)
				  		           $unitprice = $fiberdata[3];
							           elseif ($qty < 101 or $fiberdata[5]==0.00)
				  				         $unitprice = $fiberdata[4];
								          else
				  					         $unitprice = $fiberdata[5];
                    }
                    
                    //echo $unitprice.'ss  ';
                    //unset($fibercart);
                    $pbitem = array("catid"=>'FI',
                      "item"=>$item,
                      "description"=>$thisitem[$j][2],
                      "price"=>$unitprice,
                      "weight"=>$thisitem[$j][4],
                      "qty"=>$qty,
                      "totalamount"=>$unitprice*$thisitem[$j][4]*$qty,
                      "totalweight"=>$qty*$thisitem[$j][4],
                      "count"=>$count,
                      "comment"=>$thisitem[$j][9]);
			              if ($pbitem['weight'] > 0)
				              $fibercart[] = $pbitem ;
				              
                    //br();
                    //echo qd_list($pbitem);


                  }
              }

           } // if item2


               $wt = 0;
          		$cnt = 0;
          		$val = 0.00;
          		reset($fibercart);
              foreach ($fibercart as  $key  => $row)
          		{
          	 	if ($fibercart[$key]['totalweight']>0)
          	 		{
          	 		$wt = $wt + $fibercart[$key]['totalweight'];
          	 		$cnt= $cnt + 1;
          			$val= $val + $fibercart[$key]['totalamount'];
          			}
              }
        		  //Store into session_vars

          		$_SESSION["fiber_weight"]= $wt;
          		$_SESSION["fiber_items"]= $cnt;
          		$_SESSION["fiber_price"]= $val;

              //Save it in the session so frt will calc right, etc.
          		$xcart = $fibercart;
   	         $_SESSION["fibercart"] = $xcart;


            if ($item3)   //pulp
            {

              $list = explode('<br>',$item3);   // Get each line
              ### CODE COPIED FROM tr_pulp.php and modified here
              include('tr_pulp_price.php');
              // Next line added 6/29/2012 (from tr_pulp.php) to properly define $halfstuff:
              $halfstuff = pulpitems();   // 8/31/2011 Get pulpitems from table as an array  8/31/2011
              
              //echo qd_list($list).'xx<br>';
              for ($m = 0; $m < count($list)-1; $m++)   //for each lineitem
              {
                  $thisitem[$m] = explode('|',$list[$m]);
                  echo qd_list($thisitem[$m]);
                  
                  $pulpitem = array(
                    "catid"=>"PU",
                    "item"=>$thisitem[$m][1],
		                "description"=>$thisitem[$m][2],
                    "price"=>$thisitem[$m][3],
						        "colorant"=>$thisitem[$m][4],
                    "shpqty"=>$thisitem[$m][5],
						        "qty"=>$thisitem[$m][6],
                    "totalamount"=>$thisitem[$m][7],
						        "comment"=>$thisitem[$m][8],
						        "pulptext"=>$thisitem[$m][9],
                    "fives"=>$thisitem[$m][10],
						        "ones"=>$thisitem[$m][11]);

                  //echo qd_list($pulpitem);
                  
                  //We could use this except when prices change
                  //the order would be priced wrong, so we will calc
                  //price again  here    Get price arrays from...
                  

                  //echo qd_list($halfstuff[1]);
                  //Initialize some vars
                  $newprice = 0.00;

		              $pails = $pulpitem['qty'];
		              

		              if (isset($mfiber))
                    unset($mfiber);
                  //Fiber or Fibers (in a mix), are put into $mfiber
                  if ($pulpitem['item']=='PMIX')
                  {
		                $temp = explode(':',$pulpitem['comment']);
		                $cnt = count($temp);
                    For ($k=0;$k<$cnt ; $k++)
                    {
		                  if (left($temp[$k],1)=='P')
		                    $mfiber[] = $temp[$k];
                      else
                      {
                        // all the 'P' (pulp items added), next is the mixture
                        break;
                      }
                    }
                  }
                  else
                    $mfiber[] =  $pulpitem['item'];

                  //br();
                  //echo qd_list($mfiber);
                  //exit;
                  
              		//Deal with mixtures of fiber... 10/30/02
		              //Must set this_item to PMIX if a mix; concat individual item's in comment
		              //figure maxfiberprice and associated discount (charge on most expensive fiber
		              // array('Cotton Linters #27',37.00,10.00,'PHCL27'),

		              $maxprice = 0.00;
		              $discount = 0.00;
                  //Get the fiber price...
                  unset($fiberprice);
                  unset($fiberdiscount);
                	for ($row = 0; $row < count($halfstuff); $row++) //list all the halfstuff array
                	{
                		//echo '<option value='.$halfstuff[$row][3];
                		//Need to loop throught the mfiber array on each $row to see if .this. item
                		//was selected on last pass and thus needs to be 'selected'.
                		if (is_array($mfiber))
                		{
                			reset($mfiber);
                			
                			foreach ($mfiber as  $key  => $value)
                            {
                				//echo $value; br();
                				if ($value==$halfstuff[$row][3])	// 10/30/02 Mixture item matches
                				{
                					//echo 'made it here '.$value; //Save aspects of the selected item in this mixture in these arrays
                 					$fiberprice[] = $halfstuff[$row][1];
                					$fiberdiscount[] = $halfstuff[$row][2];
                				}
                			}
                		}

                	}

		              reset($mfiber);
		              for ($j=0 ; $j < count($mfiber); $j++)
		              {
			               if ($fiberprice[$j] > $maxprice)
			               {
				                $maxprice = $fiberprice[$j];
				                $discount = $fiberdiscount[$j];
			               }
		              }

		              if (count($mfiber)==1)		//If there is more than one item, item = PMIX
		              {
			               $mfiveinfour='off';
		              }
		              else
		              {
			               $mfiveinfour='on'; // added here
		              }

		              $fiberprice = $maxprice;
		              $fiberdiscount = $discount;

		              //set the prices
		              if ($pails > 4)   //Batches of five are discounted whether dewatered or not.
			               $unit5price = $fiberprice - $fiberdiscount;
		              else
			               $unit5price = $fiberprice;

		              // Calc the # of batches
		              $batchesfives = $pulpitem['fives'];
		              $batchesones = $pulpitem['ones'];

		              //calc the different prices
		              $ext5price = $batchesfives*5*$unit5price;	//discounted price for beating groups of 5 pails and
		              $ext1price = $batchesones*1*$fiberprice;	//regular price for beating one pail batches

                  // Now need to get beating charges...
                  // These are coded into the /.../.../ in the description
                  $temp = explode('/',$pulpitem['description']);
                  $mbeating = $temp[1];
	                for ($row = 0; $row < count($beatopt); $row++)
	                {
                		if ($beatopt[$row][0]==$mbeating)
		                {
                			$beatprice = $beatopt[$row][2];
		                }
                	}

                  br(2);
                  echo 'Beat $'.$beatprice;
                  br();
                  //exit;
                  
                  // Now get Additive charges.
                  $mcolorcode = '';
                  $calciumprice = 0;
                  $clayprice = 0;
                  $sizeprice = 0;
                  $cmcprice = 0;

		              $cnt = count($temp);
                  For ($k=2;$k<$cnt ; $k++)     //Skip the first two one
                  {
                      switch ($temp[$k])
                      {
                        case 'CC' :
                          $calciumprice = 1*$calciumcharge;
                          break;
                        case 'KLN' :
                          $clayprice =	$claycharge;
                          break;
                        case 'Sz' :
                          $sizeprice = $sizecharge;
                          break;
                        case 'Sz2' :
                          $sizeprice = 2*$sizecharge;
                          break;
                        case 'Sz3' :
                          $sizeprice = 3*$sizecharge;
                          break;
                        case 'CMC' :
                          $cmcprice = $cmccharge;
                          break;
                        case 'CMC2' :
                          $cmcprice = 2*$cmccharge;
                          break;
                        default :
                          //assume is a colorant option
                          $mcolorcode = '/'.$temp[$k];
                          break;
                      }
                  }
                  //Get colorant charge
	                for ($row = 0; $row < count($colorants); $row++)
	                {
                  	if ($colorants[$row][2]==$mcolorcode)
		                {
              			   $colorprice = $colorants[$row][1];
		                }
            	    }

              		$beat5price = $batchesfives*2*$beatprice;	//Beating add-on is 2x for batches of 5
		              $beat1price = $batchesones*1*$beatprice;

		              $additivesprice = ($sizeprice+$calciumprice+$cmcprice)*$pails; //additives price
		              $clayprice =	$batchesfives*5*$clayprice; // Clay added only to groups of 5

		              if ($mcolorcode != '')
              			$coloramt = $pails*$colorprice;
              		else
               			$coloramt = 0.00;
               			
		              $totalamount = 0.00+$ext5price+$ext1price+$beat5price+$beat1price+$additivesprice+$clayprice+$coloramt;
		              $avgprice = $totalamount/$pails;

                  $pulpitem['price'] = $avgprice;
                  $pulpitem['totalamount'] = $totalamount;
                  
                  echo $pails*$pulpitem['price'].'  v.'.$pails*$avgprice;
                  br();
                  echo ' ext5='."$ext5price.' ext1='.$ext1price.' b5='.$beat5price.' b1='.$beat1price.' adds='.$additivesprice.' clay='.$clayprice.' color='.$coloramt";

                  //Insert into pulpcart
		              $pulpcart[] = $pulpitem;

                  
              }
            } // if item3

              // save pulpcart into SESSION var
		          $xcart = $pulpcart;
		          $_SESSION["pulpcart"] = $xcart;
		          //Calculate pulp items... and show in header on each pass through
          		$shpcount = 0;
          		$count = 0;
          		$val = 0.00;
        		  reset($pulpcart);
              
        	  foreach ($pulpcart as  $key  => $row)
              {
        	 	    if ($pulpcart[$key]['shpqty']>0)
        	 		  {
          	 		$shpcount = $shpcount + $pulpcart[$key]['shpqty'];
        	  		   $count= $count + 1;
        	   		$val= $val + $pulpcart[$key]['totalamount'];
        		  	 }
        		  }
        		  //Store into session_vars

          		$_SESSION["pulp_shpitems"]= $shpcount;
          		$_SESSION["pulp_items"]= $count;
          		$_SESSION["pulp_price"]= $val;

        if ($item4)   //Paper
        {
        
            // 3/7/2012 Not recalc right if surface preped
            // ZWHWC0600802NLC|White Watercolor:  6x8 fthr A/CP|3.50|/B|0.18|2|7.36<br>
 /*
 ZDELP2203005NCC|Delphi:  22x30 laid HT/CP|9.45|/B|0.47|2|19.84<br>
 ZCREC2203001NCC|Cream/Callig:  22x30 HT/CP|9.45|/B|0.47|2|19.84<br>
 ZYALE2203005NCC|Yale: 22x30 laid HT/CP|9.45|/B|0.47|2|19.84<br>
 ZDOUB2203001NJC|Double XX:  22x30 HT/CP GEL|12.85|/CP|0|1|12.85<br>
 ZTAUP2203001NCC|Taupe:  22x30 HT/CP|9.45|/CP|0|1|9.45<br>
 --
 ZDELP2203005NCC|Delphi:  22x30 laid HT/CP|9.45|/B|0|2|18.9<br>
 ZCREC2203001NCC|Cream/Callig:  22x30 HT/CP|9.45|/B|0|2|18.9<br>
 ZYALE2203005NCC|Yale: 22x30 laid HT/CP|9.45|/B|0|2|18.9<br>
 ZDOUB2203001NJC|Double XX:  22x30 HT/CP GEL|12.85|/CP|0|1|12.85<br>
 ZTAUP2203001NCC|Taupe:  22x30 HT/CP|9.45|/CP|0|1|9.45<br>
 */           
            // Add the items the cart
            $list = explode('<br>',$item4);
            $nrows = count($list)-1;
            for ($j = 0; $j < $nrows; $j++)
            {
               $thisitem[$j] = explode('|',$list[$j]);
               
       		     $itemno = $thisitem[$j][0];
               $descrip = $thisitem[$j][1];
               $price = $thisitem[$j][2];
						   $surface = $thisitem[$j][3];
               $surfaceprice = $thisitem[$j][4];
               $qty = $thisitem[$j][5];
               $totalamount = $thisitem[$j][6];

               // Hot Process has an add_on of 10%  /HP;/B;hp
               $add_on = 0.00;
               switch ($surface)
               {
                case '/HP':
                  $add_on = 0.10;      // 10% add-on
                  break;
                case '/B':
                  $add_on = 0.05;      // Half price - do two at a time
                  break;
                case 'hp':
                  $add_on = 0.10;
                  break;
                default:
                  $add_on = 0.00;
                  break;
               }

               //Lookup the price again in the event the invt price has changed
               $thisitem = get_one_item($itemno);
               $price =  $thisitem[0]['price'];
               
               // rev. 7/31/13 Prevent adding paper if low or out of stock
               $onhand =  $thisitem[0]['onhand'] - $thisitem[0]['aloc'];

               if ($onhand <= 0) {
                    $qty = 0;
                    $pmsg =  $itemno.' is out of stock';
               } else {
                    if ($onhand < $qty) {
                        $qty = $onhand;
                        $pmsg =  $itemno.' only has '. number_format($qty).' items available.';
                    } else {
                        $qty = $qty;
                        $pmsg = '';
                    }                    
                                   
               } // rev. 7/31/13
               
               $surfaceprice=round($add_on*$price,2);
               $totalamount =$qty*($price+$surfaceprice);

               // Create this paper line item  -- Keep Price and surfaceprice separate so recalcpaper.php can work well
               // display_tr_papercart() shows sum as the Price
    	         $paperitem = array("item"=>$itemno,"description"=>$descrip,"price"=>$price,
						          "surface"=>$surface,"surfaceprice"=>$surfaceprice,"qty"=>$qty,
						          "totalamount"=>$totalamount);
                // rev. 7/31/13 show msg and if have onhand add partial or full qty                                  
                if (!empty($pmsg))
                    js_alert($pmsg);
                
                if ($qty > 0) {
                    //Insert this line item into papercart
		            $papercart[] = $paperitem;
                }
            }  // end for loop (and 7/31/13 rev.)

          // save papercart into SESSION var
    		  $_SESSION["papercart"] = $papercart;

    		  //Calculate paper items...
       		$count = 0;
      		$val = 0.00;
    		  reset($papercart);
        	
    		foreach ($papercart as  $key  => $row)
              {
    	 	  if ($papercart[$key]['qty']>0)
    	 		  {
      	 		$count = $count + $papercart[$key]['qty'];
    	   		$val= $val + $papercart[$key]['totalamount'];
    		  	}
    		  }
    		  //Store into session_vars

      		$_SESSION["paper_items"] = $count;
      		$_SESSION["paper_price"] = $val;

        } //if item1

           header("location:showcart.php");
           exit;
      }
      else
        $message=$webid.'  '.$sono.' '.$order_token;
         
      //$message = "Unable to add items to cart for order number $sono";
      break;
  }  //switch

	header("location:showprod.php?dept=MyMenu&grp=Shopping+Functions&prod=MyOrderMemory&msg=$message");
	exit;

?>
