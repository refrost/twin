<?php
// mjbounce.php March 10, 2016 -- List IPI's bounce & blocked emails via Mailjet's API
// 03/28/2016 added $_GET switch for IPI/Twin
// 05/02/2016 revised for set creds by servername (for twin..)
######## FUNCTIONS #########
function plural($number,$word,$ending='s') {
  // Q&D returns singular or plural string
  if ($number != 1) {
    $word = $word.$ending;
  }
  return number_format($number)." $word";
}
########## CODE ###################

// Set weeks to review  (1 week or from form 1-6)
if(isset($_POST['weeksbefore'])){
  $weeksbefore = $_POST['weeksbefore'];
} else {
  $weeksbefore = 1;
}

// Set MJ creds (IPI or Twin)
if (!$_GET && stripos($_SERVER['SERVER_NAME'],'industrialplatinginc') !== false) {
  //IPI  Mailjet Creds and access to Mailjet API -- default (no $_GET).
  $apikey = 'f858a413bc711b51cf3dd34e74b75fb9';
  $apisecret = 'da69565d76582b71091ce6ad79c4d820';
  $domain = 'industrialplatinginc.com';
  $company = 'IPI';
}
else {
  //Twinrocker  Mailjet Creds and access to Mailjet API
  $apikey = '58400a108ed12fd92150f5b990747e6d';
  $apisecret = 'e0ad37741414c0f0425be08cf5759167';
  $domain = 'twinrockerhandmadepaper.com';
  $company = 'Twin';
}

// Create the mj instance
require 'mailjet/vendor/autoload.php';// path in bounce app
use \Mailjet\Resources;
$mj = new \Mailjet\Client($apikey, $apisecret);

$since = date('Y-m-d',strtotime("-$weeksbefore week"));
echo "<h2>$company/Mailjet Bounced and Blocked Emails</h2>";

$bouncefilters = ['FromDomain' =>     $domain,
  'AllMessages' => 1,
  'FromTS' => $since,
  'ShowExtraData' => 0,
  'MessageStatus' => 'bounce',
  'Style' => "full"
];

$blockedfilters = ['FromDomain' => $domain,
  'AllMessages' => 1,
  'FromTS' => $since,
  'MessageStatus' => 'blocked',
  'Style' => "full"
];

// Get the responses
$bounceresponse = $mj->get(Resources::$Messagesentstatistics, ['filters' => $bouncefilters]);
$blockedresponse = $mj->get(Resources::$Messagesentstatistics, ['filters' => $blockedfilters]);

//  Read the responses, merge, sort and then list
if ($blockedresponse->success()  && $bounceresponse->success()){
    $list = array_merge($bounceresponse->getData(),$blockedresponse->getData());

  // Sort results on sender asc and also date desc
  $sort = array();
  foreach ($list as $key => $row)
  {
    $sort['user'][$key] = $row["Campaign"]["FromEmail"]["Email"];
    $sort['date'][$key] = $row["ArrivalTs"];
  }
  array_multisort($sort['user'], SORT_ASC, $sort['date'], SORT_DESC, $list);

  //echo "<pre>"; print_r($list); echo "</pre>";

  // Display sorted results
  $cnt = count($list);
  if ($cnt > 0) {

    echo "Listing... ".plural($cnt,'event')." in the last ".plural($weeksbefore,'week').".<br />";
    $currentuser = '';
    foreach ($list as $key => $row) {
      $thisuser = $row["Campaign"]["FromEmail"]["Email"];
      if ($currentuser != $thisuser) {
        $currentuser = $thisuser;
        // Normalize Name for all Shipping users (not as 'Pam Baker')
        if (stripos($thisuser,'shipping')!==false) {
          $usernameonly = 'Shipping';
        } else {
          $usernameonly = substr($thisuser,0,strpos($thisuser,'@'));
        }
        echo '<br />'; // blank line to separate senders
      }
      echo 'From: '.$usernameonly.'  --'. $row["Status"].':  '. $row["ToEmail"]. ' :: ' .$row["Campaign"]["Subject"].' -'.date('M d Y  H:i:s ',strtotime($row["ArrivalTs"])); //.' ' . date(DATE_RFC1123,strtotime($row["ArrivalTs"]));
    echo '<br />';
    }
  } else {
    echo 'No events in this period...';
  }
}
else {
  echo '<br />Failed in getting data:';
  //var_dump($response->getStatus());
}
// Form to enter the number of weeks back to review:
?>
<br />
<form method="POST" target="_self">
  <fieldset>
    <legend>Number of weeks to consider (1-6):</legend>
    <input autofocus type="number" min="1" max="6" step="1" name="weeksbefore" value="<?php echo $weeksbefore; ?>" style="width: 50px;font-size: .7em" /><br />
    <button style="color: #FFF; background-color: #900;   font-weight: bold;" type="submit" >Submit</button>
  </fieldset>
</form>
