<?php
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 


  include ('book_sc_fns.php');
  // add_stat_2_cart.php: 12/21/03 ref&a (converted from add_codes.php. Data entry form to allow user
  // to add (1) items codes and qty (with lookup), and (2) notes/comments lines
  // into the order. 10/17/02
  session_start();

    $anchor = $_POST['anchor'];
    $style = $_POST['stylecode'];
    $price = $_POST['price'];
    $color = $_POST['stcolor'];
    $qty = $_POST['qty'];
    
    // Pass qty_onhand and abort if qty > onhand  - qtyoncart
    $invt['whap']=$_POST['invt1'];  
    $invt['whsc']=$_POST['invt2'];
    $invt['crec']=$_POST['invt3'];
    $invt['simo']=$_POST['invt4'];
    
    //Define url to return to
    $url = parse_url($_SERVER["HTTP_REFERER"]);
    $script = strrchr($url['path'],'/');
    $script = strtolower(substr($script,1));
    $qry =  $url['query'];
                
    // Check inventory qty  01/20/2007 -ref-
    $thiscolor = strtolower($color);
    // 07/25/2015 Take into consideration this item qty already on cart
    
    $cartqty = statqtyoncart($style);
 
    $itemno = 'Y'.strtoupper($color).$style;  //Format of stationery itemno   
    if ($qty <= ($invt[$thiscolor] - $cartqty[$thiscolor]) ) 
    {
        // The is enough onhand -- add to cart..
    
        // Work with temporary cart...
    	$xcart = $_SESSION["cart"];
    
    	//  add qty to cart..
    	for ($i=0; $i<$qty; $i++)
    	{
    		// Pass the itemno. If item is in array, increment qty,
    		// else add item and qty=1 to array.
    		if(@$xcart[$itemno])
       			$xcart[$itemno]++;
    		else
       			$xcart[$itemno] = 1;
                
    	}
     
        // Now update sessions
    	
    	//calculate_cart_totals($cart);
    	$_SESSION['total_price'] = calculate_price($xcart);  
      $_SESSION['total_weight'] = calculate_weight($xcart);
      $_SESSION['items'] = calculate_items($xcart);
      $_SESSION['cart'] = $xcart;
          
    } else {
  
    // Not enough qty onhand for this item so, issue message, then return to st order form

    js_alert_back("Note: Quantity ordered exceeds onhand qty minus quantity already in cart. Please call the office to special order. [Item: $itemno]");
    exit;
  } //Endif    	

  //return to calling stationery script
  header("location:$script?$qry#$anchor");
  exit;


?>
