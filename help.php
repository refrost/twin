<?php
  include ('book_sc_fns.php');
  session_start();

  //$show_help_menu = true;   //8/2/03 move help display to come from mysql file so can be edited online..
  include('tr_header.php');

  echo "<table class=help border=1 align=$g_table1_align width=$g_table1_width>
		  <tr><td><p class=product>Help using this site...</td></tr><tr height=200><td class=descrip>";

	//help.php
	?>

	<!-- <div align=right><a href=mailto:ralph@refrost.com?subject=Twinrocker_questions>More questions?</a> </div>
	-->
	  <br>
	<h4>NAVIGATION:</h4>
	<ul>
	<li><b>Sections:</b> This ecommerce site is split into three main sections: handmade paper, paper-making supplies, 
         and invitations and cards. These sections are accessible anytime via the links on the left side 
         of the page header.  
	</li><br>
	
	<li><b>Product Groups:</b> Most sections have multiple groups of products. These product groups 
         are accessed by clicking on one of the options on the horizontal menubar above.  
         Bookarts, pulp, fiber, books, kits, etc., can be found in the "Supplies" section.
	</li><br>
	
	<li><b>Subgroups:</b> Several product groups are futher split into subgroups.  
         For example, Bookarts, Additives-Colorants, and other groups in supplies are typically 
         split into a few subgroups.  You can select a subgroup by clicking on an option on the 
         Subgroup horizontal menubar.
  </li><br>
	
	<li><b>Products:</b>  Products within a subgroup can be accessed by clicking a menu link on the 
	       left side of the product display page, or by clicking "Next" in the upper right of 
         each product description.  In some cases, scrolling in required to see all the items in a display.   
	</li><br>
	
	</ul>
	
	
  <br>
	<h4>TIME-SAVING FEATURES:</h4>
	<ul>
	<li><b>Click Price to Add-to-Cart:</b> In most cases, to  add an item to your  cart, simply click on the price.
      Once you click on a price, there will be a brief delay as the page reloads to show the updated item count and dollar amount
      in the header of the page. </li><br>
  <li><b>Item and Price data arranged in compact tables:</b> Formatting price as a link allows similar items
      to  be arranged in clear, compact tables rather than in long scrolling lists.
      (See, <a href=showprod.php?dept=Additives-Colorants&grp=Pigments>Pigments</a> for an example.)  </li><br>
  <li><b>Pulp Order Form:</b> Most pulp orders, including mixtures, can be ordered on-line from the <a href=tr_pulp.php>Pulp Order Form</a>.
         Note that pulp quantities come in units of single pails (3 pounds of fiber), or in units of 15 pounds of fiber
         (five pails of beaten pulp dewatered and shipped in four pails). <a href=showprod.php?dept=Pulp&grp=Twinrocker+Pulp&prod=Pulp+Prices#pulpinfo>More info on 5-in-4 pulp units.</a></li><br>
  <li><b>Fiber Order Form:</b> Fiber orders, from a few pounds to bale quantities, can easily be ordered on-line from the <a href=tr_fiber.php>Fiber Order Form</a>.
           (Both fiber and pulp can be found in the Papermaking "Supplies" Section.)</li><br>
  <li><b>Paper Selector:</b> Paper can be ordered on-line via the <a href=pa_main.php>Paper Selector</a>.
           (Paper can be selected by name, or by specifying usage, color, grey-scale or weight.)  </li><br>

	<li><b>MyOrderMemory:</b> When you  'Commit' your order, you can add a descriptive project, job,  or class name, and/or a purchase order number to identify  the order. Later,
         you can recall the entire order, no matter how long or complex, with <u>three</u> mouse clicks (after logging in).
         Access this feature via the <b>MyMenu</b> selection on the top menu list. </li><br>
	<li><b>
  SmartSearch:</b> In this web application, you can search for multiple items at once
       by separating search terms with semicolon '<b> ;</b> '.  You  can narrow the search for an
      item by adding qualifying text strings separated by commas '<b> ,</b>'.
      <br><br>
      You can also choose to search for a list of links in 'All Supplies' or, for a more specific list of links containing the search phrase in the inventory description field -- 'Item Name Only'.
      Searching in All Supplies gives the item(s) and related items whereas searching in Item Names is more specific -- good when you KNOW the search phrase is in the item description.
      <br><br>
      As an example, consider you  want to order  red pigment, 15 inch  polyethylene sleeves, and tweezers.
      Type or copy and paste: <b>pig,red; tweez ; sleeve;slv</b>
      into the Search Box and see what happens.  Imagine hearing and entering the word fragments that identify the product(s) you are looking for.</li><br>
  <li><b>Item Code Speed Dial:</b> If you want to order items off a prior <i>paper sales order</i> that you happen to have in hand,  just enter the item codes into <a href=addcodes.php>Speed Dial</a> (Accessible via MyMenu then Enter Codes).
        (It works for everything but pulp and fiber.)</li><br>


	</ul>
	<br>
	<h4>WORK FLOW:</h4>
	<ol>
	<li> <b>'Log in'</b> if you plan to complete an order, access MyOrderMemory, or edit your address information (Profile). If you are new to this site,
       feel free to look through the different product  groups and subgroups and add items to your cart as you go along. You can log in or  register any time during your session.</li>

  <li>Find and add the items you want to your order. (<b>Click price</b> to add a unit to the cart.)<br>
	    The  "Item Count" and "Total Amount" values change in the header as items are added.</li>
	<li>Click <b>'View Cart'</b> to view or <u>adjust quantities</u> on your order.</li>
	<li>On the 'Cart' page, click <b>'Confirm Shipping'</b>, to view all order info. <br>You can add or change Ship-to address if needed.</li>
	<li>On the 'Confirm Shipping' page,  click <b>'Enter Payment Info'</b> when ready to complete your order. You can return to shopping from the payment page to add other items to your cart. </li>
	<li>Credit card information is securely transmitted and is not stored on-line. People who wish may also
      mark their order payment method  as "Phone in"  and supply their billing information by phone or mail, after submitting the order detail on-line.</li>
	<li>To start a new order anytime, click, <b>'Clear Cart'</b> and begin again.</li>
	<li>For improved security, remember to click <b>'Logout'</b> when you  are leaving this done.</li>

	</ol>
	<br />
  
	<h4>CHANGE SHIP-TO ADDRESS:</h4>
	<p>On the 'Set Shipping' page, click
	on the 'Change Ship-to Address' hypertext link to ADD,DELETE, or SELECT a ship-to
	address, OR to make the ship-to address for the current order the same as the
	bill-to address. New ship-to addresses always start with data from the bill-to address
	so it	is quick and easy to add a new address and then delete an old one.
  (Click the left link if you wish to access your "Profile" to change your Bill-to address.)</p>
	
	<h4>Suggestions or technical questions?</h4>
	<p >Please email them to <a href=mailto:info@twinrockerhandmadepaper.com?subject=SupportRequest>Twinrocker Support</a>. </p>
	<p class=tiny>
  </p>

  </td></tr></table>
  <?php silk_footer($g_table1_align,$g_table1_width);
  ?>
