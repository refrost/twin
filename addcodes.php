<?php
//php5.3
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

  include ('book_sc_fns.php');
  // addcodes.php:  (converted from add_codes.php. Data entry form to allow user
  // to add (1) items codes and qty (with lookup), and (2) notes/comments lines
  // into the order. 10/17/02
  session_start();
      
  //HEADER
  if (!isset($_SESSION['searchstr']))
   		include('set_vars.php');

  include('tr_header.php');
  echo "<table   width=$g_table1_width align=center border=1 ><tr>
        <td class=addcodes>";
  //

  if (isset($_POST["checkit"])) //Step 2. Data was entered and this adds VALID items to cart
  {
	//echo "<br><center><h3>Item code - Verify</h3>";
	//echo "<hr class=70>";
  	//validate & display status
	$items = $_POST['itemno'];
	$qty = $_POST['qty'];
	
	$xcart = $_SESSION["cart"];
	$codemsg = '';
	for ($n=0; $n<count($items); $n++)
	{
   		if (strlen(trim($items[$n])) > 0)
		{
			//validate, is it a real item?
			$items[$n] =strtoupper($items[$n]);
			$detail = get_invt_details($items[$n]);
			if ($detail)
			{
				$descrip[$n] = $detail['descrip'];
				$price[$n] = $detail['price'];
				$cat = $detail['catid'];                
                $firstchar = left($detail['item'],1);  // added  left 7/31/13
				
				//exclude fiber and pulp...     AND Paper 7/31/13
				if ($cat == 'PU')
				{
					$descrip[$n] = "** Select Pulp from form **";
					$price[$n] = '0.00';	
					$qty[$n] = 0;			
				}

				if ($cat == 'FI')
				{
					$descrip[$n] = "** Select Fiber from form **";
					$price[$n] = '0.00';	
					$qty[$n] = 0;
				}
				if ($firstchar == 'Z')
				{
					$descrip[$n] = "** Select Paper from forms **";
					$price[$n] = '0.00';	
					$qty[$n] = 0;
				}	
                	

			}
			else
			{
				//not valid code...
				$descrip[$n] = "** Invalid Item Code **";
				$price[$n] = '0.00';	
				$qty[$n] = 0;				
			}
			
			if ($qty[$n] <= 0)
				$codemsg .= $items[$n].'   '.$descrip[$n].'<br>';
			else
			{
				//  add qty to cart..
				for ($i=0; $i<$qty[$n]; $i++)
				{
					// Pass the itemno. If item is in array, increment qty, 
					// else add item and qty=1 to array.
					$itemno = $items[$n];
    				if(@$xcart[$itemno])
      					$xcart[$itemno]++;
    				else 
      					$xcart[$itemno] = 1;
				}
					
			} //endif adding this item to cart
		} //endif itemcode not blank
				
	  } //endfor
	  if ($codemsg <> '')
	  	$codemsg = '**** NOT PROCESSED ****<br>'.$codemsg.'<br>***********************';

	  //header("location:add_codes.php?additems=Add2cart&codemsg=$codemsg&itemno=$items&qty=$qty");
	  //exit;
	//save cart...
		
	$cart = $xcart;
	$_SESSION['total_price'] = calculate_price($cart);  
    $_SESSION['total_weight'] = calculate_weight($cart);
    $_SESSION['items'] = calculate_items($cart);
	
	$_SESSION['cart'] = $cart;
	
	//Echo 'Items added to cart';
	header("location:addcodes.php?done=1&msg=".$codemsg);
	exit;
			
  }  //endif
	
 
 // Display entry form 
 
 if (!isset($_GET["additems"]) && !isset($_POST["checkit"])) 	//Step 1. Display the entry form.
  {
  	if (isset($_GET["done"]))  //also show items that didn't process the prior time...
	{
		$msg   = $_GET['msg'];
		echo $msg;
	}

		
	display_space_head('Know your item codes from a prior order?');
	  
	//echo "<br><a href=fiber.php?catid=home&clearfiber=N>Click here to enter Fiber items</a>";
	//echo "<br><a href=pulp.php>Click here to enter Pulp items</a>";
	
  echo "<form method=post action=addcodes.php><table>";
	echo "<b>--Speed Dial--</b><br>Enter item codes and quantities below:<br>
        (excludes Pulp, paper and fiber):";
	for ($n=0; $n<5; $n++)
	{
	    $items[$n] = '';
		$qty[$n]   = 1;
   		echo "<tr>
			 <td><input type=text name='itemno[]' value='".$items[$n]."' size=15>
			 </td><td><input type=text name='qty[]' value='".$qty[$n]."' size=4>
			 </tr>";
	}
	echo "<tr><td colspan=5 align=center><input class=addcodes type=submit name=checkit value='Add to Cart'></td></tr>";
	echo '</form></table>';
	br(2);

	//		echo  $_SESSION["SESSION"];


	echo '</td></tr></table>';  //Close tr_header.php table..

	//echo "<a href=index.php>Done</a>";
  }
  
silk_footer($g_table1_align,$g_table1_width);
?>
