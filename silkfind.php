<?php
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

  include ('book_sc_fns.php');
  // silkfind.php  6/23/03 Search products and show list
  session_start();
	// get the search string...
	

	if (isset($_GET['searchstr']))
		$xsearchstr = $_GET['searchstr'];
	else
		$xsearchstr = '';
	
   $searchstr = $xsearchstr;	  //6/2010 ~fix on dev box
   $_SESSION['searchstr'] = $xsearchstr;

  // Can either search in invt and get a list of items
  // or can search in descrip1/2 and keywords fields in products.
  // If set to 'products'  this forces a different display...
	if (isset($_GET['category']))
		$xcategory = $_GET['category'];
	else
		$xcategory = 'invt';


	// get whether to search just category or the entire site	
	if (isset($_GET['searchsite']))
		$searchsite = 'on';
	else
		$searchsite = '';
		
	if (isset($_GET['catid']))
		$catid = $_GET['catid'];
	else
		$catid = '';
	if (isset($_GET['calledfrom']))
		$calledfrom = $_GET['calledfrom'];
	else
		$calledfrom = "showprod.php";

		
	include ('tr_header.php');		// This is Web version of cart 	
 		
	//echo $calledfrom.'  '.$catid.'  '.$searchsite.'  '.$searchstr;
    //exit;
	// Check search terms and formulate query 
	if (!$searchstr)
  	{
     	pass_msg("<br> <a href=$calledfrom>You have not entered search details.  Please go back and try again.</a>");
     	//exit;
  	}
  
  
  Switch ($xcategory)
  {
  
  case 'invt':
  
      //Advanced query 11/4/02 --  separate search pairs by ';'  (1-2 hrs)
    	//and build  a list of disparate items. Acts like 'OR'.
    	
      //First we build the 'where' clause ($mwhere)
      //then later run the query and then display the results in a table...

    	//remove ,; at end of searchstring
    	while (right($searchstr,1)==';' || right($searchstr,1)==',')
    	{
    		$searchstr = substr($searchstr,0,strlen($searchstr)-1);
    	}

      $advancedqueryitems = count(explode(";",$searchstr));

      if ($advancedqueryitems==1)
      {
      		//Single item (Original query)
    	    if (strpos($searchstr,",")>0)
    		{
    			$str = explode(",",$searchstr,2);
    			$mwhere = " (descrip LIKE '%".addslashes(trim($str[0]))."%') AND (descrip LIKE '%".addslashes(trim($str[1]))."%') ";
    		}
    		else
    			$mwhere = " (descrip LIKE '%".addslashes(trim($searchstr))."%') ";
     	}
    	else
    	{
    		//Have a string like    'red,(4fl;blue,(64 ;apron;hake,bible'
    		//need to explode it on the ';' and then build the filt OR filt OR filt where clause
    		$advancequerystr = explode(';',$searchstr);
    		$mwhere = '';
    		for ($j=0 ; $j < $advancedqueryitems ; $j++)
    		{
    	  	if (strpos($advancequerystr[$j],",")>0)
    			{
            //Have more than one comma -- search qualifiers
    				$str = explode(",",$advancequerystr[$j]);
    				$qualifiers = count($str);
    				//Get the first qualifier
    				$mwhere .= " ( descrip LIKE '%".addslashes(trim($str[0]))."%'";
    				//Now get the rest..
    				for ($m=1 ; $m < $qualifiers ; $m++)
    				{
    				  $mwhere .= "  AND descrip LIKE '%".addslashes(trim($str[$m]))."%' ";
            }
            $mwhere .=" )";  // Close the query
          }
    			else
    				$mwhere .= " (descrip LIKE '%".addslashes(trim($advancequerystr[$j]))."%') ";

    			if ($j < $advancedqueryitems-1)
    				$mwhere .= " OR ";
    		}
    	}

    	//echo $mwhere;
    	//exit;

     	//Search either in category or in the entire item list..
     	if ($searchsite)
    	{
    		$query = "select DISTINCTROW *  from invt where ".$mwhere; //OR ITEM LIKE  '".$searchstr."%'";
    		$msg = "Items containing '".$searchstr."' in the item description in the entire site";
    	}
    	else
    	{
    		// 11/12/02 added feature to get custom search string from categories
    		if (left($catid,1)=='_')
    		{
    			$whereclause = get_category_whereclause($catid);
    			$query = "select DISTINCTROW *  from invt where (".$whereclause.") AND  (".$mwhere.")";
    			$msg = "Items containing '".$whereclause."' and '".$searchstr."' in the item description in this category";

    		}
    		else
    		{
    			if ($catid == '' || $catid=='home')
    			{
    				$query = "select DISTINCTROW *  from invt where ($mwhere)";
    				$msg = "Items containing '".$searchstr."' in the item description in this category";
    			}
    			else
    			{
    				$query = "select DISTINCTROW *  from invt where (catid = '".$catid."') AND ($mwhere)";
    				$msg = "Items containing '".$searchstr."' in the item description in this category";
    			}
    		}
    	}

    	//br();
    	//echo $query;
    	//echo '<br>'.$searchsite;

     	//Do the query...
     	$conn = db_connect();
     	$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " . ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
     	$num_rows = mysqli_num_rows($result);

    	if  ($searchsite)
    		$searchsite = 'on';
    	else
    		$searchsite = '';

    	echo "<table class=text9px bgcolor=white align=center width=100%><tr><td>";
    	if ($num_rows > 0)
    	{
    		display_space_head("Search results:",'<hr class=50>');
    		echo "$num_rows $msg<br>";
    		do_html_URL_no_br($calledfrom, 'Back');
    		// 9/26/2016 edited display_invt() to filter out Onhand<1 ( not cat=CH)
            display_invt(db_result_to_array($result),$searchstr.'&searchsite='.$searchsite.'&gotouri='.urlencode("silkfind.php?searchstr=$searchstr"));

    		//echo "<hr class=50>";
    		do_html_URL_no_br($calledfrom, 'Back');

    	}

    	else
    	{
    		br(2);
    		do_html_URL_no_br($calledfrom, 'No results found. Return to prior page.');
    	}

    	//
    	//do_html_footer();
    	echo '</td></tr></table>';
    	
    	// BREAK;   //10/10/17 list both...


  case 'products':
  
      //Query into products 11/13/03  Advanced

      //First we build the 'where' clause ($mwhere)
      //then later run the query and then display the results in a table...
      
      // 1/7/2016 Exclude not displayed products.
      $mwhere = " (display = 'y') AND ";
      // see .= below  also, clause added again below
      
    	//remove ,; at end of searchstring
    	while (right($searchstr,1)==';' || right($searchstr,1)==',')
    	{
    		$searchstr = substr($searchstr,0,strlen($searchstr)-1);
    	}

      $advancedqueryitems = count(explode(";",$searchstr));

      if ($advancedqueryitems==1)      // This one search in products...
     	{
      	//Single item (Original query)
    	  if (strpos($searchstr,",")>0)
    		{
            $str = explode(",",$searchstr);
      			$mwhere .= " (name LIKE '%".addslashes(trim($str[0]))."%' OR "
                         ."descrip1 LIKE '%".addslashes(trim($str[0]))."%' OR "
                         ."descrip2 LIKE '%".addslashes(trim($str[0]))."%' OR "
                         ."keywords LIKE '%".addslashes(trim($str[0]))."%' OR "
                         ."comments LIKE '%".addslashes(trim($str[0]))."%') "
                      ."AND ( name LIKE '%".addslashes(trim($str[1]))."%' OR "
                      ."descrip1 LIKE '%".addslashes(trim($str[1]))."%' OR "
                      ."descrip2 LIKE '%".addslashes(trim($str[1]))."%' OR "
                      ."keywords LIKE '%".addslashes(trim($str[1]))."%' OR "
                      ."comments LIKE '%".addslashes(trim($str[0]))."%') ";
    		}
    		else
               $mwhere .= " ( name LIKE '%".addslashes(trim($searchstr))."%' OR "
                        ."descrip1 LIKE '%".addslashes(trim($searchstr))."%' OR descrip2 LIKE '%".addslashes(trim($searchstr))."%' OR keywords LIKE '%".addslashes(trim($searchstr))."%' OR comments LIKE '%".addslashes(trim($searchstr))."%') ";
           //echo $mwhere;

        }
    	else
    	{
    		//Have a string like    'red,(4fl;blue,(64 ;apron;hake,bible'
    		//need to explode it on the ';' and then build the filt OR filt OR filt where clause
    		$advancequerystr = explode(';',$searchstr);
    		//$mwhere = '';
    		for ($j=0 ; $j < $advancedqueryitems ; $j++)
    		{
    	  	   if (strpos($advancequerystr[$j],",")>0)
    		   {
                    //Have more than one comma -- search qualifiers
    				$str = explode(",",$advancequerystr[$j]);
    				$qualifiers = count($str);
    				//Get the first qualifier

    				$mwhere .= " ( (descrip1 LIKE '%".addslashes(trim($str[0]))."%' OR descrip2 LIKE '%".addslashes(trim($str[0]))."%' OR keywords LIKE '%".addslashes(trim($str[0]))."%'
                          OR comments LIKE '%".addslashes(trim($str[0]))."%')";
    				//Now get the rest..
    				for ($m=1 ; $m < $qualifiers ; $m++)
    				{
    				  $mwhere .= "  AND (descrip1 LIKE '%".addslashes(trim($m))."%' OR descrip2 LIKE '%".addslashes(trim($m))."%' OR keywords LIKE '%".addslashes(trim($m))."%'
                            OR comments LIKE '%".addslashes(trim($str[0]))."%')  ";
                    }
                    $mwhere .=" )";  // Close the query
            
               }
    	       else
                   $mwhere .= "(descrip1 LIKE '%".addslashes(trim($advancequerystr[$j]))."%' OR descrip2 LIKE '%".addslashes(trim($advancequerystr[$j]))."%' OR keywords LIKE '%".addslashes(trim($advancequerystr[$j]))."%'
             OR comments LIKE '%".addslashes(trim($advancequerystr[$j]))."%')  ";


    	        if ($j < $advancedqueryitems-1)
    				$mwhere .= " OR  (display = 'y') AND ";
    		}
    	}

    	//echo $mwhere;
    	//exit;

		$query = "select DISTINCTROW `product`,`name`,`smallimg`,`dept`,`groupname`,`height`,`width` from products where ($mwhere)";
		$msg = "Products containing '".$searchstr."' in the description or keywords fields of the product file";

    	//br();
    	//echo $query;
    	//echo '<br>'.$msg;
        //exit;
      
     	//Do the query...
     	$conn = db_connect();
     	$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " . ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
     	$num_rows = mysqli_num_rows($result);

    	if  ($searchsite)
    		$searchsite = 'on';
    	else
    		$searchsite = '';

    	echo "<table  bgcolor=white align=center width=$g_table1_width><tr><td class=text9px>";
    	if ($num_rows > 0)
    	{
    		display_space_head("Search results:",'<hr class=50>');
    		echo "$num_rows $msg<br>";
    		do_html_URL_no_br($calledfrom, 'Back');

            //display_invt(db_result_to_array($result),$searchstr.'&searchsite='.$searchsite.'&gotouri='.urlencode("silkfind.php?searchstr=$searchstr"));
            display_products(db_result_to_array($result),$searchstr.'&searchsite='.$searchsite.'&gotouri='.urlencode("silkfind.php?searchstr=$searchstr"));

    		do_html_URL_no_br($calledfrom, 'Back');

    	}
    	else
    	{
    		br(2);
    		do_html_URL_no_br($calledfrom, 'No results found. Return to prior page.');
    	}

    	//
    	//do_html_footer();
    	echo '</td></tr></table>';

      BREAK;
      
  }  // endswitch
  
  // 1/28/2007 -ref- insert search info in searches table
  if (isset($_SESSION["SESSION_UNAME"]))
  {
    $usr = $_SESSION["SESSION_UNAME"];
    $usr = str_replace('&nbsp;',' ',$usr);
  }
  else
    $usr = "Guest";
    
  $query = "INSERT INTO searches VALUES (0,'$searchstr','$xcategory','$num_rows','$usr',NOW())";
  $conn = db_connect();
  $result = mysqli_query( $conn, $query) or die ("Error in query search insert query. ");
  //js_alert_back($query);
  
  silk_footer($g_table1_align,$g_table1_width);
?>
