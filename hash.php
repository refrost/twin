<?php 
// cm password_hash()
// use to manually create hash for cm admin user password. Copy into phpmyadmin
include ('book_sc_fns.php');
session_start();
if (!check_admin_user()) { //User must first run webapp and login as an admin.
    exit('Access Permission denied');
}

if (isset($_POST['pswd']) && strlen($_POST['pswd'])>5) {
    $hash = password_hash($_POST['pswd'], PASSWORD_DEFAULT);
    echo 'Hash: '.$hash.'  <br /> Length: '. strlen($hash);
    // password_verify($_POST['pswd'],$hash);
}
else {


?>
 <form action="hash.php" method="post">
  Password to hash (more than 6 char): <input type="text" name="pswd"><br>
  <br>
  <input type="submit" value="Submit" name="submit">
</form>

<?php 
}