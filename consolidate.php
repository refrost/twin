<?php
//consolidate.php 2/6/2011 -- called from merge_id.prg

//Completes DB changes to consolidate of  order and shipaddr records under one login/webid
// for customer records sharing same email
//$user = 'carynbopp';
//$email = 'shebopp80@yahoo.com';	

include ('book_sc_fns.php');
session_start();

if (!check_admin_user()) { //User must logged in as admin.
   exit('See system administrator.');
}

//Verify admin has submitted to consolidate the records.
if (isset($_POST['submit'])) {
   if ($_POST['submit'] == "Cancel" ) {
      exit('Consolidation cancelled.');
   }
} else 
   exit('No data to process.');  //Not called from merge_id.php form action

//Compete the conslidation : first retrieve the data...
$username = $_SESSION['mrg_user'];
$email = $_SESSION['mrg_email'];
$webid = $_SESSION['mrg_webid'];
$dup_webid = $_SESSION['mrg_dup_webid'];


//Create a single quoted list string to use in mysql query.
$lst = "'".implode("','",$dup_webid)."'"; 
/*
echo '<br />'.$lst.'<br/>';
$lst = serialize($dup_webid);
echo '<br />'.$lst.'<br/>';
echo json_encode($dup_webid);
*/

//Update the database...
$cnt = count($dup_webid);  // number of affected records

if (!($conn = db_connect()))  // Connect to store
   exit('Data connection failed');

//Copy all affected records to merged_* tables... 
$all_ids = $lst.",'$webid'";
$query = "INSERT INTO merged_customers SELECT * FROM customers WHERE webid IN ($all_ids)";
$result_1 = @mysqli_query( $conn, $query);
$r1 =mysqli_affected_rows($conn);
$query =  "INSERT INTO merged_orders SELECT * FROM orders WHERE webid IN ($all_ids)";
$result_2 = @mysqli_query( $conn, $query);
$r2 =mysqli_affected_rows($conn);
$query = "INSERT INTO merged_shipaddr SELECT * FROM shipaddr WHERE webid IN ($all_ids)";
$result_3 = @mysqli_query( $conn, $query);
$r3=mysqli_affected_rows($conn);

if (!$result_1 || !$result_2 || !$result_3)
   exit('Saving records to merged_tables failed.');
   
// Delete customer records, and reassign webid's in orders and shipaddr 
// of the duplicate records only
$query =  "DELETE FROM customers WHERE webid IN ($lst) LIMIT $cnt";
$result_1b = @mysqli_query( $conn, $query);
$r1b = mysqli_affected_rows($conn);
$query = "UPDATE orders SET webid = '".$webid."' WHERE webid IN ($lst) ";
$result_2b = @mysqli_query( $conn, $query);
$r2b = mysqli_affected_rows($conn);
$query = "UPDATE shipaddr SET webid = '".$webid."' WHERE webid IN ($lst) ";
$result_3b = @mysqli_query( $conn, $query);
$r3b = mysqli_affected_rows($conn);

if (!$result_1b || !$result_2b || !$result_3b)
   exit('Trouble updating tables.');

if (check_admin_user()) {
   echo '<br />Cust merged = '.$r1;
   echo '<br />Ords merged = '.$r2;
   echo '<br />Addr merged = '.$r3;
   echo '<br />Cust deleted    = '.$r1b;
   echo '<br />Ords reassigned = '.$r2b;
   echo '<br />Addr reassigned = '.$r3b;
}
echo '<br /><br /><br />Consolidation complete.  <a href="showprod.php">Continue</a>';

?>
