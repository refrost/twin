<?php
// 5.3
// Creates email confirmation of order  12/17/03
// This is a Q&D hack of tr_cart.php to create and email the order confirmation
ob_start();

if (isset($_GET['sono']))
  $sono = $_GET['sono'];
else
  $sono = '00000';
  
if ($_SERVER["SERVER_NAME"]=="127.0.0.1")
{
  	// working locally so don't do anything
}
else
{
    // 8/5/21 cmtted too many redirects
	//if ($_SERVER["SERVER_PORT"] != "443")
  	//	header("location:https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
}
  //include our function set
  include ('book_sc_fns.php');

  // The shopping cart needs sessions, so start one
  session_start();
  if (!isset($_SESSION['searchstr']))
  	include('set_vars.php');

  //echo session_id();
  $catid = ''; //$_GET["catid"];

  //Set a session var flagging that Final Review has be accessed
  // /12/13/03
  $_SESSION["final"] = true;
  
  //do_html_header("Checkout",$catid);
  //include('tr_header.php');
  echo "<table class=cart border=0 align=center width=$g_table1_width><tr>
  	 	<td >"";
  //echo '<center><b> Set Shipping -- Specify or change Ship-to Address and Shipping Method  -- Review Order Below</b></center><br>';


  //Get  the items from session vars:
  $cart = $_SESSION["cart"];
  $fibercart = $_SESSION["fibercart"];
  $pulpcart  = $_SESSION["pulpcart"];
  $shiparray['state'] = '=='; //initialize 10/30/02
  $bill_state = '==';
  $tax_id = '';


  if($cart&&array_count_values($cart) or count($fibercart)>0 or count($pulpcart)>0) //&&array_count_values($fibercart))
	{
		if (isset($_SESSION["SESSION_UACCT"]))
		{
			//display billto for the current customer..  based on webid
			$custarray = get_custarray($_SESSION["SESSION_UACCT"]); // get by webid
			$bill_state = $custarray['state'];
  		$tax_id = $custarray['taxexemptid'];
			$discount_type = $custarray['terr'];
			//if (strtolower($discount_type) == 'ss' && is)
      //    true;

       echo "Sale Order $sono Confirmation<br></td></tr><tr><td>";
       
      // The shipid is the record shipaddrid in shipaddr file.
      // This gets set in get_shiparray() below.
      //
			$shiparray['state'] = '=='; //initialize
 			if (isset($_SESSION["SESSION_SHIPID"]) && $_SESSION["SESSION_SHIPID"] <> '0')
			{

			  //echo $_SESSION["SESSION_SHIPID"];
				// If the shipid is set to -1 display the billto as the shipto..
				if ($_SESSION["SESSION_SHIPID"]== -1)
				{
					$shiparray = $custarray;
				}
				else
				{
				    //Otherwise go get the ship address then display it
					  $shiparray = get_shiparray($_SESSION["SESSION_SHIPID"]);
				}
			}
			else
			{
				$shiparray = $custarray;

					// fns action_fns.php:
					// -> get_shipaddresses($custid)   gets one of selects from list
					//    -> set_ship_id.php?s_addr=n  sets the session_shipid
					//       -> customer.php           displays shipto
			}

  /*
*/
			if (!isset($shiparray) || $shiparray==false)
				$shiparray = $custarray;

			# display header tables in a table
			echo '<table class=cart border=0><tr><td>';

			display_cust_info($custarray,$shiparray,false);  //$_SESSION["payarray"]


			echo '</td><td>';
				//echo 'Web Security -- Enter CC info on separate slip.  Print this form.';
				//if (isset($_SESSION["ship_state"])  && $_SESSION["ship_state"] != '==')
				//{

					calc_shipping($_SESSION["ship_state"],$_SESSION["ship_type"],$_SESSION["fibercart"],$_SESSION["cart"],$_SESSION["pulp_shpitems"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"],$_SESSION["ship_cod"]);

	  				$state_list = get_statelist();

          //get_ship_state($state_list,$_SESSION["ship_state"],$_SESSION["ship_type"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"],$_SESSION["ship_cod"],'tr_checkout');  //display state dropdown


				//display_payment_form($_SESSION["payarray"]);
			echo '</td></tr></table>';
		}
		if($cart&&array_count_values($cart))
				{

				display_tr_cart($cart, false, 0);
				}

		if (count($fibercart)>0)
				{
				display_tr_fibercart($fibercart,'none');
				}
		if (count($pulpcart)>0)
				{
				display_tr_pulpcart($pulpcart,'none');
				//silk2americart($pulpcart,"pulp"); //Sends local items to Americart
		}

	    //SHIPPING & TOTAL
		//recalc shipping when displaying checkout.
		//calc_shipping($_SESSION["ship_state"],$_SESSION["ship_type"],$_SESSION["fibercart"],$_SESSION["cart"],$_SESSION["pulp_shpitems"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"]);

		if (isset($_SESSION["ship_state"]) && $_SESSION["ship_state"]<>'==')
		{
			calc_shipping($_SESSION["ship_state"],$_SESSION["ship_type"],$_SESSION["fibercart"],$_SESSION["cart"],$_SESSION["pulp_shpitems"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"],$_SESSION["ship_cod"]);

			if ($_SESSION["ship_state"] <> $shiparray['state'] && $_SESSION["ship_special"]=='')
			{

				calc_shipping($shiparray['state'],$_SESSION["ship_type"],$_SESSION["fibercart"],$_SESSION["cart"],$_SESSION["pulp_shpitems"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"],$_SESSION["ship_cod"]);
			}

		}

			  echo "<table class=cart border=0 width=100%><tr>
			  	<td valign=top align=right>"; //<a href=showcart.php?catid=home#shipping>Change Shipping</a>";
			  if ($_SESSION["ship_string"])
	   				echo '<p class=text9px>'.$_SESSION["ship_string"].'</p>';
			  else
			  	echo "&nbsp;";
		echo "</td><td width=160 align=right>";

		//figure_sales_tax in display_order_totals
		// Need to get_order_totals(), then display.... see action_fns.php

    $thesetotals=get_order_totals($_SESSION["total_price"],$_SESSION["total_discount"],$_SESSION["fiber_price"],$_SESSION["ship_charge"],$_SESSION["pulp_price"],$bill_state,$tax_id,$discount_type);  // shows table _SESSION
  	display_order_totals($thesetotals);

    //display_order_totals($_SESSION["total_price"],$_SESSION["fiber_price"],$_SESSION["ship_charge"],$_SESSION["pulp_price"],$bill_state,$tax_id,$discount_type);  // shows table _SESSION
		echo "</td></tr></table>";
		//echo "<DIV align=right><p class=text9px>(Add a comment in final step.)</p></DIV>";


  }
  else
    echo "<p class=text9px>There are no items in your cart </p>";


  echo '</td></tr></table>';

//do_html_footer();
//small_menu();


$emailbody = ob_get_contents();
ob_end_clean();

$emailbody = str_replace('<br>','\n',$emailbody);
$emailbody = str_replace('</td>','\t',$emailbody);
$emailbody = str_replace('</tr>','\n',$emailbody);
$emailbody = strip_tags($emailbody);

echo ($emailbody) ;

?>
