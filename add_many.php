<?php

  include ('book_sc_fns.php');
  // Add_many.php 12/17/06 -ref-  Adds (qty of item) to cart. Called (initially) from silk_buy_form() in Decorative Paper 
  // Update the array; recalculate totals. 
  
  session_start();
  
  if (isset($_POST["gotouri"]))
	{
		$gotouri = urldecode($_POST["gotouri"]);
		echo $qotouri.'<br>';
	}
	else
		$gotouri = 'none';
	
	
  $qty = $_POST["qty"];
  if ($qty>0)
  {
      $itemno = $_POST["item"];
    	$xcart = $_SESSION["cart"];
    	
    	// Pass the itemno. If item is in array, increment qty, 
    	// else add item and qty=1 to array.
    	//  add qty to cart..
    	for ($i=0; $i<$qty; $i++)
    	{
    		// If itemno is in cart array, increment qty, 
    		// else add item and qty=1 to array.
   			if(@$xcart[$itemno])
    					$xcart[$itemno]++;
   			else 
    					$xcart[$itemno] = 1;
    	}
     
    	$cart = $xcart;
 	   $_SESSION['total_price'] = calculate_price($cart);  
      $_SESSION['total_weight'] = calculate_weight($cart);
      $_SESSION['items'] = calculate_items($cart);
	
	   $_SESSION['cart'] = $cart;
 
  }
	if ($gotouri != 'none')
	{
			header("location:".$gotouri);
			exit;
  }
	
	if ($gotouri == 'none')
	  	header("location:index.php");
		
	else
	{
		//return to calling script

    $url = parse_url($_SERVER["HTTP_REFERER"]);
    $script = strrchr($url['path'],'/');
    $script = strtolower(substr($script,1));
    $qry =  $url['query'];

    header("location:".$gotouri);
    exit;
	}
	

?>
