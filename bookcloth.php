<?php
//php5.3 9/27/14
include("book_sc_fns.php"); 
session_start();

// bookcloth.php       BC  L  BLA 360520   2/6/03
// Receives item prefix, nextfix (L/T linen or tweed), color code,item suffix to build item number
// (adapted from colored thread), and a qty, n.  
// Inserts item to cart n times..
	

	
	if (isset($_POST["sub"]))   // Must name the 'Add' button and send different vars
  {
    $item = $_POST["itemprefix"].$_POST["nextfix"].$_POST["color"].$_POST["itemsuffix"];
  	$qty = $_POST["qty"];
	  $returnto = urldecode($_POST["returnto"]);
  }
  else
  {
    $item = $_POST["itemprefix2"].$_POST["nextfix2"].$_POST["color2"].$_POST["itemsuffix2"];
  	$qty = $_POST["qty2"];
	  $returnto = urldecode($_POST["returnto2"]);
  }


  //echo $item;

	// add_qty_to_cart($itemno,$qty);
	// Adds qty of one item to the cart  1/6/03	
	if ($qty > 0)
	{	
		$xcart = $_SESSION["cart"];

		// Pass the itemno. If item is in array, increment qty, 
		// else add item and qty=1 to array.
		for ($i=0; $i < $qty; $i++)
		{
			if(@$xcart[$item])
      			$xcart[$item]++;
		    else 
		      $xcart[$item] = 1;
		}
    
 
		$cart = $xcart;
    	$_SESSION['total_price'] = calculate_price($cart);  
        $_SESSION['total_weight'] = calculate_weight($cart);
        $_SESSION['items'] = calculate_items($cart);
    	
    	$_SESSION['cart'] = $cart;
	}
	header("location:".$returnto);
	
?>
