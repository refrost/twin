<?php
// 5/6/2019 Added pdo_connect() (below)initially for password_hash()...
//    Also, added old_pswd() to implement/check mysql OLD_PASSWORD() fns in code

function db_connect()
{
// mysqli connection...
// 5/20/2002 -ref-  added 'www.twinrockerhandmadepaper.com' in move to Internet.  7/1/03
// 8/23/2915 Modified  for php5.5 mysqli_* and native driver

static $connection = null;  

if(!isset($connection)) {
         // choose beteen production or local develoment credentials...
        
    if ( preg_match("/twinrockerhandmadepaper/i",$_SERVER["SERVER_NAME"]) || $_SERVER["SERVER_NAME"]=="twinrocker.phpwebhosting.com"  || $_SERVER["SERVER_NAME"]=="fall.phpwebhosting.com")
    {
    	 // echo " got to db ";
         $connection = @mysqli_connect("127.0.0.1",  "twinrocker", "ser3,cxz", "store",3306,"/var/lib/mysql/mysql.sock"); // or die("Some error connecting to database.");
    
    } else {
    	  //echo "cmr4 local";
          $connection = @mysqli_connect("localhost",  "twinrocker", "sTd9Uk8a", "store", 3306 );// or die("Some error occurred during local connection ");
    
    }
    if($connection === false) {
        // Handle error - notify administrator, log to a file, show an error screen, etc.
        echo "Error connecting: ". mysqli_connect_error(); 
        return mysqli_connect_error(); 
    }
  }
  return $connection;
}

function db_result_to_array($result)
{
   $res_array = array();
   // Adds rows to array (Don't require mysqlnd  via mysqli_fetch_all() )
   while ($row = mysqli_fetch_array($result))
     $res_array[] = $row;

   return $res_array;
}


function pdo_connect() {
  // create mysql PDO connect object  5/6/2019

  global $g_developing; // set in book_sc_fns.php
  static $connect = null;

  if(!isset($connect)) {
    if ($g_developing)
    {
    	  // development: local";
          $host = '127.0.0.1';
          $db   = 'store';
          $user = 'twinrocker';
          $pass = 'sTd9Uk8a';
          $charset = 'latin1'; //'utf8mb4';
    
    } else {
    	  // trhmp.com: production
          $host = '127.0.0.1';
          $db   = 'store';
          $user = 'twinrocker';
          $pass = 'ser3,cxz';
          $charset = 'latin1'; //'utf8mb4';
     } 
    
    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    
     $connect = new PDO($dsn, $user, $pass, $options);
     
     return $connect;
  }
}

/* example query w/ named placeholders
$stmt = $pdo->prepare('SELECT * FROM users WHERE email = :email AND status=:status');
$stmt->execute(['email' => $email, 'status' => $status]);
$user = $stmt->fetch();
*/

    
   
function old_pswd($input, $hex = true) {
// Function replicates mysql OLD_PASSWORD() in code below
// was named shittyPassword from http://sandbox.onlinephpfunctions.com/code/a7a66c7e4b79b52aaa9f948fc8b8f23fe2644492
// -- works w/ precise12 and victory2134
// 5/6/2019 Called from authenticate, etc 
// to convert to password_hash()
 
  $nr    = 1345345333;
  $add   = 7;
  $nr2   = 0x12345671;
  $tmp   = null;
  $inlen = strlen($input);
  for ($i = 0; $i < $inlen; $i++) {
      $byte = substr($input, $i, 1);
      if ($byte == ' ' || $byte == "\t") {
          continue;
      }
      $tmp = ord($byte);
      $nr ^= ((($nr & 63) + $add) * $tmp) + (($nr << 8) & 0xFFFFFFFF);
      $nr2 += (($nr2 << 8) & 0xFFFFFFFF) ^ $nr;
      $add += $tmp;
  }
  $out_a  = $nr & ((1 << 31) - 1);
  $out_b  = $nr2 & ((1 << 31) - 1);
  $output = sprintf("%08x%08x", $out_a, $out_b);
  if ($hex) {
      return $output;
  }

  return hexHashToBin($output);
}

function hexHashToBin($hex) {
  $bin = "";
  $len = strlen($hex);
  for ($i = 0; $i < $len; $i += 2) {
      $byte_hex  = substr($hex, $i, 2);
      $byte_dec  = hexdec($byte_hex);
      $byte_char = chr($byte_dec);
      $bin .= $byte_char;
  }

  return $bin;
}

//$hash = old_pswd('victory1234');
//echo 'victory1234 = '.$hash.'  4b82f82c0a57a623';
// 3/2/2019 works w/ precise12 and victory1234
