<?php

// echo $_SERVER["SERVER_NAME"];
//  exit;
 
  if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost")
  {
      ini_set('display_errors', '1'); 
      error_reporting(-1); //shows errors, deprecated and notices    // 32767
                              //(E_ALL & E_DEPRECATED); // excludes notices
      // Let set as in .htaccess
      $g_developing = true;  // 6/23/2015 
      //echo ini_get('error_log');  echo $_SERVER["SERVER_NAME"];
      //exit;
  }
  else
  {
      ini_set('display_errors', 0); 
      error_reporting(0); 
      $g_developing = false;  // 6/23/2015 (payments.php)  
  }

  include_once("db_fns.php");
  include_once("action_fns.php");
  include_once("data_fns.php");
  include_once("output_fns.php");
  include_once("user_auth_fns.php");
  include_once("tr_fns.php");
  
  // Note: 9/2015 -- mail_fns.php added and called when used for outbound email -- it includes the mailjet.com users creds.
  
  // als: mail_fns.php, cust_fns.php
  // globals vars --company specific
  $g_app = 'showprod.php';
  $g_storename = 'Twinrocker Handmade Paper';
  $g_companyurl = 'www.twinrockerhandmadepaper.com';
  $g_headername = 'Twinrocker Papermaking Supplies';  
  $g_emaildomain='twinrockerhandmadepaper.com';    // 1/24/2010 forgot_username.php
  //$g_ssl_img = '<img src=graphics/rapidssl.gif border=1>';  //current ssl img (login, register,payment)
  // 1/5/2011 godaddy.com/starfield techologies ssl: 39332097 gail's pswd  Expires 5 yrs  2016
  $g_ssl_img = '<span id="siteseal"><script type="text/javascript" src="https://seal.starfieldtech.com/getSeal?sealID=DjmQoSml4bKKrTyceewgRpVUG5vWAQqKh5wYf0dWdSLt4vCxKH57"></script><br/></span>';
  $g_payment_phone = '1-800-757-8946';
  $g_alt_phone = '1-765-563-3119';
  $g_phnhours = 'M-F 9-5 EST';  // in header; hours also hardcoded in supply.php
  
  // Sales tax is calculated in function get_order_totals() in the include file 
  // inc/action_fns.php   <--   This is where to  SET SS & WH DISCOUNTS;  FIGURES IN SALES TAX
  $g_taxstate = 'IN';  // The state where tax is charged -- where the company is.  [Used in showcart.php]
  $g_tax_text = 'IN 7% sales tax';  //get_order_totals() & display_order_totals() in  action_fns.php 
  $g_sales_tax = 0.07;
  
  $g_table1_align = 'center';
  $g_table1_width = '100%';  //'100%';   //change here and in explicit defs in all display pages
  $g_body_bg_color = "#2f4f4f"; // TR #2f4f4f
  $g_table1_border= '1';
  $g_table1_bgcolor = '#ffffff';
  $g_headerbgcolor = '#bfbfbf';  //'#8a8987'
  
  function sys_log($msg)
  {
      //$date = new DateTime("now"); 
      //$timestring = $date->format('Y-m-d H:i:s');
      $timestring = date('Y-m-d H:i:s');
      // set destination and log msg in a file
     if ($g_developing)
     {
        error_log($msg. " $timestring \r\n",3,"c:/wamp/www/TRweb_folder/TRweb/twinrock.log");
     }
     else
     {
        error_log($msg. " $timestring \n",3,"/home/twinrocker/tmp/twinrock.log");   
     }
  }


// 7/2014 from data_valid_fns.php :
 
function filled_out($form_vars)
{
  // test that each variable has a value
  foreach ($form_vars as $key => $value)
  {
     if (!isset($key) || ($value == "")) 
        return false;
  } 
  return true;
}

function valid_email($address)
{
  // check an email address is possibly valid
  if (preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/", trim($address)))
    return true;
  else 
    return false;
} 

?>
