<?php
// data_fns.php  -- Version 4.06 fns to save data to cust and orders files
// 3/18/02
//11/25/02 added 'lastupdate' field to customers and shipaddr

function is_order_saved($token)
{
    //check to see if token is in order file.
  	$conn = db_connect();
		$query = "SELECT * FROM orders WHERE order_token = '$token' ";
    $result = @mysqli_query( $conn, $query) or die ("Error in ordertoken query. " );
    if (mysqli_num_rows($result)>0)
      return 1;
    else
      return 0;
}

function get_next_number($table)
{
    //This routine gets the next number in a sequence
    /*  In phpadmin STORE:
     drop table ai_orders
     create table ai_orders (number INT
        unsigned auto_increment not null primary key)
        auto_increment=10000
     */
    //table name is  ai_orders...
  	$conn = db_connect();
		$query = "INSERT INTO  $table SET number = NULL";
    $result = @mysqli_query( $conn, $query) or die ("Error in $table increment query. " );
		if ($result)
		{
			$query = "SELECT LAST_INSERT_ID()";
            $result = mysqli_query($conn, $query);
			if ($result)
			{
          $next = mysqli_fetch_row($result);
      }
      return $next[0];
    }
    else
      return 0;
}

function get_username()
{
    //In change_password.php, must find username of the current user
    // 8/25/03 edited IUID to UACCT and id to webid
  	$conn = db_connect();
  	$recid = $_SESSION['SESSION_UACCT'];   //Find username of this user
		$query = "SELECT username FROM customers WHERE webid = '$recid' ";
    $result = @mysqli_query( $conn, $query) or die ("Error in query. " );
    $thisuser = false;
		if ($result)
		{
			$row = mysqli_fetch_array($result);
    	$thisuser = $row["username"];
    }
    return $thisuser;
}

function get_user_question($username)
{
    //In forgot_password.php, must find username's question for forgotton password.
  	$conn = db_connect();
   	$query = "SELECT lostpswdquestion FROM customers WHERE username = '$username' ";
    $result = @mysqli_query( $conn, $query) or die ("Error in query. " );
    $thisvalue = false;
		if ($result)
		{
			$row = mysqli_fetch_array($result);
    	$thisvalue = $row["lostpswdquestion"];
    }
    return $thisvalue;
}

function get_user_answer($username)
{
    //In forgot_password.php, must find username's question for forgotton password.
  	$conn = db_connect();
   	$query = "SELECT lostpswdanswer FROM customers WHERE username = '$username' ";
    $result = mysqli_query( $conn, $query) or die ("Error in query. " );
    $thisvalue = false;
		if ($result)
		{
			$row = mysqli_fetch_array($result);
    	$thisvalue = $row["lostpswdanswer"];
    }
    return $thisvalue;
}

function cust_update($cust,$filename='customers')
// change details of person's profile
{
   $conn = db_connect();
   
   
    //checkbox values  10/13/02
    if (!isset($cust["enews"]))
		$cust["enews"] = 'off'; 
	
    if (!isset($cust["specials"]) )
		$cust["specials"] = 'off'; 
	

   $query = "update $filename set name='".addslashes($cust['name'])."', 
			firstname='".addslashes(ucfirst(strtolower($cust['firstname'])))."',
lastname='".addslashes(ucfirst(strtolower($cust['lastname'])))."',
email='".addslashes($cust['email'])."',
webaddress='".addslashes($cust['webaddress'])."',
webdescription='".addslashes($cust['webdescription'])."',
phn_area='".addslashes($cust['phn_area'])."',
phn_prefix='".addslashes($cust['phn_prefix'])."',
phn_suffix='".addslashes($cust['phn_suffix'])."',
besttime='".addslashes($cust['besttime'])."',
cell_area='".addslashes($cust['cell_area'])."',
cell_prefix='".addslashes($cust['cell_prefix'])."',
cell_suffix='".addslashes($cust['cell_suffix'])."',
fax_area='".addslashes($cust['fax_area'])."',
fax_prefix='".addslashes($cust['fax_prefix'])."',
fax_suffix='".addslashes($cust['fax_suffix'])."',
address1='".addslashes($cust['address1'])."',
address2='".addslashes($cust['address2'])."',
address3='".addslashes($cust['address3'])."',
city='".addslashes(ucfirst($cust['city']))."',
state='".addslashes($cust['state'])."',
zip='".addslashes($cust['zip'])."',
country='".addslashes($cust['country'])."',
taxexemptid='".addslashes($cust['taxexemptid'])."',
enews='".$cust['enews']."',
specials='".$cust['specials']."',
type='".addslashes(strtoupper($cust['type']))."',
source='".addslashes(strtoupper($cust['source']))."',
code='".addslashes(strtoupper($cust['code']))."',
pterms='".addslashes(ucwords(strtolower($cust['pterms'])))."',
entered='".addslashes($cust['entered'])."',
ldate='".addslashes($cust['ldate'])."',
lsale='".doubleval($cust['lsale'])."',
notes='".addslashes($cust['notes'])."',lastupdate=NOW()  where webid='".$cust['webid']."'";

//echo $query;

// 11/25/02 Add lastupdate to customers and shipaddr so SBT can be updated more easily			

			
   $result = mysqli_query($conn, $query);
   if (!$result)
   {echo "cust_update failed";
     return false;
	}
   else
   {
      //Update ship_state  6/17/2010
      $_SESSION['ship_state'] = $cust['state'];
      return true;
   } 
}

function cust_delete($custid)
// Remove the category identified by catid from the db
// If there are books in the category, it will not
// be removed and the function will return false.
{
   $conn = db_connect();
   
   // check if there are any books in category 
   // to avoid deletion anomalies   
   $query = "select *
             from books
             where catid='$catid'";
   $result = @mysqli_query($conn, $query);
   if (!$result || @mysqli_num_rows($result)>0)
     return false;

   $query = "delete from categories 
             where catid='$catid'";
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   else
     return true; 
}


function cust_insert($cust,$filename)
// insert a new profile into the database -- either customers or newcust 10/12/02
// returns last_id if successful

// 8/25/03 -- change to webid as key field, NOT custno....
{
   $conn = db_connect();
   
    //checkbox values  10/13/02
    if (!isset($cust["enews"]))
		$cust["enews"] = 'off'; 
	
    if (!isset($cust["specials"]) )
		$cust["specials"] = 'off'; 

///// ------  disable working custno creation routine
/*
	if (empty($cust['name']) && empty($cust['firstname']) && empty($cust['lastname']))
	{
		// all name fields are empty so don't add.
		return 0;
	}
	else
	{
		if (!empty($cust['name']) && !empty($cust['firstname']) && !empty($cust['lastname']))
		{
			// all three fields have something so treat as a Company
			//
			$first = explode(" ",$cust['name']);
			$cnt = count($first); 
			// Individuals' custno's are built (below)lastname+firstname'
			// but Company custno's are built (below) from first word+last word   12/12/02
			// so, (since this was added AFTER writing new id on lastname first name model, we 
			// stick the first work of the company name as 'lname' and the second word as 'fname'
 			$lname = ucwords(strtolower($first[0]));
			$fname = ucwords(strtolower($first[$cnt-1]));
			
		}
		else
		{		
			// Users can either type both names in the Name field, or leave that blank and
			// enter just first and last.  Handle both conditions.this  If user didn't enter the name, insert first last in it
			if (empty($cust['name']))
				$cust['name'] = ucwords(strtolower($cust['firstname'].' '.$cust['lastname']));

			else
			{
				if (empty($cust['firstname']) && empty($cust['lastname']))
				{
					//REPLACE ALL lastname WITH  (substr( alltrim(name),rat(' ',alltrim(name))+1,20) )
					//REPLACE ALL firstname WITH  substr( alltrim(name),1, rat(' ',alltrim(name)) )
					$first = explode(" ",$cust['name']);
					$cnt = count($first); 
			
					$cust['firstname'] = ucwords(strtolower($first[0]));
					$cust['lastname'] = ucwords(strtolower($first[$cnt-1]));
				}
			}	
			// Individuals' custno's are built (below)lastname+firstname'
			
			$lname = $cust['lastname'];	
			$fname = $cust['firstname'];
		}	
	
	}		
		
			
	// Add routine to check for matches and prevent duplicates
	// Initially, if firstname+lastname+zip are the same or 

	// Create custno by TR rules  5:1,4:2,3:3,2:4,1:1+increment
	
	$found = 100;
	$ln = 6;
	$fn = 0;
	while ($found >  0)
	{
		$ln = $ln - 1;
		$fn = $fn + 1;
		$mcust = substr(strtoupper($lname),0,$ln).substr(strtoupper($fname),0,$fn);
		$query = "select * from $filename where custno='$mcust'";
		echo $query;
		$result = mysqli_query($conn,$query);
		$found = mysqli_num_rows($result);
		echo $found;
		if ($ln == 2)
			break;
	}
	if ($found <> 0)		
	{
		//initials already used so do initials and number
		$mcust1 = substr(strtoupper($lname),0,1).substr(strtoupper($fname),0,1);

		$n = 0;
		while ($found > 0 && $n < 40)
		{
			++$n;
			
			$k = $n;
			echo $n;
			echo "$k";
			$mcust = $mcust1.$k;
			echo $mcust;
			$query = "select * from $filename where custno='$mcust'";
			echo $query;
			$result = mysqli_query($conn,$query);
			$found = mysqli_num_rows($result);
			echo $k.$found;
			
		}
		if ($found > 0)
		{
			// Can't connect or can't get unique custno
			return 0;
		}
	}
	
*/  //////------------ Now get webid  8/25/03

  $webid = get_uniqueid();
  
			
	// if here, now has a custno..., er, webid... 8/25/03
	
	//create 'custno'  this one based on initials plus phone then incrementing..
/*	$mcust1 = substr(ucwords($cust['firstname']),0,1).substr(ucwords($cust['lastname']),0,1);
	$mcust = $mcust1.$cust['phn_suffix'];
	echo $mcust.'<br>';
	
	$query = "select * from $filename where custno='$mcust'";
	echo $query;
	$result = mysqli_query($conn,$query);
	$found = mysqli_num_rows($result);
	echo $found;
	if ($found > 0)
	{
		//initials and phone already used so do initials and number
		$n = 0;
		while ($found > 0 && $n < 40)
		{
			++$n;
			
			$k = $n;
			//$k = settype($k,"string");
			echo $n;
			echo "$k";
			$mcust = $mcust1.$k;
			echo $mcust;
			$query = "select * from $filename where custno='$mcust'";
			echo $query;
			$result = mysqli_query($conn,$query);
			$found = mysqli_num_rows($result);
			echo $k.$found;
			
		}
		if ($found > 0)
		{
			// Can't connect or can't get unique custno
			return 0;
		}
	}
*/
  // Adding a new record, insert the record with ID

  //Must add code to prevent adding dups... from resubmitting...8/26/03
  
	$query = "insert into $filename set id= NULL, name='".addslashes($cust['name'])."',
			firstname='".addslashes(ucfirst(strtolower($cust['firstname'])))."',
lastname='".addslashes(ucfirst(strtolower($cust['lastname'])))."',
email='".addslashes($cust['email'])."',
webaddress='".addslashes($cust['webaddress'])."',
webdescription='".addslashes(ucwords(strtolower($cust['webdescription'])))."',
phn_area='".addslashes($cust['phn_area'])."',
phn_prefix='".addslashes($cust['phn_prefix'])."',
phn_suffix='".addslashes($cust['phn_suffix'])."',
besttime='".addslashes($cust['besttime'])."',
fax_area='".addslashes($cust['fax_area'])."',
fax_prefix='".addslashes($cust['fax_prefix'])."',
fax_suffix='".addslashes($cust['fax_suffix'])."',
address1='".addslashes(ucfirst($cust['address1']))."',
address2='".addslashes(ucfirst($cust['address2']))."',
address3='".addslashes(ucfirst($cust['address3']))."',
city='".addslashes(ucwords(strtolower($cust['city'])))."',
state='".addslashes($cust['state'])."',
zip='".addslashes($cust['zip'])."',
country='".addslashes($cust['country'])."',
taxexemptid='".addslashes($cust['taxexemptid'])."',
enews='".$cust['enews']."',
specials='".$cust['specials']."',
type='".addslashes(strtoupper($cust['type']))."',
source='".addslashes(strtoupper($cust['source']))."',
code='".addslashes(strtoupper($cust['code']))."',
pterms='".addslashes(ucwords(strtolower($cust['pterms'])))."',
entered=now(),lastupdate=now(),
ldate='".addslashes($cust['ldate'])."',
lsale='".doubleval($cust['lsale'])."',
notes='".addslashes($cust['notes'])."',
custno='NEW',
webid='".$webid."'";

//echo $query;
//br(3);

//custno='".$mcust."'";



	$result = mysqli_query($conn, $query);
	//echo '  xx  '.$filename.$result;

	if ($result)
	{
		//created unique custno and insert new record
		//$last_id = mysqli_insert_id($conn);
		return $webid;
	}
	else
	   	return 0;
}


function cust_shipto_update($cust)
// change details of customer's shipto address   Called from cust_shipto_edit.php
{

   $conn = db_connect();
   
  // check address does not already exist
  // 8/25/03 edited  UACCT and custno to webid
   $query = "select *
             from shipaddr
             where webid='".$_SESSION["SESSION_UACCT"]."'
			 and name='".addslashes(trim($cust['name']))."'
			 and firstname='".addslashes(trim($cust['firstname']))."'
			 and lastname='".addslashes(trim($cust['lastname']))."'
			 and address1='".addslashes(trim($cust['address1']))."'
			 and address2='".addslashes(trim($cust['address2']))."'
			 and address3='".addslashes(trim($cust['address3']))."'
			 and zip='".addslashes(trim($cust['zip']))."'";

//echo $query;
//br();

   $result = mysqli_query($conn, $query);
   if (mysqli_num_rows($result)== 1)
   	{
		// match found so we're done
	    return true;
	}
   else
    {
		// no match, so we need to Insert new shipaddr )
   	// first, though unregister session var so saved shipto will display
		// instead of the prior or billto (-1) shipto..
		//Global $SESSION_SHIPID;
		// 8/25/03 edited UID to UACCT ... custno to webid
		unset($_SESSION["SESSION_SHIPID"]);
		
   	$query = "insert into shipaddr
		( shipaddrid,id,name,firstname,
          lastname,phn_area,phn_prefix,phn_suffix,
  		  address1,address2,address3,city,state,zip,country,email,webid,lastupdate,custno)   values
		(NULL,".$_SESSION['SESSION_UID'].",'".addslashes($cust['name'])."',
		'".addslashes($cust['firstname'])."',
		'".addslashes($cust['lastname'])."',
		'".addslashes($cust['phn_area'])."',
		'".addslashes($cust['phn_prefix'])."',
		'".addslashes($cust['phn_suffix'])."',
		'".addslashes($cust['address1'])."',
		'".addslashes($cust['address2'])."',
		'".addslashes($cust['address3'])."',
		'".addslashes($cust['city'])."',
		'".addslashes($cust['state'])."',
		'".addslashes($cust['zip'])."',
		'".addslashes($cust['country'])."',
		'".addslashes($cust['email'])."',
		'".$_SESSION['SESSION_UACCT']."',NOW(),'".addslashes($cust['custno'])."' )";
	
	//echo "<br>".$query;
	
		$result = @mysqli_query($conn, $query) || ("ack! query failed: "
				."<li>errono=".((is_object($conn)) ? mysqli_errno($conn) : (($___mysqli_res = mysqli_connect_errno()) ? $___mysqli_res : false))
				."<li>error=".((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false))
				."<li>query=".$query );
				
		return $result;
	}
}





function ORIGINAL_display_checkout_form()
{
  //ORIGINAL FROM 4.06
  //display the form that asks for name and address
  GLOBAL $catid;
  global $tempnames;
  
  echo 'xx'.isset($tempnames);
  
  	if (!isset($tempnames))
  {
  	// If doesn't exist, initialize the variables'
	$name = ' &nbsp;';
	$address1 = ' &nbsp;';
	$address2 = ' &nbsp;';
	$city = ' &nbsp;';
	$state = ' &nbsp;';
	$zip = ' &nbsp;';
	$phone = ' &nbsp;';
	$email = ' &nbsp;';
	$country = 'USA';
	$ponumber = ' &nbsp;';
	$ccno = ' &nbsp;';
	$ccexpdate = ' &nbsp;';
	$ccaddress = ' &nbsp;';
	$comment = ' ';
	$type = ' &nbsp;';
	$taxid = ' &nbsp;';
    $shipname = $name;
	$shipaddress1 = $address1;
	$shipaddress2 = $address2;
	$shipcity = $city;
	$shipstate = $state;
	$shipzip = $zip;
	$shipphone = $phone;
	$shipemail = $email;
	$shipcountry = $country;
   }
   else
   {
	$name = $tempnames["name"];
	$address1 = $tempnames["address1"];
	$address2 = $tempnames["address2"];
	$city = $tempnames["city"];
	$state = $tempnames["state"];
	$zip = $tempnames["zip"];
	$phone = $tempnames["phone"];
	$email = $tempnames["email"];
	$country = $tempnames["country"];
	$ponumber = $tempnames["ponumber"];
	$ccno = "'' "; //$tempnames["ccno"];
	$ccexpdate = $tempnames["ccexpdate"];
	$ccaddress = $tempnames["ccaddress"];
	$comment = $tempnames["comment"];
	$type = $tempnames["type"];
	$taxid = $tempnames["taxid"];
    $shipname = $tempnames["shipname"];
	$shipaddress1 = $tempnames["shipaddress1"];
	$shipaddress2 = $tempnames["shipaddress2"];
	$shipcity = $tempnames["shipcity"];
	$shipstate = $tempnames["shipstate"];
	$shipzip = $tempnames["shipzip"];
	$shipphone = $tempnames["shipphone"];
	$shipemail = $tempnames["shipemail"];
	$shipcountry = $tempnames["shipcountry"];
   }   
  
  
echo "
  <br>
  <table border = 0 width = 100% cellspacing = 0>
  <form action = savename.php method = post>	
  <tr><th bgcolor=#cccccc>".(date ("dS of F Y h:i:s A"))."</th>
      <th align=left bgcolor=#cccccc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Order Details</th></tr>
  <tr>
  <td colspan = 2 align = center>
  <input type=submit value='Quick Save'></td></tr>
  <tr>
    <td>Name</td>
    <td><input type = text name = name value = $name maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Address1</td>
    <td><input type = text name = address1 value = $address1 maxlength = 40 size = 40></td>
  </tr>
    <tr>
    <td>Address2</td>
    <td><input type = text name = address2 value = $address2 maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input type = text name = city value = $city maxlength = 20 size = 20></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input type = text name = state value = $state maxlength = 20 size = 20></td>
  </tr>
  <tr>
    <td>Zip Code</td>
    <td><input type = text name = zip value = $zip maxlength = 12 size = 12></td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input type = text name = phone value = $phone maxlength = 20 size = 20></td>
  </tr>  
  <tr>
    <td>email</td>
    <td><input type = text name = email value = $email maxlength = 40 size = 40></td>
  </tr>   
  <tr>
    <td>Country</td>
    <td><input type = text name = country value = $country maxlength = 40 size = 40></td>
  </tr>
  
   <tr>
    <td>Tax ID </td>
    <td><input type = text name = taxid value = $taxid maxlength = 40 size = 40>
	<b>Sale Tax ??<b></td>
  </tr>  
  
   <tr>
    <td>PO Number</td>
    <td><input type = text name = ponumber value = $ponumber maxlength = 40 size = 40></td>
  </tr>
   <tr>
    <td>CC#</td>
    <td><input type = text name = ccno value = $ccno maxlength = 24 size = 24>
    Expires
    <input type = text name = ccexpdateno value = $ccexpdate maxlength = 15 size = 15>
	</td>

  </tr>
   <tr>
    <td>CC Address</td>
    <td><input type = text name = ccaddress value = $ccaddress maxlength = 40 size = 40></td>
  </tr>
  
   <tr>
    <td>Comment</td>
    <td><textarea name = comment rows=2 cols=60 wrap> $comment
	    </textarea> </td>
  </tr>
  
    <tr>
    <td>Customer Type </td>
    <td><input type = text name = type value = $type maxlength = 10 size = 10></td>
  </tr>
  <tr><td colspan = 2 align = center>
  <input type=submit value='Quick Save'></td></tr>
    
  <tr><th colspan = 2 bgcolor=#cccccc>Shipping Address (leave blank if as above)</th></tr>
  <tr>
    <td>Name</td>
    <td><input type = text name = shipname value = $shipname maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Address1</td>
    <td><input type = text name = shipaddress1 value = $shipaddress1 maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Address2</td>
    <td><input type = text name = shipaddress2 value = $shipaddress2 maxlength = 40 size = 40></td>
  </tr>  
  <tr>
    <td>City</td>
    <td><input type = text name = shipcity value = $shipcity maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input type = text name = shipstate value = $shipstate maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>Zip Code</td>
    <td><input type = text name = shipzip value = $shipzip maxlength = 12 size = 12></td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input type = text name = shipphone value = $shipphone maxlength = 20 size = 20></td>
  </tr> 
    <tr>
    <td>email</td>
    <td><input type = text name = shipemail value = $shipemail maxlength = 40 size = 40></td>
  </tr>      
   <tr>
    <td>Country</td>
    <td><input type = text name = shipcountry value = $shipcountry maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td colspan = 2 align = center>
	<input type=submit value='Quick Save'>
	
  </td>
  </tr>
  </form>
  </table><hr>";


//.this. was in place of submit button..
/*      <b>Please press Purchase to confirm your purchase,
         or Continue Shopping to add or remove items</b>" ;
      display_form_button("purchase", "Purchase These Items"); 
*/
}


function get_webid ($cust,$filename)
{
		if (!empty($cust['name']) && !empty($cust['firstname']) && !empty($cust['lastname']))
		{
			// all three fields have something so treat as a Company
			//
			$first = explode(" ",$cust['name']);
			$cnt = count($first); 
			// Individuals' custno's are built (below)lastname+firstname'
			// but Company custno's are built (below) from first word+last word   12/12/02
			// so, (since this was added AFTER writing new id on lastname first name model, we 
			// stick the first work of the company name as 'lname' and the second word as 'fname'
 			$lname = ucwords(strtolower($first[0]));
			$fname = ucwords(strtolower($first[$cnt-1]));
			
		}
		else
		{		
			// Users can either type both names in the Name field, or leave that blank and
			// enter just first and last.  Handle both conditions.this  If user didn't enter the name, insert first last in it
			if (empty($cust['name']))
				$cust['name'] = ucwords(strtolower($cust['firstname'].' '.$cust['lastname']));

			else
			{
				if (empty($cust['firstname']) && empty($cust['lastname']))
				{
					$first = explode(" ",$cust['name']);
					$cnt = count($first); 
			
					$cust['firstname'] = ucwords(strtolower($first[0]));
					$cust['lastname'] = ucwords(strtolower($first[$cnt-1]));
				}
			}	
			// Individuals' custno's are built (below)lastname+firstname'
			
			$lname = $cust['lastname'];	
			$fname = $cust['firstname'];
		}	
	
			
		
	// Initially, if firstname+lastname+zip are the same or 

	// Create custno by TR rules  5:1,4:2,3:3,2:4,1:1+increment
	
	$conn = db_connect();
	$found = 100;
	$ln = 6;
	$fn = 0;
	while ($found >  0)
	{
		$ln = $ln - 1;
		$fn = $fn + 1;
		$mcust = substr(strtoupper($lname),0,$ln).substr(strtoupper($fname),0,$fn);
		$query = "select * from $filename where custno='$mcust'";
		//echo $query;
		$result = mysqli_query($conn, $query);
		$found = mysqli_num_rows($result);
		//echo $found;
		if ($ln == 2)
			break;
	}
	if ($found <> 0)		
	{
		//initials already used so do initials and number
		$mcust1 = substr(strtoupper($lname),0,1).substr(strtoupper($fname),0,1);

		$n = 0;
		while ($found > 0 && $n < 40)
		{
			++$n;
			
			$k = $n;
			//echo $n;
			//echo "$k";
			$mcust = $mcust1.$k;
			//echo $mcust;
			$query = "select * from $filename where custno='$mcust'";
			//echo $query;
			$result = mysqli_query($conn, $query);
			$found = mysqli_num_rows($result);
			//echo $k.$found;
			
		}
		if ($found > 0)
		{
			// Can't connect or can't get unique custno
			return 0;
		}
	}
			
	// if here, now has a custno...
	return $mcust;	
}



?>
