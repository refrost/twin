<?php
// WebStore/Silk 2020 (c) REF&A 2002
// action_fns.php 
// 6/5/2010 NOTE: Default shipping charges are set in 
//                function calc_shipping
// 12/05/2010 Exclude Rush option if cart contains envelopes Set session string for use in calc_chipping.
//            has_envelopes() used in showcart...
// 1/5/2011   Went to Starfield technologies SSL (See book_sc_fns.php)
// 2/12/2011  Added merge_id and consolidate .php run by Admin in browsers
//            changed maxsize to maxlength in register,tr_pulp,change_password,authenticate
//    wip        Added specials and seconds -- Mod function calculate_price and '9'
// 3/5/2011  specials() to display/add2carts for items of a special code... IN tr_fns.php...
//           added javascript mcedit editor in admin_prod.php
// 05/09/2011 has_pulp()  - Exclude Rush if  item in pulpcart
// 05/26/2011 st_pricelist.php and fiber_des.html edits
// 07/19/2011 re-implemented Discover Card (cc_val, cust_fns, payments 0.5 hr)
// 7/20 -- billed 8 hr
// 10/19/2011 Rev load_fiber_array() 11,26,99 qty breaks to 14,49,99 to math w/ column headers
// 02/15/2012 Rev changed paper_charge, MIN from 9 & 100 to 11 &150 in calc_shipping
// questionable jump from win2000 to win7 code due to HD fail on win2k box
// 06/21/2012 rev.  pulp shipping: edited pails to bags
// 7/16/2012  rev.  round()   added in get_order_totals() since sequential 
//                  use of number_format() w/ tax + totals didn't match  for 2.205
// 5/10/2013  rev. Implement discounts
// 7/9/2014 5.3 upgrd:  e-reg s-ession_un/register  m ysql_escape_string
// 12/17/2014 Don't show any Rush charge in get_ship_state()
// 06/24/2015 Add onmaxmove js
 
function string_to_mysqlx_inarray($str)  {
    //Converts a comma separated string into a string for use in a 
    //mysqlx IN ARRAY query 
    $arr = explode(',',$str);
    $ret = '';
    foreach ($arr as $value) {
        $ret .= "'$value',";
    }
    $ret = rtrim($ret,',');
    return $ret;
}

function username($email)
{ // 1/24/2010 -- forgot_username.php
return true;
}

function insert_javascript($name)  // Inserts 1 javascript code segment into header
{
  switch ($name) {
    case 'validpulpform' :
      $mret = '<SCRIPT LANGUAGE="JAVASCRIPT" Type="TEXT/JAVASCRIPT">
              <!-- hide script
              function validpulpform(pulpform){
                if (pulpform.mpails.value.indexOf(".")!=-1){
                  alert("Please order whole pails only.")
                  pulpform.mpails.focus()
                  return false
                }
                
                return true
              }
              // End hiding -->
              </SCRIPT>';
      break;
    case 'validateInteger' :
      $mret = '<SCRIPT LANGUAGE="JAVASCRIPT" Type="TEXT/JAVASCRIPT">
              <!-- hide script
              function validateInteger(qty){
                if (qty.indexOf(".")!=-1){
                  alert("No decimals. Please order whole units only.")
                  return false
                }
                return true
              }
              // End hiding -->
              </SCRIPT>';
      break;
    case 'validstatform' :
      $mret = '<SCRIPT LANGUAGE="JAVASCRIPT" Type="TEXT/JAVASCRIPT">
              <!-- hide script
              function validstatform(statform){
                var excessive = -1
                var xcolors = statform.stcolor.length
                var mqty = parseInt(statform.qty.value,10)

                if (mqty==0 || isNaN(mqty))
                {
                  //alert(mqty)
                  statform.qty.focus()  
                  return false
                }
                
                for (i=0; i < xcolors; i++)
                {
                  if (statform.stcolor[i].checked)          {
                    if (statform.stcolor[i].value=="WHAP")
                    {
                        if (mqty > parseInt(statform.invt1.value,10))
                        {
                          excessive = 1
                        }
                    }
                    if (statform.stcolor[i].value=="WHSC")
                    {
                        if (mqty > parseInt(statform.invt2.value,10))
                        {
                          excessive = 2
                        }
                    }
                    if (statform.stcolor[i].value=="CREC")
                    {
                        if (mqty > parseInt(statform.invt3.value,10))
                        {
                          excessive = 3
                        }
                    }
                    if (statform.stcolor[i].value=="SIMO")
                    {
                        if (mqty > parseInt(statform.invt4.value,10))
                        {
                          excessive = 4
                        }
                    }
                  }
                }
                if (excessive>0)
                {
                  alert("Quantity ordered exceeds amount on hand. Try again or please call the office.")
                  statform.qty.focus()  
                  return false
                }
                return true
              }
              // End hiding -->
              </SCRIPT>';
      break;
    case 'validstatform2' :
      $mret = '<SCRIPT LANGUAGE="JAVASCRIPT" Type="text/javascript">
              <!-- hide script
              function validstatform2(statform){
                var excessive = -1;
                var mqty = parseInt(statform.qty.value,10);
                var cartqty1 = parseInt(statform.cartqty1.value,10);
                var cartqty2 = parseInt(statform.cartqty2.value,10);
                var cartqty3 = parseInt(statform.cartqty3.value,10);
                var cartqty4 = parseInt(statform.cartqty4.value,10);
                var whapqty = parseInt(statform.invt1.value,10);
                var whscqty = parseInt(statform.invt2.value,10);
                var crecqty = parseInt(statform.invt3.value,10);
                var simoqty = parseInt(statform.invt4.value,10);
                
                if (mqty==0 || isNaN(mqty)){
                 statform.qty.focus();  
                 return false;
                }
                var i = statform.stcolor.selectedIndex;
                
                if (statform.stcolor.options[i].value=="WHAP")
                {
                   if (mqty > whapqty - cartqty1) 
                   {
                       excessive = 1;
                   }
                }
                if (statform.stcolor.options[i].value=="WHSC")
                {
                   if (mqty > whscqty- cartqty2)
                   {
                     excessive = 2;
                   }
                }
                if (statform.stcolor.options[i].value=="CREC")
                {
                   if (mqty > crecqty- cartqty3)
                   {
                     excessive = 3;
                   }
                }
                if (statform.stcolor.options[i].value=="SIMO")
                {
                   if (mqty > simoqty- cartqty4)
                   {
                     excessive = 4;
                   }
                }
                
                if (excessive>0)
                {
                 alert("Quantity ordered exceeds amount on hand minus quantity already on the cart. If you need this color please call the office.");
                 statform.qty.focus();  
                 return false;
                }
                
                return true;
              }
              // End hiding -->
              </SCRIPT>';
      break;
    
    case 'moveonmax' :
      $mret = '<SCRIPT LANGUAGE="JAVASCRIPT"   Type="TEXT/JAVASCRIPT">
              <!-- hide script
        
         function moveOnMax(field, nextFieldID) {
               var target = document.getElementById(nextFieldID);
              
              if (field.value.length >= field.maxLength) { target.focus(); } 
        }
        
        // End hiding -->
        </SCRIPT>';
      break; 
    case 'validpaperform' :
      $mret = '<SCRIPT LANGUAGE="JAVASCRIPT" Type="TEXT/JAVASCRIPT">
              <!-- hide script
              function validpaperform(thisform){
                mqty = parseInt(thisform.qty.value,10)
                if (mqty==0 || isNaN(mqty))
                {
                  //alert(mqty)
                  thisform.qty.focus()  
                  return false
                }
                
                if (mqty > parseInt(thisform.onhand.value,10))
                {
                  alert("Quantity ordered exceeds amount on hand. Try again or please call the office.")
                  thisform.qty.focus()  
                  return false
                }
                return true
              }
              // End hiding -->
              </SCRIPT>';
      break;
     case 'jqziplookup' :
      // Must load jquery cdn via https:// for production site
      $mret = '<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript">
 
$(function() {
 
    $("input[name=zip]").blur(function() {
        // getting the value that user typed
        var thiszip = $("input[name=zip]").val().toString();
         
        // if searchString is not empty
        if(thiszip) {
            // ajax call
            //alert("Data: "+thiszip);
            $.ajax({
                type: "POST",
                url: "ziplookupjson.php",
                data: {"this_zip": thiszip},
                success: function(data){ // this happens after we get results
                    if (!data) {
                        alert("Postal code look up did not resolve to a US city/state. Enter your location info carefully.");
                    }
                    $("input[name=city]").val(data.city);
                    $("input[name=state]").val(data.state);
                    //$("#results").show();
                    //$("#results").append("Result: "+data.city+" "+data.state);
              }
            });    
        }
        return false;
    });
});
</script>';
      break;
      
    default :
      $mret = '';
  }
  return $mret;
}

/*
debugWin=window.open("","debugWin","height=200, width=200,resize=yes")
                  debugWin.document.writeln(i)
                  debugWin.document.writeln(statform.stcolor[i].value)
                  debugWin.document.writeln(statform.stcolor[i].checked)
                  debugWin.document.close()
*/
 
function  js_alert_back($msg)     // 01/20/2007 used in st_add2_cart.php, pa_add2_cart.php first
{
    // Issues an alert from backend page and returns to previous page
    echo  '<SCRIPT LANGUAGE="JAVASCRIPT" Type="TEXT/JAVASCRIPT">
             alert("'.$msg.'");
             history.go(-1);
           </SCRIPT>'; 
    return true;
} 

function  js_alert($msg)     // 07/31/13 used in ordermemory.php for paper first
{
    // Issues an alert 
    echo  '<SCRIPT LANGUAGE="JAVASCRIPT" Type="TEXT/JAVASCRIPT">
             alert("'.$msg.'");
           </SCRIPT>'; 
    return true;
}  

function util_show_array($custarray)
{
    //$invt_updt_list = array();
  	
    foreach ($custarray as $key => $value)
  	{
  		//echo '<br>'.$key.' '.$value['item'].' '.$value['qty'];
  		$invt_updt_list[$value['item']] = $value['qty'];
  	}

      
      foreach ($invt_updt_list as $jey => $jalue)
  	  {
  		    echo '<br> '.$jey.' '.$jalue;
  	  }

    return;
}


function update_login_count($logins,$id)
{
  //Update login count and date
  $logins = $logins+1;
  //$date = new DateTime("now");
  //$now = $date->format("Y-m-d H:i:s");
  $now = date("Y-m-d H:i:s");
  //echo $logins.' '.$now; br();
  $conn = db_connect();
	$query = "UPDATE CUSTOMERS SET logins=$logins, lastlogin=NOW() WHERE webid='$id'";
	$result = @mysqli_query( $conn, $query); // or die("Error in updating logindate"); 
  return true;
}

function refa_credit()
{
  echo '<font size=2>Dynamic Web Application by <a href="http://www.refrost.com">REF&A</a></font>';
  return true;
}

function display_myordermemory($msg='')
{
if (isset($_SESSION['SESSION_UACCT']))
    $webid  = $_SESSION['SESSION_UACCT'];  // Use to specify record belongs to .THIS. user.
  else
  {
    header("location:weblogin.php");
    exit;
    //$webid = '000zsw';  // not possible
    //$msg = "You need to login first.";
  }
//$webid = $_SESSION['SESSION_UACCT'];
$conn = db_connect();
$query = "select * from orders where webid='$webid' and order_name != 'deleted' order by sono desc";
$result = mysqli_query($conn, $query);
// sono,date,order_name,total_order,items1
if (mysqli_num_rows($result)>0)
{
  echo '<table class=ordermemory align=left border=2>
        <tr><th  colspan=8 class=ordermemory>'.$msg.'</th></tr><tr>
        <th class=ordermemory>Order#</th>
        <th class=ordermemory>Date</th>
        <th class=ordermemory>Order Name</th>
        <th class=ordermemory>Amount</th>
        <th class=ordermemory>Items</th>
        <th class=ordermemory>Fiber</th>
        <th class=ordermemory>Pulp</th>
        <th class=ordermemory>Paper</th>
        <th class=ordermemory>&nbsp;</th>
        </tr>';

  while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
  {
     /*
     if ($row['items1'])
        $item1x='&nbsp;&nbsp;X';
     else
        $item1x='&nbsp;';
     if ($row['items2'])
        $item2x='&nbsp;&nbsp;X';
     else
        $item2x='&nbsp;';
     if ($row['items3'])
        $item3x='&nbsp;&nbsp;X';
     else
        $item3x='&nbsp;';
     if ($row['items4'])
        $item4x='&nbsp;&nbsp;X';
     else
        $item4x='&nbsp;';
     */
     // <br /><a href=ordermemory.php?action=view&sono='.$row['sono'].'&token='.$row['order_token'].'>View Order WSetail</a>
     
     echo '<tr><td class=ordermemory>'.$row['sono'].'
              <br><a href=ordermemory.php?action=delete&sono='.$row['sono'].'&token='.$row['order_token'].'>Delete</a></td>
              <td class=ordermemory nowrap>'.left($row['date'],10).'<br>'.right($row['date'],8).'</td>
              <td class=ordermemory>'.$row['order_name'].'
                </td>
              <td class=ordermemory>'.$row['total_order'].'</td>
              <td class=ordermemory align=left>'.$row["items1"].'</td>
              <td class=ordermemory align=left>'.$row["items2"].'</td>
              <td class=ordermemory align=left>'.$row["items3"].'</td>
              <td class=ordermemory align=left>'.$row["items4"].'</td>
              <td class=ordermemory align=center>
              <a href=ordermemory.php?action=reorder&sono='.$row['sono'].'&token='.$row['order_token'].'>Re-view</a>
              </td>
              </tr>';
  }
  echo '<tr><td colspan=8>
        <a href=ordermemory.php?action=delete_all&sono='.$row['sono'].'&token='.$row['order_token'].'>Delete All</a>
        </td></tr>
        </table>';

}

else
  echo 'No previous orders...<br/><br/>';

return true;
}



function qd_list($result)
{
  $str = '';
  
  foreach ($result as $key => $value)
  {
    $str .= ucwords($key).":$value <br>\n";
  }
  return $str;
}

function qd_plain_list($result)     //Lists field:value pairs
{
    $str = '';
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
      $n = 1;
      
      foreach ($row as $key => $value)
      {

        $str .= $n++.':  '.ucwords($key).":$value <br>\n";
      }
    }
    return $str;
}


function qd_delimited_list($result)     //Delimited List for email to VFP/dropkick 11/19/03
{
    // Create delimited  string in commitcgi that's emailed.
    // When decrypted, values go into dropkick.orders via VFP
    $str = '';
    $flag = false;
    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
    {
      
      foreach ($row as $key => $value)
      {
        if ($key=='items1') //Once encounters first "memo" field, change string building
          $flag =  true;
        if ($flag)
        {
          $str .= '<'.strtolower($key).">\n";
          $str .= strtolower($key).":$value<br>\n";
          $str .= '</'.strtolower($key).">\n";
        }
        else
          $str .= strtolower($key).":$value<br>\n";
      }
    }
    return $str;
}


function qd_list_array($result)     //Lists field:value pairs
{
    $str = '';
      
      foreach ($result as $key => $value)
      {

        $str .= $n++.':  '.ucwords($key).":$value <br>\n";
      }
    return $str;
}

function get_uniqueid()    //create unique id's for customers/form-order records
{
  list($usec, $sec) = explode(' ', microtime());
  mt_srand((float) $sec + ((float) $usec * 100000));
  $rand_string = md5(uniqid(mt_rand(), true));
  return $rand_string;
}

function red_star()
{
  $mret='<font color=red size=1>* </font>';
  return $mret;
}


function moreinfo($url, $name, $aclass='')  
{
  // output URL to "more info" window/browser as link with no <br>

	echo  "<a $aclass href=$url target='More Info'>$name</a>";
	return;
}
function last_updated()  // get date of last update of inventory/price files

                         // get date from paper table since may update QOH in invt
						 // between file uploads in future
{
  $conn = db_connect(); // 2/23/2020 php74
	$query = "show table status from store like 'invt'";
    $info = @mysqli_fetch_array(mysqli_query( $conn, $query)); 
    
	return $info["Update_time"];
}

function display_cust_info($cust,$ship,$showlinks=true)
{
	//prints company into on "checkout page" 10/18/02 -ref-
	// middle column, 2nd row:  '.$cust["custno"].'
  if ($showlinks)
    $border=1;
  else
    $border=0;
	echo '<table class=fiber  border='.$border.'><tr align=left><th>Bill-to Address</th><th align=center>&nbsp;---</th><th>Shipping Address</th></tr>';
	echo '<tr><td>'.$cust["name"].'</td><td width=60>&nbsp;</td><td>&nbsp;'.$ship["name"].'</td></tr>';
	echo '<tr><td >'.$cust["firstname"].' '.$cust["lastname"].'</td><td width=60>&nbsp;</td><td>'.$ship["firstname"].' '.$ship["lastname"].'</td></tr>'; 
	echo '<tr><td>'.$cust["address1"].'</td><td width=60>&nbsp;</td><td>'.$ship["address1"].'</td></tr>'; 
	if ($cust["address2"] || $ship["address2"])
		echo '<tr><td>'.$cust["address2"].'</td><td width=60>&nbsp;</td><td>&nbsp;'.$ship["address2"].'</td></tr>'; 
	if ($cust["address3"] || $ship["address3"])
		echo '<tr><td>'.$cust["address3"].'&nbsp;</td><td width=60>&nbsp;</td><td>&nbsp;'.$ship["address3"].'</td></tr>'; 
	echo '<tr><td>'.$cust["city"].'  '.$cust["state"].'   '.$cust["zip"].'</td><td width=60>&nbsp;</td><td>'.$ship["city"].'  '.$ship["state"].'   '.$ship["zip"].'</td></tr>';
	if ($cust["country"] || $ship["country"])
	   echo '<tr><td>'.$cust["country"].'&nbsp;</td><td width=60>&nbsp;</td><td>'.$ship["country"].'&nbsp;</td></tr>';
	echo '<tr><td>'.$cust["phn_area"].'/'.$cust["phn_prefix"].'-'.$cust["phn_suffix"].'</td><td width=60>&nbsp;</td><td>'.$ship["phn_area"].'/'.$ship["phn_prefix"].'-'.$ship["phn_suffix"].'</td></tr>';
	if ($cust["email"] || $ship["email"])
		echo '<tr><td>'.$cust["email"].'</td><td width=60>&nbsp;</td><td>&nbsp;'.$ship["email"].'</td></tr>';
  if ($showlinks)
		echo '<tr><th colspan=3 align=center><a href=cust_scrn_form.php?cust_action=display&goto=final>Change billing address</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=shipaddr.php>Change shipping address</a>';

	echo '</th></tr></table>';


	return true;
}




function get_final_cust_info($cust)
{
	//creates strings
  $mcust='';

  $mcust.="'".$cust["custno"]."','".$cust["name"]."',";
	$mcust.="'".$cust["firstname"]."','".$cust["lastname"]."',";
	$mcust.="'".$cust["address1"]."',";
  $mcust.="'".$cust["address2"]."',";
  $mcust.="'".$cust["address3"]."',";
  $mcust.="'".$cust["city"]."','".$cust["state"]."','".$cust["zip"]."','".$cust["country"]."',";
  $mcust.="'".$cust["phn_area"].'/'.$cust["phn_prefix"].'-'.$cust["phn_suffix"]."',";
  $mcust.="'".$cust["email"]."',";
  $mcust.="'".$cust['taxexemptid']."',";
	$mcust.="'".$cust['terr']."',";
  $mcust.="'".$cust["id"]."'";
  return $mcust;
}
function get_final_ship_info($ship)
{
	//creates strings
 $mship='';
 $mship.="'".$ship["name"]."',";
 $mship.="'".$ship["firstname"]."','".$ship["lastname"]."',";
 $mship.="'".$ship["address1"]."',";
 $mship.="'".$ship["address2"]."',";
 $mship.="'".$ship["address3"]."',";
 $mship.="'".$ship["city"]."','".$ship["state"]."','".$ship["zip"]."','".$ship["country"]."',";
 $mship.="'".$ship["phn_area"].'/'.$ship["phn_prefix"].'-'.$ship["phn_suffix"]."',";
 $mship.="'".$ship["email"]."',";
 if (isset($ship["shipaddrid"]))  //if from shipaddr, get id
    $mship.="'".$ship["shipaddrid"]."'";
 else
    $mship.="''";
 return $mship;
}


function display_billto($cust = "")
{
  // NOT USED 8/17/03
  //Display info at checkout. If in error, user can update profile
	$edit = true;
	$cust_id = $cust["id"];
?>
  <form method=post
        action="<?php echo $edit?"cust_edit.php":"cust_insert.php";?>">
  <B><center>------- Bill-to Information  -------<center></B><br>
  <?php /* if (isset($_SESSION["SESSION_UID"])) echo "&nbsp;&nbsp;<a href=cust_scrn_form.php?cust_action=display&this_id=$cust_id&returnto=checkout>Edit Profile</a>"; */ ?>
  
  <table border=0 align = center bordercolor=red>
  <tr>
    <td><b>Name/Company:</b></td><td><input type=text size=40 maxlength=40 name=name value="<?php  echo $edit?$cust["name"]:""; ?>"READONLY>
	<?php  if (check_admin_user())
		 echo "&nbsp;&nbsp;<b>ID:</b>&nbsp;<input type=text size='8' maxlength='8' name=custno value=".stripslashes($cust["custno"])." READONLY></td>";
	   else
	     echo "</td>";
    ?>

  </tr><tr>
    <td ><b>Contact (F/L Name):</b></td><td NOWRAP><input type=text size=20 maxlength=20 name=firstname value="<?php  echo $edit?$cust["firstname"]:""; ?>"READONLY>
	                 <input type=text size=20 maxlength=20 name=lastname value="<?php  echo $edit?$cust["lastname"]:""; ?>"READONLY></td>
  </tr><tr>
    <td><b>Email:</b></td><td><input type=text size=40 maxlength=60 name=email value="<?php  echo $edit?$cust["email"]:""; ?>"READONLY></td>
  </tr><tr>
    <td><b>Phone:</b></td><td><input type=text size=3 maxlength=3 name=phn_area value="<?php  echo $edit?$cust["phn_area"]:""; ?>"/ READONLY>
				       <input type=text size=3 maxlength=3 name=phn_prefix value="<?php  echo $edit?$cust["phn_prefix"]:""; ?>"- READONLY>
					   <input type=text size=4 maxlength=4 name=phn_suffix value="<?php  echo $edit?$cust["phn_suffix"]:""; ?>" READONLY>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Best time to call:</b>&nbsp;&nbsp;
					   <input type=text size=10 maxlength=10 name=besttime value="<?php  echo $edit?$cust["besttime"]:""; ?>" READONLY></td>
  </tr><tr>
    <td><b>Cell Phone:</b></td><td><input type=text size=3 maxlength=3 name=cell_area value="<?php  echo $edit?$cust["cell_area"]:""; ?>"/ READONLY>
				       <input type=text size=3 maxlength=3 name=cell_prefix value="<?php  echo $edit?$cust["cell_prefix"]:""; ?>"- READONLY>
					   <input type=text size=4 maxlength=4 name=cell_suffix value="<?php  echo $edit?$cust["cell_suffix"]:""; ?>" READONLY></td>					   
<!--
    </tr><tr>
    <td><b>Fax:</b></td><td>  <input type=text size=3 maxlength=3 name=fax_area value="<?php  echo $edit?$cust["fax_area"]:""; ?>"/>
				       <input type=text size=3 maxlength=3 name=fax_prefix value="<?php  echo $edit?$cust["fax_prefix"]:""; ?>"->
					   <input type=text size=4 maxlength=4 name=fax_suffix value="<?php  echo $edit?$cust["fax_suffix"]:""; ?>"></td>
-->				
  </tr><tr>
    <td><b>Bill Address1:</b></td><td><input type=text size=40 maxlength=40 name=address1 value="<?php  echo $edit?$cust["address1"]:""; ?>" READONLY></td>
  </tr><tr>
    <td><b>Bill Address2:</b></td><td><input type=text size=40 maxlength=40 name=address2 value="<?php  echo $edit?$cust["address2"]:""; ?>" READONLY></td>
  </tr><tr>
    <td><b>Bill Address3:</b></td><td><input type=text size=40 maxlength=40 name=address3 value="<?php  echo $edit?$cust["address3"]:""; ?>" READONLY></td>
  </tr><tr>
    <td><b>City:</b></td><td><input type=text size=20 maxlength=20 name=city value="<?php  echo $edit?$cust["city"]:""; ?>" READONLY>
        <b>&nbsp;State:&nbsp;&nbsp;</b><input type=text size=20 maxlength=20 name=state value="<?php  echo $edit?$cust["state"]:""; ?>" READONLY>
        <b>&nbsp;Zip:&nbsp;&nbsp;</b><input type=text size=10 maxlength=10 name=zip value="<?php  echo $edit?$cust["zip"]:""; ?>" READONLY></td>
  </tr><tr>
    <td><b>Country:</b></td><td><input type=text size=20 maxlength=20 name=country value="<?php  echo $edit?$cust["country"]:""; ?>" READONLY></td>
	</tr><tr> 
    <td><b>Tax Exemption #:</b></td><td><input type=text size=40 maxlength=40 name=taxexemptid value="<?php  echo $edit?$cust["taxexemptid"]:""; ?>" READONLY></td>
  </tr><tr><td colspan='2'>&nbsp;<hr></td> 
  </tr></table></form>
<?php
  Return;
} 

function display_shipto($cust = "")
{
  //Display info at checkout. If in error, user can update profile
	$edit = true;
?>
  <form method=post
        action="<?php  echo $edit?"cust_shipto_edit.php":"cust_shipto_insert.php";?>">
  <B><center>------- Ship-to Address 
  <?php 
  IF ($_SESSION['SESSION_SHIPID']== -1)
   echo "same as Bill to Address";
  else 
   echo "  (ID = ". $_SESSION['SESSION_SHIPID'].")"; 
   ?>
-------</center></B><br>
  <table class=shipaddr border=0 align = center bordercolor=red>
  <tr>
    <td><b>Name/Company:</b></td><td><input class=shipaddr type=text size=40 maxlength=40 name=name value="<?php  echo $edit?$cust["name"]:""; ?>">

  </tr><tr>
    <td ><b>Contact (F/L Name):</b></td><td NOWRAP><input class=shipaddr type=text size=20 maxlength=20 name=firstname value="<?php  echo $edit?$cust["firstname"]:""; ?>">
	                 <input class=shipaddr type=text size=20 maxlength=20 name=lastname value="<?php  echo $edit?$cust["lastname"]:""; ?>"></td>
  </tr><tr>
    <td><b>Ship to Address1:</b></td><td><input class=shipaddr type=text size=40 maxlength=40 name=address1 value="<?php  echo $edit?$cust["address1"]:""; ?>" ></td>
  </tr><tr>
    <td><b>Ship to Address2:</b></td><td><input class=shipaddr type=text size=40 maxlength=40 name=address2 value="<?php  echo $edit?$cust["address2"]:""; ?>" ></td>
  </tr><tr>
    <td><b>Ship to Address3:</b></td><td><input class=shipaddr type=text size=40 maxlength=40 name=address3 value="<?php  echo $edit?$cust["address3"]:""; ?>" ></td>
  </tr><tr>
    <td><b>City:</b></td><td><input class=shipaddr type=text size=20 maxlength=20 name=city value="<?php  echo $edit?$cust["city"]:""; ?>" >
        <b>&nbsp;State:&nbsp;&nbsp;</b><input class=shipaddr type=text size=20 maxlength=20 name=state value="<?php  echo $edit?$cust["state"]:""; ?>" >
        <b>&nbsp;Zip:&nbsp;&nbsp;</b><input class=shipaddr type=text size=10 maxlength=10 name=zip value="<?php  echo $edit?$cust["zip"]:""; ?>" ></td>
  </tr><tr>
    <td><b>Country:</b></td><td><input class=shipaddr type=text size=20 maxlength=20 name=country value="<?php  echo $edit?$cust["country"]:""; ?>" >
	&nbsp;</td>
  </tr><tr>
      <td><b>Email:</b></td><td><input class=shipaddr type=text size=40 maxlength=60 name=email value="<?php  echo $edit?$cust["email"]:""; ?>"></td>
  </tr><tr>
    <td><b>Phone:</b></td><td><input class=shipaddr type=text size=3 maxlength=3 name=phn_area value="<?php  echo $edit?$cust["phn_area"]:""; ?>"/ >
				       <input class=shipaddr type=text size=3 maxlength=3 name=phn_prefix value="<?php  echo $edit?$cust["phn_prefix"]:""; ?>"- >
					   <input class=shipaddr type=text size=4 maxlength=4 name=phn_suffix value="<?php  echo $edit?$cust["phn_suffix"]:""; ?>" >&nbsp;&nbsp;&nbsp;
               &nbsp;&nbsp;<input class=shipaddr type='submit' value="<?php  echo $edit?'Update':'Add'; ?> Ship Address"> </td>
		
  </tr><tr>
  <td colspan='2'>&nbsp;<hr></td> 
  </tr></table></form>
<?php
  Return;
  // end cust_shipto()
} 

function display_payment_form($tempnames)
{
  //display the form that asks for payment info  5/19/02 -- interim -- on intranet
  
  if (empty($tempnames['ccno']))
  {
  	// If not initialize, initialize the variables with blanks
	$ponumber = '';
	$ccno = '';
	$ccexpdate = '';
	$ccaddress = '';
	$comment = '';
   }
   else
   {
	$ponumber = $tempnames["ponumber"];
	$ccno = $tempnames["ccno"];				//On intranet !!
	$ccexpdate = $tempnames["ccexpdate"];
	$ccaddress = $tempnames["ccaddress"];
	$comment = $tempnames["comment"];
   }   
  
  
echo "
  <br>
  <table border = 0  cellspacing = 0>
  <form action = savename.php method = post>	
  ";
  //</th>  <tr> <th align=left bgcolor=#cccccc>
echo "<th colspan=2 >".(date ("dS of F Y h:i:s A"))."</th>";

echo  "<tr>
    <td>PO Number</td>
    <td><input type = text name = ponumber value ='". $ponumber."' maxlength='40' size='40'></td>
  </tr>
   <tr>
    <td>CC#</td>
    <td><input type = text name = ccno value ='". $ccno."' size='24' maxlength='24' >
    Expires
    <input type = text name = ccexpdate value ='". $ccexpdate."' maxlength='5' size='5'>
	</td>

  </tr>
   <tr>
    <td>CC Address</td>
    <td><input type = text name = ccaddress value ='". $ccaddress."' maxlength='40' size='40'></td>
  </tr>
  
   <tr>
    <td>Comment</td>
    <td><textarea name = comment rows=2 cols=40 wrap>$comment</textarea> </td>
  </tr>";
  
  echo "<tr>
    <td  align = center>
	<input type=submit value='Save'></td><td align=left>  <u>AND THEN</u>, during Startup, you <b>MUST</b> press the PRINT <br>button on your browser, NOW!! </td> </tr>
    </form>
    </table>";
	
}

function go_showprod()
{
	header("location:showprod.php");
	exit;
}

function say_change_passwd()
{
   br(2);
   echo '<a href=weblogin.php?function=change>Change Password</a>';
  return true;
}

function grayline($text="")
{
  echo "<table width=100%><tr>
          <th bgcolor=#cccccc align=center>&nbsp;$text</th></tr></table>";
return true;
}



function display_cust_form($cust = "",$table_width="100%",$goto='')
// This displays the customer form.
// This form can be used for inserting or editing.
// To insert, don't pass any parameters.  This will set $edit
// to false, and the form will go to cust_insert.php.
// To update, pass an array containing a customer.  The
// form will be displayed with the old data and point to cust_update.php.
// It will also add a "Delete" button for Admin users.
{
  
  // if passed an existing customer and the custno is not '^new^' (adding new record)proceed in "edit mode"
  echo "<table class=profile width=$table_width align=center ><tr><td class=profile>";
  
  $edit = is_array($cust) && $cust['custno']<>'^new^';
/*  if ($edit)
  	echo stripslashes($cust["name"]); //display_space_head(stripslashes($cust["name"]));
  else
	Echo  "Add new customer record";
*/

?>
  
  <table class=profile border=0 align=center bordercolor=red >
   <tr>
    
	<td>
            <form method=post action=weblogin.php?function=change>
            <input class=profilebutton  type='submit' name='change' value='Change Password'>

            </form>
            <!-- <a href=weblogin.php?function=change>Change Password</a> -->


  </td>
	<td align=right>
  <form method=post
        action="<?php  echo $edit?"cust_edit.php":"cust_add.php";?>">

	         <?php
		 	//if (!$edit)
			   // echo "colspan=2>";
            if($edit)
           		echo "<input class=profile type=hidden name=webid
                    value=".$cust['webid'].">";
            echo "<input type='hidden' name='goto' value='$goto'>";
         ?>

    <input class=profilebutton  type='submit' name='<?php  echo $edit?'updatecust':'addcust'; ?>'
               value='<?php  echo $edit?'Update':'Add'; ?> Record'>
    <input class=profilebutton  type='submit' name='cancel' value='Cancel'></td>
			   
  </tr><tr><?php if (!$edit) echo "<td>&nbsp;</td><td colspan=2>If a new Company, fill in Company AND First/Last Names fields</td></tr><tr>"; ?>
    <td><b>Name/Company:</b></td><td><input class=profile type=text size=40 maxlength=40 name=name value="<?php  echo stripslashes($cust["name"]); ?>">
	<?php if (check_admin_user())
		 echo "&nbsp;&nbsp;<b>ID:</b>&nbsp;<input class=profile type=text size='8' maxlength='8' name=custno value='".stripslashes($cust["custno"])."' READONLY></td>";
	   else
	     echo "</td>";
    ?>
  </tr><tr>
    <td NOWRAP><b>Contact (F/L Name):</b></td><td NOWRAP><input class=profile type=text size=20 maxlength=20 name=firstname value="<?php  echo stripslashes($cust["firstname"]); ?>">
	                 <input class=profile type=text size=20 maxlength=20 name=lastname value="<?php  echo stripslashes($cust["lastname"]); ?>">
                    </td>
  </tr><tr>

    <td><b>Bill Address1:</b></td><td><input class=profile type=text size=40 maxlength=40 name=address1 value="<?php  echo stripslashes($cust["address1"]); ?>"> (Set ship address during checkout.)</td>
  </tr><tr>
    <td><b>Bill Address2:</b></td><td><input class=profile type=text size=40 maxlength=40 name=address2 value="<?php  echo stripslashes($cust["address2"]); ?>">   </td>
  </tr><tr>
    <td><b>Bill Address3:</b></td><td><input class=profile type=text size=40 maxlength=40 name=address3 value="<?php  echo stripslashes($cust["address3"]); ?>">  </td>
  </tr><tr>
    <td><b>City:</b></td><td><input class=profile type=text size=20 maxlength=20 name=city value="<?php  echo stripslashes($cust["city"]); ?>">
        <b>&nbsp;State:&nbsp;&nbsp;</b><input class=profile type=text size=20 maxlength=20 name=state value="<?php  echo stripslashes($cust["state"]); ?>">
        <b>&nbsp;Zip:&nbsp;&nbsp;</b><input class=profile type=text size=10 maxlength=10 name=zip value="<?php  echo stripslashes($cust["zip"]); ?>"></td>
  </tr><tr>
    <td><b>Country:</b></td><td><input class=profile type=text size=20 maxlength=20 name=country value="<?php  echo stripslashes($cust["country"]); ?>"></td>
	</tr><tr> 
    <td><b>Phone:</b></td><td>
    <input class=profile type=text onkeyup="moveOnMax(this,'phn_prefix')" size=3 maxlength=3 name=phn_area value='<?php  echo stripslashes($cust["phn_area"]); ?>'/>
	<input  class=profile type=text id="phn_prefix" onkeyup="moveOnMax(this,'phn_suffix')" size=3 maxlength=3 name=phn_prefix value='<?php echo stripslashes($cust["phn_prefix"]);?>'/>
	<input class=profile type=text id="phn_suffix" size=4 maxlength=4 name=phn_suffix value='<?php  echo stripslashes($cust["phn_suffix"]); ?>' >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Best time to call:</b>&nbsp;&nbsp;
					   <input class=profile type=text size=10 maxlength=10 name=besttime value='<?php  echo stripslashes($cust["besttime"]); ?>'></td>
  </tr><tr>
    <td><b>Cell Phone:</b></td><td>
    <input class=profile type=text onkeyup="moveOnMax(this,'cell_prefix')" size=3 maxlength=3 name=cell_area value='<?php  echo $cust["cell_area"]; ?>'/>
	<input class=profile type=text id="cell_prefix" onkeyup="moveOnMax(this,'cell_suffix')" size=3 maxlength=3 name=cell_prefix value='<?php  echo $cust["cell_prefix"]; ?>' />
	<input class=profile type=text id="cell_suffix" size=4 maxlength=4 name=cell_suffix value='<?php  echo $cust["cell_suffix"]; ?>' ></td>					   
					   
  </tr><tr>
    <td><b>Fax:</b></td><td>  
    <input class=profile type=text onkeyup="moveOnMax(this,'fax_prefix')" size=3 maxlength=3 name=fax_area value="<?php  echo stripslashes($cust["fax_area"]); ?>"/>
	<input class=profile type=text id="fax_prefix" onkeyup="moveOnMax(this,'fax_suffix')" size=3 maxlength=3 name=fax_prefix value="<?php  echo stripslashes($cust["fax_prefix"]); ?>"->
	<input class=profile type=text id="fax_suffix" size=4 maxlength=4 name=fax_suffix value="<?php  echo stripslashes($cust["fax_suffix"]); ?>"></td>
  </tr><tr>	
    <td><b>Tax Exemption #:</b></td><td><input class=profile type=text size=40 maxlength=40 name=taxexemptid value="<?php  echo stripslashes($cust["taxexemptid"]); ?>" READONLY> (IN Res - Send Form ST-105)</td>
  </tr><tr>
    <td><b>Email:</b></td><td><input class=profile type=text size=40 maxlength=60 name=email value="<?php  echo stripslashes($cust["email"]); ?>" ></td>
  </tr><tr>
    <td><b>Web Address:</b></td><td><input class=profile type=text size=60 maxlength=80 name=webaddress value="<?php  echo stripslashes($cust["webaddress"]); ?>" ></td>
  </tr><tr>
    <td><b>Web Description:</b></td><td><input class=profile type=text size=60 maxlength=80 name=webdescription value="<?php  echo stripslashes($cust["webdescription"]); ?>"></td>
  </tr><tr>
<!--  <td colspan='2'>&nbsp;<hr></td>  -->
	

<!--
  </tr><tr>
    <td><b>State:</b></td><td><?php  echo $edit?CB_STATES($cust["state"]):CB_STATES(""); ?>
  </td>

  
  </tr><tr>  -->
<?php
     //checkbox values  10/13/02
    if (!isset($cust["enews"]))
		$cust["enews"] = 'off'; 
	
    if (!isset($cust["specials"]) )
		$cust["specials"] = 'off';  
?>
    <td><b>Receive all emails:</b></td><td><input class=profile type=checkbox  name=enews <?php if (stripslashes($cust["enews"])=='on') echo "CHECKED";?> ></td>
  </tr><tr>
    <td><b>Receive specials:</b></td><td><input class=profile type=checkbox name=specials <?php if (stripslashes($cust["specials"])=='on') echo "CHECKED";?>></td>


<!--
    <td><b>Receive all emails:</b></td><td><input type=checkbox  name=enews value=<?php echo $cust["enews"]; ?> ></td>
  </tr><tr>
    <td><b>Receive specials:</b></td><td><input type=checkbox name=specials value=<?php echo $cust["specials"]; ?>></td>
-->
  </tr><tr> 
    
<?php
	if (check_admin_user())
	{ 
	echo "	 
    <td><b>Type:</b></td><td><input class=profile type=text size='10' maxlength='10' name=type value='".stripslashes($cust["type"])."'>
  		<b>&nbsp;Source:&nbsp;&nbsp;</b><input class=profile type=text size='5' maxlength='5' name=source value='".stripslashes($cust["source"])."'>
  		<b>&nbsp;Code:&nbsp;&nbsp;</b><input class=profile type=text size='2' maxlength='2' name=code value='".stripslashes($cust["code"])."'>
		<b>&nbsp;Terms:&nbsp;&nbsp;</b><input class=profile type=text size=20 maxlength=20 name=pterms value='".stripslashes($cust["pterms"])."'></td>
  </tr><tr>
    <td><b>Entered:</b></td><td><input class=profile type=text size=20 maxlength=20 name=entered value='".stripslashes($cust["entered"])."' READONLY></td>
	</tr><tr>
	<td><b>Last Date:</b></td><td><input class=profile type=text size=20 maxlength=20 name=ldate value='".stripslashes($cust["ldate"])."' READONLY>
	
		<b>&nbsp;Last Sale:&nbsp;&nbsp;</b><input class=profile type=text size=20 maxlength=20 name=lsale value='".stripslashes($cust["lsale"])."' READONLY></td>

  </tr><tr>
    <td><b>Notes:</b></td><td><textarea name=notes rows='7' cols='40'>".stripslashes($cust["notes"])."</textarea></td>
	";
	}
	else
	{
	echo "
	<input type=hidden name=type value='".stripslashes($cust["type"])."'>
	<input type=hidden name=source value='".stripslashes($cust["source"])."'>
	<input type=hidden name=code value='".stripslashes($cust["code"])."'>
	<input type=hidden name=pterms value='".stripslashes($cust["pterms"])."'>
	<input type=hidden name=entered value='".stripslashes($cust["entered"])."'>
	<input type=hidden name=ldate value='".stripslashes($cust["ldate"])."'>
	<input type=hidden name=lsale value='".stripslashes($cust["lsale"])."'>
	<input type=hidden name=notes value='".stripslashes($cust["notes"])."'>
	";
	}
	?>
<!--
  </tr><tr>
    <td><b>country:</b></td><td><input type=text size=20 maxlength=20 name=country value="<?php  echo $edit?stripslashes($cust["country"]):""; ?>"></td>
  </tr><tr>
    <td><b>country:</b></td><td><input type=text size=20 maxlength=20 name=country value="<?php  echo $edit?stripslashes($cust["country"]):""; ?>"></td>
-->
  </tr><tr>
	<td>
	         <?php
		 	//if (!$edit)
			   // echo "colspan=2>";
            if($edit)
           		echo "<input type=hidden name=id 
                    value=".$cust["id"].">";
         ?>
	<td align=right>
      <input class=profilebutton type='submit' name='<?php  echo $edit?'updatecust':'addcust'; ?>'
               value='<?php  echo $edit?'Update':'Add'; ?> Record'>
    <input class=profilebutton type='submit' name='cancel' value='Cancel'></td>
        
        <?php
/*           if ($edit && check_admin_user())
           {  
             echo "<td>";
             echo "<form method=post action=\"cust_delete.php\">";
             echo "<input type=hidden name=id 
                    value=\"".$cust["id"]."\">";
             echo "<input type=submit 
                    value=\"Delete record\">";
             echo "</form></td>";
            }
*/          ?>
         </td>
      </tr>
  </table>
  </form>
  <td></tr></table>
  </center>

<?php
//br();
//do_html_footer();

}


function get_shipto()
{
	// 8/25/03 $id = $_SESSION["SESSION_UID"];
	$webid = $_SESSION["SESSION_UACCT"];    // Was custno, 8/25/03 now matches on webid
	$shipto = false;
	$shipid = 0 ;  
	
	$shipaddr = get_shipaddresses($webid);
	$num_rows = mysqli_num_rows($shipaddr);
	//echo $custno ."  get_shipto() rows : ".$num_rows;
	
	//if there is one or more shipaddr in the file for this customer

	if ($num_rows > 0)
	{
		// There is one or more ship-to addresses
		
/*			if ($num_rows == 1)
			{
				//get the one...
		 	 	$shipto = @mysqlx_fetch_array($shipaddr);
		   		$shipid = $shipto["shipaddrid"];
			}
			else
*/			{
				//Display the list and select one...
				display_space_head("$num_rows Ship Addresses");
				echo "<FORM METHOD=POST ACTION=set_shipid.php>";
				echo "<input class=shipaddr type=submit name=Go value=Select>";
				echo "<input class=shipaddr type=submit name=delete value=Delete>";

				echo "<table border=1 align=center>";
		  		for ($i=0; $i <$num_rows; $i++)
		  		{
     				$row = mysqli_fetch_array($shipaddr);
					$phn = stripslashes($row["phn_area"])."/"
						.stripslashes($row["phn_prefix"])."-"
						.stripslashes($row["phn_suffix"]);
				
	    			echo  "<tr><td class=shipaddr>";
				  	echo  "<input type=checkbox name=cb[$i] value="
	   					.stripslashes($row["shipaddrid"])."></td><td class=shipaddr>";
	    	  	echo   stripslashes(trim($row["name"])).' '.stripslashes(trim($row["firstname"]).' '.trim($row["lastname"]));
              
            echo "</td><td class=shipaddr>&nbsp;"
     				    .stripslashes($row["address1"])."</td><td class=shipaddr>&nbsp;"
     					.stripslashes($row["address2"])."</td><td class=shipaddr>"
						.stripslashes($row["city"])."</td><td class=shipaddr>&nbsp;"
						.stripslashes($row["state"])."</td><td class=shipaddr>&nbsp;$phn</td></tr>";										
  				}
				echo "</table>";
				echo "<input class=shipaddr type=submit name=Go value=Select>";
				echo "<input class=shipaddr type=submit name=delete value=Delete>";
				echo "</form>";
		}
	}	
	else
	{
		//get_shipaddresses() returned no rows  so set $shipdid -1  to force using billto address
		br();
		display_space_head("Shipping Address same as Billing Address");

		//$shipid = -1;
		//header("location:set_shipid.php?s_addr=".$shipid);
    	//exit;
		
	}
	br();
	do_html_url("set_shipid.php?s_addr=-1", "Use Bill-to Address");

	br();
	do_html_url("shipaddr.php?action=add", "Add a new Ship Address");
	br(2);

	//header("location:set_shipid.php?s_addr=".$shipid);
    //exit;
}
	
	
function get_ziplookup()	
{
		echo "<form action = ziplookup.php method = post>";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;Add new contact whose 5-digit zipcode is:&nbsp;&nbsp;";
		echo "<input type=text size='5' maxlenght='5' name=this_zip value=''>";
		echo "<input type=submit value=Go>";
		echo "</form><br> <br> ";
		return;
}

function get_addcustzip($zip)	
{
		echo "<form action = cust_add.php?this_zip method = get>";
		echo "<p class=intro><b>Add</b> new contact whose 5-digit zipcode is:&nbsp;&nbsp;";
		echo "<input type=text size='5' maxlenght='5' name=this_zip value=$zip>";
		echo "<input type=submit name=submit value=Go>";
		echo "</form></p> ";
		return;
}

function get_shipaddresses($custid)
{
  //gets record from customers file
   
  $custid = trim($custid);
  
  if (!$custid || $custid=="")
     return false;

   $conn = db_connect();
   $query = "select * from shipaddr where webid = '".$custid."'";
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   return $result;
}

function msgcontinue()
{
  if (isset($_SESSION['SECTION_MAINFILE']))
    $goto = $_SESSION['SECTION_MAINFILE'];
  else
    $goto = 'showprod.php';
  $mret = "<br><br><a href=$goto>Continue</a>";
  return $mret;
}


function weblogon($returnto='')
{ 
  // 4/25/2017 for New User, changed from
  // authenticate.php to register.php directly.
  echo "<table class=login border=0 >
  		<form action=register.php method=POST>
		<input type=hidden name=gotouri value='$returnto'>
        <tr><td class=login>New? Register =></td>
		<td class=login align=left> 
        <input class=login type=submit	name=submit 
        value='New User'>
        </form>
        </td> </tr>
        <tr>
		<td class=login colspan=2 align=center><br />
        <font color=red><b>Registered users login below</b>
        </font></td></tr>
        <tr>
        <form action=authenticate.php method=POST>
		<input type=hidden name=gotouri value='$returnto'>
		<td class=login><br />&nbsp;&nbsp;User Name</td>			
		<td class=login align=left><input type=text size=16
         name=f_user maxlength=16 autofocus></td>
		</tr><tr>
		<td class=login><br />&nbsp;&nbsp;Password</td>
		<td class=login><input type=password size=16 
        name=f_pass maxlength=16></td>
		</tr><tr>
		<td class=login colspan=2 align=center><br />	
		<input class=login type=submit	
        name=submit value='Log In'>
        </form>

		<form action='weblogin.php?function=forgot' 
        method=POST><br />
		<input class=login type=submit	
        name=submit value='Forgot password' /> 
        </form>
						
		<form action='weblogin.php?function=user' 
        method=POST><br />
		<input class=login type=submit	name=submit
         value='Forgot username' /> 
        </form>                  
        </td> </tr>
		</table>";
	return true;
}


function webnewuser($returnto='')
{
 	echo "<table class=login border=0 >
						<form action=webnewuser.php method=POST>
						<input type=hidden name=gotouri value='$returnto'>
						<tr>
						<td class=login>Email Address</td>			
						<td class=login align=left><input type=text size=16 name=f_user></td>
						</tr><tr>
						<td class=login>Password</td>
						<td class=login><input type=password size=16 name=f_pass></td>
						</tr><tr>
						<tr>
						<td class=login>Password</td>
						<td class=login><input type=password size=16 name=f_pass2></td>
						</tr>
						<td class=login colspan=2 align=center>		
						<input class=login type=submit	name=submit value='Log In'></td> </tr> 
						</form> </table>";
	
	
	return true;
}



function initialize_blank_profile_array()
{
   //$date = new DateTime("now");
   $todayYmd = date("Y-m-d");  //$date->format("Y-m-d");
	$new_cust = array('id'=>0,
		  		   'custno'=>'^new^',
	               'name'=>'',
				   'firstname'=>'',
				   'lastname'=>'',
				   'email'=>'',
				   'webaddress'=>'',
				   'webdescription'=>'',
				   'phn_area'=>'',
				   'phn_prefix'=>'',
				   'phn_suffix'=>'',
				   'besttime'=>'',
				   'cell_area'=>'',
				   'cell_prefix'=>'',
				   'cell_suffix'=>'',
				   'fax_area'=>'',
				   'fax_prefix'=>'',
				   'fax_suffix'=>'',
				   'address1'=>'',
				   'address2'=>'',
				   'address3'=>'',
				   'city'=>'',
				   'state'=>'',
				   'country'=>'USA',
				   'taxexemptid'=>'',
				   'enews'=>'on',
				   'specials'=>'off',
				   'type'=>'',
				   'source'=>'',
				   'code'=>'',
				   'pterms'=>'PREPAY',
				   'entered'=>$todayYmd,
				   'ldate'=>'',
				   'lsale'=>0.00,
				   'notes'=>'',
				   'webid'=>''
				   );
	return $new_cust;
}

function get_custarray($custid)  //,$fld='')
{
  //gets record from customers file
  // 8/25/03 *** changed id to webid
  $custid = trim($custid);

  if (!$custid || $custid=="")
     return false;

   $conn = db_connect();
   
   //When called from cust_scrn_form.php we might have wanted to search on id,
   //At other times we seek the record based on webid (default)

   //if ($fld)
   // $query = "select * from customers where id = '".$custid."'";
   //else
    $query = "select * from customers where webid = '".$custid."'";
    
   $result = mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $thisresult = @mysqli_fetch_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   return $thisresult;
}


function get_shiparray($id)
{
  //get records from shipaddr file

   $conn = db_connect();
   $query = "select * from shipaddr where shipaddrid = $id ";
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $result = @mysqli_fetch_array($result);
   return $result;
}

function delete_shipaddress($id)
{
  //deletes record from shipaddr file

   $conn = db_connect();
   $query = "delete FROM shipaddr where shipaddrid =". $id;
   $result = @mysqli_query( $conn, $query);
   return $result;
}


function display_space($catid)
{
  //Main Page Display Space
      //echo get_category_text($catid);  // this need to be hooked to url on category header text
      // Now, we are going to get/show groups or else SHOW items from the invt data file for catid.
      /*
      $group_array = get_groups($catid);
      //echo "zz".$group_array[2]['grpname'];
      if ($group_array)
       {
        //show groups url at top of table on rows of five
        $grptblcolor = "#e0e0e0";
        show_groups($catid,$group_array,$grptblcolor);
        echo "<br><hr><center>";
	    $invt_array = get_invt($catid,$grpid);
	    display_invt($invt_array);
        echo "</center>";
      }
      else
      {
      */
	 // Display either a price-break or other list or simple 'invt' list to order from
	 
	 switch($_SESSION['catid'])
  			{
			 case "home":
			   {
			   	  if (isset($_SESSION["SESSION_UNAME"]))  //if not working w an customer..
				  	echo "<br>Twinrocker has been making paper since 1971. We began supplying artists with supplies in 1973. It's our rule that we only sell supplies that we actually use in our own papermaking.
            This means the materials and techniques we recommend to our customers are constantly and thoroughly tested. Twinrocker offers you solid information to help you choose and use our quality papermaking supplies.";
			      else
				  {
				  	br();
				  	get_customer();

				    //Update the last time inventory (prices) was updated.First
					$last_file_update = last_updated();
					$_SESSION["last_file_update"] = $last_file_update;

				  }
				  
				  echo "<br><center><IMG SRC='graphics/long/SupplyHome/home2_on.gif' width=70 height=70></center>";
			      //echo "<br><br><center>&nbsp". randimg('images/bookarts')."</center><br><br>";
 				  break;
			   }

			 case "logon":
			   {
			   	//if already logged in, logout first...
				  display_space_head("Log In");
				  
			      echo "<br><br><center><table border=0>
						<form action=authenticate.php method=POST>
						<tr>
						<td>Username</td>
						<td><input type=text size=16 name=f_user></td>
						</tr><tr>
						<td>Password</td>
						<td><input type=password size=16 name=f_pass></td>
						</tr><tr>
						<td colspan=2 align=center><input type=submit
						name=submit value='Log In'></td> </tr> </form> </table></center>";
						
						//<td><input type=checkbox name='cbadd' value='add'>Add Salesperson</td>
						//</tr><tr>
						
				    //Update the last time inventory (prices) was updated.First
					$last_file_update = last_updated();
					$_SESSION["last_file_update"] = $last_file_update;

 
 				  break;
			   }

    		 case "FIO" :
			    {
					
					//header("location:fiber.php?catid=$catid&clearfiber=false");
					break;
				}
  
  			default:
			    //Get items from regular inventory (invt)
				display_cat_head($catid);
  				$invt_array = get_invt($catid);
  				display_invt($invt_array);	
				echo "<hr>";
  				break;
  		   }
	    
		//$invt_array = get_invt($catid);  //,$grpid
	    //display_invt($invt_array);
        // echo "<a href=$PHPSELF"."index.php?catid=$catid>Top</a>";
        echo "</center>";
      //}
   
	  
}

function center($value)
{
	echo '<center>';
	$value;
	echo '</center>';
}	

function display_cat_head($catid)
{
	//small_menu();
	echo "<center><h3>".get_category_head($catid)."</h3></center>";
	echo "<hr><center>";
}

function display_space_head($value,$line='<hr class=70>')
{
	//small_menu();
	echo "<center><h3>".$value."</h3>$line";
}

function partial_cat_list($arrayname,$start,$end,$tblborder,$tblcolor)
// make a partial list of category URL's  in a table
{    
   echo "<table border = $tblborder > 
        <tr bgcolor = '$tblcolor' >
		<td>";   //<b>Pick a Category</b><br>
        
	 //echo "<ul>";
	for ($j = $start; $j < $end; $j++ )
	{
		 //echo 'xx'.$arrayname[$j]['catid'].'   '.$arrayname[$j]['catname'];
		  $url = "index.php?catid=".($arrayname[$j]['catid']); //
          $title = (trim($arrayname[$j]['catname'])); 
		  
		  
		  // EXCEPTIONS Selection pages:  Paper; Fiber, Pul, Stationery..
		  
		  SWITCH ($arrayname[$j]['catid'])
		  {
				case 'FIO':
					{
						$url = "fiber.php?catid=FIO&clearfiber=false";  // 3/12/02}
						break;
					}
				case 'PAO':
					{	
						$url = "paper.php?catid=PAO";  // paper 7/6/02
						break;
					}
				case 'STO':
					{	
						$url = "station.php?catid=STO";  // Stationery 7/18/02
						break;
					}			
				case 'PUO':
					{	
						$url = "pulp.php?catid=PUO";  // Pulp 7/23/02
						break;
					}		
				case 'COO':
					{	
						$url = "add_codes.php?catid=COO";  // Add codes and qty form  10/10/02
						break;
					}					
								
							
		}
		  
          do_html_url_no_br($url, $title); 
  		  if ($j <> $end)
   	        echo "<br>";
	}    
    //echo "</ul>";
	echo "</td></tr></table>";
	return;
}

function add_qty_to_cart($item,$qty)
{
	// Adds qty of one item to the cart  1/6/03	
	if ($qty > 0)
	{	
		$xcart = $_SESSION["cart"];
		
		// Pass the itemno. If item is in array, increment qty, 
		// else add item and qty=1 to array.
		for ($i=0; $i < $qty; $i++)
		{
			if(@$xcart[$item])
      			$xcart[$item]++;
		    else 
		      $xcart[$item] = 1;
		}
    
 
		$cart = $xcart;
	
		$_SESSION['total_price'] = calculate_price($xcart);  //_SESSION["
	   $_SESSION['total_weight'] = calculate_weight($xcart);
      $_SESSION['items'] = calculate_items($xcart);
	}
return true;

}




function add_items_to_cart($item,$price,$catid)
{
     //echo "http://".$_SERVER["SERVER_ADDR"].urlencode($_SERVER["REQUEST_URI"]);
	  
    $buy = "add2_cart.php?new=$item&catid=$catid&gotouri=".urlencode($_SERVER["REQUEST_URI"]);
	  $title = number_format($price, 2);
	  $mret = "<a href=$buy>$title</a>";
	  Return $mret;
	  
}

function show_link_add_paperform($item,$price,$onhand,$descrip)
{
    $title = number_format($price, 2);
    $buy = "pa_add_one_form.php?itemno=$item&price=".urlencode($title)."&onhand=$onhand&descrip=".urlencode($descrip)."&gotouri=".urlencode($_SERVER["REQUEST_URI"]);

    $mret = "<a href=$buy>$title</a>";
	  Return $mret;

}

function add_items_to_wishlist($item,$price,$catid)
{
     //echo "http://".$_SERVER["SERVER_ADDR"].urlencode($_SERVER["REQUEST_URI"]);
	  
    $buy = "add2_cart.php?new=$item&catid=$catid&gotouri=http://".$_SERVER["SERVER_ADDR"].urlencode($_SERVER["REQUEST_URI"]);
	  $title = number_format($price, 2);
	  $mret = "<a href=$buy>Wish</a>";
	  Return $mret;
	  
}

function silk_info($item)
{
	// Disabled 1/4/03 -ref-
	
	//$moreinfo  = "<a title='More Info' href=moreinfo.php?item=$item  target=moreinfo.php><font size=2> Moreinfo </font></a>";
	//echo $moreinfo;
	return true;
}

function add_one_item_to_cart_bo($item,$price,$catid,$anchor,$target)
{
     //For Bo Leaves only, called from st_main.php   Hardcoded for now...

      $buy = "add2_cart.php?new=$item&catid=$catid&gotouri=st_main.php?dept=Leaves+&+Liners&grp=Bo+Leaves&prod=Bo+Leaves#boleaves";
	  //echo $buy;

	  $title = number_format($price, 2);
	  $mret = "<a href=$buy>$$title</a>";
	  Return $mret;

}



function add_one_item_to_cart($item,$price,$catid,$anchor,$target)
{
    //echo "http://".$_SERVER["SERVER_ADDR"].urlencode($_SERVER["REQUEST_URI"]);

    //Remove any anchor from $target
    $returnto = $_SERVER["REQUEST_URI"];
    If (strpos($returnto,'#')>0)
    {
      $frag =explode('#',$returnto);
      $returnto =$frag[0];
    }

    //echo $returnto;

    $buy = "add2_cart.php?new=$item&catid=$catid&gotouri=".urlencode($returnto."#$anchor");

	  //echo $buy;
	  
	  // If in Books, add 'Add2cart' to price link
	  if ($catid == 'BK' )
	  	  $title = number_format($price, 2).' Add2cart';
    else
    	  $title = number_format($price, 2);
    	  
	  $mret = "<a href=$buy>$$title</a>";
	  Return $mret;
	  
}

function stat_price($stat,$num_styles,$target,$anchor='')   //stationery price list called from: st_pricelist.php 12/22/03
{
  //Gets row $n from $stat array
  for ($n=0 ; $n < $num_styles ; $n++)
  {
    if  ($stat[$n]['stylecode']==$target)
      break;
  }
  
  if ($n == $num_styles)  //Didn't find
    $mret = 'Style not found.';
  else
  {
  // 12/28/03 - some colors are NOT made. This is indicated by the 'stocked" field.
  // Typically, this will be '1234' -- all four colors.. but, it can be anything.
  // Here we check the string and set the cell(s) to show or not.
  $mstocked = $stat[$n]['stocked'];
  $cols = strlen($mstocked)-1;

  // str_pad($stat[$n]['simo'],6,'-',STR_PAD_LEFT) could be used to aproxx equal width boxes
  $mret = "<table border=1>";
  $mret .="<tr ><td align=center width=10><font size='2'>$".$stat[$n]['price']."</font>";
  if (stristr($mstocked,'1'))
    $mret .= "</td><td nowrap  align=right valign=Middle width=10 bgcolor='#ffffff'><font size='1'>".$stat[$n]['whap']."</font>";
  if (stristr($mstocked,'2'))
    $mret .= "</td><td nowrap  align=right valign=Middle width=10 bgcolor='#e0e0e0'><font size='1'>".$stat[$n]['whsc']."</font>";
  if (stristr($mstocked,'3'))
    $mret .= "</td><td nowrap  align=right valign=Middle width=10 bgcolor='#ffffcc'><font size='1'>".$stat[$n]['crec']."</font>";
    // was #ffe7c6
  if (stristr($mstocked,'4'))
    $mret .= "</td><td nowrap  align=right valign=Middle width=10 bgcolor='#99cc99'><font size='1'>".$stat[$n]['simo']."</font>";

  // 01/20/2007 Revise below and in st_add2_cart.php to pass and check if order exceeds inventory onhand (in stationery table)
    $mret .= "</td>
          <form method=post  action=st_add2_cart.php onSubmit=\"return validstatform(this)\">
          <input type=hidden name=anchor value='".$anchor."'>
          <input type=hidden name=stylecode value='".$stat[$n]['stylecode']."'>
          <input type=hidden name=price value='".$stat[$n]['price']."'>
          <input type=hidden name=invt1 value='".$stat[$n]['whap']."'>
          <input type=hidden name=invt2 value='".$stat[$n]['whsc']."'>
          <input type=hidden name=invt3 value='".$stat[$n]['crec']."'>
          <input type=hidden name=invt4 value='".$stat[$n]['simo']."'>
          <td class=st_order_form_left valign=middle>";
          
  // Was a <select style='font:8pt Arial' name=color size=1>
  
  if (stristr($mstocked,'1'))
    $mret .= "<input type=radio name=stcolor value='WHAP' CHECKED>White<br />";
  if (stristr($mstocked,'2'))
    $mret .= "<input type=radio name=stcolor value='WHSC'><NOBR>White w/ Silver</NOBR><br />";
  if (stristr($mstocked,'3'))
    $mret .= "<input type=radio name=stcolor value='CREC'>Cream<br />";
  if (stristr($mstocked,'4'))
    $mret .= "<input type=radio name=stcolor value='SIMO'><NOBR>Simon Green</NOBR><br />";

    $mret .= "
          </td><td class=st_order_form valign=Middle><input style='font:8pt Arial' type=text name=qty value=0 size=3 maxlength=3>
          </td><td class=st_order_form valign=Middle><br><input style='font:8pt Arial' type=submit name=submit value='Add'>
          </form>
          </td></tr></table>";
  }
  return $mret;
}

function stat_price_only($stat,$num_styles,$target,$anchor='')   //stationery price list called from: st_pricelist.php 12/22/03
{
  //12/21/06 -ref- revised prior function to only show Price, not invt qty and order form 
  //Gets row $n from $stat array
  for ($n=0 ; $n < $num_styles ; $n++)
  {
    if  ($stat[$n]['stylecode']==$target)
      break;
  }
  
  if ($n == $num_styles)  //Didn't find
    $mret = 'Style not found.';
  else
  {
  $mret = "<font size='2'>$".$stat[$n]['price']."</font>";
  }
  return $mret;
}

function stationery_price($style,$anchor='')   //stationery price list called from: st_pricelist.html 12/20/03  Displays the stationery  ordering form for each style.
{
  //This developed 1st ?? Took repeated db calls to fill price list --
  //switched to stat_price (above)  for the st_pricelist which makes calls to array $stat.
  //This routine called from within html in product records of various types of stationery.
  // 1/26/2007 -ref- revised to add js chk of qty > onhand
  
  // Get the  (record) as an array from the stationery table for this one style.
  $thisstyle = get_one_style($style);
    
  // 12/28/03 - some colors are NOT made. This is indicated by the 'stocked" field.
  // Typically, this will be '1234' -- all four colors.. but, it can be anything.
  // Here we check the string and set the cell(s) to show or not.
  $mstocked = $thisstyle[0]['stocked'];
  $cols = strlen($mstocked)-1;
  // str_pad($thisstyle[0]['simo'],6,'-',STR_PAD_LEFT) could be used to aprox equal width boxes
  $mret = "<table border=1><tr><td align=center width=10><font size='1'><font color='white'>$".$thisstyle[0]['price']."</font></font>";
  if (stristr($mstocked,'1'))
    $mret .= "</td><td width=10 align=right width=10 bgcolor='#ffffff'><font size='1'>".$thisstyle[0]['whap']."</font>";
  if (stristr($mstocked,'2'))
    $mret .= "</td><td width=10 align=right width=10 bgcolor='#e0e0e0'><font size='1'>".$thisstyle[0]['whsc']."</font>";
  if (stristr($mstocked,'3'))
    $mret .= "</td><td width=10 align=right width=10 bgcolor='#ffffcc'><font size='1'>".$thisstyle[0]['crec']."</font>";
    // peachy 1/27/2007  for cream (just above)   #ffe7c6  ; #e9e0e0
  if (stristr($mstocked,'4'))
    $mret .= "</td><td width=10 align=right width=10 bgcolor='#99cc99'><font size='1'>".$thisstyle[0]['simo']."</font>";
    // #b5bd31 
    
    // Create form and pass vars to js...
    // 7/25/2015 -- remove js :  onSubmit=\"return validstatform2(this)\"  from <form ..> just below. Qty check done in st_add2_cart.php
    
    $mret .= "</td>
          </tr><tr>
          <td valign=middle colspan=$cols>
          <form method=post action=st_add2_cart.php >
          <input type=hidden name=anchor value='".$anchor."'>
          <input type=hidden name=stylecode value='".$thisstyle[0]['stylecode']."'>
          <input type=hidden name=price value='".$thisstyle[0]['price']."'>
          <input type=hidden name=invt1 value='".$thisstyle[0]['whap']."'>
          <input type=hidden name=invt2 value='".$thisstyle[0]['whsc']."'>
          <input type=hidden name=invt3 value='".$thisstyle[0]['crec']."'>
          <input type=hidden name=invt4 value='".$thisstyle[0]['simo']."'>
          <select style='font:8pt Arial' name=stcolor size=1>";
  if (stristr($mstocked,'1'))
    $mret .= "<option selected value='WHAP'>White</option>";
  if (stristr($mstocked,'2'))
    $mret .= "<option value='WHSC'>White w/ Silver</option>";
  if (stristr($mstocked,'3'))
    $mret .= "<option value='CREC'>Cream</option>";
  if (stristr($mstocked,'4'))
    $mret .= "<option value='SIMO'>Simon Green</option>";
    
    $mret .= "</select>
          </td><td valign=middle><input style='font:8pt Arial' type=text name=qty value=0 size=6 maxlength=6>
          </td><td valign=middle><input style='font:7pt Arial' type=submit value='Add'>
          </form>
          </td></tr></table>";

  return $mret;
}

function stationery_price_example($style,$anchor='')      //stationery price list called from: st_pricelist.html 12/20/03
{
  //This developed 1st. Takes repeated db calls to fill price list --
  //switched to stat_price (above) which makes calls to array $stat.
  // Get the array (record) from the stationery table for this one style.
  $thisstyle = get_one_style($style);

  // 12/28/03 - some colors are NOT made. This is indicated by the 'stocked" field.
  // Typically, this will be '1234' -- all four colors.. but, it can be anything.
  // Here we check the string and set the cell(s) to show or not.
  $mstocked = $thisstyle[0]['stocked'];
  $cols = strlen($mstocked)-1;
  // str_pad($thisstyle[0]['simo'],6,'-',STR_PAD_LEFT) could be used to aproxx equal width boxes
  $mret = "<table style='font:8pt Arial' border=1 bgcolor=silver>
           <tr><td align=center colspan=7><b>How to interpret and use the stationery order form </td></tr>
           <tr><td align=right nowrap> Price, each  >> </td><td align=center width=10 bgcolor=#8a8987><font size='1'>$".$thisstyle[0]['price']."</font>";

  
  if (stristr($mstocked,'1'))
    $mret .= "</td><td width=10 align=right  bgcolor='#ffffff'><font size='1'>".$thisstyle[0]['whap']."</font>";
  if (stristr($mstocked,'2'))
    $mret .= "</td><td width=10 align=right  bgcolor='#e0e0e0'><font size='1'>".$thisstyle[0]['whsc']."</font>";
  if (stristr($mstocked,'3'))
    $mret .= "</td><td width=10 align=right  bgcolor='#ffffcc'><font size='1'>".$thisstyle[0]['crec']."</font>";
  if (stristr($mstocked,'4'))
    $mret .= "</td><td width=10 align=right  bgcolor='#99cc99'><font size='1'>".$thisstyle[0]['simo']."</font>
              <td align=left><< Approx. quantities onhand of up to 4 stocked colors:
                             White, White w/ Silver Thread, Cream, and Simon's Green</td>";

    $mret .= "</td>
          </tr><tr><td align=right nowrap> Order form  >> </td>
          <td valign=middle colspan=$cols>
          <form method=post action=".$_SESSION['SECTION_MAINFILE'].">
          <input type=hidden name=anchor value='".$anchor."'>
          <input type=hidden name=style value='".$thisstyle[0]['stylecode']."'>
          <input type=hidden name=price value='".$thisstyle[0]['price']."'>
          <select style='font:8pt Arial' name=color size=1>";
  if (stristr($mstocked,'1'))
    $mret .= "<option selected value='WHAP'>White</option>";
  if (stristr($mstocked,'2'))
    $mret .= "<option value='WHSC'>White w/ Silver</option>";
  if (stristr($mstocked,'3'))
    $mret .= "<option value='CREC'>Cream</option>";
  if (stristr($mstocked,'4'))
    $mret .= "<option value='SIMO'>Simon Green</option>";

    $mret .= "</select>
          </td><td valign=middle><input style='font:8pt Arial' type=text name=qty value=0 size=6 maxlength=6>
          </td><td valign=middle><input style='font:7pt Arial' type=submit value='Add' DISABLE><td align=left><< Select color, Enter Qty, and press 'Add'</td>
          </form>
          </td></tr></table><br>";

  return $mret;
}


function get_one_style($style)
{
  $conn = db_connect();
   $query = "select * from stationery where stylecode='$style'";
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   if ($num_invt ==0)
      return false;
   $thisitem = db_result_to_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   return $thisitem;
}

function get_all_styles()
{
  $conn = db_connect();
   $query = "select * from stationery";
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   if ($num_invt ==0)
      return false;
   $thisitem = db_result_to_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   return $thisitem;
}

function silk_buy($item,$anchor="",$target="")
{
	// 9/14/02 -ref-;  12/23/02
	// look up info on item number
	$thisitem = get_one_item($item);  // Now have price, weight, etc..
	
    // 2/22/2021 Add chk for onhand > 0
    /* 3/1/2021 -- Joni only wants 
      chk on qty for  CSZ and pulp samples
      
      in_array(substr($item,0,2),
      ["CSZ","S87","S29","SCT"])
      
    if (not found item OR (in array and qty<=0)
    then only return the itemno
    else
    build link and return
    
    per phone call: undo all qty checking
    so commented code below
    */
    //$chk_these = ["CSZ","S87","S29","SCT"];
 
    IF (!$thisitem /* || 
      (in_array(strtoupper(substr($item,0,3)),
      $chk_these) &&
      ($thisitem[0]["onhand"] - 
          $thisitem[0]["aloc"] <= 0)) */ )
        {return $item; }
    else
	{
  	    // create link(s) to add_to_cart first to silk cart  one unit/click
		$row = 0;  //  s/b always on the first (0th row) since there s/b only on row
		$priceurl = add_one_item_to_cart($thisitem[$row]['item'],$thisitem[$row]['price'],$thisitem[$row]['catid'],$anchor,$target);
	
        return $priceurl;
	}
} // end of silk_buy fnc	

function silk_buy_form($item,$anchor="")
{
	// 12/17/2006 -ref- Created to add many to cart from form9/14/02 -ref-;  12/23/02 ; Created qty/add form 12/8/06
	// look up info on item number
	$thisitem = get_one_item($item);  // Now have price, weight, etc..
    if ($thisitem /* && 
         ($thisitem[0]["onhand"] - 
          $thisitem[0]["aloc"] > 0)*/ )
	{
	  //Remove any anchor from $target
    $returnto = $_SERVER["REQUEST_URI"];
    if (strpos($returnto,'#')>0)
    {
      $frag =explode('#',$returnto);
      $returnto =$frag[0];
    }
    if ($anchor)
    {
      $returnto = $returnto.'#'.$anchor;
    }

  	// Create the "Buy" form: 
		$row = 0;  //  s/b always on the first (0th row) since there s/b only on row
		$mret = '<form action="add_many.php" method="POST">
	           <table  border="0"   >
	           <tr><td class="silk_buy">$ '.$thisitem[$row]['price'].' each. Qty:   
	             <input  type="text" size="3" maxlength="3" name="qty" value=>
	             <input style="font:8pt Arial" type="submit" value="Buy">
	             <input type="hidden" name=item value="'.$thisitem[$row]["item"].'">
	             <input type="hidden" name=gotouri value="'.urlencode($returnto).'">
	           </td></tr></table>
	           </form>';
	}
	else
		$mret = $item;  // if not found, print out the "bad" item number
	
  return $mret;
}	




function silk_buy_bo($item,$anchor="",$target="")
{
	// 12/28/03 BO LEAVES ONLY -ref-

	$thisitem = get_one_item($item);  // Now have price, weight, etc..

	IF ($thisitem && 
         ($thisitem[0]["onhand"] - 
          $thisitem[0]["aloc"] > 0) )
	{
		// create link(s) to add_to_cart first to silk cart  one unit/click
		$row = 0;  //  s/b always on the first (0th row) since there s/b only on row
		$priceurl = add_one_item_to_cart_bo($thisitem[$row]['item'],$thisitem[$row]['price'],$thisitem[$row]['catid'],$anchor,$target);
		$mret = $priceurl;
	}
	else
		$mret = $item;  // if not found, print out the "bad" item number
	return $mret;
}

function npspace($n)
{
	$mret = '';
	for ($i=0;$i<$n;$i++)
	{
		$mret .= '&nbsp;';
	}
	return  $mret;
}

function silk_show($item)
{
	// 2/6/03 SHOW the price, not as a link
	// look up info on item number
	$thisitem = get_one_item($item);  // Now have price, weight, etc..

	IF ($thisitem)
	{
		$row = 0;  //  s/b always on the first (0th row) since there s/b only on row
		$mret = '$'.$thisitem[$row]['price'];
		//echo $mret;
	}
	else
		$mret = $item;  // if not found, print out the "bad" item number
	return $mret;
}	

function find_item()
{
	if (isset($_GET['catid']))
		$catid = $_GET['catid'];
		
	$this_string = $_SESSION['searchstr'];
	?>
	
	<form action=finditem.php method=get>
	<br>
	<table class=search border=0  width=30 >
	<tr><th align+center><?php do_html_URL_no_br('help.php#search','Focus'); ?></th></tr>
	<tr><td>
	<input style="font-size=1" type=text size=16 name=searchstr value='<?php echo $this_string; ?>'>
	<input style="font-size=1" type=checkbox name=searchsite 
	<?php 	if (!isset($catid) || $catid=='home') echo 'CHECKED';?>>All
	<input style="font-size=1" type=submit value='Find,Item;'>
	<input type=hidden name=catid value='<?php echo $catid;?>'>
	<input type=hidden name=calledfrom value='<?php echo $_SERVER["REQUEST_URI"];?>'>
	</td></tr></table>
	</form>
	<?php
	//$filename = $_SERVER["REQUEST_URI"].'---'.$_SERVER["PHP_SELF"].'..'.$catid;
	//echo $filename;
}

function small_menu($size=1,$align='center')
{
if ($align=='center')
	echo '<center>';
	
  echo "<font size=$size>";
?>
  <a href=index.php?catid=home>Items</a> |
  <a href=pulp.php?catid=home>Pulp</a> |
  <a href=fiber.php?catid=FIO&clearfiber=false>Fiber</a> |
  <a href=paper.php?catid=PAO>Paper</a> |
  <a href=station.php?catid=STO>Stationery</a> |
  <a href=add_codes.php>Codes</a> |
  <a  HREF=show_cart.php?catid=home>Cart</a> | 
  <a  HREF=https://birch.phpwebhosting.com/~refrost/twinrocker/checkout.php?<?php echo session_name()."=".session_id()?>&catid=home>Checkout</a> 
  <!-- <a  HREF=checkout.php?catid=home>Checkout</a> -->


  </font>
<?php
if ($align=='center')
	echo '</center>';
}


function stuff_menu($word,$wordlen)
{
	//stuff string with &nbsp; to make total lenght of ~18  in TR Frames menu.
	$space = ($wordlen - strlen($word))/2;
	for ($i=0; $i<=$space; $i++)
	{
		$word = '_'.$word.'_';
	}
	echo $word;
	return  true;
}
	

Function do_menu($xcatid="")
{

// changed 10/25/02 with addition of th 'small_menu()'

   IF (isset($_SESSION["SESSION_UID"]))
   		$cust_id = $_SESSION["SESSION_UID"];

   IF (isset($_SESSION["menubgcolor"]))
   		$mnucolor = $_SESSION['menubgcolor'];
   else
   		$mnucolor = 'silver';
		
   echo "<table border=1 bgcolor=$mnucolor width=100%>
         <tr>";
         //<td width=30 nowrap > <a href=index.php?catid=home>Home </a> </td>";
		 
		 if (isset($_SESSION["SESSION"])) 	// someone is logged in or in adminmode
		 {
		 	// 10/7/02 Intranet feature gets Admin user name from \silk.ini file
			// so there is no sense to show 'logout'.   12/13/02 Added 'adminmode" which is set in sysdata
			// in index.php. When true, does auto-login as admin. When false 
			// (internet mode), web user 
			// 
			
			if ($_SESSION["SESSION"]!= 'Admin')
				echo "<td width=30 nowrap bgcolor=> <a href=logoff.php>Logout </a> </td>";
			else
			{
				if (isset($_SESSION["SESSION_UID"])) 
					echo "<td width=90 nowrap bgcolor=> <a href=cust_scrn_form.php?cust_action=display>Profile</a> </td>";  //&this_id=$cust_id
				else
					echo "<td width=90 nowrap bgcolor=> &nbsp;</td>";
			}
				
		 }
		 else
            echo "<td width=30 nowrap bgcolor=> <a href=index.php?catid=logon>Login </a> </td>";
			
		 if (isset($_SESSION["SESSION"]) 	&& 	($_SESSION["SESSION"]== 'Admin'))
		 	{
				//if (!isset($_SESSION["SESSION_UNAME"])) //don't show since get_customer() is already on the home screen 10/16/02
				
					echo "<td width=90 nowrap bgcolor=> <a href=customer.php?menu=new>Person</a> </td>";
			}
		else 
   			echo "<td width=90 nowrap bgcolor=>&nbsp;</td>";  //Spacer 
			
   echo '<td align=center colspan=3 bgcolor=white>'; small_menu(2,'notcentered'); echo '</td>';
   
   // Next decide and control whether to show "Checkout"
   if ((isset($_SESSION["items"]) && $_SESSION["items"] > 0)  || (isset($_SESSION["fiber_items"]) && $_SESSION["fiber_items"] > 0) ||(isset($_SESSION["pulps_items"]) && $_SESSION["pulp_items"] > 0))
   		$show_checkout = true;
    else 
		$show_checkout = false;
		
   if ($show_checkout)
   		$spacer = '160';
   else
   		$spacer = '220';
   
   		 //<td width=$spacer nowrap>&nbsp;&nbsp; </td>
   echo "<td width=90 align = right nowrap bgcolor=> <a  HREF=help.php>Help</a> </td>"; 		 
   echo "<td width=90 align = right nowrap bgcolor=> <a  HREF=clear_cart.php>Clear Cart</a> </td>"; 
         //<td width=90 nowrap bgcolor=> <a  HREF=show_cart.php?catid=".$xcatid.">View Cart</a> </td>";
//if ($show_checkout)
//	{
//		echo  "<td width=90 nowrap bgcolor=> <a  HREF=checkout.php?catid=".$xcatid.">Checkout</a> </td>";
//	}
	
	// 10/7/02 make local cart only, hold off on Americart
	// echo "<td width=50 nowrap bgcolor=> <a href=http://www.cartserver.com/sc/cart.cgi?item=p-2914&view.x=1>
	//									Americart</a></td>";
		 
	
	echo "</tr></table>"; 
	

 //       <td width=30 nowrap bgcolor=> <a href=index.php?catid=home>Gallery </a> </td>
 //	      <td width=60 nowrap bgcolor=> <a href=../phpadmin/index.php>phpadmin</a> </td>
  
}

function display_categories($cat_array,$query='') 
{
  // Called from index.php
  
  // Code below splits categories into  a left and right 
  // list of URL's to click on. w/ numincol at 13, can handle 26 
  // categories before page starts to scroll. Width of 18% in
  // left and right cells may need adjusting.  
  // Also, features need to be abstracted better to eliminate 
  // to repeat calls partial_cat_list()  -ref- 01/01/2002
  
  $catid = $_SESSION['catid'];
			  
  $numincol =17;  //max number of links to list in single column
  $tblborder = 1;
  $tblcolor = $_SESSION['catmenubgcolor'];
  
  $arraysize = sizeof($cat_array);
  
  //echo '&nbsp;'; //'xx'.$catid.'  '.$arraysize;
  //bgcolor=".$_SESSION['catmenubgcolor']."
  echo "<table border=0 width=99% >
        <tr valign = top>
		<td width = 19% cellspacing=0 cellpadding=0 >"  ;  // Create main 3-cell table
	
  // Three conditions: list fits on left; lists splits between left and right;
  // list slpits evenly and page may scroll.
  
  if ( $arraysize <= $numincol )
    {
	$mstart1 = 0;
	$mend1   = $arraysize;
	$mtbl2   = 0;
	$mstart2 = 0;
	$mend2   = 0;
	}
  elseif ( $arraysize <= (2 * $numincol) )
    {
	$mstart1 = 0;
	$mend1   = $numincol;
	$mtbl2   = 1;
	$mstart2 = $numincol;
	$mend2   = $arraysize;
	}
  elseif ( $arraysize > (2 * $numincol))
	{
	$mstart1 = 0;
	$mend1   = round($arraysize/2);
	$mtbl2   = 1;
	$mstart2 = $mend1 + 1;
	$mend2   = $arraysize;
	}
		
	//Display first list of 'buttons'
	//echo '&nbsp;';
	
	partial_cat_list($cat_array,$mstart1,$mend1,$tblborder,$tblcolor);


	//Display the remainder if there is a second table

	if(!$mtbl2)  
	  {
	  echo "</td><td width=80%  align = left>";
	  display_space($catid,$query);	//main
	  }
	else
	  {
	  echo "</td><td align = left>"; 
      display_space($catid,$query);	//main
	  echo "</td><td width = 20% align = right >";
	  partial_cat_list($cat_array,$mstart2,$mend2,$tblborder,$tblcolor);
	  }	
   	  echo '<div align=right>';
	  find_item();
	  echo '</div>';
  
    echo "</td></tr></table>";
    //echo "<hr>";
	
}


function show_groups ($catid,$arrayname,$tblcolor)
 // Show horizontal menu five across and n deep  Not Active 3/9/02
{    
      $arraysize = sizeof($arrayname);
      $nrows = ceil($arraysize/5);
      
      echo "<center><table border = 1 rules='none' bordercolor = red bgcolor = $tblcolor>"; 
      //frame='box' rules='none' bordercolor = red
      
      for ($i = 0; $i <= $nrows-1; $i++)
      {
        echo "<tr>";
		   
	     for ($j = 0+$i*5; $j <= 4+$i*4; $j++ )
		 {
          echo "<td nowrap>";
		  $url = "set_gid.php?grpid=".($arrayname[$j]['grpid']); // from ...showinvt.php  
          $title = (trim($arrayname[$j]['grpname'])); 
          do_html_url_no_br($url, $title); 
  		  echo "&nbsp;&nbsp;</td>";
		 }    
	     echo "</tr>";
         
       }
	   echo "</center></table>";
}


function callamericart($thisitem,$qty,$type)
{
  // Takes one item and returns the display of price as hypertext 
  // with correct call to Americart
  // 6/16/2002 -ref-
  // $type is either  "regular or fiber"  item
  
	/*Notice that Americart uses the + char for spaces in the product 
	description for these type of simple calls.
	If you use any of the AMCART special part number tags like =SF=, 
	you must replace the = with %3D. So =SF= would be %3DSF%3D 
	*/
  
  if ($type=="regular")	
  {
  //regular item as stored in  "invt" array format
	$description = str_replace(" ","+",trim($thisitem["descrip"]));
	$description = str_replace("++","+",trim($description));
	echo $description;
	$ac_price = number_format($thisitem["price"], 2);
	$acstr = "p-2914^"
	      .$thisitem["item"]."^"
		  .$description."^"
		  .$ac_price."^"
		  .number_format($qty,0)."^"."^"."^"."^"
		  .number_format($thisitem["weight"], 3);
		  
  }
  else
  {
  //fiber item as stored in  slightly different format in fibercart 
/*  		$row["description"]
			$row['item'] 
			$row['comment']
			number_format(($row['totalweight']),1)
			$row['price']
			$row['count']
			number_format($row['totalamount'],2)
*/
    // Product description = description + comments ("ships frt collect" or "Cheaper by the bale")
	$description = str_replace(" ","+",trim((trim($thisitem["description"])." ".$thisitem['comment'])));
	$unitprice = $thisitem["price"];
	$thiswt = $thisitem["weight"];

	
	// Now figure if bales or pounds
	if (strpos($description,"(BALE)"))
	{
		//buying a bale so the unitmeasure = bale the weight is 0 since ships frt collect
		//and the qty = 1 or count, and the unit price is weight x price !! That is..

		$price = number_format(($thiswt * $unitprice),2);
		$description = $description."+$$price+per+lb";  // Amcart ship free signal
		$price = number_format(($thiswt * $unitprice),2);
		$ums = "bale";
		$weight = "0.000";
		$qty = number_format(str_replace("b","",$thisitem["count"]),0);
		
	}
	else
	{
		//fiber bought by the pound so qty = weight
		$wt = number_format(($thisitem['totalweight']),1);
		$price = number_format($unitprice,2);
		$ums = "lb";
		$weight = 1;
		$qty = $wt;
		
	}
	
	
	
	$acstr = "p-2914^"
	      .$thisitem["item"]."^"
		  .$description."^"
		  .$price."^"
		  .$qty."^"."^"."^"
		  .$ums."^"
		  .$weight;
	

	$ac_price = $price;
	
	/*	
	$ret = "<FORM ACTION=http://www.cartserver.com/sc/cart.cgi METHOD=POST>
			<INPUT TYPE=hidden NAME=item VALUE=$acstr>
			<FONT FACE=verdana size=-1>
			<input type=image src=ac_graphics/gw_rd_add.gif name=nvadd width=20 height=10 border=0>
			</form>";*/
  
  }
  echo $acstr;
  //header("location:http://www.cartserver.com/sc/cart.cgi?item=".$acstr."&nvadd.x=1");
 
  $ret = "<a href=http://www.cartserver.com/sc/cart.cgi?item=$acstr&nvadd.x=1>$$ac_price</a>"  ;
 
  RETURN $ret;
}


function silk2americart($cart,$type)
{
  // Takes the items in the silk shopping cart and format/sends them to Americart
  // 6/16/2002 -ref-
 
 echo "<table border=0 align=center width=100% cellspacing=0>";
 if ($type == "regular") 
 {
  foreach ($cart as $item => $qty)
  {
    $invt = get_invt_details($item);
	echo "<tr><td>";
	echo callamericart($invt,$qty,$type);
    echo "</td></tr>\n";
  }
 }
 else
 {
	// must be fiber item...

	
    foreach ($cart as $key => $row)
	{
		
		if ($cart[$key]['totalweight']>0)
		  {
			echo "<tr><td>";
	echo callamericart($row,$row["qty"],$type);
    		echo "</td></tr>\n";
	     }		
	}
 }
   echo "</table>";
   echo "<a href=http://www.cartserver.com/sc/cart.cgi?item=p-2914&view.x=1>View Cart</a>";
   
//<a href="http://www.cartserver.com/sc/cart.cgi?item=a-1358&checkout.x=1">

}  //end of silk2americart




function display_fibercart($fcart,$catid,$disp_button)
{
	$bgcolor = $_SESSION['barsbgcolor'];
   $images = 0;
  
   //$this_wt $_SESSION["fiber_weight"];	//check to see if weight changes. 
   												//If so, recalc ups shipping charge (if state is defined.)
 
   echo "<table border = 1 width = 100% cellspacing = 0>
        <form action = show_cart.php method = post>
        <tr>
        <th bgcolor=$bgcolor width=110>Weight lbs</th>
		<th bgcolor=$bgcolor width=180>Item</th>
		<th bgcolor=$bgcolor>No. & Comments</th>
        <th bgcolor=$bgcolor width=60>Bale Cnt</th>
		<th bgcolor=$bgcolor width=60>Price</th>
        <th bgcolor=$bgcolor width=90>Total</th></tr>";
		
  
  //display each item echo counas a table row
 
  // for ($key = 0; $key < count($fcart); $key++)
  
  foreach ($fcart as $key => $row)
	{
		 //echo "<br>$key   $row";

			// $pbitem = array("catid"=>$catid,"item"=>$item,"description"=>$desc,
			// "price"=>$unitprice,"weight"=>$unitwt,
			// "qty"=>$qty,"totalamount"=>$unitprice*$qty*$unitweight,"totalweight"=>$qty*$unitwt);           "totalamount"=>$unitprice*$qty*$unitweight,"totalweight"=>$qty*$unitwt);					
		if ($fcart[$key]['totalweight']>0)
		  {
			echo "<tr>";
			echo "<td align=center>".number_format(($row['totalweight']),1)."</td>";
			echo "<td>".$row["description"]."</td>";
			echo "<td NOWRAP>[".$row['item']."]&nbsp;&nbsp; 
			       ".$row['comment']."</td>";
			echo "<td align=center>&nbsp;".$row['count']."</td>"; //".$row['qty']."
			echo "<td align=center>$".$row['price']."</td>";
			echo "<td align=right>$".number_format($row['totalamount'],2)."</td>";
			echo "</tr>";
			
	    }		
	}
	

  // Calculate total weight/item/price for fiber (and store in 'fiber_'session_vars)
  // calc_fiber_vars($fcart);// this is set in fiber.php, no need to rerun...
  // display totals row

  switch ($disp_button)
  	{
		case 'clear' :
			$fiber_action = "<a href=fiber.php?catid=$catid&clearfiber=Y>Clear Fiber Order</a>";
			break;
  		case 'edit'  :
    		$fiber_action = "<a href=fiber.php?catid=$catid&clearfiber=N>Edit Fiber Order</a>";
			break;
		case 'none'  :
			$fiber_action = '-';
			break;
	}

  echo "<tr>
          <th align = center bgcolor=$bgcolor width=100>"
              .number_format($_SESSION["fiber_weight"], 1)."
          </th>
          <th align = left  bgcolor=$bgcolor>&nbsp;$fiber_action </td>
          <th align = center bgcolor=$bgcolor>
              &nbsp
          </th>
          <th align = center bgcolor=$bgcolor>". 
              $_SESSION["fiber_items"]."&nbsp;Types
          </th>
          <th align = center bgcolor=$bgcolor>
              &nbsp
          </th>
          <th align = right bgcolor=$bgcolor>
              \$".number_format($_SESSION["fiber_price"], 2).
          "</th>
        </tr>";

  echo "</form></table>";
  
  
}

function display_pulpcart($pulpcart,$edit='')
{
		$bgcolor = $_SESSION['barsbgcolor'];
  	    echo "<table border = 1 width = 100% cellspacing = 0 >
        
        <tr>
		<th bgcolor=$bgcolor nowrap width=110>5/4 | 1/1 :: Ord/Shp</th>
		<th bgcolor=$bgcolor width=180>Item</th>
		<th bgcolor=$bgcolor>No. & Comments</th>
		<th bgcolor=$bgcolor>&nbsp;</th>
        <th bgcolor=$bgcolor width=80>Avg Price</th>
        <th bgcolor=$bgcolor width=80>Total</th></tr>";
  
  		//display each item as a table row
 		
		reset($pulpcart);
		
  		
        foreach ($pulpcart as $key => $row)
  		{ 
			echo "<tr><td align=center width=100>&nbsp;".$row['fives']." | ".$row['ones']."   ::  ".$row['qty']."/".$row['shpqty']." <a href=pulp.php?del=$n>&nbsp;&nbsp;&nbsp;del</a></td>"; 
			echo "<td>".$row["description"]."</td>";
			echo "<td colspan=2 >[".$row['item']."]&nbsp;&nbsp; 
		       ".$row['comment']."</td>";
			//echo "<td align=center>".number_format(($row['totalweight']),1)."</td>";
			echo "<td align=center>$".number_format($row['price'],2)."</td>";
			echo "<td align=right>$".number_format($row['totalamount'],2)."</td>";
			echo "</tr>";
			echo "<tr><td>&nbsp;</td><td colspan=5>&nbsp;&nbsp;".$row['pulptext']."</td>
			</tr>";
  		}

		$pulp_action = '';
		if ($edit=='edit') //called from checkout..
			$pulp_action = "<a href=pulp.php>Edit Pulp Order</a>";
		elseif ($edit=='')
			$pulp_action = "<a href=pulp.php?clearpulp=Y>Clear Pulp Order</a>";
		echo "<tr>
          <th align = center bgcolor=$bgcolor>Ship ".number_format($_SESSION["pulp_shpitems"],0)." pails</th>
		  <th align = left colspan=2 bgcolor=$bgcolor>&nbsp;$pulp_action</th>
          <th align = center bgcolor=$bgcolor>&nbsp;</th>
          <th align = center bgcolor=$bgcolor>&nbsp</th>
          <th align = right bgcolor=$bgcolor>\$".number_format($_SESSION["pulp_price"],2)."</th>
        </tr>";

		echo "</table>";
	
  
}

function get_discount_amount($firstletter)  // in display_order_totals()) in action_fns.php
{
  $value = 0.00;
	// sum up amounts for Y & Z items in cart
	// first figure if have items with item beginning with $firstletter are in cart...
  $itemlist= get_items_beginning($firstletter);
  // If this order has paper or stationery, build lineitem array and sum amounts
  if ($itemlist)
  {
      $thislist =  build_lineitem_array($itemlist);
      $value = 0.00;
      foreach ($thislist as $item)
      {
          $value=$value + $item['amount'];   //7/30/04 This doesn't address papercart
                                             //        See below in figuring $this_amount
      }
  }
  return $value;      //Return the value of the
}


function get_order_totals($t_price,$t_discount,$f_price,$s_charge,$p_price,$paper_price,$bill_state,$tax_id,$discount_type='')
{
	// Called from payments,showcart;tr_checkout:  - figure and  subtotal, ship_charge and order_total
	// 8/12/03 -- code
    // 5/13/2013 Added customer  codes discount:   $t_discount
	
  //  SET SS & WH DISCOUNTS;  FIGURES IN SALES TAX   HERE
  GLOBAL $g_taxstate,$g_sales_tax,$g_tax_text;
  
	$discount=0.00;
	$disc_text='';

 	// Twinrocker Customer discounts: ss - studio services OR wr/pr -30% $200 min
    //   r -50% $200 min wholesale paper resellers:
	//   ss receive qty discount.   All on "XYZ" (paper) items only
	//   7/30/04 -- Added $papercart: the total in that is all Z items so we add..
 	if ($discount_type=='ss' || $discount_type=='wr' || $discount_type=='pr' || $discount_type=='r')
	{
		$this_amount =get_discount_amount('XYZ') + $paper_price;   // 7/30/04 Added $paper_price

        switch ($discount_type)
        {
          case 'ss':
    
    		    if  ($this_amount>0)
    		    {
    			     // July 2003 Studio services discount structure:  (lower limit, discount, disc descript)
    			     $ssdisc = array(array(50,0.00,'SS Disc on $0-$50: None'),
    					   array(99,0.05,'SS Disc on $50-$99: 5%'),
    					   array(199,0.10,'SS Disc on $100-$199: 10%'),
    					   array(399,0.15,'SS Disc on $200-$399: 15%'),
    					   array(799,0.20,'SS Disc on $400-$799: 20%'),
    					   array(1499,0.25,'SS Disc on $800-$1499: 25%'),
    					   array(90000,0.30,'SS Disc on $1500+   : 30%')
    				    	);
              $asize = count($ssdisc);
              for ($i=0;$i<$asize;$i++) //
              {
                if ($this_amount <= $ssdisc[$i][0])
                {
                  $disc_rate= $ssdisc[$i][1];
                  $disc_text= $ssdisc[$i][2];
                  break;
                }
              }
              $discount = $disc_rate*$this_amount;  // Figure SS discount on paper and stationery only
            }
            break;  //from ss case
          case 'wr':
             if ($this_amount >= 200.00)  //If the amount of paper is less than $200, no discount
              {
                $disc_text= 'Wedding Retailer Disc -30%';
                $discount = 0.30*$this_amount;
              }
              break;
          case 'pr':
             if ($this_amount >= 200.00)  //If the amount of paper is less than $200, no discount
             {
                $disc_text= 'Paper Retailer Disc -30%';
                $discount = 0.30*$this_amount;
             }
             break;
          case 'r':
             if ($this_amount >= 200.00)  //If the amount of paper is less than $200, no discount
             {
                $disc_text= 'Retailer Discount -50%';
                $discount = 0.50*$this_amount;
             }
             break;
    
          } //switch
     } //endif    Customer has discount ss.wr.r.pr codes (traditional)
     else
     {
            //Now we figure discount based on special discount codes, if any.
            // 5/14/2013 added var and these 3 lines...
            if ($t_discount>0.00) {
                $disc_text= 'Customer Discount';
                $discount = $t_discount;
            }
     }
  
	   $subtotal = $t_price + $f_price + $p_price + $paper_price;

/*
	if ($discount_type=='wh')    // change here if giving discounts on Subtotal
	{
      $disc_rate = 0.50;
      $disc_text= 'Whlsl Disc';  //force to 30% on huge orders
    	$discount =  $disc_rate*$subtotal;  // Figure SS discount on paper and stationery only
	}
*/

      //add tax on non exempt Indiana orders  reduced by the discount
	   if ($bill_state==$g_taxstate && !$tax_id)
		    $tax = $g_sales_tax * ($subtotal - $discount);
	   else
		    $tax = 0.00;


    // Now figure the totals
    $total_order = $subtotal + $s_charge + $tax - $discount;

    //Create the array...
	  $totals['subtotal']  = round($subtotal,2);    // 7/16/2012   round()
	  $totals['disc_text'] = $disc_text;
	  $totals['discount']  = round($discount,2);    // 7/16/2012   round()
	  $totals['s_charge']  = $s_charge;
	  if ($tax>0.00)
		  $tax_text = $g_tax_text;  //'IN 6% sales tax';  
    else
      $tax_text = '';
    $totals['tax_text'] = $tax_text;
	$totals['tax'] =  round($tax,2);   // 7/16/2012   round()
    $totals['total_order'] = round($total_order,2);    // 7/16/2012   round()
	$totals['tax_id'] =  $tax_id;

 //echo qd_list_array($totals);
 
 return $totals;
        
}
function display_order_totals($totals)
{
  //function display_order_totals($t_price,$f_price,$s_charge,$p_price,$bill_state,$tax_id,$discount_type='')

	// Called fr showcart.php and checkout - figure and print subtotal, ship_charge and order_total
	// s/b  total = $total_price (cart) + $fiber_total_price + pulp_price +$ship_charge
  Global $g_tax_text;

  // Display the table.
  echo "<div align=right>
		  <table class=totals border=1 bordercolor=#ff0000 rules=rows bgcolor='#e0e0e0'>
		  <tr><td width=70>Subtotal</td><td width=60 align=right >$".number_format($totals['subtotal'],2)."</td></tr>";
  if ($totals['discount']>0.00)
    echo "<tr><td width=70>".$totals['disc_text']."</td><td width=60 align=right ><font color=red>$".number_format(($totals['discount']*-1),2)."</font></td></tr>";
    
	echo "<tr><td width=70>S&H</td><td width=60 align=right >$".number_format($totals['s_charge'],2)."</td></tr>";
	if ($totals['tax']>0.00)
		echo "<tr><td width=70>$g_tax_text</h4></td><td width=60 align=right >$".number_format($totals['tax'],2)."</td></tr>";
	echo "
		  <tr><td width=70>TOTAL </td><td width=60 align=right >$".number_format($totals['total_order'],2)."</td></tr>";

	//If called from checkout.php, go to "commit.php"
	if (strpos(strtoupper($_SERVER["PHP_SELF"]),'CHECKOUT')>0)
	{
			echo "<tr><td colspan=2 align=center><a href=payments.php>Enter Payment Info</a></td></tr>";
	}
	else
	{
		// If on showcart but person hasn't logged in, need to get their name
		if (isset($_SESSION["SESSION"]) ) //|| isset($_SESSION["ship_state"])
		{
			echo "<tr><td colspan=2 align=center><a href=tr_checkout.php>Continue...</a></td></tr>";
		}
		else 
		{
			//Next line is added to showcart.php
			echo "<tr><td colspan=2 align=center><a class=cartmenu HREF=weblogin.php>&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;&nbsp;&nbsp;</a></td></tr>";
		}
	}
			  
	echo  "</table>		  
		  </div>";

return true;

}


function br($n =1)
{
	$brk = '';
	$k = 1;
	While ($k <= $n)
	{
		$brk = $brk.'<br />';
		$k++;
	}
	echo $brk;
	return ;
}


function get_ups_rate($s_state)
{

	//fetch the row
    $conn = db_connect();
    $query = "select * from shipping where state='$s_state'";   
	//echo $query; 
    $result = @mysqli_query( $conn, $query);
    if (!$result)
     return false;
    $num_recs = @mysqli_num_rows($result);
    if ($num_recs != 1)		// s/b one row
      return false;
	$value = db_result_to_array($result);
	
	return $value;
}


function get_statelist()
{
	//display form to get state to allow recalc shipping...
	 
	//create table
    $conn = db_connect();
    $query = "select * from shipping order by state";   
    $result = @mysqli_query( $conn, $query);
    if (!$result)
     return false;
    $num_recs = @mysqli_num_rows($result);
    if ($num_recs ==0)
      return false;
    $statelist = db_result_to_array($result);

	RETURN $statelist;
}

function has_envelopes()
{
   // 12/05/2010  find our if cart includes envelopes
   $ccart = $_SESSION["cart"];
   $env = false;
   if (is_array($ccart))
   {
      //Array ( [YAUGU001_] => 22 )
      foreach($ccart as $item => $qty)
      {
          $firstchar = substr($item,0,1);
		    if ($firstchar=='Y' ) // First character is a 'Y'  
		    {
			     //  It's stationery, look for "env" code in tiem: Certain types are envelopes
			     $number = intval(substr($item,6,2));
              $env_types = array(3,7,10,11,17,18,24,25,31,36,38,40,42,44,47,50,57,58,63);
              if (in_array($number,$env_types))
              {
                  $env = true;  //At first envelope, done here..
                  break;
              }
          }    
     }
   }
   if ($env && strtoupper($_SESSION["ship_rush"])=='ON')
   {
      $_SESSION["ship_rush"] = 'OFF'; 
   }

   $_SESSION["has_envelopes"] = $env;  //Used in get_ship_state()
   return $env; 
}

function has_pulp()
//5/9/2011 Disable Rush, in get_ship-state()if pulp is ordered
{
   if (isset($_SESSION["pulpcart"]) && count($_SESSION["pulpcart"])>0)
   {
      if (strtoupper($_SESSION["ship_rush"])=='ON')
      {
         $_SESSION["ship_rush"] = 'OFF'; 
      }
      $ans = true;
   }
   else
      $ans = false;
      
   $_SESSION["has_pulp"] = $ans;  //Used in get_ship_state()
   return $ans; 
      
}

function get_ship_state($statelist,$s_state='IN',$s_type,$catid,$s_special,$s_rush='',$s_cod='',$returnto='')
//function get_ship_state($statelist,$s_state='ups',$s_type,$catid,$s_special,$s_rush='',$s_cod='',$returnto='')
//5/9/2011 Disable Rush if any items in pulpcart
{
	
	echo "<form action = set_shipping.php?catid=".$catid." method = post>";
	echo "<table align=left border=2 class=shipping bgcolor=#ffcc00>
		  <tr valign=top NOWRAP>";
	
	// 6/20/2010 In showcart, allow state switch while User not logged in 
	//
	if  ($returnto=='showcart' &&  (!isset($_SESSION['SESSION']) || $_SESSION['SESSION']=='Admin') )
	{
		echo "<td valign=center NOWRAP>Ship to: </td>
		  <td><select name=this_state size=1>"; 

		foreach ($statelist as $row) 
		{ 
			if ($row[0]==$s_state)
				echo "<option selected value=".trim($row[0]).">".trim($row[0]); 
			else
				echo "<option value=".trim($row[0]).">".trim($row[0]);
		} 	
		echo "</select>";
	}
	else
	{
	      // 6/17/2010 Next line probably causing odd ship_string events:
			//echo "<td valign=center NOWRAP>
         //   <input type=hidden name=this_state value='IN'>";
         echo "<td valign=center NOWRAP>
            <input type=hidden name=this_state value='".$_SESSION["ship_state"]."'>";	  
   }	  

		
	echo "<br><input type=radio name=this_type value='ups'";
	if ($s_type=='ups')
		echo 'checked';
	echo ">Ground Shipping
		  <br><input type=radio name=this_type value='pickup'";
	if ($s_type=='pickup')
		echo 'checked';
	echo " >Pick up
		  </td><td valign=center width='90'>";
		  
	if (check_admin_user())
	{
		$dontshow = true;
		if  ($returnto!='tr_checkout')
			echo "Special, <input type=text name=this_special_charge value='".$s_special."' ><br>";
		else
			echo '<br>'	;
	}
	else
		$dontshow = false;
		
	//12/05/2010 Exclude Rush option if cart contains envelopes Set session string for use in calc_chipping.
	//05/09/2011 Modified, added exclude Rush if  item(s) in pulpcart
    
// 12/17/2014 Don't show any rush
IF (false) {    

   if ($_SESSION["has_envelopes"] || $_SESSION["has_pulp"]) //Set in has_envelopes()/has_pulp() functions in action_fns.php (just above..)
   {
      if ($_SESSION["has_envelopes"] && $_SESSION["has_pulp"])
         echo "No Rush shipping<br />w/ envelope<br /> or pulp orders";
      else
      {
            if ($_SESSION["has_envelopes"] )
               echo "No Rush shipping<br />w/ envelope<br />orders";
            else
               echo "No Rush shipping<br />w/ pulp<br />orders";
      }
   }
   else
   {
      //Show the rush option in regular way		
   	if (strtoupper($s_rush)=='ON')
   		 echo "<input type=checkbox name=this_rush  CHECKED>Rush<br/>&nbsp;(Ships same day<br/>&nbsp;UPS Ground<br/>&nbsp;if in by 12N)";
   	else
   		 echo "<input type=checkbox name=this_rush >Rush<br />&nbsp;(Ships same day<br/>&nbsp;UPS Ground<br/>&nbsp;if in by 12N)";
   }
} // Don't show any rush    
   		 
	if ($dontshow)
	{
		if (strtoupper($s_cod)=='ON')
			 echo "<input type=checkbox name=this_cod  CHECKED>COD";
		else
			 echo "<input type=checkbox name=this_cod >COD";
	}	
	echo "<input type=hidden name=returnto value='$returnto'>	  
		  <input type=hidden name=catid value=".$catid.">
		  </td><td  ><br /><input type=submit value=Go>
		  ";
	echo "</form></td></tr></table><br>";

}	

function calc_shipping($s_state,$s_type,$fibercart,$cart,$pcart,$pulppails,$catid,$s_special,$s_rush='OFF',$s_cod='OFF')
{  //Added $pcart 6/3/2010 (see count_oversized()
   // 09/19/2019 Rev. remove Tube; add $35 Crate
   //   changed oversized to w>220 
	if (!defined("RUSH_CHARGE"))
		define ("RUSH_CHARGE",10.00);  //10/22/02  define rush charge here...
	if (!defined("COD_CHARGE"))
		define ("COD_CHARGE",7.00);    //08/18/03  define COD charge here...
	if (!defined("PAPER_CHARGE"))
	{
	   // 2/15/2012 changed next two from 9 & 100
	   define ("PAPER_CHARGE",11.00);  //11/07/02  define SH fee for paper items
	   define ("FREE_PAPER_AMOUNT",200.00);  //01/04/19 If amount of paper order is under this, 
      // add paper_charge as shipping, otherwise, free.
      
      // June 2010 Add Tube shipping charge for oversized sheets
      // Sept 2019 replqce tube w/ crate	                                         
	   //define ("PAPER_TUBE_CHARGE", 17.00);  //Cost of one paper tube
	   //define ("PAPER_TUBE_CAPACITY", 20.00);//Number of oversized sheets in each tube approx.
       // 09/19/2019 Crate replaces tube
       //    oversized  w>220 excluded from $200 paper value
       define ("PAPER_CRATE_CHARGE", 35.00);  //Cost of one paper crate
	   define ("PAPER_CRATE_CAPACITY", 300.00);//Number of oversized sheets in 
	}

   //get the cart_weight
	if ($cart)
	{
	   	$cart_weight = calculate_weight($cart);
    }
	else
		$cart_weight = 0.00;

    //get the fiber_weight
  $fiberwt = 0;
	$balewt = 0;
	$bales = 0;

	if ($fibercart)
	{
			reset($fibercart);
		    
            foreach ($fibercart as $key => $row)
			{
		 		if ($fibercart[$key]['totalweight']>0)
		 			{
						if (strtoupper(substr($fibercart[$key]['item'],-2)) == '-B')
						{
							//it's a bale
							$bales = $bales + 1;
							$balewt = $balewt + $fibercart[$key]['totalweight'];
						}
						else
		 					$fiberwt = $fiberwt + $fibercart[$key]['totalweight'];
					}
			}
			//get the pulp_weight
	}  
	// Figure shipping charges
	
	$s_charge = 0.00;
	$ret = '';
	$retlast = '';  //list Free thing last
	

	switch ($s_type)
  	{
  			case "ups" :
			{  
					$rates = get_ups_rate($s_state);	//$ship_state in session _var??
					
					if ($s_state<>'==' && is_array($rates)  )  // should return an array unless NOT in continental US
					{
						foreach ($rates as $key => $value)
							{
								//echo "<br>Key:  $key    Value:  $value[0] $value[1] $value[2]  $value[3]  <br>";
								$this_state =$value[0];
								$first5 = $value[1];
								$over5 = $value[2];
								$pulprate = $value[3];
								$days = $value[4];		//10/22/02 Show travel time for ups ground
							}
			
						$total_wt = ceil($cart_weight+$fiberwt);

						$wt_over5 = $total_wt - 5;
						if ($wt_over5 < 0)
							$wt_over5 = 0;
						
						// Get charge and text for regular UPS and fiber item shipping.
						if ($total_wt > 0) 
						{
							$ret =	"Ground Shipping ".$total_wt ." lbs to ".$this_state." takes $days days.<br>
							Charge  for 1st 5 lbs =  $".$first5.'<br/>';
							if ($total_wt > 5) 
								$ret = $ret. "plus ".$wt_over5." lbs over 5 lbs at $".$over5."/lb =  $".number_format(($wt_over5*$over5),2).'<br />' ;	
						    else 
								$ret = $ret.'<br />';
							
							
							$s_charge = ($first5+($wt_over5*$over5));
							
						}			
						
						//echo $s_charge.'<br />';
						// Calculate pulpcharge 10/20/02 (even if zero)
						if ($pulppails > 0)
						{
							$pulpcharge = $pulppails*$pulprate;
              if ($pulppails == 1)
                 $xplural = '';
              else
                 $xplural = 's';  
							$ret .= "<br/>Pulp shipping: $pulppails bag".$xplural." to $this_state at $".number_format($pulprate,2)." = $".number_format($pulprate*$pulppails,2).'<br>';
							$s_charge = $s_charge + $pulpcharge;
						}	
						
            // Oversized sheets were > 24-1/4 or > 33 1/2 ; 20 sheets per tube
            // 09/19/2019 -- Crate; oversize now  is w or l >320
            $oversized_sheets = 0;
            $oversized_sheets = count_oversized($cart,$pcart);
            // Initial constraint calls for one crate fee
            if ($oversized_sheets > 0)
            {
               $s_charge = $s_charge + PAPER_CRATE_CHARGE;  //Set at start of this function
            	if (!empty($ret)) // Start a new line
            		$ret .= '<br />';
				$ret .= "Paper larger than 22-1/2x32 requires a  $".number_format(PAPER_CRATE_CHARGE,2).' crate and actual shipping. <br/>'.
                                'Shipping cost will be applied after crate is packed for shipping.<br/><br/>';
				if ($oversized_sheets > PAPER_CRATE_CAPACITY)
					$ret .= 'Note: '.number_format($oversized_sheets,0).' sheets MAY require extra crates and cost. Call.<br>';
            } // end $oversize_sheets  > 0
            
            else
            {
               //if NO oversize paper 
               //then figure regular paper shipping charge
               //otherwise all paper shipping in  the crate
               			
			   // Add Paper S&H fee if order has up to [$200] in paper (Z) items	
               // In cart and/or  $papercart
               // 4/6/06 Added Stationery to have a paper ship charge if under free_paper_amount

               // 09/19/2019 rev. added $pcart into fnc call	            
				$paper_fee = calc_paper_ship_fee($cart,FREE_PAPER_AMOUNT,$pcart);
								
				switch ($paper_fee)
				{
					case 'Free' :
						$retlast = "Regular size Paper/Stationery value is over $".FREE_PAPER_AMOUNT.". The $".number_format(PAPER_CHARGE,2).' Paper S&H fee is waived.<br>';
						break;
                    case 'Add' :
						$s_charge = $s_charge + PAPER_CHARGE;  //Set at start of this function
						$ret .= "Regular size Paper/Stationery value is under $".FREE_PAPER_AMOUNT.": Paper S&H fee = $".number_format(PAPER_CHARGE,2).'<br>';
						break;
					default :
						// No paper in order so say nothing...
						break;
					}  //case
                    
                    } //end else no oversize paper
								
						//echo 'xx'.$ret.'--'.$s_charge.'zz'.$paper_fee;
						
						
					}
					else
					{
						//This customer is outside the continental US so set the ship charge high
						$s_charge = 0.00;
						if ($s_state <> '==')
							$ret = '<br>Ship State NOT in List. Shipping charge will be added off-line.<br>';
						else
							$ret = '<br>Please select a customer or a state<br>';
					}

											
			    	break; //end 'usp'
			}  		
  			case "pickup" :
			{
			  		$ret = '<br>Hold for pickup<br>';
			  		break;
			}	
	} //end switch

	// Special shipping charge overrides ups  or pickup  charges 
	// but allows RUSH and COD which follows below
	if (isset($s_special) && !empty($s_special) && $s_special != 'OFF') 
	{
	
					//echo $s_special.$s_charge.$total_wt;
					//echo $s_state.$cart_weight.$s_type.$s_special;
	          //exit;

		$ret = '';
		$s_charge = 0.00;
		//get special charge AND any text added after a comma
		$special = explode(",",$s_special,2);
		if (isset($special[1]))
			$msg = '&nbsp;&nbsp;&nbsp;'.TRIM($special[1]).':    $'.TRIM($special[0]);
		else
			$msg = '';
							
		$s_charge = $special[0]+0.00; 
				
		//echo $special[0].'    '.$special[1];			 
		$ret = "Special shipping and handling charge applied:".$msg. '<br>';
	}	
	
	// Calc 'rush' charge
	if ($s_rush=='ON')
	{
		$s_charge = $s_charge + RUSH_CHARGE ;
		$ret .= "Rush Charge = $".number_format(RUSH_CHARGE,2) ."<br>";
	}
	// Calc 'COD' charge
	if ($s_cod=='ON')
	{
		$s_charge = $s_charge + COD_CHARGE ;
		$ret .= "COD Charge = $".number_format(COD_CHARGE,2) ."<br>";
	}
					
					
	// Add text for bale shipping..
	if ($bales > 0)
	{
		//echo "<br>$bales  bales to be sent Frt Collect weighing: ".$balewt." lbs";
		if ($bales > 1)
			$mword = 'bales';
		else
			$mword = 'bale';
		$ret .= $bales."  $mword to be sent Freight Collect weighing: ".$balewt." lbs.  " ;
	}
	
	$ret .= $retlast;  // Add 'free' shipping for paper to list last
	$_SESSION["ship_string"] = $ret;
	$_SESSION["ship_charge"] = $s_charge;
		
	//echo "<a href=show_cart.php>Go</a>";
	//echo $ret.' '.$s_charge.' wt= '.$cart_weight.' st= '.$s_state;
	//exit;
	return;
}

function load_fiber_array($fiber_vars,$catid)    
{
    // 5/24/2018 rev to work w/ 2 less price breaks. 
    //      Prior version saved below as 
    //      load_fiber_arry_ORIGINAL()
    // 10/19/2011 Rev 11,26,99 qty breaks to 14,49,99 to math w/ column headers
    // 05/24/2018 Rev 14,49,99 qty breaks to 14,>14 and...math w/ column headers
	
    $lineno = -1;
    foreach ($fiber_vars as $key => $value)
	{
       	
		$lineno = $lineno + 1;
		// Only process rows with  qty > 0 or if ordering a bale
    	if ($value > 0 || strtoupper(substr($value,0,1))=='B')
		 //
		 {
		 
		  // 10/21/2005 -- force/round up fiber values to next highest integer
		  //Only  works if client side javascript  off and user enteres a decimal
		  if (strtoupper(substr($value,0,1))!='B')
		    $value = ceil($value);
		  
			$count = '';
			echo "<tr>";
			//break string into fields
			$line = explode("|",$key);
            $cnt = count($line);
			for ($n = 0; $n<$cnt; $n++)
	  		{
			    //Change _ to decimal point in prices/lb
				$line[$n] = str_replace("_",".",$line[$n]);
				//Replace + with spaces 
				$line[$n] = str_replace("+"," ",$line[$n]);
		 		//echo "<br>zz  ".$line[$n].'  val'.$value;	// shows exploed text
	  		}
			$lineno = $line[1];
			$item = $line[0];
			$desc = $line[6];  // 8    5/24/2018
			$balewt = $line[5];  // 7
			$baleprice = $line[4];  //6
			$qty = $value;
			$unitprice = 0.00;
			$unitwt = 1;
			$comment = "&nbsp;";
			
			// 10/08/2009 Added 2nd way to figure if customer ordered a Bale
			// by entering increments of bale weight (rather than initial way of 
         // typing 'b').
         if ( strtoupper(substr($value,0,1))!='B' 
              && $balewt > 0 
              && $value % $balewt  == 0)
         {
            // If not marked as a bale, and balewt  > 0, 
            // and the balewt is evenly divisible by the qty...
            // reset the vars as though entered as b+number
            $value = 'b'.intval($value/$balewt); 
            $qty   = $value;                       
         }
         // End of 10/08/2009 variation. Return to original flow...
         	
			// Ordering a bale
			if (strtoupper(substr($value,0,1))=='B')
			{
			  if ($balewt> 0)   //see if balewt > 0
			  {				  
			  	$baleqty =intval(substr($value,1)); // expects '3b' to order 3 bales
			  	if ($baleqty == 0)
			  		$baleqty = 1;
			
				$item = $item.'-B';
				$desc = $desc.'(BALE)';
				$qty  = $baleqty;
				$count = 'b'.$baleqty;
				$unitprice = $baleprice;
				$unitwt = $balewt;
				$comment = '*Ships Frt Collect*';
			  }
			  else
			  {
			    $qty = 0;
			  	$comment = "Order by pounds only."  ;
			  }
			}
			else //figure price break  HARDCODED!!
			{
			  
			  if ($balewt > 0)
			    If (strtoupper(substr($value,0,1))!='B' 
                 && $value/$balewt >= 1)
			      $comment = "Cheaper by the bale.";
		 		  	//echo $item.$comment;
                    
            // 5/24/2018  less that 14lb and more than 14lbs or Bale
			if ($qty < 15 or $line[3]==0.00)  // 10/19/2011 Was: $qty < 11
				$unitprice = $line[2];
            else
                $unitprice = $line[3];  // More than 14lbs
                
            /*    old version w/ 4 breaks  
			elseif ($qty < 50 or $line[4]==0.00)  // Was, < 26
				  		$unitprice = $line[3];
							elseif ($qty < 101 or $line[5]==0.00)  // Was < 101
				  				$unitprice = $line[4];
								else
				  					$unitprice = $line[5];
			*/
            }
				
			// insert into fibercart  ** Note fibercart is unset() in clear_cart

			$pbitem = array("catid"=>$catid,
                      "item"=>$item,
                      "description"=>$desc,
                      "price"=>$unitprice,
                      "weight"=>$unitwt,
                      "qty"=>$qty,
                      "totalamount"=>$unitprice*$unitwt*$qty,
                      "totalweight"=>$qty*$unitwt,
                      "count"=>$count,
                      "comment"=>$comment);
			if ($pbitem['weight'] > 0)
				$fibercart[$lineno] = $pbitem ; 
			
			//echo $lineno .'   '.$fibercart[$lineno]['item']."<br>";
				
		 }
    }
	if (isset($fibercart))
		RETURN $fibercart;
	else
		RETURN;
}


function load_fiber_array_ORIGINAL($fiber_vars,$catid)    
{
    // 5/24/2018: This version worked w/ fiber_old table
    // and tr_fiber_old.php
    // 10/19/2011 Rev 11,26,99 qty breacks to 14,49,99 to math w/ column headers
	$lineno = -1;
	
    foreach ($fiber_vars as $key => $value)  
  	 {	
		$lineno = $lineno + 1;
		// Only process rows with  qty > 0 or if ordering a bale
    	if ($value > 0 || strtoupper(substr($value,0,1))=='B')
		 //
		 {
		 
		  // 10/21/2005 -- force/round up fiber values to next highest integer
		  //Only  works if client side javascript  off and user enteres a decimal
		  if (strtoupper(substr($value,0,1))!='B')
		    $value = ceil($value);
		  
			$count = '';
			echo "<tr>";
			//break string into fields
			$line = explode("|",$key);
			for ($n = 0; $n<count($line); $n++)
	  		{
			    //Change _ to decimal point in prices/lb
				$line[$n] = str_replace("_",".",$line[$n]);
				//Replace + with spaces 
				$line[$n] = str_replace("+"," ",$line[$n]);
		 		//echo "<br>zz  ".$line[$n].'  val'.$value;	// shows exploed text
	  		}
			$lineno = $line[1];
			$item = $line[0];
			$desc = $line[8];
			$balewt = $line[7];
			$baleprice = $line[6];
			$qty = $value;
			$unitprice = 0.00;
			$unitwt = 1;
			$comment = "&nbsp;";
			
			// 10/08/2009 Added 2nd way to figure if customer ordered a Bale
			// by entering increments of bale weight (rather than initial way of 
         // typing 'b').
         if ( strtoupper(substr($value,0,1))!='B' 
              && $balewt > 0 
              && $value % $balewt  == 0)
         {
            // If not marked as a bale, and balewt  > 0, 
            // and the balewt is evenly divisible by the qty...
            // reset the vars as though entered as b+number
            $value = 'b'.intval($value/$balewt); 
            $qty   = $value;                       
         }
         // End of 10/08/2009 variation. Return to original flow...
         	
			// Ordering a bale
			if (strtoupper(substr($value,0,1))=='B')
			{
			  if ($balewt> 0)   //see if balewt > 0
			  {				  
			  	$baleqty =intval(substr($value,1)); // expects '3b' to order 3 bales
			  	if ($baleqty == 0)
			  		$baleqty = 1;
			
				$item = $item.'-B';
				$desc = $desc.'(BALE)';
				$qty  = $baleqty;
				$count = 'b'.$baleqty;
				$unitprice = $baleprice;
				$unitwt = $balewt;
				$comment = '*Ships Frt Collect*';
			  }
			  else
			  {
			    $qty = 0;
			  	$comment = "Order by pounds only."  ;
			  }
			}
			else //figure price break  HARDCODED!!
			{
			  
			  if ($balewt > 0)
			    If (strtoupper(substr($value,0,1))!='B' 
                 && $value/$balewt >= 1)
			      $comment = "Cheaper by the bale.";
		 		  	//echo $item.$comment;
				  if ($qty < 15 or $line[3]==0.00)  // 10/19/2011 Was: $qty < 11
				  	$unitprice = $line[2];
				  	elseif ($qty < 50 or $line[4]==0.00)  // Was, < 26
				  		$unitprice = $line[3];
							elseif ($qty < 101 or $line[5]==0.00)  // Was < 101
				  				$unitprice = $line[4];
								else
				  					$unitprice = $line[5];
			}
				
			// insert into fibercart  ** Note fibercart is unset() in clear_cart
					
			/*
			if (!strstr($item,'-B'))
				$count = 1 ;  // just show count of items.
			else
				$count =intval(substr($value,1,2));
		    */

			$pbitem = array("catid"=>$catid,
                      "item"=>$item,
                      "description"=>$desc,
                      "price"=>$unitprice,
                      "weight"=>$unitwt,
                      "qty"=>$qty,
                      "totalamount"=>$unitprice*$unitwt*$qty,
                      "totalweight"=>$qty*$unitwt,
                      "count"=>$count,
                      "comment"=>$comment);
			if ($pbitem['weight'] > 0)
				$fibercart[$lineno] = $pbitem ; 
			
			//echo $lineno .'   '.$fibercart[$lineno]['item']."<br>";
			
			/*	
			$itemwt = ($fibercart[$lineno]['qty'])*($fibercart[$lineno]['weight']);
			$itemamount = $fibercart[$lineno]['qty']*$fibercart[$lineno]['weight']*$fibercart[$lineno]['price']; 
				
			echo "<td>".$fibercart[$lineno]['item']." </td>";
			echo "<td>".$fibercart[$lineno]['description']." </td>";
			echo "<td align=right>".$fibercart[$lineno]['qty']."</td>";
			 
		    echo "<td align=right>".number_format(($itemwt),1)."</td>";
			echo "
		     <td align=right>{$fibercart[$lineno]['price']}</td>
			 <td align=right>$".number_format($itemamount,2)."</td>
			 <td><h5>$comment</h5></td> </tr>";
			// count 'valid orders'; sum the wt and amounts
				
				
			if (intval($qty) > 0)
		  	{
		 		$pbcount = $pbcount + 1;
				$pbweight= $pbweight+ $itemwt;
				$pbamount= $pbamount+ $itemamount;
		  	}
			*/
				
		 }
		 
    	/*
		//Show totals
  		Echo "<tr>
  		<td>&nbsp;</td>
		<td align=right bgcolor=#e0e0e0>Totals --> </td>
  		<td align=right bgcolor=#e0e0e0>$pbcount</td>
		<td align=right bgcolor=#e0e0e0>".number_format($pbweight,1)."</td>
		<td>&nbsp;</td>
		<td align=right bgcolor=#e0e0e0>$".number_format($pbamount,2)."</td>
		<td> &nbsp;</td>
		</tr>";	
        */ 
		 
    }
	if (isset($fibercart))
		RETURN $fibercart;
	else
		RETURN;
}

function calculate_price($cart)
{
  // sum total price for all items in shopping cart
  $price = 0.0;
  if(is_array($cart))
  {
    $conn = db_connect();
    foreach($cart as $item => $qty)
    { 
	  
      $query = "select price from invt where item='$item'";
      $result = mysqli_query( $conn, $query);
      if (mysqli_num_rows($result)>0)
      {
        $row = mysqli_fetch_assoc($result);
        $item_price =$row['price'];
        $price +=$item_price*$qty;
      }
    }
  }
  
  return $price;
}

function calculate_weight($cart)
{
  // sum total weight for all items in shopping cart
  $weight = 0.000;
  if(is_array($cart))
  {
    $conn = db_connect();
    foreach($cart as $item => $qty)
    { 
	  if (left($item,1)!='Y')  // don't include weight of Stationery 1/12/03
	  { 
      	$query = "select weight from invt where item='$item'";  // 7/15/15 was 'price'
      	$result = mysqli_query( $conn, $query);
      	if (mysqli_num_rows($result)>0)
      	{
            $row = mysqli_fetch_assoc($result);
            $item_weight =$row['weight'];
            // 7/15/15 was 'price'
        	$weight +=$item_weight*$qty;
      	}
	  }
    }
  }
  return $weight;
}

function calculate_pulp_weight($cart)
{
  // Stub 2/26/02 -ref sum total pulp weight price for all items in shopping cart
  $weight = 0.000;
   return $weight;
}

function calculate_items($ccart)
{
  // sum total items in shopping cart
  $items = 0;
  if(is_array($ccart))
  {
    foreach($ccart as $item => $qty)
    { 
	  $items += $qty;
    }
  }
  return $items;
}

function is_oversized($item) {
  //09/19/2019 checks if paper width or length > 32 inch
  $width = intval(substr($item,5,3));
  $length = intval(substr($item,8,3));
  if ($width > 320 || $length > 320)
    return true;
  else
    return false;
}

function calc_paper_ship_fee($ccart,$free_amount_limit,$papcart)
{
  // Checks to see if there are paper items ordered (first char of item = 'Z')
  // If there are, then build an array of those items-qty and find out the 
  // value of the paper items is  > $free_amount_limit  (Free; otherwise, change PAPER_CHARGE)
  // Called from calc_shipping(), calls calc_price()  11/07/02

  // 09/19/2019 Rev. to exclude oversized paper items
  //   oversized  are: width or length > 320 (32 inch)
  //   also revised to scan both carts
  
  $fee = '';  //if no paper then send back blank string
 
  // This allows that if someone adds a Z items into their cart
  // it still will be counted below... Not great, but...

  $xcart = [];  // initialize array

  if(is_array($ccart))
  {
      foreach($ccart as $item => $qty)
      {
        $firstchar = substr($item,0,1);
		    if (($firstchar=='Z' && !is_oversized($item))  || $firstchar=='Y') 
            // First character is a 'Z'  ; 4/6/06 OR 'Y'
		    {
			     // make an array
			     for ( $j=0; $j < $qty; $j++)
			     {
			         if(@$xcart[$item])
      				    $xcart[$item]++;
			         else
			             $xcart[$item] = 1;
			     }
			
		    }
      }
  }  // end scan of $ccart
      
      // Scan paper cart
      if (is_array($papcart))  //Paper cart
      {
       //Array structure is different:
       //Array ( [0] => Array ( [item] => ZARTH3004001NDC [description] => Arthurs Prairie: 30x40 LA/CP [price] => 31.50 [surface] => /CP [surfaceprice] => 0 [qty] => 4 [totalamount] => 126 ) )
          for ($n=0; $n < count($papcart); $n++)
          { 
            $item = $papcart[$n]['item'];
            $qty = $papcart[$n]['qty'];
        
            $firstchar = substr($item,0,1);
  		    if ($firstchar=='Z' ) // First character is a 'Z'  
  		    {
  			     // exclude oversized
  			     if (!is_oversized($item))
  			     {
                 // add to array
	                 for ( $j=0; $j < $qty; $j++)
	                 {
    			         if(@$xcart[$item])
          				    $xcart[$item]++;
    			         else
    			             $xcart[$item] = 1;
    			     }
                   } 
  		         } 
          }     
      } // end scan of $papcart   
      
      /* echo 'XXXX <br />';
      var_dump($xcart);
      exit;  */
      
	if (count($xcart)>0 )
	    $value = calculate_price($xcart);
  
	
	 
	if ($value > 0.00 )  //if there is paper in either paper_cart or cart
	{
      if ($value >= $free_amount_limit)  // and if over limit, ship is free
    			$fee='Free';
	    else   // otherwise add the paper_ship_charge
			    $fee = 'Add';
	}
  
  return $fee;
}


function count_oversized($ccart,$papcart)
{
  // Counts the number of oversized sheets, 
  // either in cart or papercart.  
  // Oversized (09/19/2019)is Width or Length > 320 (32 inch)

  // Checks to see if there are paper items ordered (first char of item = 'Z')
  $sheets = 0;
  if (is_array($ccart))
  {
      foreach($ccart as $item => $qty)
      {
          $firstchar = substr($item,0,1);
		    if ($firstchar=='Z' ) // First character is a 'Z'  
		    {
			     // Rev. 09/19/2019: 
                 // w or l >320 gets all larger papers & circles
          
          	     
			     if (is_oversized($item))
			         $sheets = $sheets+$qty; 
		    }
      }
  }
  
  if (is_array($papcart))  //Paper cart
  {
   //Array structure is different:
   //Array ( [0] => Array ( [item] => ZARTH3004001NDC [description] => Arthurs Prairie: 30x40 LA/CP [price] => 31.50 [surface] => /CP [surfaceprice] => 0 [qty] => 4 [totalamount] => 126 ) )
      for ($n=0; $n < count($papcart); $n++)
      { 
            $item = $papcart[$n]['item'];
            //echo $papcart[$n]['item'];
            $qty = $papcart[$n]['qty'];
            {
                $firstchar = substr($item,0,1);
      		    if ($firstchar=='Z' ) // First character is a 'Z'  
      		    {
      			     // check size
      			     if (is_oversized($item))
      			         $sheets  = $sheets+$qty; 
      		    }
            } 
      }     
  }
  return $sheets;
}

function old__tube_count_oversized($ccart,$papcart)
{
  // old -- used for $17 tube
  // Counts the number of oversized sheets, 
  // either in cart or papercart.  Oversized is larger than 24-1/4 x 33 1/2
  // Checks to see if there are paper items ordered (first char of item = 'Z')
  $sheets = 0;
  if (is_array($ccart))
  {
      //Array ( [ZARTH3004001NDC] => 22 )
      foreach($ccart as $item => $qty)
      {
          $firstchar = substr($item,0,1);
		    if ($firstchar=='Z' ) // First character is a 'Z'  
		    {
			     // check width and length: 241 ~ 24-1/4    332 ~ 33-1/2
			     // Rev. 7/28/2010: w>241 gets all larger papers including circles
              // and w<250 &  L>332 gets long, skinny papers
			     $width = intval(substr($item,5,3));
			     $length = intval(substr($item,8,3));
			     if (($width >241 )||($width < 250 && $length > 332))
			         $sheets = $sheets+$qty; 
		    }
      }
  }
  
  if (is_array($papcart))  //Paper cart
  {
   //Array structure is different:
   //Array ( [0] => Array ( [item] => ZARTH3004001NDC [description] => Arthurs Prairie: 30x40 LA/CP [price] => 31.50 [surface] => /CP [surfaceprice] => 0 [qty] => 4 [totalamount] => 126 ) )
      for ($n=0; $n < count($papcart); $n++)
      { 
            $item = $papcart[$n]['item'];
            $qty = $papcart[$n]['qty'];
            {
                $firstchar = substr($item,0,1);
      		    if ($firstchar=='Z' ) // First character is a 'Z'  
      		    {
      			     // check width and length
      			     $width = intval(substr($item,5,3));
      			     $length = intval(substr($item,8,3));
      			     if (($width >241 ) ||($width < 250 && $length > 332) )
      			         $sheets  = $sheets+$qty; 
      		    }
            } 
      }     
  }


  return $sheets;  // old -- tube
}


function get_pricebreak($filename)
{
   // query database for the invt in a category
   //GLOBAL $grpid;
   
   //if (!$catid || $catid=="")
   //  return false;
   
   $conn = db_connect();
   
   $query = "select * from $filename where active = 'Y' ";   
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   if ($num_invt ==0)
      return false;
   $result = db_result_to_array($result);
   return $result;
   
}
   


function calculate_cart_totals($cart)
{
  $_SESSION["total_price"] = calculate_price($cart);
  $_SESSION["total_weight"] = calculate_weight($cart);
  $_SESSION["items"] = calculate_items($cart);
}


 function get_cust_action()
{
	//lists actions that can be done on a selected customer 
	//called from customer.php
	if ($_SESSION["SESSION"] == "Admin")
		{
			echo "<select name=cust_action >
			    <option value=continue selected>Continue
		  		<option value=checkout>Go to checkout
		  		<option value=edit>View/Edit
		  		<option value=history>History
		  		<option value=email>Email
		  		<option value=notes>Notes
		  		</select>";
		}
	else
		{
			echo "<select name=cust_action >
		  		<option value=edit selected>View/Edit
		  		<option value=history>History
				<option value=checkout>Go to checkout
		  		</select>";
		}
		  
}



		
function set_cust_session_vars($cust)
{
	//global $SESSION_COMPANY, $_SESSION["SESSION_COMPANY"];
	
	echo $SESSION_COMPANY;
  $_SESSION["SESSION_COMPANY"] = $cust["name"];
	echo $SESSION_COMPANY;
	return;
}

function add_cust_button()
{
	echo "&nbsp;&nbsp;&nbsp; <a href=cust_add.php>&nbsp;&nbsp;&nbsp;Add a new contact record&nbsp;&nbsp;&nbsp;</a>";
}

function get_customer($returnto='')
{
	// Called from showcart and new order; display search form; find and display customer address and ship address
	// seek on custno,lastname,phone,zip,company (name) -- browse select display
	echo '<table class=profile border=1><tr><td>';
	echo "<form action=customer.php method=get>
		  <input type=hidden name=gotouri value='$returnto'>
		  <p class=intro>&nbsp;&nbsp;&nbsp;<b>Find</b> a contact where:<br> 
		  <input type=text name=cust_search_string>
		  is in 
		  <select name=cust_search_field>
		  <option value=lastname>Last Name
		  <option value=zip>Zipcode
		  <option value=phone>Phone 
		  <option value=name>Company
		  <option value=custno>Account ID
		  </select>
		  <input type=submit value=Go> ";
	echo "</form></p>";	  			

	//echo "&nbsp;&nbsp; or ";
	
	//get_ziplookup();
	//add_cust_button();	
	$this_zip = '';	  
	get_addcustzip($this_zip);	//displays zip prompt form which sends zip to cust_add.php...
	
	// 12/21/02 Login to add to MOJO maillist
	//add_to_maillist('trnews','Sign up to receive <br><b>TR News</b><br>',$_SESSION['menubgcolor']);
	echo '</td></tr></table>';

}	

function add_to_maillist($list,$string='Join our maillist',$color='#e0e0e0')
{

//add_to_maillist('trnews','Sign up to receive <br><b>TR News</b><br>',$_SESSION['menubgcolor']);

echo "<table border=0><tr><td valign=top align=center bgcolor=$color >
<FONT FACE='VERDANA, ARIAL, HELVETICA' SIZE='-2'>
<form action='http://www.refrost.com/cgi-bin/dada/mail.cgi' method=POST>
$string
<input type='hidden' name='flavor' value='subscribe'>
<input type='text' name='email' value='email address' size='14'><br>
<input type='hidden' name='list' value=$list>
<input style='font-size=1' type='submit' value='Subscribe'>
</td></tr></table>
</form></FONT>";
/*
<!--
<FORM ACTION="subscribe.php"  METHOD="post">
<INPUT TYPE="HIDDEN" NAME="list"  VALUE="trnews">
<DIV align=center>
<B>Your email address:</B><BR>
<INPUT TYPE="text" NAME="email" SIZE="12" maxlength="150" VALUE=""><br
<B>Password:</B><BR>
<INPUT type="Password" name="pw" SIZE="12" maxlength="50" VALUE=""><br>
<B>Confirm Password:</B><BR>
<INPUT type="Password" name="pwconf" SIZE="12" maxlength="50" VALUE=""><br>
<INPUT TYPE="submit" name="email-button" value="Subscribe">
</DIV>
-->
*/
}

function dump_globals()
{
echo "<TABLE CELLPADDING=5 BORDER=1>
	<TR><TH ALIGN=LEFT>Variable</TH><TH ALIGN=LEFT>Value</TH></TR>";

    foreach ($GLOBALS as $key => $value) {
    if (strtolower($key) != "globals") {
      echo "<TR><TD><I>" . $key . "</I></TD><TD>$value";
        if (strlen($value) == 0) {
         echo "&nbsp;";
        } 
      echo "</TD></TR>\n";
     }
  }
echo "</TABLE>";

}

 
function CB_STATES($vState = NULL) 
 { 
 $dataSet=""; 
 $DBconn=""; 
 $stateRS=""; 
 $lb=""; 
 $statearr=GetAllStates(); 
 $lb="<select name='textState' size='1'>"; 
 //while(list($key,$val)=each($statearr))
 foreach ($statearr as $key => $value){ 
    $lb=$lb . "<option value='$key'"; 
            if (!is_null($vState)) 
                 if (trim($vState)==trim($key)) $lb=$lb . " Selected "; 
                 
            $lb=$lb . ">$val</option>"; 
            
         } //end while 
     $lb = $lb . "</Select>"; 
     return $lb; 
      
    } //end function CB_STATES 


function GetAllStates() 
{ 
$thisarr=array("AL"=>"Alabama", 
"AK"=>"Alaska", 
"AB"=>"Alberta, Canada", 
"AS"=>"American Samoa", 
"AZ"=>"Arizona", 
"AR"=>"Arkansas", 
"BC"=>"British Columbia, Canada", 
"CA"=>"California", 
"CO"=>"Colorado", 
"CT"=>"Connecticut", 
"DE"=>"Delaware", 
"DC"=>"District of Columbia", 
"FM"=>"Federated Micronesia", 
"FL"=>"Florida", 
"GA"=>"Georgia", 
"GU"=>"Guam", 
"HI"=>"Hawaii", 
"ID"=>"Idaho", 
"IL"=>"Illinois", 
"IN"=>"Indiana", 
"IA"=>"Iowa", 
"KS"=>"Kansas", 
"KY"=>"Kentucky", 
"LA"=>"Louisiana", 
"ME"=>"Maine", 
"MB"=>"Manitoba, Canada", 
"MH"=>"Marshall Islands", 
"MD"=>"Maryland", 
"MA"=>"Massachusetts", 
"MI"=>"Michigan", 
"MN"=>"Minnesota", 
"MS"=>"Mississippi", 
"MO"=>"Missouri", 
"MT"=>"Montana", 
"NE"=>"Nebraska", 
"NV"=>"Nevada", 
"NB"=>"New Brunswick, Canada", 
"NH"=>"New Hampshire", 
"NJ"=>"New Jersey", 
"NM"=>"New Mexico", 
"NY"=>"New York", 
"NF"=>"Newfoundland, Canada", 
"NC"=>"North Carolina", 
"ND"=>"North Dakota", 
"NT"=>"North West Territory, Canada", 
"MP"=>"Northern Marianas", 
"NS"=>"Nova Scotia, Canada", 
"OH"=>"Ohio", 
"OK"=>"Oklahoma", 
"ON"=>"Ontario, Canada", 
"OR"=>"Oregon", 
"PW"=>"Palau", 
"PA"=>"Pennsylvania", 
"PE"=>"Prince Edward Island, Canada", 
"PR"=>"Puerto Rico", 
"PQ"=>"Quebeq, Canada", 
"RI"=>"Rhode Island", 
"SK"=>"Saskatchewan, Canada", 
"SC"=>"South Carolina", 
"SD"=>"South Dakota", 
"TN"=>"Tennessee", 
"TX"=>"Texas", 
"UT"=>"Utah", 
"VT"=>"Vermont", 
"VI"=>"Virgin Islands", 
"VA"=>"Virginia", 
"WA"=>"Washington", 
"WV"=>"West Virginia", 
"WI"=>"Wisconsin", 
"WY"=>"Wyoming", 
"YT"=>"Yukon Territories, Canada"); 

return $thisarr; 

} //end function 

function random_password($length) {
    srand(date("s"));
    $possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $string = "";
    while(strlen($string)<$length) {
        $string .= substr($possible_charactors, rand()%(strlen($possible_charactors)),1);
    }
    return($string);
}

function gen_readable_password()
{
  $words =
  array('dished','mother','basset','detain','sudden','fellow','logged','sonora',
      'earths','remove','dustin','snails','direct','serves','daring','cretan',
      'chirps','reward','snakes','mchugh','uphold','wiring','gaston','nurses',
      'regent','ornate','dogmas','singed','mended','hinges','latent','verbal',
      'grimes','ritual','drying','hobbes','chests','newark','sourer','rumple');

  mt_srand((double) microtime() * 1000000);
  $word_count = count($words);

  $password = sprintf('%s%02d%s',
                    $words[mt_rand(0,$word_count - 1)],
                    mt_rand(0,99),
                    $words[mt_rand(0,$word_count - 1)]);
   return $password;
}



function genpassword($length){

    srand((double)microtime()*1000000);
    $password = '';
    $vowels = array("a", "e", "i", "o", "u");
    $cons = array("b", "c", "d", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "u", "v", "w", "tr",
    "cr", "br", "fr", "th", "dr", "ch", "ph", "wr", "st", "sp", "sw", "pr", "sl", "cl");
     
    $num_vowels = count($vowels);
    $num_cons = count($cons);
     
    for($i = 0; $i < $length; $i++){
        $password .= $cons[rand(0, $num_cons - 1)] . $vowels[rand(0, $num_vowels - 1)];
    }
     
    return substr($password, 0, $length);
}



/*examples:

include("genpassword.function")
echo genpassword(10);

makes a password on 10 chars

*/



function randimg($mdir)
{
# Directory goes here relative to script
$kataloog = $mdir;
# Directory should contain only images, else you can add some more filtering
$i = 0;
$kat = opendir ($kataloog);
	while ($f = readdir($kat))
	{	
		//echo substr($f,0,1);
		
		if ($f != '.' && $f != '..' && substr($f,0,1) == "x" ) # Directory should contain only images, else you can add some more filtering
		{		
				{$nimed[$i++] = $f;}
		}
	}
closedir ($kat);
srand((double)microtime()*1000000);
$nimi = $kataloog.'/'.$nimed[rand(0,sizeof($nimed)-1)];
$suurus = GetImageSize ($nimi);
//echo $nimi;

// 
$randlink = get_link_from_picture($nimi);
 
//echo $nimi.'   '.$randlink['descrip'];
 
if (!$randlink)
{
	$mret = "<img src= $nimi  $suurus[3] alt=$nimi >   ";
}
else
{
   //$url = "show_invt.php?item=".($randlink["item"])."&catid=home";
	 //do_html_url_no_br($url, $randlink['title'])." <br>";
	 
	 //$mret = "<img src= $nimi  $suurus[3] alt='".($randlink["title"])."'>";
	 //$link = "dept=".urlencode($randlink["dept"])."&grp=".urlencode($randlink["groupname"])."&prod=".urlencode($randlink["product"]);
	 $dept = urlencode($randlink["dept"]);
	 $group = urlencode($randlink["groupname"]);
	 $product = urlencode($randlink["product"]);
	 //echo 'Click Image for more info.'; //$dept.' '.$group.' '.$product.'<br>';
	 $mret = "<a href=showprod.php?dept=$dept&grp=$group&prod=$product><img src= $nimi  $suurus[3] alt='".urlencode($randlink["name"])."'></a>Click this image for more info.";

}

RETURN $mret;
 }
 
 function get_link_from_picture($imgname)
 {
   // query database for all details for a particular invt

   $conn = db_connect();
   $query = "select * from products where smallimg='$imgname'";
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;

   $thisresult = @mysqli_fetch_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   return $thisresult;
 }
 
 function ziplookup($this_zip)
 {
    // limit to zip5
	$this_zip = substr($this_zip,0,5);
 	$conn = db_connect();
	$query = "SELECT * from zip5 WHERE zip = '$this_zip'";
	$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " . ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
 	if (mysqli_num_rows($result) == 1)
	{
		$row = mysqli_fetch_assoc($result);
		//echo $row['city'].'   '.$row['state'].'   '.$row['zip'];
		((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
		return $row;
	}
	else
		return false;
	
 }
 
 function get_stationeryitems($mfilter)
{
   // query database for a list 
   $conn = db_connect();
   //echo "conn=".$conn;
   $query = "select ITEM,DESCRIP,PRICE,QOH,COLORGROUP
             from stationery
			 WHERE $mfilter 
			 order by ITEM"; 
	
	//echo $query;
	
   $result = @mysqli_query( $conn, $query);
   if (!$result)
      return false;
   $num_cats = @mysqli_num_rows($result);
   if ($num_cats ==0)
      return false;  
   $thisresult = db_result_to_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);      
   return $thisresult; 
}

function get_one_item($item)
{
  $conn = db_connect();
   $query = "select * from invt where item='$item'";   
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   if ($num_invt ==0)
      return false;
   $thisresult = db_result_to_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   return $thisresult;
}

function get_item_price($item)		//gets the price of an invt item 1/6/03
{
  $conn = db_connect();
   $query = "select price from invt where item='$item'";   
   $result = @mysqli_query( $conn, $query);
   if (!$result)
     return false;
   $num_invt = @mysqli_num_rows($result);
   if ($num_invt ==0)
      return false;
   $thisitem = db_result_to_array($result);
   ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
   return $thisitem[0][0];
}

function left($str,$n)
{
	return substr($str,0,$n);
}

function right($str,$n)
{
	return substr($str,-$n);
}


function session_dump() { 
 // dumps info about a session 
 $session_array = explode(";",session_encode()); 
 $html = "<!-- SESSION VARIABLES DUMP\n\n"; 
 for ($x = 0; $x < count($session_array); $x++) { 
   $html .= "     $session_array[$x] \n"; 
 } 
 $html .= " -->\n\n"; 
 return $html; 
} 

?>