<?php

// Rev.  03/27/2013 -ref- Added Google Analytics Code in silk_footer()

function page_footer()
{
  echo '</body></html>';
  return true;
}
function silk_footer($align,$width)
{
  $g_year = date('Y');
  echo "<table border=0  align=$align width=$width><tr><td class='footer'>
  Copyright &copy; Twinrocker, Inc. $g_year Content</td></tr>
  <tr><td class='footer'>Dynamics by 
    <a class='fibermenu' href='http://www.refrost.com' target='_blank'>REF&A</a>
  </td></tr>
  </table>";
  ?>
 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39646714-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php
  echo "</body></html>";
}

function do_html_header($title,$catid)  //,$sship_type
{
  // print an HTML header
 
  // declare the session variables we want access to inside the function 
  // original totals with cart:
  //ession_start();
    
  //handle 'odd' Catid's : redirect to 'home'
  
  if ($catid == "FIO"||$catid=="PAO" || $catid=="PUO")
  	$xid='home';
  else
    $xid=$catid;
	
  //echo $GLOBALS["fiber_items"];
  //echo $GLOBALS["fiber_weight"];		
  
  $_SESSION['companycolor'] = "ffe7c6"; //pinkish   //#d0e0e0"; //greenish
  $_SESSION['buttoncolor']  = "#c6eff7";
  $_SESSION['menubgcolor']  = "#c6e7de"; //blue-green  //"#ffcc00";
  $_SESSION['catmenubgcolor'] = "white";
  $_SESSION['barsbgcolor'] = '#e0e0e0';
  
  //needs to have external stylesheet
?>
  <html>
  <head>
  <title><?php echo $title; ?></title>
	<!-- <script type="text/javascript" language="Javascript"> </script>  -->
	
    <style type ="text/css">
      h2 { font-family: Arial, Helvetica, sans-serif; font-size: 18pt; color: black; margin: 2px }
	  h3 { font-family: Arial, Helvetica, sans-serif; font-size: 14pt; color: black; margin: 2px }
	  h4 { font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: black; margin: 2px }
	  h5 { font-family: Arial, Helvetica, sans-serif; font-size: 8pt; color: black; margin: 2px  }
      body,td,th { font-family: Arial, Helvetica, sans-serif; font-size: 9pt }
      li { font-family: Arial, Helvetica, sans-serif; font-size: 9pt; margin: 2px }
      hr { color: #FF0000; width=85%; text-align=center}
	  hr.70 { color: #FF0000; width=70%; text-align=center}
      hr.50 { color: #FF0000; width=50%; text-align=center}
	  A:visited { color:blue;text-decoration:none }
	  A:link { text-decoration:none }	  
	  A:hover   {background:blue;color:white;text-decoration:none}
      table.search  {
	 		background:#c6e7de;
			color:#000000;
	  		font-family:Arial;
			font-weight:normal;
		  	font-size:10px }

    </style>
  </head>
  
   <body topmargin = 0 leftmargin=0 rightmargin=0  >
  
  
  <table rules='none' width=100% border=1 bgcolor=<?php echo $_SESSION['companycolor'];?> cellpadding = 0 cellspacing = 0>  
  <tr>
  <td align = left valign=BOTTOM NOWRAP>
  <?php
  /*
  <!--	  A:link    {color:blue;text-decoration:underline}
	  A:visited {background:none;color:#840052;text-decoration:underline} <!- //#D63194 -->
	  A:hover   {background:blue;color:white;text-decoration:none}
	  A:active  {background:black;color:white;text-decoration:none}
	  * 
	  * 	  
	  * 
	  a { color: #000000 }
	  A:link { text-decoration:none }
	  A:visited { text-decoration:none }
	  A:active { text-decoration:none }
	  A:hover { text-decoration:underline; color:#FF0000 }
	  A:hover { background:#FFFFFF;text-decoration:underline; color:#FF0000 }
-->
  */
  //<!--
  //<a href = "index.php"><img src="images/invt-O-Rama.gif" alt="invtorama" border=0
  //     align=left valign=bottom height = 55 width = 325></a>
  //Papermaking and Book Arts 
  //-->
 
  echo "<a STYLE=background:none  href = index.php?catid=$xid ><h3>Twinrocker Papermaking and Book Arts Supplies</h3></a>";
 
  ?>
 
  </td>
  <td align = right valign='BOTTOM'>
  <?php
  	 // Indicate if items/fiber/pulp order has been placed 
	 
	 $mark = '';
     if ($_SESSION['fiber_items']>0)
	 	$mark = 'F';
     if ($_SESSION["items"]>0)   //($_SESSION['items']>0)
	  	$mark = $mark.'I';
     if ($_SESSION['pulp_items']>0)
	 	$mark = $mark.'P';	
	 
	 	
/*     if (isset($_SESSION['admin_user']))
       echo "&nbsp;";
     else
*/
       echo '<font size=1 color=#ff0000>'.$mark."</font>&nbsp;Total Items =".($_SESSION["items"]+$_SESSION["fiber_items"]+$_SESSION["pulp_items"])."&nbsp"; 
  ?>
  </td>
  <td align = right valign='BOTTOM'  bgcolor=>&nbsp;
  
  	<?php if (isset($_SESSION["SESSION_ADNAME"])) echo do_html_URL_no_br('silkadmin.php','Admin:').'&nbsp;'.$_SESSION["SESSION_ADNAME"]; ?>



	
         <!--<table border=0 bgcolor= width=100% >
         <tr>
         <td width=30 nowrap bgcolor=> <a href=login.php>Login </a> </td> 
		 <td>&nbsp;</td>
         <td width=30 nowrap bgcolor=> <a href=login.php>Help </a> </td> 
         </tr>
         </table>  -->

  </td>
  </tr>
  <tr>
  <td align = left valign = baseline><h5>&nbsp;Brookston, Indiana</h5></td>
  <td align = right valign = 'baseline'>
  <?php
/*   if (isset($_SESSION['admin_user']))
       echo "&nbsp;";
     else
*/
       echo "Total Price = $".number_format($_SESSION["total_price"]+$_SESSION["fiber_price"]+$_SESSION["pulp_price"],2); 
       echo "&nbsp;";
  ?>
  </td>
  <td align = right valign='BOTTOM'>&nbsp; 
  <?php if (isset($_SESSION["SESSION_UNAME"])) echo $_SESSION["SESSION_UNAME"]; 
			
     //if(isset($_SESSION['admin_user']))
      // display_button("logout.php", "log-out", "Log Out");
      //else
       //<td align = right rowspan = 2 valign='BOTTOM'>
       //display_button("show_cart.php", "view-cart", "View Your Shopping Cart");
       //echo "<a  HREF=clear_cart.php>Clear Cart</a>&nbsp;&nbsp; " ;  //STYLE=background:$buttoncolor;color:blue
       //echo "<a  HREF=show_cart.php?catid=$catid >View-cart</a>&nbsp;&nbsp;" ; //STYLE=background:$buttoncolor;color:blue
    ?>
  </td>
  </tr>
  </table>
<?php
  do_menu($catid);
  
  if($title)
    do_html_heading($title);

}


function do_html_footer($nomenu='')
{
  // print an HTML footer
  //phpinfo();
  //$_SERVER["REQUEST_URI"]
  //<a href='index.php'>Home</a> &nbsp;&nbsp;&nbsp;
?>
  </center>
  <br>
  <?php if (!$nomenu) small_menu(); ?>
  <br><font size="1">
   Copyright (c) 2002 Twinrocker  -   Content  -  All rights reserved.<br>
   <?php if (isset($_SESSION['last_file_update'])) echo "   Price and quantity data last updated on ".$_SESSION['last_file_update'].'<br>'?>
   Powered by&nbsp;&nbsp;<a href='http://www.refrost.com'>Silk 2020</a>  
   <?php if (isset($_SESSION["SESSION"]) && $_SESSION["SESSION"]= 'Admin') {echo '  &nbsp;&nbsp;&nbsp;&nbsp;Maintain ';do_html_URL('http://www.refrost.com/cgi-bin/mojo/mojo.cgi','Maillist');} ?>
   </font>
  </body>
  </html>
<?php
}

function do_html_heading($heading)
{
  // print heading
  echo "<b>";
  echo $heading;
  echo "</b>";
}

function do_html_URL($url, $name)
{
  // output URL as link and br
?>
  <a href="<?php echo $url; ?>"><?php echo $name; ?></a><br />
<?php
}

function do_html_URL_class($url, $name,$class='')
{
  // output URL as link and br

 echo "<a href='$url' class='$class' >$name</a><br />";

}



function do_html_URL_no_br($url, $name, $aclass='')  // Works in cart to list caegories
{
  // output URL as link with no <br>

	echo  "<a $aclass href=$url>$name</a>";
	return;
}


// Products 11/13/03 THIS is COOL!
function display_products($invt_array,$query='',$showprod='')
{
  //Display list of Products cntaining search terms
  //This routine build from display_invt (see below)
  //Array contains `product`,`name`,`smallimg`,`dept`,`groupname` from products

  if (!is_array($invt_array))
  {
     echo "<br>No products contain the search phrase(s)<br>";
  }
  else
  {
    //create table   ...width = 70%
    echo "<table  border = 0>";  // width = 70% in original
    echo "<thead>";
	  echo "<th colspan=2 class=text9px_grey align='left' bgcolor='#e0e0e0'>Select a link below and then use the Back button on your browser to return to this list.</th>";  # 11/13/03 -ref-
	  //echo "<th class=text9px_grey align='left' bgcolor='#e0e0e0'>Description</th>";
	  echo "</thead>";
  	echo "<tbody>";
    //create a table row for each product item
    foreach ($invt_array as $row)
    {

	      echo "<tr><td class=text9px>";

        if ($row["smallimg"])   // Show smallimg
        {
          //echo $row["smallimg"];
          $ht = '';
          $wd = '';
          if ($row['height'])
   			    $ht = " height=".$row['height'];
	        if ($row['width'])
   			    $wd = " width=".$row['width'];

          ?>
          <img src= <?php echo $row["smallimg"]."  ".$wd."  ".$ht; ?> >
          <?php
        }
        else
        {
          echo "&nbsp;";
        }
 	      //echo "</td><td><font color=blue>". $descrip.'</font>';
	      // Make URL to show the product
        echo "</td><td class=text9px>";
        echo product_URL_no_br("showprod.php?dept=".urlencode($row["dept"])."&grp=".urlencode($row["groupname"])."&prod=".urlencode($row["product"]),stripslashes($row["name"]),"");

        echo '</td></tr>';
    }
    echo '</table>';
  }

}



function display_invt($invt_array,$query='',$showprod='')
{
  //display all invt in the array passed in
  // 09/26/2016 edited display_invt() to filter out Onhand<1 ( not cat=CH)
  //$gotouri = parse_url(getenv("HTTP_REFERER"));  	//REQUEST_URI
/*  echo $gotouri['scheme'];
  br();
  echo $gotouri['host'];
  br();
  echo $gotouri['query'];
  br();
*/  
  if (isset($_REQUEST['catid']))
  	$catid = $_REQUEST['catid'];
  else
  	$catid = '';
  //$invt_array = $_GET['invtarray'];
  
    
  if (!is_array($invt_array))
  {
     echo "<br>No items currently available in this category<br>";
  }
  else
  {
    //create table   ...width = 70%
    echo "<table class=text9px border = 0>";  // width = 70% in original
    echo "<thead>";  
	echo "<th>&nbsp;</th>";
	echo "<th align='left' bgcolor='#e0e0e0'>Item Number</th>";  # 10/9/02 -ref-
	echo "<th align='left' bgcolor='#e0e0e0'>Items &nbsp;&nbsp;</th>";
	echo "<th align='right' bgcolor='#e0e0e0'>$ Unit Price <br>(Add-to-Cart)</th>";
	if ($query)
			echo "<th align='center' bgcolor='#e0e0e0'>Paper/Stationery<br>Qty onhand</th>";
	echo "</thead>";
	echo "<tbody>";
    
    //create a table row for each invt item
    foreach ($invt_array as $row)
    {
	  // Display all chemical 'CH' items
      // or otherwise items with 1 or more qty/available. 09/26/2016
      if (($row["qty"]>1 || ($row["onhand"]-$row["aloc"])>1) || ($row["catid"] == 'CH'))
      { //startfilter
	  echo "<tr><td>";
      $url = "show_invt.php?item=".($row["item"])."&catid=$catid";
	  if ($query)		//11/04/02 change redirection on Queries
	  {
	  	// if called from showprod.php, want to return there..
	    if (!$showprod)
			$buy = "add2_cart.php?new=".($row["item"])."&catid=$catid&searchstr=$query";
		else
			$buy = "add2_cart.php?new=".($row["item"])."&catid=$catid&searchstr=$query&gotouri=silkfind.php?searchstr=$query";
	  }
	  else
	    $buy = "add2_cart.php?new=".($row["item"])."&catid=$catid";  //show_cart    &addonly=NO
		
	  // $buy = "show_cart.php?new=".($row["item"]);
	  //echo $buy;
	  
      if (false)   // (@file_exists("images/".$row["item"].".jpg"))  //-ref- skip image display 
      {
        $descrip = "<img src=\"images/".($row["item"]).".jpg\" border=0 width=60 height=80>";
        do_html_url($url, $descrip);
      }
      else
      {
        echo "&nbsp;";
      }
	  //echo $buy;
	  echo "</td><td><font color='black'>".$row["item"].'</font>';	# 10/9/02 -ref-
      //echo "</td><td>";
      $descrip =  $row["descrip"];  # ." by ".$row["author"];
	  //if ($query)
	  	echo "</td><td><font color=blue>". $descrip.'</font>';
	  //else
	  //{
      //  echo "</td><td>"; 
	  //	do_html_url($url, $descrip);
	  //}
      echo '</td>';
	  echo '<td align="right">';
	  $descrip = number_format($row["price"], 2);
	  do_html_url($buy, $descrip);
	  echo '</td>';
	  $firstchar = substr($row["item"],0,1);
	  if ($query and ($firstchar=='Z' OR $firstchar=='Y'))
	  	echo '<td align=center>'.number_format(($row["onhand"]-$row["aloc"]), 0).'</td>';
	  echo '</tr>';
      }   //endfilter
    }   // endfor printing row
    echo "</table>";
  }
  
}
 



function display_invt_details($invt)
{
  // display all details about this invt
  $catid = $_SESSION['catid'];
  if (is_array($invt))
  {
    
    echo "<br><br>
		  <table><tr>"; 
    //display the picture if there is one in an images director named the same at item #
    if (@file_exists("images/".($invt["item"]).".jpg"))
    {
      $size = GetImageSize("images/".$invt["item"].".jpg");
      if($size[0]>0 && $size[1]>0)
        echo "<td><img src=\"images/".$invt["item"].".jpg\" border=0 width=120 height = 180 ></td>"; //".$size[3]."
    }
	else
	{
		// SILK: have imagesrc from invt table
		if ($invt["img"])
		{
			  $bigimg = str_replace("x","",$invt["img"]);  // removes "x" from TR jpg name 8/14/02
			  
		      $size = GetImageSize($invt["img"]);
      		  if($size[0]>0 && $size[1]>0)
			  	// TR - if $bigimg exists then show small picture as a hyperlink.  
				if (file_exists($bigimg))
        	  		echo "<td> <a href=$bigimg> <img src=".$invt["img"]." border=0 width=".$size[0]." height = ".$size[1]." ></a></td>"; //".$size[3]."
				else
					echo "<td> <img src=".$invt["img"]." border=0 width=".$size[0]." height = ".$size[1]." ></td>"; //".$size[3]."

		}
	
	}
    echo "<td>&nbsp;<ul>";
    # echo "<li><b>Author:</b> ";
    # echo $invt["author"];
	
	//echo $invt["catid"];
	
    echo "<li><b>Item:</b> ";
    echo $invt["item"];
    echo "<li><b>Our Price:</b> ";
    echo number_format($invt["price"], 2);
    echo "<li><b>Description:</b> ";
    echo $invt["description"];
    echo "</ul></td></tr></table>"; 
  }
  else
    echo "The details of this item cannot be displayed at this time.";
  echo "<hr>";
}

function display_checkout_form()
{
  //display the form that asks for name and address
  GLOBAL $catid;
  
  	if (!isset($name))
  {
  	// If doesn't exist, initialize the variables'
	$name = ' &nbsp;';
	$address1 = ' &nbsp;';
	$address2 = ' &nbsp;';
	$city = ' &nbsp;';
	$state = ' &nbsp;';
	$zip = ' &nbsp;';
	$phone = ' &nbsp;';
	$email = ' &nbsp;';
	$country = 'USA';
	$ponumber = ' &nbsp;';
	$ccno = ' &nbsp;';
	$cc_expdate = ' &nbsp;';
	$cc_address = ' &nbsp;';
	$comment = ' ';
	$type = ' &nbsp;';
	$taxid = ' &nbsp;';
  }
 
   if (!isset($ship_name))
   {
    $ship_name = $name;
	$ship_address1 = $address1;
	$ship_address2 = $address2;
	$ship_city = $city;
	$ship_state = $state;
	$ship_zip = $zip;
	$ship_phone = $phone;
	$ship_email = $email;
	$ship_country = $country;
   }
  
  
echo "
  <br>
  <table border = 0 width = 100% cellspacing = 0>
  <form action = purchase.php?catid=$catid method = post>	
  <tr><th bgcolor=#cccccc>".(date ("dS of F Y h:i:s A"))."</th>
      <th align=left bgcolor=#cccccc>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Order Details</th></tr>
  <tr>
    <td>Name</td>
    <td><input type = text name = name value = $name maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Address1</td>
    <td><input type = text name = address1 value = $address1 maxlength = 40 size = 40></td>
  </tr>
    <tr>
    <td>Address2</td>
    <td><input type = text name = address2 value = $address2 maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>City</td>
    <td><input type = text name = city value = $city maxlength = 20 size = 20></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input type = text name = state value = $state maxlength = 20 size = 20></td>
  </tr>
  <tr>
    <td>Zip Code</td>
    <td><input type = text name = zip value = $zip maxlength = 12 size = 12></td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input type = text name = phone value = $phone maxlength = 20 size = 20></td>
  </tr>  
  <tr>
    <td>email</td>
    <td><input type = text name = email value = $email maxlength = 40 size = 40></td>
  </tr>   
  <tr>
    <td>Country</td>
    <td><input type = text name = country value = $country maxlength = 40 size = 40></td>
  </tr>
  
   <tr>
    <td>Tax ID </td>
    <td><input type = text name = taxid value = $taxid maxlength = 40 size = 40>
	<b>Sale Tax ??<b></td>
  </tr>  
  
   <tr>
    <td>PO Number</td>
    <td><input type = text name = ponumber value = $ponumber maxlength = 40 size = 40></td>
  </tr>
   <tr>
    <td>CC#</td>
    <td><input type = text name = ccno value = $ccno maxlength = 20 size = 20>
    Expires
    <input type = text name = cc_expdateno value = $cc_expdate maxlength = 15 size = 15>
	</td>

  </tr>
   <tr>
    <td>CC Address</td>
    <td><input type = text name = cc_address value = $cc_address maxlength = 40 size = 40></td>
  </tr>
  
   <tr>
    <td>Comment</td>
    <td><textarea name = comment rows=2 cols=60 wrap> $comment
	    </textarea> </td>
  </tr>
  
    <tr>
    <td>Customer Type </td>
    <td><input type = text name = type value = $type maxlength = 10 size = 10></td>
  </tr>
  <tr><th colspan = 2 bgcolor=#cccccc>Shipping Address (leave blank if as above)</th></tr>
  <tr>
    <td>Name</td>
    <td><input type = text name = ship_name value = $ship_name maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Address1</td>
    <td><input type = text name = ship_address1 value = $ship_address1 maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td>Address2</td>
    <td><input type = text name = ship_address2 value = $ship_address2 maxlength = 40 size = 40></td>
  </tr>  
  <tr>
    <td>City</td>
    <td><input type = text name = ship_city value = $ship_city maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>State</td>
    <td><input type = text name = ship_state value = $ship_state maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td>Zip Code</td>
    <td><input type = text name = ship_zip value = $ship_zip maxlength = 12 size = 12></td>
  </tr>
  <tr>
    <td>Phone</td>
    <td><input type = text name = ship_phone value = $ship_phone maxlength = 20 size = 20></td>
  </tr> 
  <tr>
    <td>email</td>
    <td><input type = text name = ship_email value = $ship_email maxlength = 40 size = 40></td>
  </tr>      
   <tr>
    <td>Country</td>
    <td><input type = text name = ship_country value = $ship_country maxlength = 20 size = 40></td>
  </tr>
  <tr>
    <td colspan = 2 align = center>
      <b>Please press Purchase to confirm your purchase,
         or Continue Shopping to add or remove items</b>" ;
      display_form_button("purchase", "Purchase These Items"); 
	  ?>
    </td>
  </tr>
  </form>
  </table><hr>"

<?php
}

function display_shipping($shipping)
{
  // display table row with shipping cost and total price including shipping
  global $total_price;
?>
  <table border = 0 width = 100% cellspacing = 0>
  <tr><td align = left>Shipping</td>
      <td align = right> <?php echo number_format($shipping, 2); ?></td></tr>
  <tr><th bgcolor="#cccccc" align = left>TOTAL INCLUDING SHIPPING</th>
      <th bgcolor="#cccccc" align = right>$<?php echo number_format($shipping+$total_price, 2); ?></th>
  </tr>
  </table><br>
<?php
}

function display_card_form($name)
{
  //display form asking for credit card details
?>
  <table border = 0 width = 100% cellspacing = 0>
  <form action = process.php?catid=<?php echo $catid;?> method = post>
  <tr><th colspan = 2 bgcolor="#cccccc">Credit Card Details</th></tr>
  <tr>
    <td>Type</td>
    <td><select name = card_type><option>VISA<option>MasterCard<option>American Express</select></td>
  </tr>
  <tr>
    <td>Number</td>
    <td><input type = text name = card_number value = "" maxlength = 16 size = 40></td>
  </tr>
  <tr>
    <td>AMEX code (if required)</td>
    <td><input type = text name = amex_code value = "" maxlength = 4 size = 4></td>
  </tr>
  <tr>
    <td>Expiry Date</td>
    <td>Month <select name = card_month><option>01<option>02<option>03<option>04<option>05<option>06<option>07<option>08<option>09<option>10<option>11<option>12</select>
    Year <select name = card_year><option>00<option>01<option>02<option>03<option>04<option>05<option>06<option>07<option>08<option>09<option>10</select></td>
  </tr>
  <tr>
    <td>Name on Card</td>
    <td><input type = text name = card_name value = "<?php echo $name; ?>" maxlength = 40 size = 40></td>
  </tr>
  <tr>
    <td colspan = 2 align = center>
      <b>Please press Purchase to confirm your purchase,
         or Continue Shopping to add or remove items</b>
     <?php //display_form_button("purchase", "Purchase These Items"); 
	    echo "<a href=purchase.php>Purchase these items<a>"; ?>
    </td>
  </tr>
  </table>
<?php
}




function display_cart($cart,$catid, $change = true, $images = 0)
{
  // display items in shopping cart
  // optionally allow changes (true or false)
  // optionally include images (1 - yes, 0 - no)
 
 $bgcolor = $_SESSION['barsbgcolor'];
 echo "<table border=1 align=center width=100% cellspacing=0>";
 echo "<form action = recalc_cart.php method = post>";
 echo " <tr>
 		<th bgcolor=$bgcolor width=110>Quantity</th>
		<th bgcolor=$bgcolor width=180>Item </th>
		<th align=center  bgcolor=$bgcolor>No. & Comment</th>
        <th bgcolor=$bgcolor width=60>Weight</th>
        <th bgcolor=$bgcolor width=60>Price</th>
        <th bgcolor=$bgcolor colspan=2 align = right width=90>Total&nbsp;&nbsp;</th></tr>";
		
		//<th colspan = ". (1+$images) ." bgcolor=$bgcolor>Item </th>


  //display each item as a table row
  foreach ($cart as $item => $qty)
  {
    $invt = get_invt_details($item);
    echo "<tr>";
	
	//echo $item."   ".$invt[$item]."    ".$qty."    w=".$invt["weight"];
	
    if($images ==true)
    {
      echo "<td align = left>";
      if (file_exists("images/$item.jpg"))
      {
         $size = GetImageSize("images/".$item.".jpg");  
         if($size[0]>0 && $size[1]>0)
         {
           echo "<img src=\"images/".$item.".jpg\" border=0 ";
           echo "width = ". $size[0]/3 ." height = " .$size[1]/3 . ">";
         }
      }
      else
         echo "&nbsp;";
      echo "</td>";
    }
    echo "<td align=center>";
    // if we allow changes, quantities are in text boxes
    if ($change == true)
      echo "<input type = text name = \"$item\" value = $qty size = 3>";
    else
      echo $qty;
    // echo "</td><td width=180><a href = show_invt.php?item=$item&catid=$catid&ref=cart>".$invt['descrip']."</a>";
	echo "</td><td width=180>".$invt['descrip'];
	echo "</td><td align = left >[".trim($invt["item"])."]";  // ref trs removed _by_
	
	if (left($item,1)!='Y')  // don't include weight of Stationery 1/12/03 (Stationery uses wt field for "code"
    	echo "</td><td align = center width=60>".number_format($invt["weight"], 3);
	else
		echo "</td><td align = center width=60>0.00";
		
    echo "</td><td align = right width=60>$".number_format($invt["price"], 2);
    echo "&nbsp;&nbsp;</td>";
    echo "<td align = right width=90>$".number_format($invt["price"]*$qty,2)."</td></tr>\n";
  }
  // display total row
  echo "<tr>
          <th align = center bgcolor=$bgcolor>" .
              $_SESSION['items']."
          </th>
          <th colspan = ". (1+$images) ." bgcolor=$bgcolor>&nbsp;</th>
          <th align = center bgcolor=$bgcolor>
              &nbsp
          </th>
          <th align = center bgcolor=$bgcolor>"
              .number_format($_SESSION["total_weight"], 3)."
          </th>
          <th align = center bgcolor=$bgcolor>
              &nbsp
          </th>
          <th align = right bgcolor=$bgcolor>
              \$".number_format($_SESSION["total_price"], 2).
          "&nbsp;</th>
        </tr>";
  // display save change button
  if($change == true)
  {
    // Next button (Recalculate)was "Save Changes"
    echo "<tr>
            <td colspan = ". (6+$images) ."  align = left>
              <input type = hidden name = save value = true> 
			  <input type = hidden name = catid value =". $catid.">
              <input type = submit value = 'Recalc items' STYLE='font:8pt ARIAL;background:#c6eff7'>
            </td>
            </tr>";
        /* <input type = image src = \"images/save-changes.gif\" 
                     border = 0 alt = \"Recalculate\"> */
  }
  echo "</form></table>";
}



function display_login_form()
{
  // display form asking for name and password
?>
  <form method=post action="admin.php">
  <table bgcolor=#cccccc>
   <tr>
     <td>Username:</td>
     <td><input type=text name=username></td></tr>
   <tr>
     <td>Password:</td>
     <td><input type=password name=passwd></td></tr>
   <tr>
     <td colspan=2 align=center>
     <input type=submit value="Log in"></td></tr>
   <tr>
 </table></form>
<?php
}

function display_admin_menu()
{
?>
<br>
<a href="index.php">Go to main site</a><br>
<a href="insert_category_form.php">Add a new category</a><br>
<a href="insert_invt_form.php">Add a new invt</a><br>
<a href="change_password_form.php">Change admin password</a><br>
<?php

}


function display_password_form()
{
  // display html change password form
?>
   <br>
   <form action="change_password.php" method=post>
   <table class=login width=250 cellpadding=2 cellspacing=0 bgcolor=#cccccc>
   <tr><td class=login>Old password:</td>
       <td ><input type=password name=old_passwd size=16 maxlength=16  autofocus></td>
   </tr>
   <tr><td class=login>New password:</td>
       <td ><input type=password name=new_passwd size=16 maxlength=16></td>
   </tr>
   <tr><td class=login>Repeat new password:</td>
       <td class=login><input type=password name=new_passwd2 size=16 maxlength=16></td>
   </tr>
   <tr><td class=login colspan=2 align=center><input class=login type=submit value="Change password">
   </td></tr>
   </table>
   <br>
<?php
};

function display_forgot_username_form()
{
  // display HTML form to email username
  //1/23/2010 -ref-
?>
   <br>
   <form action="forgot_username.php" method=post>
   <table class=login width=250 cellpadding=2 cellspacing=0 bgcolor=#cccccc>
   <tr><td class=login >Enter your email</td>
       <td><input type=text name=email size=16 maxlength=60 autofocus></td>
   </tr>
   <tr><td class=login colspan=2 align=center><input class=login type=submit value="Get username">
   </td></tr>
   </table>
   <br>
<?php
};

function display_forgot_password_form()
{
  // display HTML form to reset and email password
?>
   <br>
   <form action="forgot_password.php" method=post>
   <table class=login width=250 cellpadding=2 cellspacing=0 bgcolor=#cccccc>
   <tr><td class=login >Enter your username</td>
       <td><input type=text name=username size=16 maxlength=16 autofocus></td>
   </tr>
   <tr><td class=login colspan=2 align=center><input class=login type=submit value="Get new password">
   </td></tr>
   </table>
   <br>
<?php
};

function display_forgot_question_form($question)
{
  // display HTML form to reset and email password
?>
   <br>
   <form action="forgot_password2.php" method=post>
   <table class=login width=250 cellpadding=2 cellspacing=0 bgcolor=#cccccc>
   <tr><td class=login colspan=2><b>Please answer the following question.</b></td>
   </tr>
   <tr><td class=login ><?php echo $question ;?></td>
       <td><input type=text name=answer size=20 maxlength=20 autofocus ></td>
   </tr>
   <tr><td class=login colspan=2 align=center>
   <input class=login type=submit value="Check my answer">
   </td></tr>
   </table>
   <br>
<?php
};


function display_button($target, $image, $alt)
{
/*  
  echo "<center><a href=\"$target\"><img src=\"images/$image".".gif\" 
           alt=\"$alt\" border=0 height = 45 width = 118></a></center>";
*/           
 		
  echo "<br><center><STYLE='font:14pt ARIAL;background:#c6eff7'>
        <a href=$target><<&nbsp;". ucwords($image)."&nbsp;>></a>
        </STYLE></center>";

  //echo "<center><input type = submit value = 'Save Changes' STYLE='font:9pt ARIAL;background:#c6eff7'>";
}

function display_form_button($image, $alt)
{
  echo "<center><input type = image src=\"images/$image".".gif\" 
           alt=\"$alt\" border=0 height = 45 width = 118></center>";

}

?>
