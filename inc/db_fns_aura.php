<?php

function conn() {
    // create mysql Aura/Sql ExtendedPDO object  3/26/2018 
    static $cnt = null;

    if(!isset($cnt)) { 
        $cnt = new Aura\Sql\ExtendedPdo(
        'mysql:host=127.0.0.1;dbname=fsci2',
        'fsci2user',
        'victory',
        [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4', 
         PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
         PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
         PDO::ATTR_EMULATE_PREPARES   => false
        ], // driver attributes/options as key-value pairs
        []  // queries to execute after connection
    );
    return $cnt;
    }
}


function db_connect()
{
// 5/20/2002 -ref-  added 'www.twinrockerhandmadepaper.com' in move to Internet.  7/1/03
// 8/23/2915 Modified  for php5.5 mysqli_* and native driver

static $connection = null;

if(!isset($connection)) {
         // choose beteen production or local develoment credentials...

         if ( preg_match("/frostscientific/i",$_SERVER["SERVER_NAME"]) || preg_match("/fci2/i",$_SERVER["SERVER_NAME"]))
         {
              // echo " got to db ";
         $connection = @mysqli_connect("127.0.0.1",  "fsci2user", "victory", "fsci2",3306,"/var/lib/mysql/mysql.sock"); // or die("Some error connecting to database.");

    } else {
    	  //echo "fsci2 local";
          $connection = @mysqli_connect("localhost",  "fsci2user", "victory", "fsci2", 3306 );// or die("Some error occurred during local connection ");

    }
    if($connection === false) {
        // Handle error - notify administrator, log to a file, show an error screen, etc.
        echo "Error connecting: ". mysqli_connect_error();
        return mysqli_connect_error();
    }
  }
  return $connection;
}


function db_obj()
{
// 5/20/2002 -ref-  added 'fsci2.net' in move to Internet.  7/1/03
// 8/23/2915 Modified  for php5.5 mysqli_* and native driver
// 3/24/2018 Returns connection object

static $connection = null;

if(!isset($connection)) {
         // choose beteen production or local develoment credentials...

    if ( preg_match("/frostscientific/i",$_SERVER["SERVER_NAME"]) || preg_match("/fci2/i",$_SERVER["SERVER_NAME"]))
    {
    	 // echo " got to db ";
         $connection = new mysqli("127.0.0.1", "silkuser", "victory", "fsci2", 3306 ,"/var/lib/mysql/mysql.sock"); // or die("Some error connecting to database.");

    } else {
    	  //echo "fsci2 local";
          $connection = new mysqli("localhost",  "silkuser", "victory", "fsci2", 3306 );// or die("Some error occurred during local connection ");

    }
    if($connection === false) {
        // Handle error - notify administrator, log to a file, show an error screen, etc.
        echo "Error connecting: ". mysqli_connect_error();
        return mysqli_connect_error();
    }
  }
  return $connection;
}




function db_result_to_array($result)
{
   $res_array = array();
   // Adds rows to array (Don't require mysqlnd  via mysqli_fetch_all() )
   while ($row = mysqli_fetch_array($result))
     $res_array[] = $row;

   return $res_array;
}


// Testing... 3/30/2o17 msqli oob

class mysqliSingleton
{
    private static $instance;
    private $connection;
    public $error; //refa added


    private function __construct()
    {
        $this->connection = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    }

    public static function init()
    {
        if(is_null(self::$instance))
        {
            self::$instance = new mysqliSingleton();
        }

        return self::$instance;
    }


    public function __call($name, $args)
    {
        if(method_exists($this->connection, $name))
        {
             return call_user_func_array(array($this->connection, $name), $args);
        } else {
             trigger_error('Unknown Method ' . $name . '()', E_USER_WARNING);
             return false;
        }
    }
}

?>
