<?php
  // Initializes session variables  
  // Updated $_SESSION 6/2010
  // 04/10/2013  Edited  to load safe  pages directly rather than redirect 
  //             to showprod home page   ($firsttimeonpage).
  // 04/10/2013 (Caution: different pages check for searchstr OR ship_state)
  
  IF (!isset($_SESSION['ship_state']))
      $firsttimeonpage = true;
  else  
      $firsttimeonpage = false;

  // set TWINROCKER frame colors: 1/4/03
  $_SESSION['frmlftframebgcolor'] = "#2f4f4f";  // left frame bgcolor  (#2f4f4f - dark green )
  $_SESSION['frmlfttablebgcolor']  = "#8a8987"; // left frame table bgcolor (#8a8987 - flesh color)

  $_SESSION['frmtopbgcolor']  = "#2f4f4f";  // top frame bgcolor
  $_SESSION['frmtopcolor']  = "#ffffff"; 	// top frame text color
  
  $_SESSION['frmrightbgcolor'] = "#ffffff"; 	// right frame bgcolor		 //ltblue--#c6eff7 :pinkishred--#b58c8c
  $_SESSION['frminfotablebgcolor'] = "#c6eff7"; // right frame table bgcolor //"#e0e0e0";  //#c6eff7";	//BGcolor for any "info" table
 
  // the flowing are repeat of silk colors set in inc/output_fns.php  1/5/03 -- FIX
  $_SESSION['companycolor'] = "ffe7c6"; //pinkish   //#d0e0e0"; //greenish
  $_SESSION['buttoncolor']  = "#c6eff7";
  $_SESSION['menubgcolor']  = "#c6e7de"; //blue-green  //"#ffcc00";
  $_SESSION['catmenubgcolor'] = "white";
  $_SESSION['barsbgcolor'] = '#e0e0e0';

  if (!isset($_SESSION['searchstr']))  //11/05/02
  {
	$_SESSION['searchstr'] = '' ;
  }
  
  if (!isset($_GET['catid']))
  {
		$catid = "home";
  }
  else
  {
		$catid = $_GET['catid'];
  }
  
  $_SESSION['catid'] = $catid;
 
  //echo $_SESSION['catid'];
  
	if (!isset($_SESSION['items']))
   { 
		$cart = array(); 
      $_SESSION["cart"] = $cart; //$cart;
      $_SESSION["items"] = 0;
      $_SESSION["total_price"] = 0.00;
      $_SESSION["total_weight"] = 0.00;
		
		// get last update time for inventory/prices   in action_fns.php
 		$_SESSION["last_file_update"] = last_updated(); 
		
	}
	if (!isset($_SESSION['fibercart']))
	{
		//echo 'index fibcart not set';
		$fibercart = array();	 	//row, isbn => qty, unit price, unit wt, catid
 		$_SESSION["fibercart"] =$fibercart; 
		$_SESSION["fiber_items"] = 0;    
	   $_SESSION["fiber_price"] = 0.00;
		$_SESSION["order_items"] = 0;  // for cart+fibercart
		$_SESSION["order_price"] = 0.00;
	}
	if (!isset($_SESSION['pulpcart']))	// 10/19/02 -ref-
	{
   	$pulpcart = array();	 		  //See pulper.php for definitions
   	$_SESSION["pulpcart"] = $pulpcart;
		$_SESSION["pulp_items"] = 0;
		$_SESSION["pulp_shpitems"] = 0;   
	   $_SESSION["pulp_price"] = 0.00;
		$_SESSION["order_items"] = 0;  // for cart+fibercart
		$_SESSION["order_price"] = 0.00;
	}	

  //7/30/04 Added Papercart
  if (!isset($_SESSION['papercart']))
  {
 		$papercart = array();	 	//row, isbn => qty, unit price, unit wt, catid
 		$_SESSION["papercart"] = $papercart;
		$_SESSION["paper_items"] = 0;
        $_SESSION["paper_price"] = 0.00;
		$_SESSION["order_items"] = 0;  // for cart+fibercart+pulpcart+papercart
		$_SESSION["order_price"] = 0.00;
  }

  // 4/12/2013 Implement special discount code in showcart... 
  if (!isset($_SESSION["discount_codes"]))
  { 
    $_SESSION["discount_codes"] =  '';    // comma delimited VALID ACTIVE codes  
    $_SESSION["total_discount"] = 0.00;  // set in check_calc_discount() showcart.php
    
    
  }    


  if (!isset($_SESSION['ship_type']))
  {
		if (!isset($_SESSION["ship_string"]))  //If not retained in clear_cart
		{
			$_SESSION["ship_state"] = "==";   //$ship_state determines default in state dropdown
		}
		$_SESSION["ship_type"] = "ups";
		$_SESSION["ship_charge"] = 0.00;
		$_SESSION["ship_string"] = "No State Selected"; 
		$_SESSION["ship_special"] = ''; 
		$_SESSION["ship_rush"] = ''; 
		$_SESSION["ship_cod"] = ''; 

		//call again to clear warnings
		$sect='supply';
		$mainfile = 'showprod.php';

        $uri = $_SERVER["REQUEST_URI"];
		
		if (strpos($uri,'error') !== false)
			$mainfile = 'weblogin.php';
 		if (strpos($uri,'pa_') !== false)
 		{
          $sect='paper';
          $mainfile = 'pa_main.php';
        }
 		if (strpos($uri,'st_') !== false)
 		{
          $sect='stationery';
          $mainfile = 'st_main.php';
        }

        if (!isset($header))
            $header = '';
            
        if (!isset($_SESSION['SECTION_MAINFILE']))
        {
          $_SESSION["SECTION_NAME"] = $sect;
          $_SESSION["SECTION_HEADER"] = $header;
          $_SESSION["SECTION_MAINFILE"] = $mainfile;
    
          $SECTION_NAME = $sect;
          $SECTION_HEADER = $header;
          $SECTION_MAINFILE = $mainfile;
        }

        // 04/10/2013 -refa- First time on page so re-direct to full URL if safe 
        IF ($firsttimeonpage) {
            // This segment added so external links can be added in Facebook
            // to take users to a specifc item page.  Previously, first page 
            // redirected to 'showprod_php, etc', after setting session vars. 
            // We allow direct loads to 'safepages' only.
              
            $safepages = array('showprod.php','pa_main.php',
                               'st_main.php','tr_fiber.php','tr_pulp.php',
                               'st_pricelist.php');
                               
            // Get script name to check w/ safepages
            $file = $_SERVER["SCRIPT_NAME"];
            $break = Explode('/', $file);
            $currentfile = $break[count($break) - 1];

            if(!in_array($currentfile, $safepages))  {
                // If not a safepage relaod a section page
                header("location:$mainfile");
                exit;
           }
           else {
                // otherwise we're setting the session vars  AND 
                // loading the page  as called  including the anchor
           }
        }
        else {
            // Not firsttime so resort to original behavior -- redirect
            header("location:$mainfile");
            exit;
        }
        //header("location:$mainfile");
        //exit;
  }
	
?>
