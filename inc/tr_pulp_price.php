<?php

  // Pulp price:  SET HERE!!  (And also in pulpitems table in )
  
  // This is in file  inc/tr_pulp_price.php      (inc is a directory in the HOME directory above the www directory
  // 05/09/2011 Stop 'rush' on pulp -- see has_pulp() in showcart and action_fns
  // 08/31/2011 Build $halfstuff array from 'pulpitems' database table'
  //            so that people can edit the active pulp items via the mysql database.

	
	//The '$halfstuff' array contains menu selections that show up on the pulp order form
	//The values are: description, single pail price, discount for 5in4,sbt item number,
	//seleced for items marked active='Y', and set in increding "displayorder",

   
   function pulpitems() {
      // Get pulpitems from database table 'pulpitems' 
      // (called from tr_price.php, and also ordermenory.php to define $halfstuff array.)
      $conn = db_connect();
      $query = "select descrip, price, discount, item
                  from `pulpitems`
                  where active = 'Y'
                  order by displayorder";   
      $result = @mysqli_query($conn, $query);
      if (!$result)
        return false; 
       return db_result_to_array($result);
   }
   
/* The Old way: stopped 8/31/2011 so user can now easily 
   and safely edit the data (not code)-ref-  
     
  $halfstuff = array( array('Cotton Linters #27',47.00,10.00,'PHCL27'),
					array('Cotton Linters #29',47.00,10.00,'PHCL29'),
					array('Cotton Rag #89',47.00,10.00,'PHCR89'),
					array('Black Cotton Rag',58.00,10.00,'PHCRBA'),
					array('Raw Cotton',75.00,0.00,'PRCTTN'),
					array('Raw Flax',75.00,0.00,'PRFL'),
					array('Raw Hemp',75.00,0.00,'PRRH'),
					array('Raw Wheat Straw',75.00,0.00,'PRWS')
					);
  
//	 <-- comments a line  (Old way to edit the active pulpitems. 
//      After 6/2011, edit in mysql table: pulpitems via website control panel)
//	 array('Abaca, Bleached',63.00,10.00,'PHAB'),
//  array('Abaca, Unbleached',63.00,10.00,'PHAU'),
//	 array('Raw Hemp',75.00,0.00,'PRRH'),  //7/20/2008 -ref  In=>7/8/2009
//  array('Hemp',53.00,10.00,'PHH'),      //7/20/2008 -ref
//	 array('Blue Cotton Rag',42.00,10.00,'PHCRBL'),
//	 array('Flax',40.00,9.00,'PHF'),  // removed 3/5/08 -ref-
*/
	
   //Beater prices and options

	$beatopt = array( array('VC','Very coarse',0.00),
		  			array('C','Coarse',0.00),
				   array('MC','Medium coarse',0.00),
		  			array('M','Medium',0.00),
		  			array('MF','Medium fine (+ $5)',5.00),
		  			array('F','Fine (+ $10)',10.00),
		  			array('VF','Very fine (+ $15)',15.00)
				);

	//Custom colorants added (charge by pail here)!
	$colorants = array( array('None',0.00,''),
		  			array('Custom(+$15)',15.00,'/c15')
				);

	//This is the list of types of things the customer intends to use the pulp for
	$pulpusage = array( array('All','All Purpose'),
		 	array('Casting','Casting'),
		 	array('Art','Art Paper'),
		 	array('Book','Book Paper'),
 		 	array('Calligraphy','Calligraphy Paper'),
 		 	array('Special','Special - add comment')
			);

   // SET PRICE HERE!
   $sizecharge = 1;   // sizing charge
   $cmccharge  = 1;   // cmc charge
   $claycharge = 1;   // clay charge
   $calciumcharge = 1;  // calcium charge
   
?>
