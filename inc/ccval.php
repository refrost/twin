<?php

//  7/19/2011 -ref- reimplement Discover card

// From
// http://www.credit-card-processing.tiv.net/validation.html
/*
    * Major credit cards have 13-16 digits
    * First four digits indicate type of card (VISA, MC, AMEX, etc.):
      The first four digits Card Issuer
      3000 to 3059
      3600 to 3699
      3800 to 3889 Diners Club
      3400 to 3499
      3700 to 3799 American Express
      3528 to 3589 JCB
      3890 to 3899 Carte Blanche
      4000 to 4999 Visa
      5100 to 5599 MasterCard
      5610 Australian BankCard
      6011 Discover / Novus
*/
// Below is array for visa,mcard,amex,discover  -ref- 8/9/03

function IsCCNumberValid( $ccnum ) {

// 5/12/2011 Stopped accepting Discovery

# --- Strip out the blanks
$a_CCNumber = preg_replace("/[^0-9]/","", $ccnum);

# --- Card Number has to have 13-16 digits
$NumDigits = strlen( $a_CCNumber );
//br();
//echo $a_CCNumber.'  '.$NumDigits;

if( $NumDigits < 13 or $NumDigits > 16 ) return false;

# --- First 4 digist must be within a certain range

// Limited to visa,amex,mcard
// $aAllValidFirst4Digits = array(
// 	array(4000,4999),
// 	array(3400,3499),
// 	array(3700,3799),
// 	array(5100,5599)
// 	);

// Limited to visa,amex,mcard,discover/novus
$aAllValidFirst4Digits = array(
	array(4000,4999),
	array(3400,3499),
	array(3700,3799),
	array(5100,5599),
	array(6011,6011)
	);
	
/*  ALL Cards
  //removed from
	array(3000,3059),
	array(3600,3699),
	array(3800,3889),
	array(3528,3589),
	array(3890,3899),
	array(5610,5610),
	
$aAllValidFirst4Digits = array(
	array(4000,4999),
	array(3000,3059),
	array(3600,3699),
	array(3800,3889),
	array(3400,3499),
	array(3700,3799),
	array(3528,3589),
	array(3890,3899),
	array(5100,5599),
	array(5610,5610),
	array(6011,6011)
	);
*/

$First4Digits = substr($a_CCNumber, 0, 4);

//echo  '  '.$First4Digits;

$First4DigitsOK = false;
foreach( $aAllValidFirst4Digits as $aV4D ) {
	if( $First4Digits >= $aV4D[0] and $First4Digits <= $aV4D[1] ) {
		$First4DigitsOK = true;
		break;
		}
	}

if( !$First4DigitsOK ) return false;

# --- Control sum of digits must be correct


// Use check from ccval below
// The Luhn formula works right to left, so reverse the number.
    $Num = $a_CCNumber;
    
    $Num = strrev($Num);

    $Total = 0;

    for ($x=0; $x<strlen($Num); $x++) {
      $digit = substr($Num,$x,1);

//    If it's an odd digit, double it
      if ($x/2 != floor($x/2)) {
        $digit *= 2;

//    If the result is two digits, add them
        if (strlen($digit) == 2)
          $digit = substr($digit,0,1) + substr($digit,1,1);
      }

//    Add the current digit, doubled and added if applicable, to the Total
      $Total += $digit;
    }

//  If it passed (or bypassed) the card-specific check and the Total is
//  evenly divisible by 10, it's cool!
    if ($Total % 10 == 0) return true; else return false;
}


  function CCVal($Num, $Name = 'n/a') {

//  Innocent until proven guilty
    $GoodCard = true;

//  Get rid of any non-digits
    $Num = preg_replace("/[^0-9]/","", $Num);

//  Perform card-specific checks, if applicable
    switch ($Name) {

    case "mcd" :
      $GoodCard = preg_match("/^5[1-5][0-9]{14}$/", $Num);
      break;

    case "vis" :
      $GoodCard = preg_match("/^4[0-9]{15}$|^4[0-9]{12}$/", $Num);
      break;

    case "amx" :
      $GoodCard = preg_match("/^3[47][0-9]{13}$/", $Num);
      break;

    case "dsc" :
      $GoodCard = preg_match("/^6011[0-9]{12}$/", $Num);
      break;

    case "dnc" :
      $GoodCard = preg_match("/^30[0-5][0-9]{11}$|^3[68][0-9]{12}$/", $Num);
      break;

    case "jcb" :
      $GoodCard = preg_match("/^3[0-9]{15}$|^2131|1800[0-9]{11}$/", $Num);
      break;
    }

//  The Luhn formula works right to left, so reverse the number.
    $Num = strrev($Num);

    $Total = 0;

    for ($x=0; $x<strlen($Num); $x++) {
      $digit = substr($Num,$x,1);

//    If it's an odd digit, double it
      if ($x/2 != floor($x/2)) {
        $digit *= 2;

//    If the result is two digits, add them
        if (strlen($digit) == 2) 
          $digit = substr($digit,0,1) + substr($digit,1,1);
      }

//    Add the current digit, doubled and added if applicable, to the Total
      $Total += $digit;
    }

//  If it passed (or bypassed) the card-specific check and the Total is
//  evenly divisible by 10, it's cool!
    if ($GoodCard && $Total % 10 == 0) 
      return true; 
    else 
      return false;
  
}
?>
