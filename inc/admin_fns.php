<?php
// This file contains functions used by the admin interface 

function display_category_form($category = "")
// This displays the category form.
// This form can be used for inserting or editing categories.
// To insert, don't pass any parameters.  This will set $edit
// to false, and the form will go to insert_category.php.
// To update, pass an array containing a category.  The
// form will contain the old data and point to update_category.php.
// It will also add a "Delete category" button.
{
  // if passed an existing category, proceed in "edit mode"
  $edit = is_array($category);

  // most of the form is in plain HTML with some
  // optional PHP bits throughout
?><form method=post 
      action="<?=$edit?"edit_category.php":"insert_category.php";?>"> 
  <table border=0>
  <tr>
    <td>Category Name:</td>
    <td><input type=text name=catname size=40 maxlength=40
          value="<?=$edit?$category["catname"]:""; ?>"></td>
   </tr>
  <tr>
    <td <? if (!$edit) echo "colspan=2"; ?> align=center>
      <? if ($edit) 
         echo "<input type=hidden name=catid 
                value=\"".$category["catid"]."\">";
      ?>
      <input type=submit 
       value="<?=$edit?"Update":"Add"; ?> Category"></form>
     </td>
     <? if ($edit)
       // allow deletion of existing categories 
       {
          echo "<td>";
          echo "<form method=post action=\"delete_category.php\">";
          echo "<input type=hidden name=catid value=\"".$category[catid]."\">";
          echo "<input type=submit value=\"Delete category\">";
          echo "</form></td>";
       }
     ?>
  </tr>
  </table>
<?
}

function display_book_form($book = "")
// This displays the book form.
// It is very similar to the category form.
// This form can be used for inserting or editing books.
// To insert, don't pass any parameters.  This will set $edit
// to false, and the form will go to insert_book.php.
// To update, pass an array containing a book.  The
// form will be displayed with the old data and point to update_book.php.
// It will also add a "Delete book" button.
{
  
  // if passed an existing book, proceed in "edit mode"
  $edit = is_array($book);

  // most of the form is in plain HTML with some
  // optional PHP bits throughout
?>
  <form method=post
        action="<?=$edit?"edit_book.php":"insert_book.php";?>">
  <table border=0>
  <tr>
    <td>ISBN:</td>
    <td><input type=text name=isbn 
         value="<?=$edit?$book["isbn"]:""; ?>"></td>
  </tr>
  <tr>
    <td>Book Title:</td>
    <td><input type=text name=title 
         value="<?=$edit?$book["title"]:""; ?>"></td>
  </tr>
  <tr>
    <td>Book Author:</td>
    <td><input type=text name=author 
         value="<?=$edit?$book["author"]:""; ?>"></td>
   </tr>
   <tr>
      <td>Category:</td>
      <td><select name=catid>
      <?
          // list of possible categories comes from database
          $cat_array=get_categories();
          foreach ($cat_array as $thiscat)
          {
               echo "<option value=\"";
               echo $thiscat["catid"];
               echo "\"";
               // if existing book, put in current catgory
               if ($edit && $thiscat["catid"] == $book["catid"])
                   echo " selected";
               echo ">"; 
               echo $thiscat["catname"];
               echo "\n"; 
          }
          ?>
          </select>
        </td>
   </tr>
   <tr>
    <td>Price:</td>
    <td><input type=text name=price 
               value="<?=$edit?$book["price"]:""; ?>"></td>
   </tr>
   <tr>
     <td>Description:</td>
     <td><textarea rows=3 cols=50 
          name=description>
          <?=$edit?$book["description"]:""; ?>
          </textarea></td>
    </tr>
    <tr>
      <td <? if (!$edit) echo "colspan=2"; ?> align=center>
         <? 
            if ($edit)
             // we need the old isbn to find book in database
             // if the isbn is being updated
             echo "<input type=hidden name=oldisbn 
                    value=\"".$book["isbn"]."\">";
         ?>
        <input type=submit
               value="<?=$edit?"Update":"Add"; ?> Book">
        </form></td>
        <? 
           if ($edit)
           {  
             echo "<td>";
             echo "<form method=post action=\"delete_book.php\">";
             echo "<input type=hidden name=isbn 
                    value=\"".$book["isbn"]."\">";
             echo "<input type=submit 
                    value=\"Delete book\">";
             echo "</form></td>";
            }
          ?>
         </td>
      </tr>
  </table>
  </form>
<?
}


function insert_category($catname)
// inserts a new category into the database
{
   $conn = db_connect();

   // check category does not already exist
   $query = "select *
             from categories
             where catname='$catname'";
   $result = mysqli_query($conn,$query);
   if (!$result || mysqli_num_rows($result)!=0)
     return false;  

   // insert new category 
   $query = "insert into categories values
            ('', '$catname')"; 
   $result = mysqli_query($conn,$query);
   if (!$result)
     return false;
   else
     return true;
}

function insert_book($isbn, $title, $author, $catid, $price, $description)
// insert a new book into the database 
{
   $conn = db_connect();

   // check category does not already exist
   $query = "select *
             from books 
             where isbn='$isbn'";
   $result = mysqli_query($conn,$query);
   if (!$result || mysqli_num_rows($result)!=0)
     return false;
   // insert new category
   $query = "insert into books values
            ('$isbn', '$author', '$title', '$catid', '$price', '$description')";
   $result = mysqli_query($conn,$query);
   if (!$result)
     return false;
   else
     return true;
}

function update_category($catid, $catname)
// change the name of category with catid in the database
{
   $conn = db_connect();

   $query = "update categories
             set catname='$catname'
             where catid='$catid'";
   $result = @mysqli_query($conn,$query);
   if (!$result)
     return false;
   else
     return true; 
}

function update_book($oldisbn, $isbn, $title, $author, $catid, 
                     $price, $description)
// change details of book stored under $oldisbn in
// the database to new details in arguments
{
   $conn = db_connect();

   $query = "update books
             set isbn='$isbn',
             title ='$title',
             author = '$author',
             catid = '$catid',
             price = '$price',
             description = '$description'
             where isbn='$oldisbn'";

   $result = @mysqli_query($conn,$query);
   if (!$result)
     return false;
   else
     return true; 
}

function delete_category($catid)
// Remove the category identified by catid from the db
// If there are books in the category, it will not
// be removed and the function will return false.
{
   $conn = db_connect();
   
   // check if there are any books in category 
   // to avoid deletion anomalies   
   $query = "select *
             from books
             where catid='$catid'";
   $result = @mysqli_query($conn,$query);
   if (!$result || @mysqli_num_rows($result)>0)
     return false;

   $query = "delete from categories 
             where catid='$catid'";
   $result = @mysqli_query($conn,$query);
   if (!$result)
     return false;
   else
     return true; 
}


function delete_book($isbn)
// Deletes the book identified by $isbn from the database.
{
   $conn = db_connect();

   $query = "delete from books
             where isbn='$isbn'";
   $result = @mysqli_query($conn,$query);
   if (!$result)
     return false;
   else
     return true;
}

?>
