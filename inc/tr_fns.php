<?php
// tr_fns.php   functions highly specific to Twinrocker Display
// 8/30/2010 Added class="mceEditor" 
// 3/5/2011  specials() to display/add2carts for items of a special code...
// 4/24/2012 remove violet  in tr_pigments()
// 6/21/2012 edited: display_tr_pulpcart()  pails to bags 
// 5/7/2013  Guache->Gouache
// 5/9/2013  Implement discounts in the four carts  --  inactive 2013..
// 7/10/2014  relocated fns from book_fns.php (at end))
// 7/22/2015 paperqtyoncart() statqtyoncart() 



// Discounts fns: 5/9/2013 -- in showcart.php and tr_checkout.php -- inactive 2013
// 10/28/2019 change order by DESC
//     in select_one_watercolor_paper_type()

function excluded_from_discounts($custid='') {
    
    // NOT Implemented yet...
    
    // customer with terr IN ('ss','wr','pr','r') are excluded 
    // from seeing/getting other customer discounts.
    // Don't show the form or calc as it is NOT calc'ed in action_fns.php get_order_totals()
	if (isset($_SESSION["SESSION_UACCT"]))
	{
		//display billto for the current customer..  based on webid
		$custarray = get_custarray($_SESSION["SESSION_UACCT"]); // get by webid
		$discount_type = $custarray['terr'];
     }   
    
     return false;  // NOT Implemented yet... 
}

function  paperqtyoncart($item)
{  // pa_add2_cart.php
    // add up the qty of item already on the papercart
    $itemqty = 0;
    $pcart = $_SESSION["papercart"];
	
    foreach ($pcart as $key => $row) {
	 	  if ($pcart[$key]['item']== $item){
  	 		   $itemqty = $itemqty + $pcart[$key]['qty'];
	   		
		 }
	 }
     unset($pcart);
     return $itemqty;
} // end fns

function  statqtyoncart($style)
{  // Called from st_add2_cart.php
    // Adds up the qty of the 4 colors of stationery items which may already be on the cart. This gets returned to allow/block adding more to the cart.
    $cqty['whap'] = 0;
    $cqty['whsc'] = 0;
    $cqty['crec'] = 0;
    $cqty['simo'] = 0;
    $scart = $_SESSION["cart"];
    reset($scart);
	
    foreach ($scart as $key => $value) {
        switch ($key) {
            case "YWHAP".$style :
                $cqty['whap'] = $cqty['whap'] +  $value;
                break;
            case "YWHSC".$style :
                $cqty['whsc'] = $cqty['whsc'] +  $value;
                break;
            case "YCREC".$style :
                $cqty['crec'] = $cqty['crec'] +  $value;
                break;
            case "YSIMO".$style :
                $cqty['simo'] = $cqty['simo'] +  $value;
                break;
          } // switch           
	 } //foreach
     
     unset($scart);
     return $cqty;  // returns array
} // end fns


function price($item)     {
    // get the price of an item in invt  (Used in calc discount for cart items.)
    $conn = db_connect();
    $query = "SELECT price FROM invt WHERE item = '". $item ."' LIMIT 1";
    $row = mysqli_fetch_array(mysqli_query( $conn, $query));
    return $row['price'];
}



function show_connect_links($str='',$brk=false) { 
        if ($brk)
            $brk='<br />';
        else    
            $brk = '';
        $links = '&nbsp;'.$str.$brk.'&nbsp;&nbsp;<a href="http://www.facebook.com/twinrocker" target="_blank"><img src="graphics/fb.gif" /></a>&nbsp;&nbsp;<a href="http://www.twitter.com/TwinrockerPaper" target="_blank"><img src="graphics/twit.gif" /></a> ';
    
   echo $links;
return true;
}


function show_discount_form($dcodes,$returnto='showcart.php',$show=true) {
    // If there are no active discounts don't show the form 
    $all_dc_info = get_discounts();
    if (count($all_dc_info)>0) {  
        $form = <<<EOT
            <form id="discountsForm" action="set_discounts.php" method="POST"> 
                <input type="hidden" name="returnto" value="$returnto" />
                Discount Codes: <input type="text" name="discountcode" value ="$dcodes" />
                <input type="submit" value="Apply" /> 
                <span title="Enter valid, unexpired discount codes (separated by 
                 commas) then press Apply"><img  src="graphics/info.png" height="20px" align="top"/>
                </span>
            </form>
EOT;
        echo $form.'<br />';
        if ($show)
            show_connect_links('Watch for codes:');    // Show fb and twit icons

    }

    return true;
}


function  get_discounts($list='')  {
    // Get list of all active discount codes from table, or just those in a list; return array.
    // Do the query... order by applies_to to group in the four differnt carts: item,fiber,pulp,paper
    $conn = db_connect();
    if (empty($list))
        $query = "select * from discounts where active AND NOW() < expires ORDER BY applies_to";
    else {
        //get fmt right  for mysql IN (array)
        $list = string_to_mysqlx_inarray($list);   // action_fns
        $query = "select * from discounts where active AND NOW() < expires AND code IN ($list)  ORDER BY applies_to";
    }
    //echo $query;br(1);
    $result = mysqli_query( $conn, $query) or die ("Error in get_discounts query. ");
    $discnt = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $discnt[] = array('code' => $row['code'],
                   'applies_to' => $row['applies_to'],
                   'item' => $row['item'],
                   'minqty' => $row['minqty'],
                   'amount' => $row['amount'], 
                   'type' => $row['type'], 
                   'expires' => $row['expires']);
    } 
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
    return $discnt;
}

function validate_discounts($codes) {
    // Check codes user enters compared w/ active ones in table and
    // then return valid, active codes.
    $ret = '';
    $thesecodes = explode(',',$codes);
    //Do the query...
    $conn = db_connect();
    $query = "select `code` from discounts where active AND NOW() < expires";
    $result = mysqli_query( $conn, $query) or die ("Error in discounts query. ");
    $validdiscounts = array();
    while ($row = mysqli_fetch_row($result)) 
        $validdiscounts[]=$row[0];
    
    ((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);

    //print_r($validdiscounts);
    foreach ($thesecodes as $value) { 
        //echo $value.' - <br />';
        if (in_array($value,$validdiscounts))
            $ret = $ret.$value.',';
    }
    //remove trailing comma
    if (right($ret,1) == ',')
        $ret = substr($ret,0,strlen($ret)-1);
    return $ret;
}

function check_discount($thiscart,$disc,&$disc_msg,&$dc) {
    // Check and calculate discount amt for discounted items in paper, fiber, pulp carts.
    // Create $disc_msg string and $dc array

    $amt = 0.00;
    $disc_itemno = trim($disc['item']);
    $len = strlen($disc_itemno);
    $disc_minqty =  $disc['minqty'];

    foreach ($thiscart AS  $pcart) {

       $cart_item = $pcart['item'];
       $cart_qty = $pcart['qty'];
       if ($disc_itemno==left($cart_item,$len)) {
            // Matches
            if ($cart_qty>=$disc['minqty']) {
                //$disc_msg .= " Qty exceeds Min Qty <br /> "; 
                // calc_price()
                // calc_discount()
                if ($disc['type'] == 'percent') {
                    $total = $pcart['totalamount'];
                    $val =  $total * ($disc['amount']/100.0); 
                    // percent discount of total price 
                    $str =  $disc['amount'].'%';          
                }
                if ($disc['type'] == 'fixed')  {
                    $val =  $disc['amount'] * $cart_qty;     
                    // fixed amt ea item
                    $str =  $disc['amount'].'ea';
                } 

                $amt += $val;
                $disc_msg .= " Match! ". $disc['code']." $str : $disc_itemno : 
                             $cart_item $cart_qty $".number_format($val,2)." <br />";
                $dc[] = array('code'=>$disc['code'],
                              'type'=>$str,
                              'item'=>$disc_itemno,
                              'expires'=>$disc['expires'],
                              'cart_item'=>$cart_item,
                              'cart_qty'=>$cart_qty,
                              'amt'=>$val);


            }
            else  {
                //$disc_msg .= " Min Qty < Cart Qty <br /> ";
            }
   
               
       }
       else  {
        //$disc_msg .= "NoMatch $disc_itemno : $cart_item  <br />";
       }
    
    }
    return $amt;
}

function calc_discounts($dcodes){  //Open $cart,$fibercart,$pulpcart,$papercart from session if needed
    if (trim($dcodes)=='') {   
        // if no person has no codes then discount = 0.00 and done
        $_SESSION["total_discount"] = 0.00;
        $_SESSION["dc"] =  array();
        return true;
    }

    // Get discount info for codes person has entered, into an array 
    $discounts = get_discounts($dcodes);
    
    if ($discounts) {
        // Calculate discounts for the discount codes entered if critera are met
        
        $cart = $_SESSION['cart'];
        $fibercart = $_SESSION['fibercart'];
        $pulpcart = $_SESSION['pulpcart'];
        $papercart = $_SESSION['papercart'];
        
        $item_disc = 0.00;
        $paper_disc = 0.00;
        $pulp_disc = 0.00;
        $fiber_disc = 0.00;
        $total_disc = 0.00;
        $disc_msg = '';
        $dc = array();   //To store data for later entry into orders_discounts table
                
        //Check each valid discount:
        foreach ($discounts as $disc)   {
            
            $thisdiscount = strtolower(trim($disc['applies_to']));
            
            /*
                Alternate logic might also  work if ONLY using itemno 
                to spec discounts in discounts table:
                left($disc_itemno,1)  : Z - paper; P - pulp; F - fiber; else cart 
                $char = left($disc_itemno,1);
                
            */
            
            // pulp, fiber, and paper carts have ~common array structure 
            // wrt discount checking/calculation.  Items and Stationery 
            // go on the "cart" which has a simple, itemno=>qty array structure.
            
            switch ($thisdiscount) {
                case 'item':
                    // Simple cart array structure 
                    if  (isset($cart) && count($cart)) {
                    
                        $disc_itemno = trim($disc['item']);
                        $len = strlen($disc_itemno);
                        $disc_minqty =  $disc['minqty'];
    
                        foreach ($cart AS  $cart_item => $cart_qty) {
                           if ($disc_itemno==left($cart_item,$len)) {
                                // itemno  or partial matches
                                if ($cart_qty>=$disc_minqty) {
                                     //$disc_msg .= " Qty exceeds Min Qty <br /> "; 
                                    // calc_discount()
                                    if ($disc['type'] == 'percent') {
                                        // calc_item_price()
                                        $total = $cart_qty*price($cart_item);
                                        // percent discount of total price                      
                                        $val =  $total * ($disc['amount']/100.0);
                                        $str =  $disc['amount'].'%';
                                    }
                                    if ($disc['type'] == 'fixed') {
                                        $val =  ($disc['amount'] * $cart_qty);     
                                        // fixed amt ea item
                                        $str =  $disc['amount'].'ea';
                                    }
                                     
                                    $amt += $val;
                                    $disc_msg .= " Match! ". $disc['code']." $str : 
                                    $disc_itemno : $cart_item $cart_qty $".
                                    number_format($val,2)." <br />";
                                    $dc[] = array('code'=>$disc['code'],
                                                  'type'=>$str,
                                                  'item'=>$disc_itemno,
                                                  'expires'=>$disc['expires'],
                                                  'cart_item'=>$cart_item,
                                                  'cart_qty'=>$cart_qty,
                                                  'amt'=>$val);

                                }
                                else  {
                                    //$disc_msg .= " Min Qty < Cart Qty <br /> ";
                                }   
                           }
                           else  {
                            // $disc_msg .= "NoMatch $disc_itemno : $cart_item  <br />";
                           }
                            
                        }
                        
                    }
                    $item_disc = $amt;
                    break;
                case 'paper':
                    if  (isset($papercart) && count($papercart)) {
                        $paper_disc += check_discount($papercart,$disc,$disc_msg,$dc);
                    }
                    break;
                case 'fiber':
                    if  (isset($fibercart) && count($fibercart)) {
                        $fiber_disc += check_discount($fibercart,$disc,$disc_msg,$dc);
                    }
                    break;
                case 'pulp':
                    if  (isset($pulpcart) && count($pulpcart)) {
                        $pulp_disc += check_discount($pulpcart,$disc,$disc_msg,$dc);
                    }
                    break;
            }  // end switch
    
            //$dc[] = array($disc['applies_to'],$disc['code'],$disc['item'],$disc['amount'],$disc['type'],$disc['expires']);
        } // for all the discounts
        //echo $disc_msg;
        //br(3);
        if (empty($disc_msg)) {
            $disc_msg = 'None';
        
        }
        $disc_msg = str_replace('<br />','&#10;',$disc_msg);  // Put text in img title
        echo 'Discounts  Applied:<span title="'.$disc_msg.'"><img  src="graphics/info.png"
              height="20px" align="top"/></span> ';
        if ($item_disc>0)
            echo  'items = '.number_format($item_disc,2).'<br />';
        if ($paper_disc>0)
             echo  "paper = ".number_format($paper_disc,2)."<br />";
        if ($fiber_disc>0)
            echo "fiber = ".number_format($fiber_disc,2)."<br />";
        if ($pulp_disc>0)
            echo " pulp = ".number_format($pulp_disc,2)."<br />";
        
        //print_r($dc);
       
        $total_disc = number_format($item_disc+$paper_disc+$fiber_disc+$pulp_disc,2);
    } // if there are valid discounts entered by user
    else   {
        //echo 'None found'; // No active, unexpired discounts
    }
    //Store the values or return as array (w/ msg)
    $_SESSION["total_discount"] =  $total_disc;
    $_SESSION["dc"] = $dc;    // Used in payments.php to save records
    
    return true;    

}
//End discounts fns



 function specials()
 {
   // 3/5/2011 -- display list for specials on paper
   // These begin with '9' in the itemcode if invt
   
   $query = "select * from invt where left(item,1)='9' AND (onhand-aloc) > 0";
   //Do the query...
   $conn = db_connect();
   $result = @mysqli_query( $conn, $query) or die ("Error in Specials query. " . ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
   $num_rows = mysqli_num_rows($result);
   $specials =db_result_to_array($result);
   foreach ($specials as $row)
   {
     $buy = "add2_cart.php?new=".($row["item"])."&catid=SPC"; ;  
     echo $row['item']."\t".$row['descrip']."\t $". number_format($row['onhand']-$row['aloc'])."\t";
     $price = number_format($row["price"], 2);
	  do_html_url($buy, $price);
   }

   return true;
 }
 

 
function build_onhand_update_list($itemarray)
{
  	
    foreach ($itemarray as $key => $value)
  	{
  		echo '<br>'.$key.' '.$value['item'].' '.$value['qty'];

  	}
    return;
}

function select_one_watercolor_paper_type()  // 1/25/2007 select_one_watercolor_paper_type()
{
  // 
  //This builds the <select>  element for only water color
  $conn = db_connect();
// !! The following works in MySQL 4.1  but NOT in 4.023  1/28/2007 
//  $query = "select papername,papercode from papergroups 
//             where papercode IN (SELECT DISTINCT substring(item,2,4)as code  
//             FROM `invt` WHERE left(item,1)='Z' and onhand-aloc >0 
//             and substring(item,14,1) >'G')";
            // 14th character in item of H-N denotes Gel papers  1/26/2007
            // (`invt`.onhand-`invt`.aloc > 0) 
  //Old: $query = 'SELECT papername,papercode FROM `papergroups` 
  //          where active="y" and wat="y" order by 1 ';
  
  // Re-wrote as a join 1/28/2007  -- works faster than subselect!
  // Order by DESC 10/28/2019 
  // added a.displayorder to select php7.4 2/23/2020
  $query = "select DISTINCT a.papername,a.papercode,a.displayorder from papergroups a, invt b
            where a.papercode = substring(b.item,2,4)
            AND left(b.item,1)='Z' 
            AND b.onhand-b.aloc >0 
            AND substring(b.item,14,1) >'G' 
            ORDER BY concat(a.displayorder,a.papername)";  // 1 DESC
  
  $result = mysqli_query($conn, $query);
  $num_types = mysqli_num_rows($result);
  $thisgroup = db_result_to_array($result);
  @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
  //$thisgroup[0][0]='Eighteenth';
  // 3/25/2019 size=6
  $thisselector="<select name='watercolor' size='6'>"; 
  for ($n=0;$n<$num_types ; $n++)
  { 
             $thisselector=$thisselector . "<option value='".$thisgroup[$n][1]."'"; 
             //if (!is_null($vState)) 
            if ($thisgroup[$n][1]=='WHWC') 
              $thisselector=$thisselector . " SELECTED "; 
                 
            $thisselector=$thisselector . ">".stripslashes($thisgroup[$n][0])."</option>"; 
            
  } //end for 
  $thisselector = $thisselector . "</Select>"; 
  
  return $thisselector;   //.'xx '.$thisgroup[0][0];;
}

function xx_select_one_watercolor_paper_type()
{
  // Select a favorite Twinrocker  watercolor paper 2/6/05
  $thisselector="<select name='watercolor' size='1'>
                    <option value='WHWC' SELECTED>White Watercolor</option>
                    <option value='CREC' >Cream</option>
                    <option value='PATR' >Patriot</option>
                    <option value='DUSP' >Pale Dusty Rose</option>
                    <option value='MAYL' >May Linen</option>
                    <option value='WILL' >Willow Creek</option>
                    <option value='SAND' >Sand</option>
                    <option value='BISC' >Biscuit</option>
                    <option value='SIMO' >Simon's Green</option>
                    <option value='TURN' >Turner Green</option>
                 </select>"; 
                 // <option value='AUGU' >August Blue</option>
                 // <option value='PINK' >Pink Peppermint</option>
                 // <option value='CHAP' >Chapin</option>
                 // <option value='TAUP' >Taupe</option>
  return $thisselector;
}

function select_one_paper_type()
{
  // Select a favorite Twinrocker Paper by Name  10/08/04
  //This builds the <select>  element
  //Order ny displayorder+papername 10/28/2019
  $conn = db_connect();
  $query = 'SELECT papername,papercode FROM `papergroups` 
            where active="y" order by concat(displayorder,papername)';
  $result = mysqli_query($conn, $query);
  $num_types = mysqli_num_rows($result);
  $thisgroup = db_result_to_array($result);
  @((mysqli_free_result($result) || (is_object($result) && (get_class($result) == "mysqli_result"))) ? true : false);
  //$thisgroup[0][0]='Eighteenth';
  // 3/26/2019 size=6
  $thisselector="<select name='papertype' size='6'>"; 
  for ($n=0;$n<$num_types ; $n++)
  { 
             $thisselector=$thisselector . "<option value='".$thisgroup[$n][1]."'"; 
            //if (!is_null($vState)) 
            //     if (trim($vState)==trim($key)) $thisselector=$thisselector . " Selected "; 
                 
            $thisselector=$thisselector . ">".stripslashes($thisgroup[$n][0])."</option>"; 
            
  } //end for 
  $thisselector = $thisselector . "</Select>"; 
  
  return $thisselector;   //.'xx '.$thisgroup[0][0];;
}


function paper_radios($paperuses='--',
                       $papercolor='--',
                       $papervalue='--',
                       $paperweight='--')
{
  // 10/07/04 Add form to let user pick one papertype by name then display results...

  // Revised display 10/17/04 ABC  $paperuses,$papercolor,$papervalue,$paperweight
  // Revised 2/6/05 added watercolor as section A
  $mret= '<table class="pa_radio_text"  border="1" cellspacing="0" >
           <tr>
           <td colspan="4" > 
               <a name="TWP"></a>
               <h3>Twinrocker Watercolor Paper</h3> 
               <p class=pa_text>
                 <b>To view or order, scroll/select a watercolor paper from the list below and then press "Go"</b> 
                 <form method="POST" action="pa_main.php"><br />&nbsp;&nbsp;&nbsp;'.
                  select_one_watercolor_paper_type().'&nbsp;
                  <input type="SUBMIT" name="one_watercolor" value="Go"> 
                 </form><br />
                </p></tr>
            <tr>
              <td colspan="4" > 
              <h3> Twinrocker All-Purpose Papers</h3>
                <p class=pa_text>
                <b>To see the current paper inventory or to order,
                scroll/select a paper from the list below and then click on "Go". </b>
                  <form method="POST" action="pa_main.php"><br />&nbsp;&nbsp;&nbsp;'.
                    select_one_paper_type().'&nbsp;
                    <input type="SUBMIT" name="find_one" value="Go">
                    <br /></form>
              <br />
              <br />
              <br />
              ...Or, if you have <u>specific usage, 
              color and weight requirements</u>,<br />
               select them below then click the "Select papers" button below.
               
              </p>
              </td></tr><tr>
              <th >Usage</th>
              <th>Color</th>
              <th>Value/grayscale</th>
              <th>Weight</th></tr>
              <tr>
              <form action ="pa_main.php" method ="post">
              <td>'.radio_paperuses($paperuses).'</td>
              <td>'.radio_papercolor($papercolor).'</td>
              <td>'.radio_papervalue($papervalue).'</td>
              <td>'.radio_paperweight($paperweight).'</td><tr>
              <th colspan="4" >
                <input type="submit" name="submit" value="Select papers" STYLE="font:8pt ARIAL;">
              </th></tr></form></table>';
              
/*
              <tr>
               <td colspan="4"><br />
                 <b>C.  Or, click <a href="http://twinrocker.com/pstocked.htm" target=_blank>here</a> 
                     to view paper inventory in the<br />
                      (traditional, non-ecommerce) format and to phone in your order.</b>
                     <br /><br /> 
               </td>
              </tr>
*/
       
return $mret;
}

function paper_selects($paperuses='--',
                       $papercolor='--',
                       $papervalue='--',
                       $paperweight='--')
{
  $mret= '<form action = pa_main.php method ="post">';


  //$paperuses,$papercolor,$papervalue,$paperweight
  $mret.= "<table class=pa_radio_text  border=1 cellspacing=0><tr>
              <th colspan=4 >Select...</th></tr><tr>
              <th>Usage</th>
              <th>Color</th>
              <th>Value/grayscale</th>
              <th>Weight</th></tr>
              <tr>
              <td>".select_paperuses($paperuses)."</td>
              <td>".select_papercolor($papercolor)."</td>
              <td>".select_papervalue($papervalue)."</td>
              <td>".select_paperweight($paperweight)."</td><tr>
              <th colspan=4 ><input type=submit name=\"submit\" value=\"Select papers\">
              </th></tr><tr>
              </tr>
              </table>

        </form>";

return $mret;
}


function radio_paperuses($mselected='--')
{
   // 5/7/2013 Retain 'water media/Guache' in data lookup since 
   // it is in mysql that way. Change display to Gouache.  
   // Added 'watercolor' as a use/button
   
   $a_paperuses = array(array('-Any use-','--'),
                  array('Book Repair','book repair'),
                  array('Book Arts','books arts'),
                  array('Cards and Invitations','invitations and cards'),
                  array('Letterpress','letterpress'),
                  array('Printmaking','printmaking'),
                  array('Gouache','water media/Guache'),
                  array('Watercolor','watercolor'),
                  array('Pastel','pastel'),
                  array('Charcoal','charcoal'),
                  array('Drawing','drawing'),
                  array('Pen and Ink','pen and ink'),
                  array('Calligraphy','calligraphy') );

  $numitems = count($a_paperuses);
  $mret = '';
 	for ($row = 0; $row < $numitems; $row++)
	{
      $mret .= "<input name='paperuses' type='radio' value='".$a_paperuses[$row][1]."' ";
		  if ($a_paperuses[$row][1]==$mselected)
		  {
			 $mret .= ' CHECKED';
		  }
		  $mret .= '> '.$a_paperuses[$row][0].'<br>';

	}
return $mret;
}

function radio_suface_prep($itm = '')  
{
      // 10.15.05 created to replace <Select..option> on pa_main.php
      //          The <Select..> didn't display right when the list(html filesize) was large
      // 9/20/2013  -ref- Remove hot and both for Mica Rose
      $mret = "<input  name='surface' type='radio' value='/CP' CHECKED> Cold <br />";
      if (strtoupper(left($itm,5))!='ZMICA')
      {
        $mret .= "<input  name='surface' type='radio' value='/HP' > Hot <br />";
        $mret .= "<input  name='surface' type='radio' value='/B' > Both";
      }
return $mret;
}

function radio_papercolor($mselected='--')
{

  $a_papercolor = array(array('-Any color-','--'),
                  array('Whites','white'),
                  array('Off-whites','off-white'),
                  array('Tan-browns','tan-brown'),
                  array('Yellows','yellow'),
                  array('Greens','green'),
                  array('Blues','blue'),
                  array('Purple-reds','purple-red'),
                  array('Greys','grey')
                  );

  $numitems = count($a_papercolor);
  $mret = '';
 	for ($row = 0; $row < $numitems; $row++)
	{
      $mret .= "<input name='papercolor' type='radio' value='".$a_papercolor[$row][1]."' ";
		  if ($a_papercolor[$row][1]==$mselected)
		  {
			 $mret .= ' CHECKED';
		  }
		  $mret .= '> '.$a_papercolor[$row][0].'<br>';

	}
return $mret;
}

function radio_papervalue($mselected='--')
{
   $a_papervalue= array(array('-Any Grayscale-','--'),
                  array('White - 0','white'),
                  array('Off-White - 1/2','off-white'),
                  array('Pale - 1-2','pale'),
                  array('Middle-value - 3-5','middle value'),
                  array('Dark-very dark - 6-9','dark-very dark')
                  );

  $numitems = count($a_papervalue);
  $mret = '';
 	for ($row = 0; $row < $numitems; $row++)
	{
      $mret .= "<input name='papervalue' type='radio' value='".$a_papervalue[$row][1]."' ";
		  if ($a_papervalue[$row][1]==$mselected)
		  {
			 $mret .= ' CHECKED';
		  }
		  $mret .= '> '.$a_papervalue[$row][0].'<br>';

	}
return $mret;
}


function radio_paperweight($mselected='--')
{

  $a_paperweight=array(array('-Any weight-','--'),
                    array(' 80g/sq m Thin Text ','TT'),
                    array('115g/sq m Text','T'),
                    array('200g/sq m Heavy Text','HT'),
                    array('255g/sq m Light Art','LA'),
                    array('410g/sq m Art','A'),
                    array('460g/sq m Heavy Art','HA'),
                    array('650g/sq m Board','B')
                  );

  $numitems = count($a_paperweight);
  $mret = '';
 	for ($row = 0; $row < $numitems; $row++)
	{
      $mret .= "<input name='paperweight' type='radio' value='".$a_paperweight[$row][1]."' ";
		  if ($a_paperweight[$row][1]==$mselected)
		  {
			 $mret .= ' CHECKED';
		  }
		  $mret .= '> '.$a_paperweight[$row][0].'<br>';

	}
return $mret;
}

function select_paperuses($mselected='--')
{
   $a_paperuses = array(array('-Any use-','--'),
                  array('Book Repair','book repair'),
                  array('Book Arts','books arts'),
                  array('Cards and Invitations','invitations and cards'),
                  array('Letterpress','letterpress'),
                  array('Printmaking','printmaking'),
                  array('Gouache','water media/Guache'),
                  array('Watercolor','watercolor'),
                  array('Pastel','pastel'),
                  array('Charcoal','charcoal'),
                  array('Drawing','drawing'),
                  array('Pen and Ink','pen and ink'),
                  array('Calligraphy','calligraphy') );

  $numitems = count($a_paperuses);
  $mret = "<Select name='paperuses' >" ;
	for ($row = 0; $row < $numitems; $row++)
	{
		  $mret .= "<option value='".$a_paperuses[$row][1]."'";
		  if ($a_paperuses[$row][1]==$mselected)
		  {
			 $mret .= ' SELECTED';
		  }
		  $mret .= '>'.$a_paperuses[$row][0];
	}
	$mret .= '</SELECT>';

return $mret;
}

function select_papercolor($mselected='--')
{
  $a_papercolor = array(array('-Any color-','--'),
                  array('White','white'),
                  array('Off-white','off-white'),
                  array('Tan-brown','tan-brown'),
                  array('Yellow','yellow'),
                  array('Green','green'),
                  array('Blue','blue'),
                  array('Purple-red','purple-red'),
                  array('Grey','grey')
                  );

  $numitems = count($a_papercolor);
  $mret = "<Select name='papercolor' >" ;
	for ($row = 0; $row < $numitems; $row++)
	{
		  $mret .= "<option value='".$a_papercolor[$row][1]."'";
		  if ($a_papercolor[$row][1]==$mselected)
		  {
			 $mret .= ' SELECTED';
		  }
		  $mret .= '>'.$a_papercolor[$row][0];
	}
	$mret .= '</SELECT>';
return $mret;
}

function select_papervalue($mselected='--')
{
   $a_papervalue= array(array('-Any Grayscale-','--'),
                  array('White - 0','white'),
                  array('Off-White - 1/2','off-white'),
                  array('Pale - 1-2','pale'),
                  array('Middle-value - 3-5','middle value'),
                  array('Dark-very dark - 6-9','dark-very dark')
                  );
  $numitems = count($a_papervalue);
  $mret = "<Select name='papervalue' >" ;
	for ($row = 0; $row < $numitems; $row++)
	{
		  $mret .= "<option value='".$a_papervalue[$row][1]."'";
		  if ($a_papervalue[$row][1]==$mselected)
		  {
			 $mret .= ' SELECTED';
		  }
		  $mret .= '>'.$a_papervalue[$row][0];
	}
	$mret .= '</SELECT>';

return $mret;
}



function select_paperweight($mselected='--')
{

  /* $a_paperweight=array(array('Any weight',''),
                    array(' 80g/sq m 0.004-0.005 in. Thin Text ','TT'),
                    array('115g/sq m 0.006-0.007 in. Text','T'),
                    array('200g/sq m 0.008-0.011 in. Heavy Text','HT'),
                    array('255g/sq m 0.012-0.017 in. Light Art','LA'),
                    array('410g/sq m 0.018-0.025 in. Art','A'),
                    array('460g/sq m 0.025-0.033 in. Heavy Art','HA'),
                    array('650g/sq m 0.034+ in. Board','B')
                  );
  */
  $a_paperweight=array(array('-Any weight-','--'),
                    array(' 80g/sq m Thin Text ','TT'),
                    array('115g/sq m Text','T'),
                    array('200g/sq m Heavy Text','HT'),
                    array('255g/sq m Light Art','LA'),
                    array('410g/sq m Art','A'),
                    array('460g/sq m Heavy Art','HA'),
                    array('650g/sq m Board','B')
                  );

  $numitems = count($a_paperweight);
  $mret ="<Select name='paperweight' >" ;
	for ($row = 0; $row < $numitems; $row++)
	{
		  $mret .= "<option value='".$a_paperweight[$row][1]."'";
		  if ($a_paperweight[$row][1]==$mselected)
		  {
			 $mret .= ' SELECTED';
		  }
		  $mret .= '>'.$a_paperweight[$row][0];
	}
	$mret .= '</SELECT>';

return $mret;
}



function outofstock($msg='')
{
    return  '<font color=red>Out of Stock '.trim($msg).'</font>';
}

function limitedsupply($msg='')
{
    return  '<font color=red>Limited Supply '.trim($msg).'</font>';
}

function redmessage($msg='')
{
    return  '<font color=red>'.trim($msg).'</font>';
}

function get_items_beginning($str)
{
   $thislist=array();
 	$cart= $_SESSION['cart']; //$items;

   if ($cart)
    foreach ($cart as $key=>$value)
    {
      if (preg_match("/^[$str]/i",$key))  // 6/5/10: if any char in &str are the firstchar in $key
        $thislist[$key]=$value;
    }

  if (count($thislist)>0)
    return $thislist;
  else
    return false;
}

function build_lineitem_array($list)
{
    //Passed  item=>qty pairs.  Look up and create array item description qty price and totalamount for each lineitem

    foreach ($list as $item => $qty)
    {
      $invt = get_invt_details($item);
      if (left($item,1)=='Y')  // don't include weight of Stationery 1/12/03 (Stationery uses wt field for "code"
        $wt = 0.00;
      else
    	  $wt = $invt["weight"];
      //create a more complete array...
      $mlist[] = array('item'=>$invt['item'],'description'=>$invt['descrip'],'qty'=>$qty,'weight'=>$wt,'price'=>$invt['price'],'amount'=>$qty*$invt['price']);
    }

  return $mlist;
}
  


function pass_msg($string,$ssl=0)
{
	$msg =urlencode($string);
	if ($ssl)         // If a 1 is passed after the msg string then stay in ssl mode
	   header("location:message.php?cntrl=1&message=$msg");
  else
     header("location:message.php?message=$msg");
	exit;
}

function silk_find()
{
	if (isset($_GET['catid']))
		$catid = $_GET['catid'];
	else
		$catid = '';
		
	$this_string = $_SESSION['searchstr'];
	?>
	<table border="1"><tr><td nowrap class="headadmin">
	<form action="silkfind.php" method="get">
 	<input type="hidden" name="catid" value="<?php echo $catid;?>">
	<input type="hidden" name="calledfrom" value="<?php echo $_SERVER["REQUEST_URI"];?>">

  Enter Search Word:<br>
	<input class="headadmin" type="text" size="16" name="searchstr" value="<?php echo $this_string;?>">
  in<select class="headadmin" name="category">
		<option value="products" selected>All Products
		<option  value="invt" >Item Names Only
	</select>
	<input style="font:7pt Arial"  type="submit" value="Search" >


	</form></td></tr></table>

	<?php
	/*
<!--
		<option value='books'>Books only
		<option value='paper'>Paper
		<option value='mfg1'>Mfg1
-->
  */
	//	<input style="font-size=1" type=checkbox name=searchsite 
	// 	if (!isset($catid) || $catid=='home') echo 'CHECKED'; 

	//$filename = $_SERVER["REQUEST_URI"].'---'.$_SERVER["PHP_SELF"].'..'.$catid;
	//echo $filename;
	//return true;
}

function display_header($g_headername='Twinrocker', $g_width='100%',$g_headercolor)
{
  global $g_payment_phone, $g_alt_phone, $g_phnhours;
  // 12/22/03 ref&a change header name based on first 3 char of program
  // st_ = stationery, else supplies
 $url = parse_url($_SERVER["REQUEST_URI"]);
 $script = strrchr($url['path'],'/');
 $script4 = strtolower(substr($script,0,4));
 //echo $script.'  -'.$script4;

 if ( $script4 == '/st_')
 {
    $g_headername = 'Twinrocker Handmade Invitations and Stationery';
    $goto = 'st_main.php';
 }

 else
 {
   switch ($script)
   {
      case '/showprod.php':   //fall thru
      case '/tr_fiber.php':
      case '/fiberdes.html':
          $g_headername = 'Twinrocker Papermaking Supplies';
          $goto = 'showprod.php';
          break;
      case '/st_main.php':
          $g_headername = 'Twinrocker Handmade Invitations and Stationery';
          $goto = 'st_main.php';
          break;
      case '/pa_main.php':     //fall thru
      case '/paper.php':
          $g_headername = 'Twinrocker Handmade Paper';
          $goto = 'pa_main.php';
          break;
       case '/showcart.php' :   //fall thru
       case '/tr_checkout.php':
       case '/payments.php':
          $g_headername = 'Twinrocker';
          //$goto = $_SESSION['SECTION_MAINFILE'];//$_SERVER["REQUEST_URI"];
          if (isset($_SESSION['SECTION_MAINFILE']))
             $goto = $_SESSION['SECTION_MAINFILE'];
          else
             $goto = 'showprod.php';

          break;
       default :
         $g_headername = 'Twinrocker';
         if (isset($_SESSION['SECTION_MAINFILE']))
             $goto = $_SESSION['SECTION_MAINFILE'];
         else
             $goto = 'showprod.php';
   }
 }

   echo "<table  border=\"1\" align=\"center\" width=\"$g_width\" bgcolor=\"$g_headercolor\"><tr>
   		 	<td width='100' align=\"center\" valign=\"top\" bgcolor=\"#e0e0e0\" >".home2_on().
		    "<br />
         <div class=\"hdrsection\">
         <a class='hdrsection' href=\"sections.php?next=paper&amp;anchor=wcpaper\">Watercolors</a>
         <br><a class='hdrsection' href=\"sections.php?next=paper\">Paper</a>
         <br><a class='hdrsection' href=\"sections.php?next=supply\">Supplies</a>
         <br><a class='hdrsection' href=\"sections.php?next=stationery\">Invitations</a>
         </div></td><td>
			<table border=\"0\"><tr><td colspan=\"4\">
   			<a class=\"header\" href=\"$goto\">$g_headername</a></td></tr><tr>
			
			
			
			<td class=\"headadmin\">$g_payment_phone / $g_alt_phone: Talk with real people $g_phnhours </td>";
			echo "<td class=\"headadmin\">
				  <a  href=\"mailto:info@twinrockerhandmadepaper.com\">Contact Us</a>
	  			  </td>";
			//<font size=-6 color=#ff8c28>
			// <A class=cartmenu
	
/*	$mark = '';
    if ($_SESSION['fiber_items']>0)
		$mark = 'F';
    if ($_SESSION["items"]>0)   //($_SESSION['items']>0)
	  	$mark = $mark.'I';
    if ($_SESSION['pulp_items']>0)
	 	$mark = $mark.'P';	
*/	 
	// <font color=red>'.$mark."</font>&nbsp;		 	
  $items = ($_SESSION["items"]+$_SESSION["fiber_items"]+$_SESSION["pulp_items"]+$_SESSION["paper_items"]);
  
  $amount = $_SESSION["total_price"]+$_SESSION["fiber_price"]+$_SESSION["pulp_price"]+$_SESSION["paper_price"];
	//Show cart items and amount
	if ($amount > 0.00 || $items > 0)
	{
       if ($items == 1)
          $itemword = "Item";
       else
          $itemword = "Items";
          
		   echo "<td align=\"center\"><p class=\"text9px\">".$items
			  ." $itemword Amt= $".number_format($amount,2);
	     echo "</p></td></tr> ";
	}
	else
	{
		echo '<td>&nbsp;</td></tr>';
  }

    // &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
	// Show admin and/or current login username
	echo "<tr><td>";
	echo silk_find();      // Display  search box.
	echo "</td><td>" ; 
  if (isset($_SESSION["SESSION_ADNAME"]))
	{
    // If admin logged in then create link to go to admin functions page.
		echo '<p class="headadmin">Admin:</p></td><td>';
		//echo do_html_URL_no_br('silkadmin.php','Admin:').'</p></td><td>';  //'&nbsp;'.$_SESSION["SESSION_ADNAME"].
	}
	else {
        //show_connect_links('Connect:',true);  // 5/13/2013
		echo '&nbsp;</td>';
    }               
    echo '<td align="right" colspan="2">';  // else leave blank

  //Display the logged in user's name
  if (isset($_SESSION["SESSION_UNAME"]))
	{
		echo '<p class="headadmin"><a href="cust_scrn_form.php?cust+action=display">'.$_SESSION["SESSION_UNAME"].'</a></p></td>';
	}         
	else
		echo '&nbsp;</td>';
		
    echo "</tr></table>";

	// Header RIGHT MENU Features: help,login,myprofile|profile,person,clear cart,view cart
	// Change background of menu area on the right side of the header...

  // figure out if something is ordered
  	 $ordered = '';
     if ($_SESSION['fiber_items']>0)
	 	$ordered = 'F';
     if ($_SESSION["items"]>0)   //($_SESSION['items']>0)
	  	$ordered = $ordered.'I';
     if ($_SESSION['pulp_items']>0)
	 	$ordered = $ordered.'P';
     if ($_SESSION['paper_items']>0)
	 	$ordered = $ordered.'Z';




  // In next line look for both since person can order a catalog or MSDS that cost 0.00.
 	if ( ($amount > 0.00 || $items > 0)  || isset($_SESSION["SESSION"]))
 	{
    //White display... logged in or has item
		echo '</td><td  nowrap valign="top" align="left" bgcolor="white" width="100">';
		echo '<a class="cartmenu" HREF="help.php">Help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>';
    if (strpos($_SERVER["REQUEST_URI"],'weblogin.php')===FALSE)
		{
		  // If user is logged in then..
	    if (isset($_SESSION["SESSION"]))
	    {
          // Display Logout link...
		      echo '<a class="cartmenu" href="weblogoff.php">Logout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> <br>';
      }
      else
          echo '<a class="cartmenu"  HREF="weblogin.php">Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>';

    }
	  if (isset($_SESSION["SESSION_UNAME"]))
		{
       // Display MyProfile link
		   //echo "<a class=cartmenu href=cust_scrn_form.php?cust_action=display>MyProfile&nbsp;</a><br>";
		   echo '<a class="cartmenu" href="showprod.php?dept=MyMenu&grp=MyMenu">MyMenu&nbsp;&nbsp;</a><br>';
		}

    if ( isset($_SESSION["SESSION"]) && $_SESSION["SESSION"]=='Admin')
		{
		  // Allow admin users to seek the next person
			echo '<a class="cartmenu" href="customer.php?menu=new">Person&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a> <br>';
		}

	}
  else
  {
    // Grey menu bgcolor
		echo '	</td><td  nowrap valign="top" align="left"  width="100">';
		echo '<a class="cartmenugrey"  HREF="help.php">Help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>';
    if (strpos($_SERVER["REQUEST_URI"],'weblogin.php')===FALSE)
		{
		  echo '<a class="cartmenugrey"  HREF="weblogin.php">Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>';
    }
  }
		
	// If item is ordered, 'clear cart'
	if ($_SESSION["total_price"] >0.00 || $ordered )
	{			
		echo '<a class="cartmenu" HREF="clear_cart.php?catid=home" >Clear Cart</a><br>';
	}
	//	else

  if ($_SESSION["total_price"] >0.00 || $ordered )
	{
		echo '<a class="cartmenu" HREF="showcart.php?catid=home" TARGET="_top">View Cart</a>';
	}
  if (isset($_SESSION["final"]))  //Set true in tr_checkout.php, unset in clearcartcode.php 
	{
		echo '<br><a class="cartmenu" HREF="tr_checkout.php" TARGET="_top">Set Shipping</a>';
	}
  //echo "<br>bb";
	echo '</td>	</tr></table>';
	
	//END of right MENU  and end of TR_HEADER
return true;
}

function call_login()
{
	echo "<a class=cartmenu HREF=weblogin.php>Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>";
	return true;
}

function ba_style() 
{
  //Supplies
	echo "<LINK rel='stylesheet'  href='tr.css' TYPE='text/css'>";
  return true;
}

function st_style()
{
  //Stationery
	echo "<LINK REL='STYLESHEET' href='st.css' TYPE='text/css' />";
  return true;
}

function pa_style()
{
  //paper
	echo "<LINK REL='STYLESHEET' href='pa.css' TYPE='text/css' />";
  return true;
}

function home2_on()
{
    // Redirects browser to TR "Home" page -- different on WEB/TR than in test
    if ($_SERVER["SERVER_NAME"]=="localhost"  ||  $_SERVER["SERVER_NAME"]=="127.0.0.1")
    {
    	$mret = '<a href="index.html" target="_top"><img src="graphics/home2_on.gif" alt="Return to Twinrocker Home Page" width="48" height="38"  align="bottom"></a>';
    }
    else
    {
    	$mret = '<a href="http://www.twinrocker.com/index.html" target="_top"><img src="graphics/home2_on.gif" alt="Return to Twinrocker Home Page" width="48" height="38"  align="bottom"></a>';
    }
	return $mret;
}

function show_pulp()
{
	include('tr_pulp_desc.php'); 
	return true;
}

function pulp_prices()
{
	include('tr_pulp_price.php');   //loads pulp pricing  -- array $halfstuff
	
	//echo '<div align=right>Click <a href=showprod.php?dept=Pulp&grp=Pulp+Order+Form> Here</a> to go  to the <strong>Pulp Order Form. </strong></div><br>';
	echo '<strong>PULP PRICES</strong><br>';
	echo "<table align =center class=fiber border=1 ><tr><td align=center colspan=3>
	      <B>Pulp made from Half-stuff</b></td></tr>
		  <tr><td>FIBER</td><td>Price per pail<br>SINGLE PAIL</td><td>Price per pail<br>GROUPS of 5</td></tr>";
		  
	for ($i=0;$i<8;$i++)
	{
		echo '<tr><td>'.$halfstuff[$i][0].'</td><td align=center>$'.$halfstuff[$i][1].'</td><td align=center>$'.($halfstuff[$i][1]-$halfstuff[$i][2]).'</td></tr>';
	}
	echo "<tr><td align=center colspan=3>
	      <b>Pulp made from Raw Fiber</b>-<br>(i.e. Fiber must be cooked before beating)</td></tr>
		  <tr><td>FIBER</td><td >Price per pail<br>SINGLE PAIL</td><td>&nbsp;</td></tr>";
	for ($i=8;$i<count($halfstuff);$i++)
	{
		echo '<tr><td>'.$halfstuff[$i][0].'</td><td align=center>$'.$halfstuff[$i][1].'</td><td align=center>&nbsp;</td></tr>';
	}
		  
	echo "</table>";
	br(2);
	
	// Pricing below comes from tr_pulp_price.php
	echo "<a NAME=pulpinfo></a>
		<p><strong>PULP PRICING NOTES:</strong> 
		<p>1. Each single pail batch is made from 3 POUNDS OF DRY FIBER, beaten to your specification.
		   Please describe your intended use of the pulp (casting, sheet forming, etc.) 
        <p>2. BEATING: Please specify as VC, C, MC, M, MF, F, or VF. For MF, add $".$beatopt[4][2]." per
		   pail, for F, add $".$beatopt[5][2]." per pail or for VF, add $".$beatopt[6][2]." per pail. Large batches (<b>5-in-4</b>)
		   are $".(2*$beatopt[4][2])." for MF, $".(2*$beatopt[5][2])." for F & $".(2*$beatopt[6][2])." for VF.
        <p>3. ADDITIVES: Sizing, CMC, Clay, or Calcium Carbonate, add $1.00 each per 
		   additive, per 3 pound batch. Clay added in Groups of 5 only.
        <p>4. PAILS: Pulp is shipped in 5 gallon plastic pails, each of which hold 
		   1 batch (3 lbs dry wt). To save shipping weight, each batch is concentrated 
		   by removing about 2/3 of the water.
        
        <p>5. GROUPS OF 5 <b>(5-in-4)</b>: We have a beater that accommodates 5 batches of pulp at a time (15 pounds of dry fiber),
		   hence the dramatic price reduction (see above) for groups of 5 batches. When you order
		   one <b>5-in-4</b> batch of the same pulp or pulp mixture(equal to five single pail batches), we are able to drain enough water to ship in
		   4 pails, (thus the name, 5-in-4) unless you specifically tell us otherwise. ";

	echo "
		  <p><strong>SHIPPING PULP </strong>
	      <p>A pail of pulp is heavy, 42 lbs. We must add a shipping surcharge, which 
		  depends on distance. It is much less than the actual freight charges, however. ";
		   
	

	// Get state & pulp shipping. 
	$ps = get_pulp_shipping();
	
	echo "<br><br><table align =center class=fiber border=1 ><tr><td align=center colspan=5>
	      Wet, Ready-To-Use Pulp, Shipping Charge Per Pail</td></tr>";
		
	$max = count($ps);
	$j = 0 ;
	do 
	{
		echo '<tr>';
		for ($i = 0; $i<5;$i++)
		{
			if ($j+$i < $max && $ps[$j+$i][0])
				echo '<td>'.$ps[$j+$i][0].':$'.$ps[$j+$i][1].'</td>';
			else
				echo '<td>&nbsp;</td>';
				
		}
		echo '</tr>';
		$j = $j+5;
	}
	while ($j < 47);
	
	echo '</table>';
	
	
return true;		
	
}

function product_admin($dept,$groupname,$prod)
{
	$editbtn = '<a class=cartmenu href=admin_prod.php?action=edit&dept='.urlencode($dept).'&grp='.urlencode($groupname).'&product='.urlencode($prod).'>Edit</a>';
	$addbtn  = '<a class=cartmenu href=admin_prod.php?action=add&dept='.urlencode($dept).'&grp='.urlencode($groupname).'>Add</a>';

	return $editbtn.'  '.$addbtn;
}

function show_next($productarray,$currentprod)
{
	//create link to next item in list. When at end, got to top...
	//
	$cnt = count($productarray);
	
	if ($cnt<1)
		return 'No Data';	//flawed data
	else
	{
		for ($i=0;$i<$cnt; $i++)
		{
			if ($productarray[$i][0]==$currentprod)
				break;
		}
		// could be   1 for 1;  1 of many;  last of last
		$i++;
		if ($i >= $cnt)
			$i = 0;
	}
	//echo $cnt;br();echo $i;echo $productarray[$i][0];
	return $productarray[$i][0];
}



function tr_menu()
{
   IF (isset($_SESSION["menubgcolor"]))
   		$mnucolor = $_SESSION['menubgcolor'];
   else
   		$mnucolor = 'silver';
   echo "  
   <style type ='text/css'>
	  A:visited { background:black;color:white;text-decoration:underline }
	  A:link { background:black;color:white;text-decoration:underline }
	  A:hover   {background:white;color:black;text-decoration:underline}
    </style>";
	
 	//echo "<table border=1 width=100% bgcolor='$mnucolor'><tr><td width=10% nowrap><font face=arial size=2><a  HREF=show_cart.php?catid=home>View Cart</a> |
  	//<a  HREF=checkout.php?catid=home>Checkout</a></font></td><td align=right><font face=arial size=2>  &nbsp;&nbsp;&nbsp;&nbsp;(Click on item prices to add items to the cart.)
	//</font></td></tr></table>";
	echo "&nbsp;&nbsp;&nbsp;<font size=1 face=arial>
	<a  HREF=showcart.php?catid=home TARGET='baright'>View Cart</a> |
  	<a  HREF=checkout.php?catid=home TARGET='baright'>Checkout</a> | 
	<a  HREF=index.html TARGET='_top'>Home</a></font>";
}

function tr_pricestyle()
{
   echo "  
   <style type ='text/css'>
      P.product  {
			background:#f7ffce;
	  		color:#000000;
	  		font-family:Georgia;
			font-weight:bold;
		  	font-size:14px;
	      	text-align:left;
			text-transform:uppercase; }
      table.header  {
			background:#8a8987;
	  		color:#ffffff;
	  		font-family:serif;
			font-weight:bold;
		  	font-size:18px;
	      	text-align:left;
			text-transform:uppercase; }			
      td.topmenu2  {
			background:#000000;
	  		color:#ffffff;
	  		font-family:verdana;
			font-weight:normal;
		  	font-size:10px;
	      	text-align:left;
			text-transform:Capitalize; }		
			
	  td.leftmenu {text-align:center; background:#ffcc99
	  A:visited { background:#ffcc99;color:#660000; font-weight: bold; font-size: 11px; text-decoration:none }
	  A:link { text-decoration:none }
	  A:hover   {background:#660000;color:#ffcc99; font-weight: bold; font-size: 11px; text-decoration:none}
	  }
				
      table.pricetable {white-space:nowrap;
	  	  font-family:arial,sans-serif;
		  font-face:arial;
		  font-size:12px;
	      text-align:left; 
		  background:#e9e0e0}
	  table.descrip {white-space:nowrap;
	  	  font-family:arial;
		  font-size:12px;
	      text-align:left; 
		  color:#000000;
		  background:#fffff}
	  tr.info {white-space:nowrap;
	  	  font-family:arial;
		  font-size:10px;
	      text-align:center;
		  color:#000000; 
		  background:#c6eff7}
		 
	  A:visited { color:blue;text-decoration:underline }
	  A:link { text-decoration:underline }
	  A:hover   {background:blue;color:white;text-decoration:underline}
    </style>";
	return true;
}

function tr_frmleftstyle()
{
	echo "<style type ='text/css'>
	  TD {text-align:center; background:#ffcc99}
	  A:visited { background:#ffcc99;color:#660000; font-weight: bold; font-size: 11px; text-decoration:none }
	  A:link { text-decoration:none }
	  A:hover   {background:#660000;color:#ffcc99; font-weight: bold; font-size: 11px; text-decoration:none}
    </style>";
	return true;
}


function binder_board()
{
   // This is complicated code, look carefully and make a back 
   // up before editing.                
   // 1/28/05 - Changed 74 to 79 when product items changed
   // 3/5/07  - Changed to 79 to 74 for 2nd item line... reactivated 74, cmmtd 79
echo "<br>
<b>Half Sheets: (Prices are per half sheet, <u>minimum 2 sheets</u>)</b> <br>
Note: Half sheets can be combined to get the quantity discounts<br> when ordered at one time from this form.
<form action=bboardcost.php method=post>
<input type=hidden name=gotouri value='showprod.php?dept=Book+Arts&grp=Covers&prod=Binders+Board'>
-----------------------------------------------    2-9 Shts  10-25 Shts  26+ Shts<br>";
echo "<input type=text name='qty059' value=0 size=2>   0.059 in thick x 26 in  x 19 in";
echo npspace(10).silk_show('BDH59A').npspace(8).silk_show('BDH59B').npspace(8).silk_show('BDH59C').'<br>';
//echo "<input type=text name='qty079' value=0 size=2>   0.079 in thick x 18 in  x 24 in";
//echo npspace(10).silk_show('BDH79A').npspace(8).silk_show('BDH79B').npspace(8).silk_show('BDH79C').'<br>';

echo "<input type=text name='qty074' value=0 size=2>   0.079 in thick x 26 in  x 19 in";
echo npspace(10).silk_show('BDH74A').npspace(8).silk_show('BDH74B').npspace(8).silk_show('BDH74C').'<br>';

echo "<input type=text name='qty082' value=0 size=2>   0.089 in thick x 26 in  x 19 in";
echo npspace(10).silk_show('BDH82A').npspace(8).silk_show('BDH82B').npspace(8).silk_show('BDH82C').'<br>';
echo "<input type=text name='qty098' value=0 size=2>   0.098 in thick x 26 in  x 19 in";
echo npspace(10).silk_show('BDH98A').npspace(8).silk_show('BDH98B').npspace(8).silk_show('BDH98C').'<br>';
echo "<input type=submit value='Submit Half Sheet Order'>
</form>";

return true;       
}

function book_tape()
{
// This is complicated code, look carefully and make a back 
// up before editing.                
echo "<br>
<b>Grey Linen Book Tape: <br><br></b>
&nbsp;&nbsp;Width  &nbsp;&nbsp;  Price/foot &nbsp;&nbsp;   Price/yard  &nbsp;&nbsp;  Price/36 yd spool<br>
&nbsp;&nbsp;------------------------------------------------------------------ <br>";
echo '&nbsp;&nbsp;  1/4"'.npspace(10).silk_buy('TLF025').npspace(10).silk_buy('TLY025').npspace(16).silk_buy('TLS025').'<br>';
echo '&nbsp;&nbsp;  3/8"'.npspace(10).silk_buy('TLF037').npspace(10).silk_buy('TLY037').npspace(16).silk_buy('TLS037').'<br>';
echo '&nbsp;&nbsp;  1/2"'.npspace(10).silk_buy('TLF050').npspace(10).silk_buy('TLY050').npspace(16).silk_buy('TLS050').'<br>';
echo '&nbsp;&nbsp;  3/4"'.npspace(10).silk_buy('TLF075').npspace(10).silk_buy('TLY075').npspace(16).silk_buy('TLS075').'<br>';


return true;       
}

function fluid_4to128($prefix,$name)
{
echo "<br>
<b>$name: <br><br></b>
&nbsp;&nbsp;&nbsp;4 oz.  ".npspace(10)."  8 oz. ".npspace(12)."   Pint  ".npspace(10)."  1/2 gal.  ".npspace(9)."  1 gal.<br>
&nbsp;&nbsp;-------------------------------------------------------------------------- <br>";
echo '&nbsp;&nbsp;'.  silk_buy($prefix.'004').npspace(10).silk_buy($prefix.'008').npspace(10).silk_buy($prefix.'016').npspace(10).silk_buy($prefix.'064').npspace(10).silk_buy($prefix.'128').'<br>';
return true;
}

function fluid_02to16($prefix,$name)
{
echo "<br>
<b>$name: <br><br></b>
&nbsp;&nbsp;&nbsp;2 oz.  ".npspace(12)."  4 oz. ".npspace(12)."   8 oz. ".npspace(10)."   Pint  <br>
&nbsp;&nbsp;-------------------------------------------------------------------------- <br>";
echo '&nbsp;&nbsp;'.  silk_buy($prefix.'002').npspace(10).silk_buy($prefix.'004').npspace(10).silk_buy($prefix.'008').npspace(10).silk_buy($prefix.'016').'<br>';
return true;
}


function fluid_08to128($prefix,$name)
{
echo "<br>
<b>$name: <br><br></b>
&nbsp;&nbsp;&nbsp;1/2 pint ".npspace(12)."   Pint  ".npspace(10)."  1/2 gal.  ".npspace(9)."  1 gal.<br>
&nbsp;&nbsp;-------------------------------------------------------------------------- <br>";
echo '&nbsp;&nbsp;&nbsp;'.silk_buy($prefix.'008').npspace(13).silk_buy($prefix.'016').npspace(12).silk_buy($prefix.'064').npspace(10).silk_buy($prefix.'128').'<br>';
return true;
}


function fluid_16to128($prefix,$name)
{
echo "<br>
<b>$name: <br><br></b>
&nbsp;&nbsp;&nbsp;Pint  ".npspace(10)." 1/2 Gal.".npspace(12)."  1 Gal. <br>
&nbsp;&nbsp;-------------------------------------------------- <br>";
echo '&nbsp;&nbsp;'.  silk_buy($prefix.'016').npspace(10).silk_buy($prefix.'064').npspace(14).silk_buy($prefix.'128').'<br>';
return true;
}

function fluid_16to640($prefix,$name)
{
echo "<br>
<b>$name: <br><br></b>
&nbsp;&nbsp;&nbsp;Pint  ".npspace(8)." 1/2 Gal.".npspace(10)."  1 Gal.".npspace(10)."  5 Gal. <br>
&nbsp;&nbsp;-------------------------------------------------------------------- <br>";
echo '&nbsp;&nbsp;'.  silk_buy($prefix.'016').npspace(8).silk_buy($prefix.'064').npspace(12).silk_buy($prefix.'128').npspace(12).silk_buy($prefix.'640').'<br>';
return true;
}

function tr_pearlescents()
{
$pig = array(array('Sparkle','C-WS'),
			 array('Super Sparkle','C-SW'),
			 array('Sparkle Gold','C-SG'),
			 array('Antique Gold','C-AG'),
			 array('Golden Bronze','C-GB'),
			 array('Copper','C-CO'),
			 array('Super Russet','C-SR'),
 			 array('Blue','C-PB'),
			 array('Red','C-PR'),
			 array('Green','C-PG'),
			 array('Violet','C-PV'),
			 array('Orange','C-PO')
			 );
			 
echo '<b>Individual Pearlescent Pigments:</b><br><table class="fiber" border=1>
	  <tr><th>Color</th><th>2 oz</th><th>4 Oz</th><th>8 Oz</th><th>Pint</th></tr>';
$max = count($pig);
for ($i=0;$i<$max;$i++)
{
	echo '<tr><td data-label="Color">'.$pig[$i][0].'</td><td data-label="2 oz">'.silk_buy($pig[$i][1].'02').'</td><td data-label="4 oz">'.silk_buy($pig[$i][1].'04').'</td><td data-label="8 oz">'.silk_buy($pig[$i][1].'08').'</td><td data-label="Pint">'.silk_buy($pig[$i][1].'16').'</td></tr>';
}
echo '</table>';
return true;
}

function tr_pig_samplers()
{
$pig = array(array('Full Sampler:','All 12 colors except white','C-SA'),
			 array('Primary Set','Warm Red, Warm Yellow, Blue, Black','C-SP'),
			 array('Secondary Set','Cool Red, Cool Yellow, Turquoise, Magenta','C-SS'),
			 array('Earthtone Set','Yellow Ochre, Raw Umber, Burnt Umber, Red Oxide','C-SE')
			 );
			 
echo '<b>Pigment Sets:</b><br><table class="fiber"  border=1>
	  <tr><th>Color</th><th>Basis</th><th>2 oz</th><th>4 Oz</th><th>8 Oz</th><th>Pint</th></tr>';
$max = count($pig);
for ($i=0;$i<$max;$i++)
{
	echo '<tr><td data-label="Color">'.$pig[$i][0].'</td><td data-label="Basis">'.$pig[$i][1].'</td><td data-label="2 oz">'.silk_buy($pig[$i][2].'02').'</td><td data-label="4 oz">'.silk_buy($pig[$i][2].'04').'</td><td data-label="8 oz">'.silk_buy($pig[$i][2].'08').'</td><td data-label="Pint">'.silk_buy($pig[$i][2].'16').'</td></tr>';
}
echo '</table>';
return true;
}

function tr_pigments()
{
$pig = array(array('Black','black 7','C-BA'),
			 array('Magenta','red 122','C-MA'),
			 array('Cool Red','red 170','C-CR'),
			 array('Warm Red','red 22','C-WR'),
			 array('Red Oxide','red 101','C-RO'),
			 array('Yellow Ochre','yellow 42','C-YO'),
			 array('Warm Yellow','yellow 83','C-WY'),
 			 array('Cool Yellow','yellow 97','C-CY'),
			 array('Turquoise','green 7','C-TU'),
			 array('Blue','blue 15','C-BU'),
			 array('Raw Umber','brown 7','C-UR'),
			 array('Burnt Umber','brown 7','C-UB'),
			 array('White','(n/a)','C-WH')
			 );
       
       //4/24/2012 remove violet
       //			 array('Violet','violet 23','C-VI'),

			 
echo '<b>Individual Pigments:</b><br><table class="fiber" border=1>
	  <tr><th>Color</th><th>Index</th><th>2 oz</th><th>4 Oz</th><th>8 Oz</th><th>Pint</th></tr>';
$max = count($pig);
for ($i=0;$i<$max;$i++)
{
	echo '<tr><td data-label="Color">'.$pig[$i][0].'</td><td data-label="Index">'.$pig[$i][1].'</td><td data-label="2 oz">'.silk_buy($pig[$i][2].'02').'</td><td data-label="4 oz">'.silk_buy($pig[$i][2].'04').'</td><td data-label="8 oz">'.silk_buy($pig[$i][2].'08').'</td><td data-label="Pint">'.silk_buy($pig[$i][2].'16').'</td></tr>';
}
echo '</table>';
return true;
}

function waxed_linen_colored_thread($gotouri='')
{

	$thrd_colors = array( array('White','WHT'),
				    array('Black','BLA'),
					array('Royal Blue','BLU'),
					array('Teal','TEA'),
					array('Slate Grey','GRY'),
					array('Country Red','RED'),
					array('Maroon','MAR'),
					array('Plum','PLU'),
					array('Walnut Brown','BRN'),
					array('Extra Dark Green','GRN')
				   );
	$fiveyardprice = get_item_price('TLWBLA');
	$spoolprice	= get_item_price('TLWBLAS');		   
	$thrd_size = array(array("5 Yards of Any Color: $$fiveyardprice",'_'), 
				       array("100 Yard Spool of Any Color: $$spoolprice",'S')
					  );
					  
	$goto = urlencode($_SERVER["REQUEST_URI"]."#$gotouri");
	
	echo "<form action=trclrdthrd.php method=post>
		  <input type=hidden name=itemprefix value='TLW'>
		  <input type=hidden name=returnto value='$goto'>
		  ";
		
	//echo $goto;
    br(2);
	build_select($thrd_colors,'color','BLA');
	br(2);
    build_select($thrd_size,'itemsuffix','_');
	br(2);
    echo "<input type=text name=qty value='1' size=4> Qty    
	      <br /><br />
          <input type=submit value='Add' />
          <br />";
          // 4/19/2017 Stacked vertically for mobile
    //echo "<input type=text name=qty value='1' size=4> Qty    
	//      &nbsp;&nbsp;&nbsp;&nbsp;<input type=submit value='Add' />";

	// 06/27/2015 Fixed missing '>' above
	echo '</form>';
  	return true;
}

function poe_leaves()
{
echo "<table border=1 class=smalltext cellpadding=3 cellspacing=3>
	  <tr><th align=right>Sets of =></th><th align=center>10</th><th align=center>50</th><th align=center>100</th></tr>
	  <tr><td align=right>Natural Color (Narrow)</td><td align=center>".silk_buy('LVPOEN010')."</td><td align=center>".silk_buy('LVPOEN050')."</td><td align=center>".silk_buy('LVPOEN100')."</td></tr>
	  <tr><td align=right>Natural Color (Wide)</td><td align=center>".silk_buy('LVPOEW010')."</td><td align=center>".silk_buy('LVPOEW050')."</td><td align=center>".silk_buy('LVPOEW100')."</td></tr>
	  <tr><td align=right>Gold</td><td align=center>".silk_buy('LVPOEG010')."</td><td align=center>".silk_buy('LVPOEG050')."</td><td align=center>".silk_buy('LVPOEG100')."</td></tr>
	  <tr><td align=right>Silver</td><td align=center>".silk_buy('LVPOES010')."</td><td align=center>".silk_buy('LVPOES050')."</td><td align=center>".silk_buy('LVPOES100')."</td></tr>
	  <tr><td align=right>Blue</td><td align=center>".silk_buy('LVPOEB010')."</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td></tr>
	  <tr><td align=right>Sampler  - ".silk_buy('LVSAMPLER')."</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td></tr>
	  
	  </table>";
return true;
}

function bo_leaves()
{
echo "<table border=1 class=smalltext cellpadding=3 cellspacing=3>
	  <tr><th align=right>Sets of =></th><th align=center>10</th><th align=center>50</th><th align=center>100</th></tr>
	  <tr><td align=right>Natural Color (Narrow)</td><td align=center>".silk_buy_bo('LVPOEN010')."</td><td align=center>".silk_buy_bo('LVPOEN050')."</td><td align=center>".silk_buy_bo('LVPOEN100')."</td></tr>
	  <tr><td align=right>Natural Color (Wide)</td><td align=center>".silk_buy_bo('LVPOEW010')."</td><td align=center>".silk_buy_bo('LVPOEW050')."</td><td align=center>".silk_buy_bo('LVPOEW100')."</td></tr>
	  <tr><td align=right>Gold</td><td align=center>".silk_buy_bo('LVPOEG010')."</td><td align=center>".silk_buy_bo('LVPOEG050')."</td><td align=center>".silk_buy_bo('LVPOEG100')."</td></tr>
	  <tr><td align=right>Silver</td><td align=center>".silk_buy_bo('LVPOES010')."</td><td align=center>".silk_buy_bo('LVPOES050')."</td><td align=center>".silk_buy_bo('LVPOES100')."</td></tr>
	  <tr><td align=right>Blue</td><td align=center>".silk_buy_bo('LVPOEB010')."</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td></tr>
	  <tr><td align=right>Sampler  - ".silk_buy_bo('LVSAMPLER')."</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td></tr>

	  </table>";
return true;
}

function clear_sleeves()
{
echo "<table border=1 class=smalltext cellpadding=3 cellspacing=3>
	  <tr><th align=right>Sleeve size, opening first</th><th align=center>Qty 10</th><th align=center>Qty 50</th><th align=center>Qty 100</th></tr>
	  <tr><td align=left>6\"x4-1/2\" (A2)</td><td align=center>".silk_buy('SC060043010')."</td><td align=center>".silk_buy('SC060043050')."</td><td align=center>".silk_buy('SC060043100')."</td></tr>
	  <tr><td align=left>4-1/2\"x6-1/4\" (A2)</td><td align=center>".silk_buy('SC043061010')."</td><td align=center>".silk_buy('SC043061050')."</td><td align=center>".silk_buy('SC043061100')."</td></tr>
	  <tr><td align=left>6-7/16\"x6-1/4\"</td><td align=center>".silk_buy('SC062061010')."</td><td align=center>".silk_buy('SC062061050')."</td><td align=center>".silk_buy('SC062061100')."</td></tr>
	  <tr><td align=left>7-7/16\"x5-1/4\" (A7)</td><td align=center>".silk_buy('SC072051010')."</td><td align=center>".silk_buy('SC072051050')."</td><td align=center>".silk_buy('SC072051100')."</td></tr>
	  <tr><td align=left>5-7/16\"x7-1/4\" (A7)</td><td align=center>".silk_buy('SC052071010')."</td><td align=center>".silk_buy('SC052071050')."</td><td align=center>".silk_buy('SC052071100')."</td></tr>
	  <tr><td align=left>8-7/16\"x6-1/4\"</td><td align=center>".silk_buy('SC082061010')."</td><td align=center>".silk_buy('SC082061050')."</td><td align=center>".silk_buy('SC082061100')."</td></tr>
	  <tr><td align=left>10-7/16\"x8-1/4\"</td><td align=center>".silk_buy('SC102081010')."</td><td align=center>".silk_buy('SC102081050')."</td><td align=center>".silk_buy('SC102081100')."</td></tr>
	  <tr><td align=left>7-7/16\"x11\"</td><td align=center>".silk_buy('SC072110010')."</td><td align=center>".silk_buy('SC072110050')."</td><td align=center>".silk_buy('SC072110100')."</td></tr>
	  <tr><td align=left>11-7/16\"x8-3/4\"</td><td align=center>".silk_buy('SC112084010')."</td><td align=center>".silk_buy('SC112084050')."</td><td align=center>".silk_buy('SC112084100')."</td></tr>
	  <tr><td align=left>&nbsp;</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td></tr>
	  <tr><td align=left>Sampler,  2 of each size above: </td><td align=center colspan=3>".silk_buy('SCSAMPLE')."</td></tr>
	  <tr><td align=left>&nbsp;</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td></tr>

	  </table>";
	  
/*  odd sizes removed per GS 12/11/03
	  <tr><td align=left>11-1/4\"x14-1/4\"</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>".silk_buy('SC111141100')."</td></tr>
	  <tr><td align=left>16-1/4\"x20-1/4\"</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>".silk_buy('SC161201100')."</td></tr>
	  <tr><td align=left>20-1/4\"x30-1/4\"</td><td align=center>&nbsp;</td><td align=center>&nbsp;</td><td align=center>".silk_buy('SC201301100')."</td></tr>
*/
return true;
}

function storage_bags()
{
echo "<table border=1 class=smalltext cellpadding=3 cellspacing=3>
	  <tr><th align=left>Bag size, opening first</th><th colspan=3 align=center>No. of Bags  & Price</th></tr>
	  <tr><td align=left>8-1/2\"x11\"</td><td align=center>10 ".silk_buy('SP083110010')."</td><td align=center>25 ".silk_buy('SP083110025')."</td><td align=center>50 ".silk_buy('SP083110050')."</td></tr>
	  <tr><td align=left>10-1/2\"x15-1/4\"</td><td align=center>10 ".silk_buy('SP103151010')."</td><td align=center>25 ".silk_buy('SP103151025')."</td><td align=center>50 ".silk_buy('SP103151050')."</td></tr>
	  <tr><td align=left>15-1/4\"x18-3/4\"</td><td align=center>10 ".silk_buy('SP151184010')."</td><td align=center>25 ".silk_buy('SP151184025')."</td><td align=center>50 ".silk_buy('SP151184050')."</td></tr>
	  <tr><td align=left>21\"x26\"</td><td align=center>10 ".silk_buy('SP210260010')."</td><td align=center>25 ".silk_buy('SP210260025')."</td><td align=center>50 ".silk_buy('SP210260050')."</td></tr>
	  <tr><td align=left>25-1/4\"x34\"</td><td align=center>10 ".silk_buy('SP251340010')."</td><td align=center>25 ".silk_buy('SP251340025')."</td><td align=center>50 ".silk_buy('SP251340050')."</td></tr>
	  <tr><td align=left>28\"x38\"</td><td align=center>10 ".silk_buy('SP280380010')."</td><td align=center>25 ".silk_buy('SP280380025')."</td><td align=center>50 ".silk_buy('SP280380050')."</td></tr>
	  <tr><td align=left>36\"x48\"</td><td align=center> 5 ".silk_buy('SP360480005')."</td><td align=center>10 ".silk_buy('SP360480010')."</td><td align=center>25 ".silk_buy('SP360480025')."</td></tr>
	  <tr><td align=left>49\"x66\"</td><td align=center> 5 ".silk_buy('SP490660005')."</td><td align=center>10 ".silk_buy('SP490660010')."</td><td align=center>25 ".silk_buy('SP490660025')."</td></tr>
	  
	  </table>";
return true;
}

function storage_bags2($bags)
{
// 5/5/2015  New Poly bags
// change true to false to inactivate/prevent display
// copy a line and insert/edit to add a new bag
// delete line to permanently remove
// Last entry in array has no trailing comma
// $polybags = array(
//           array(true,'9x12','-','SP090120010','SP090120025','SP090120050'),
//           array(true,'11x16','-','SP110160010','SP110160025','SP110160050'),
//           array(true,'16x18','-','SP160180010','SP160180025','SP160180050'),
//           array(true,'20x30','-','SP200300010','SP200300025','SP200300050'),
//           array(true,'28x34','-','SP280340010','SP280340025','SP280340050'),
//           array(true,'36x48','SP360480005','SP360480010','SP360480025','-'),
//           array(true,'48x54 (3 mil)','SP480540005','SP480540010','SP480540025','-')          
//     );
    
echo "<table border=1 class=smalltext cellpadding=3 cellspacing=3>
	  <tr><th align=left>Bag size, opening first</th>
          <th align=center>Qty=5</th>
          <th align=center>Qty=10</th>
          <th align=center>Qty=25</th>
          <th align=center>Qty=50</th>    
      </tr>";

      $num = count($bags);
      for($i = 0; $i < $num; ++$i) {
        $bag = $bags[$i];
        if($bag[0]===false)  {
            // if  'false', skip to next bag size
            continue;
        }
        
        $val = array();
        for ($k = 2; $k < 6; ++$k) {
            if ($bag[$k] == '-')
                $val[$k] = '-';
            else
                $val[$k] = silk_buy($bag[$k]);
        }
         
        echo "<tr>
                 <td align=center>$bag[1]</td>
                 <td align=center>$val[2]</td>
                 <td align=center>$val[3]</td>
                 <td align=center>$val[4]</td>
                 <td align=center>$val[5]</td>
             </tr>";
      }
      echo "</table>";
return true;
}

function stacor_cabinets()
{
echo "<table border=1 class=smalltext cellpadding=3 cellspacing=3>
	  <tr><th align=Center>Cabinet Size</th><th align=center>Drawer Size</th><th align=center>Price</th><th align=center>Weight</th><th align=center>6\" Base Price/Wt.</th></tr>
	  <tr><td align=left>29-3/4\"x22-5/8\"x7-1/2\"</td><td align=center>26\"x20\"x2\"</td><td align=center>".silk_buy('CALL')."</td><td align=center>50 ".silk_buy('CALL')."</td><td align=center>50 ".silk_buy('CALL')."</td></tr>
	  
	  </table>";
return true;
}

function french_wax($gotouri)
{

	$wax_colors = array( array('Black','BK'),
					array('Blue','BE'),
					array('Red','RD'),
					array('Plum','PM'),
					array('Gold','GD'),
					array('Silver','SR')
				   );
	$fourinchprice = get_item_price('W-FRD4');
	$eightinchprice	= get_item_price('W-FRD8');		   
	$wax_size = array(array('4" (four inch) stick of any color: $'.$fourinchprice,'4'), 
				       array('8" (eight inch) stick of any color: $'.$eightinchprice,'8')
					  );
	$goto = urlencode($_SERVER["REQUEST_URI"]."#$gotouri");
	
	echo "<form action=trclrdthrd.php method=post>
		  <input type=hidden name=itemprefix value='W-F'>
		  <input type=hidden name=returnto value='$goto'>";
	
	//echo $goto;
	
	build_select($wax_colors,'color','RD');
	build_select($wax_size,'itemsuffix','4');
	echo "<input type=text name=qty value='1' size=4> Qty    
	      &nbsp;&nbsp;&nbsp;&nbsp;<input type=submit value='Add'";
	
	echo '</form>';
  	return true;
}


function book_cloth($style,$gotouri='')
{
	//2/6/06 in showprod.php coverings  does both colors and tweed ($style)
    // 05/02/2015 Fixed '>' html; removed 'TAN' from Tweed
    
$goto = urlencode($_SERVER["REQUEST_URI"]."#$gotouri");

if ($style=='Solid Colors')
{
	$colors = array(array('Black','BLA'),
					array('Navy Blue','NAV'),
					array('Royal Blue','ROY'),
					array('Lavender','LAV'),
					array('Maroon','MAR'),
					array('Grey','GRY'),
					array('Scarlet','SCA'),
					array('Muslin','MUS'),
					array('Tan','TAN')					
				   );
				   
	/*Tan, Muslin, Scarlet, Grey ,
	Maroon, Lavender, Royal Blue 
	Navy Blue, Black*/ 
				   
	$colorfootprice = get_item_price('BCLBLA120260');
	$colorhalfyardprice	= get_item_price('BCLBLA180520');		
	$colorfullyardprice	= get_item_price('BCLBLA360520');	   
	$size = array(array('12"x26": $'.$colorfootprice,'120260'), 
				       array('18"x52" half-yard: $'.$colorhalfyardprice,'180520'),
					   array('36"x52" yard: $'.$colorfullyardprice,'360520')
					  );
	$nextfix = 'L';
	$selcolor = 'ROY';
	$selsize  = '120260';
	//BC  L  BLA 360520
	echo "<br><br><b>$style:</b> Select Color and Size, enter quantity and press 'Add'<br><br>";
	echo "<form action='bookcloth.php' method=post>
		  <input type=hidden name=itemprefix value='BC'>
		  <input type=hidden name=nextfix value='L'>
		  <input type=hidden name=returnto value='$goto'>";


	build_select($colors,'color',$selcolor);
	build_select($size,'itemsuffix',$selsize);
	echo "<input type=text name=qty value='1' size=4> Qty
	      &nbsp;&nbsp;&nbsp;&nbsp;<input type=submit name=sub value='Add'>";

	echo '</form>';

}
else
{
	//tweed     
    // 5/4/2015 cmted Tan out of color choices
	$colors = array(array('Black','BLA')
    //, 					array('Tan','TAN')					
				   );
	/*BCTBLA120210 Book Cloth: Black Tweed 12x21" 4.50
	  BCTBLA180420 Book Cloth: Black Tweed 18x42" 11.00
 	  BCTBLA360420 Book Cloth: Black Tweed 36x42"*/ 
		   
	$colorfootprice = get_item_price('BCTBLA120210');
	$colorhalfyardprice	= get_item_price('BCTBLA180420');		
	$colorfullyardprice	= get_item_price('BCTBLA360420');	   
	$size = array(array('12"x21": $'.$colorfootprice,'120210'), 
				       array('18"x42" half-yard: $'.$colorhalfyardprice,'180420'),
					   array('36"x42" yard: $'.$colorfullyardprice,'360420')
					  );
	$nextfix = 'T';	
	$selcolor = 'BLA';
	$selsize  = '120210';
	
	//BC  L  BLA 360520
	echo "<br><br><b>$style:</b> Select Color and Size, enter quantity and press 'Add'<br><br>";
	echo "<form action='bookcloth.php' method=post>
		  <input type=hidden name=itemprefix2 value='BC'>
		  <input type=hidden name=nextfix2 value='T'>
		  <input type=hidden name=returnto2 value='$goto'>";


	build_select($colors,'color2',$selcolor);
	build_select($size,'itemsuffix2',$selsize);
	echo "<input type=text name=qty2 value='1' size=4> Qty
	      &nbsp;&nbsp;&nbsp;&nbsp;<input type=submit name=sub2 value='Add'>";

	echo '</form>';

}

  	return true;
}




function build_select($arrayopts,$name,$checked_val='')
{
	$rows = count($arrayopts);
    echo '<SELECT NAME='.$name.' size=1>';
    for ($i=0; $i<$rows; $i++) 
	{
        echo "
            <OPTION VALUE='".$arrayopts[$i][1]."'";
        if ($arrayopts[$i][1] == $checked_val) 
		{
            echo ' SELECTED';
        }
        echo '>'.$arrayopts[$i][0].'</OPTION>';
    }
    echo '</SELECT>';
	return true;
}

function product_URL_no_br($url, $name, $aclass='')
{
  // output URL as link with no <br>

	$mret =  "<a $aclass href=$url>$name</a>";
	return $mret;
}

function confirm_delete($dept,$grp,$prod)
{
  echo "<br>
        <table border=0><tr><td>Are you sure you want to permanently DELETE product:<b> $prod?</b>
        </td><td>
        <form method=POST action=admin_prod.php>
        <input type='hidden' name='action' value='Cancel Delete'>
        <input type='hidden' name='dept' value='$dept'>
        <input type='hidden' name='groupname' value='$grp'>
        <input type='hidden' name='product' value='$prod'>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type='submit' value=' No '>
        </form>
        </td><td>
        <form method=post action=admin_prod.php>
        <input type='hidden' name='action' value='Delete Confirmed'>
        <input type='hidden' name='dept' value='$dept'>
        <input type='hidden' name='groupname' value='$grp'>
        <input type='hidden' name='product' value='$prod'>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type='submit' value='Yes'>
        </form>
        </td></tr></table>";
  return true;
}


function display_prod_form($prod = "")
// This displays the product form.
// This form can be used for inserting or editing.
// To insert, don't pass any parameters.  This will set $edit
// to false, and the form will go to prod_insert.php.
// To update, pass an array containing a product.  The
// form will be displayed with the old data and point to prod_update.php.
// It will also add a "Delete" button for Admin users.

// 8/30/2010 Added mceEditor:
{
   if ($_SERVER["SERVER_NAME"]=="127.0.0.1")
   {
      $mce_path = '../../js/tinymce/jscripts/tiny_mce/tiny_mce.js';
   }
   else
   {
      $mce_path = './js/tinymce/jscripts/tiny_mce/tiny_mce.js';
   }
   
?>
<!--
// <script type="text/javascript" src="<?php echo $mce_path;?>"></script>

// <script type="text/javascript">
//tinyMCE.init({
//	mode : "exact",
//	elements : "mce",
//	theme : "advanced"
//});
//</script>
   
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
<head>
<title>Edit Product Descriptions</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>

<!-- OF COURSE YOU NEED TO ADAPT NEXT LINE TO YOUR tiny_mce.js PATH -->

</head>
<body>
<?php
  $a_alert = array(
              array ('None',''),
              array ('Temporarily out of stock','Temporarily out of stock'),
              array ('Limited Supply','Limited Supply')
              );
              
  $edit = is_array($prod); 
  if ($edit)
  {
  	if (empty($prod[0]["product"]))
		$mprod = 'Add product to '.$prod[0]["dept"].'  '.$prod[0]["groupname"];
	else
		$mprod = 'Edit '.$prod[0]["product"];
  	display_space_head(stripslashes($mprod));
  }
  else
	display_space_head("Add new Product record");

/*
"INSERT INTO `products` (`product`, `name`, `subheader`,`display`, `alert`, `bigimg`,
`alt`, `smallimg`, `height`, `width`, `align`, `descrip1`, `descrip2`, 
`items`, `pricetable`, `comments`, `keywords`, `moreinfo`, 
`dept`, `groupname`, `catid`, `displayorder`, `entered`) 
VALUES ('', '', '', 'y', '', '',
 '', '', '0', '0', '', '', '',
 '', '', '', '', '',
 '', '', '', '0', '0000-00-00'); 
*/
 
	
?>
  
  <form method=post
        action="<?php echo $edit?"admin_prod.php":"admin_prod.php";?>">
  <table WIDTH="100%" border=1 align=center bgcolor=#e0e0e0  >
   <tr>
    <td>&nbsp;</td>
	<td align='right'><input type='submit' name='<?php echo $edit?'action':'action'; ?>' 
               value='<?php echo $edit?'Save':'Add'; ?> Record'>
    <input type='submit' name='action' value='Cancel'>
	<input type=submit name=action value='Delete Record'>

	</td>
  </tr><tr>
    <td><b>Button:</b></td><td><input type=text size=20 maxlength=20 name=product value="<?php echo stripslashes($prod[0]["product"]); ?>">
  </tr><tr>
    <td><b>Header:</b></td><td NOWRAP><input type=text size=60 maxlength=100 name=name value="<?php echo stripslashes($prod[0]["name"]); ?>"> </td>
  </tr><tr>
    <td><b>Subheader:</b></td><td><input type=text size=60 maxlength=100 name=subheader value="<?php  echo stripslashes($prod[0]["subheader"]); ?>"></td>
  </tr><tr>
    <td><b>Display:</b></td><td>&nbsp;&nbsp;&nbsp;<input type=checkbox name='display' <?php if ($prod[0]["display"]=='y') echo 'CHECKED'; ?> > (Uncheck to immediately stop this product from displaying)</td>
  </tr><tr>
    <td><b>Alert:</b></td><td>
    <?php build_select($a_alert,'alert',stripslashes($prod[0]["alert"])); ?></td>
  </tr><tr>
    <td><b>Big Image URL:</b></td><td><input type=text size=40 maxlength=100 name=bigimg value="<?php  echo stripslashes($prod[0]["bigimg"]); ?>"></td>
  </tr><tr>
    <td><b>Alt Img Text:</b></td><td><input type=text size=40 maxlength=100 name=alt value="<?php  echo stripslashes($prod[0]["alt"]); ?>"></td>
  </tr><tr>
    <td><b>Small Image URL</b></td><td><input type=text size=40 maxlength=100 name=smallimg value="<?php  echo stripslashes($prod[0]["smallimg"]); ?>">
  </tr><tr>
    <td><b>Image size</b></td><td>&nbsp;&nbsp;&nbsp;&nbsp; Height&nbsp;<input type=text size=3 maxlength=3 name=height value="<?php  echo stripslashes($prod[0]["height"]); ?>">&nbsp;&nbsp;&nbsp;&nbsp;Width&nbsp; <input type=text size=3 maxlength=3 name=width value="<?php  echo stripslashes($prod[0]["width"]); ?>"></td>
  </tr><tr>
    <td><b>Align</b></td><td><input type=text size=6 maxlength=6 name=align value="<?php  echo stripslashes($prod[0]["align"]); ?>">&nbsp;&nbsp;&nbsp;(Align image with description)
  </tr><tr> 
    <td><b>Description1:</b></td><td><input type=text size=40 maxlength=100 name=descrip1 value="<?php  echo stripslashes($prod[0]["descrip1"]); ?>">&nbsp;&nbsp;&nbsp;(Highlighted description)</td>
  </tr><tr>
    <td><b>Description2:</b></td><td><textarea id="mce" class="mceEditor" name=descrip2 ROWS='16' COLS='60'> <?php  echo stripslashes($prod[0]["descrip2"]); ?> </textarea></td>
  </tr><tr>
    <td><b>Items:</b>
        <br>itemno1|descrip
        <br>&nbsp;&nbsp;&nbsp; .|some text
        <br>itemno2|descrip</td><td><textarea name=items ROWS='6' COLS='60'> <?php  echo stripslashes($prod[0]["items"]); ?> </textarea></td>
  </tr><tr>
    <td><b>Price Table HTML Code:</b>
        <br>(php Code)</td><td><textarea name=pricetable ROWS='6' COLS='60'> <?php  echo stripslashes($prod[0]["pricetable"]); ?> </textarea></td>
  </tr><tr>
    <td><b>Comments or Instructions</b>
        <br>(php Code)</td><td><textarea name=comments ROWS='6' COLS='60'> <?php  echo stripslashes($prod[0]["comments"]); ?> </textarea></td>
  </tr><tr>
    <td><b>Keywords:</b>
        <br>(Search terms or related product terms)</td><td><textarea name=keywords ROWS='6' COLS='60'> <?php  echo stripslashes($prod[0]["keywords"]); ?> </textarea></td>
  </tr><tr>
    <td><b>Moreinfo:</b>
        <br>(Shows in separate browser)</td><td><textarea name=moreinfo ROWS='6' COLS='60'> <?php  echo stripslashes($prod[0]["moreinfo"]); ?> </textarea></td>
  </tr><tr> 
    <td><b>Department:</b></td><td><input type=text size=40 maxlength=40 name=dept value="<?php  echo stripslashes($prod[0]["dept"]); ?>">(Must be defined in tr_header.php)</td>
  </tr><tr> 
    <td><b>Group:</b></td><td><input type=text size=40 maxlength=60 name=groupname value="<?php  echo stripslashes($prod[0]["groupname"]); ?>"></td>
  </tr><tr> 
    <td><b>Category ID:</b></td><td><input type=text size=6 maxlength=6 name=catid value="<?php  echo stripslashes($prod[0]["catid"]); ?>">(Optional)</td>
  </tr><tr> 
    <td><b>Display Order:</b></td><td><input type=text size=5 maxlength=5 name=displayorder value="<?php  echo stripslashes($prod[0]["displayorder"]); ?>">(Button display order. Use increments of 10. Negative numbers also work!)</td>
  </tr><tr>
    <td><b>Entered:</b></td><td><input type=text size=20 maxlength=20 name=entered value="<?php  echo stripslashes($prod[0]["entered"]); ?>" ></td>
  </tr><tr><td colspan='2'>&nbsp;<hr></td> 
   </tr><tr>
	<td>
	<td align='right'><input type='submit' name='action' 
               value='<?php  echo $edit?'Save':'Add'; ?> Record'>
   	     <input type='submit' name='action' value='Cancel'>
         <input type=submit name=action value='Delete Record'>

         </td>
      </tr>
  </table>
  </form>
  </center>
  </body>
  </html>

<?php
return; 	// from function display_prod_form()
}


function display_tr_cart($cart,$catid, $change = true, $images = 0)
{
  // display items in shopping cart
  // optionally allow changes (true or false)
  // optionally include images (1 - yes, 0 - no)
 
 global $g_table1_width;
 //echo 'XX1';  // 7/7/10 Debug IE6 removed align=right from table
 echo "<table class='fiber' width='$g_table1_width' border=1 cellspacing = 0  bgcolor=white>";
 echo "<form action = recalccart.php method = post>
        <input type=hidden name=returnto value='showcart.php'>";
 echo " <tr><td colspan=6><b>GENERAL ITEMS</b></td></tr>
      <tr>
        <td class='fiber' width=90>Quantity</td>
		  <td width=180>Item </td>
		  <td >No. & Comment</td>
        <td >Weight</td>
        <td width=60>Price</td>
        <td   align=right >Total&nbsp;&nbsp;</td>
      </tr>";
		
		//<th colspan = ". (1+$images) ." bgcolor=$bgcolor>Item </th>


  //display each item as a table row
  foreach ($cart as $item => $qty)
  {
    $invt = get_invt_details($item);
    echo "<tr>";
    echo "<td class=lineitem>";
    // if we allow changes, quantities are in text boxes
    if ($change == true)
      echo "<input type = text name = '$item' value = '$qty' size = 3 ".'onchange="return validateInteger(this.value);"'.">";
    else
      echo $qty;
    // echo "</td><td width=180><a href = show_invt.php?item=$item&catid=$catid&ref=cart>".$invt['descrip']."</a>";
	echo "</td><td class=lineitem width=100>".$invt['descrip'];
	echo "</td><td class=lineitem width=100 align = left >[".trim($invt["item"])."]";  // ref trs removed _by_
	
	if (left($item,1)!='Y')  // don't include weight of Stationery 1/12/03 (Stationery uses wt field for "code"
    	echo "</td><td class=lineitem width=60>".number_format($invt["weight"], 3);
	else
		echo "</td><td class=lineitem width=60>0.00";
		
    echo "</td><td class=lineitem width=60>$".number_format($invt["price"], 2);
    echo "&nbsp;&nbsp;</td>";
    echo "<td class=lineitem style='text-align:right' >$".number_format($invt["price"]*$qty,2)."</td></tr>";
  }
  // display total row
  echo "<tr>
          <td class=lineitem >" .
              $_SESSION['items']."&nbsp;Items
          </td>
          <td class=lineitem colspan = ". (1+$images) ." >&nbsp;</td>
          <td class=lineitem >
              &nbsp
          </td>
          <td class=lineitem >"
              .number_format($_SESSION["total_weight"], 3)."
          </td>
          <td class=lineitem >
              &nbsp
          </td>
          <td class=subtotal style='text-align:right' >
              \$".number_format($_SESSION["total_price"], 2).
          "&nbsp;</td>
        </tr>";
  // display save change button
  if($change == true)
  {
    // Next button (Recalculate)was "Save Changes"
    echo "<tr>
            <td class=lineitem colspan = ". (6+$images) ."  align = left>
              <input type = hidden name = save value = true> 
			  <input type = hidden name = catid value =". $catid.">
              <input type = submit value = 'Recalculate' STYLE='font:8pt ARIAL;background:#c6eff7'>
              <font color=red>(Change quantities, or set to zero to delete an item, then press 'Recalculate'.</font>)
            </td>
            </tr>";
        /* <input type = image src = \"images/save-changes.gif\" 
                     border = 0 alt = \"Recalculate\"> */
  }
  echo "</form></table>";
  return true;
}

function display_tr_fibercart($fcart,$disp_button)
{
   global $g_table1_width; 
   // 
   echo "<table  border=1 width='$g_table1_width' cellspacing = 0  bgcolor=white>
        <form action = showcart.php method = post> 
        <tr><td class=fiber colspan=6><b>FIBER ITEMS</b></td></tr><tr>
        <td class=fiber width=110 >Weight lbs</td>
		  <td class=fiber width=180>Item</td>
		  <td class=fiber>No. & Comments</td>
        <td class=fiber width=60>Bale Cnt</td>
		  <td class=fiber width=80>Price</td>
        <td class=fiber  style='text-align:right'>Total</td></tr>";
		
  
  //display each item echo counas a table row
 
  // for ($key = 0; $key < count($fcart); $key++)
  
  foreach ($fcart as $key => $row)
	{
		 //echo "<br>$key   $row";

			// $pbitem = array("catid"=>$catid,"item"=>$item,"description"=>$desc,
			// "price"=>$unitprice,"weight"=>$unitwt,
			// "qty"=>$qty,"totalamount"=>$unitprice*$qty*$unitweight,"totalweight"=>$qty*$unitwt);           "totalamount"=>$unitprice*$qty*$unitweight,"totalweight"=>$qty*$unitwt);					
		if ($fcart[$key]['totalweight']>0)
		  {
			echo "<tr>";
			echo "<td class=lineitem >".number_format(($row['totalweight']),1)."</td>";
			echo "<td class=lineitem>".$row["description"]."</td>";
			echo "<td class=lineitem NOWRAP>[".$row['item']."]&nbsp;&nbsp; 
			       ".$row['comment']."</td>";
			echo "<td class=lineitem>&nbsp;".$row['count']."</td>"; //".$row['qty']."
			echo "<td class=lineitem>$".$row['price']."</td>";
			echo "<td class=lineitem style='text-align:right'>$".number_format($row['totalamount'],2)."</td>";
			echo "</tr>";
			
	    }		
	}
	

  // Calculate total weight/item/price for fiber (and store in 'fiber_'session_vars)
  // calc_fiber_vars($fcart);// this is set in fiber.php, no need to rerun...
  // display totals row

  switch ($disp_button)
  {
		case 'clear' :
			$fiber_action = "<a href='tr_fiber.php?clearfiber=Y'>Clear Fiber Order</a>";
			break;
  		case 'edit'  :
    		$fiber_action = "<a href='tr_fiber.php?clearfiber=N'>Edit Fiber Order</a>";
			break;
		  case 'none'  :
			$fiber_action = '-';     //
			break;
	}

  echo "<tr>
          <td class=lineitem  width=100>"
              .number_format($_SESSION["fiber_weight"], 1)."&nbsp;lbs.
          </td>
          <td class=lineitem  >&nbsp;$fiber_action </td>
          <td class=lineitem >
              &nbsp
          </td>
          <td class=lineitem >". 
              $_SESSION["fiber_items"]."&nbsp;Types
          </td>
          <td class=lineitem>
              &nbsp
          </td>
          <td class=subtotal style='text-align:right'>
              \$".number_format($_SESSION["fiber_price"], 2).
          "</td>
        </tr>";

  echo "</form></table>";

}

function display_tr_pulpcart($pulpcart,$edit='')
{
		global $g_table1_width;	
  	    echo "<table border=1 width='$g_table1_width' cellspacing = 0  bgcolor=white>
        <tr><td class=fiber colspan=6><b>PULP ITEMS</b></td></tr>
        <tr>
		   <td class=fiber nowrap width=110>5/4|1/1 ::Ord/Shp</td>
		   <td class=fiber width=180>Item/Instructions</td>
		   <td class=fiber colspan=2>No. & Comments</td>
   		<td class=fiber weight=60 >Avg Price</td>
         <td class=fiber width=80 style='text-align:right'>Total</td>
        </tr>";
  
  		//display each item as a table row
 		
		reset($pulpcart);
		
  		
        foreach ($pulpcart as $n => $row)
  		{ 
			echo "<tr><td class=lineitem width=100>&nbsp;".$row['fives']." | ".$row['ones']."   ::  ".$row['qty']."/".$row['shpqty']." <a href=tr_pulp.php?del=$n>&nbsp;&nbsp;&nbsp;del</a></td>"; 
			echo "<td class=lineitem>".$row["description"]."</td>";
			echo "<td class=lineitem colspan=2>[".$row['item']."]&nbsp;&nbsp; 
		       ".$row['comment']."</td>";
			//echo "<td align=center>".number_format(($row['totalweight']),1)."</td>";
			echo "<td class=lineitem >$".number_format($row['price'],2)."</td>";
			echo "<td class=lineitem style='text-align:right'>$".number_format($row['totalamount'],2)."</td>";
			echo "</tr>";
			echo "<tr><td class=lineitem>&nbsp;</td><td class=lineitem colspan=5>&nbsp;&nbsp;".$row['pulptext']."</td>
			</tr>";
  		}

		$pulp_action = '';
		if ($edit=='edit') //called from checkout..
			$pulp_action = "<a href='tr_pulp.php'>Edit Pulp Order</a>";
		elseif ($edit=='')
			$pulp_action = "<a href='tr_pulp.php?clearpulp=Y'>Clear Pulp Order</a>";
    // 06/21/2012 changes pails to bags, handle the 's'
    if ($_SESSION["pulp_shpitems"] == 1)
                 $xplural = '';
              else
                 $xplural = 's';  
		echo "<tr>
          <td class=lineitem>Ship ".number_format($_SESSION["pulp_shpitems"],0)." bag".$xplural."</td>
		    <td class=lineitem colspan=2 >&nbsp;$pulp_action</td>
          <td class=lineitem>&nbsp;</td>
          <td class=lineitem>&nbsp</td>
          <td class=subtotal style='text-align:right'>\$".number_format($_SESSION["pulp_price"],2)."</td>
        </tr>";

		echo "</table>";
		return true;
}

function display_tr_papercart($papercart,$change = true, $catid='PP',$images = 0)
{
    // Created 7/30/04 -refa-
   global $g_table1_width;
   
  	echo "<table border=1 width='$g_table1_width' cellspacing = 0  bgcolor=white>  	      
  	      <form action = recalcpaper.php method = post>
  	      <input type=hidden name=returnto value='showcart.php'>
         <tr>
     		<td class=fiber colspan=6><b>PAPER ITEMS</b></td>
         </tr>
         <tr>
		     <td class=fiber nowrap width=110>Qty</td>
		     <td class=fiber width=180>Item</td>
		     <td class=fiber colspan=2>No. & Comments</td>
           <td class=fiber weight=60 >Price</td>
           <td class=fiber  style='text-align:right'>Total</td>
         </tr>";

  		//display each item as a table row

		reset($papercart);

  		
        foreach ($papercart as $n => $row)
  		{
			   echo "<tr><td class=lineitem width=100>&nbsp;";
         // if we allow changes, quantities are in text boxes
         if ($change == true)
         {
            echo "<input type = hidden name = 'rownum[]' value = '$n' >";
            echo "<input type = text name = 'newqty[]' value ='".$row['qty']."' size = 3>";
         }
         else
            echo $row['qty']." </td>";
            
			   echo "<td class=lineitem>".$row["description"]."</td>";
			   //Don't show $0 add-on proce if /CP   8/20/04 refa
			   $add_on ='';

			   if ($row['surfaceprice']+1.00 > 1.00)
            $add_on = ' +$'.$row['surfaceprice'];

			   echo "<td class=lineitem colspan=2>[".$row['item']."]&nbsp;&nbsp;
		       ".$row['surface'].' '.$add_on."</td>";
   		   echo "<td class=lineitem >$".number_format($row['price']+$row['surfaceprice'],2)."</td>";
			   echo "<td class=lineitem style='text-align:right'>$".number_format($row['totalamount'],2)."</td>";
			   echo "</tr>";
   		}
/*
		$pulp_action = '';
		if ($edit=='edit') //called from checkout..
			$pulp_action = "<a href=tr_pulp.php>Edit Pulp Order</a>";
		elseif ($edit=='')
			$pulp_action = "<a href=tr_pulp.php?clearpulp=Y>Clear Pulp Order</a>";
*/
			
		echo "<tr>
          <td class=lineitem>Ship ".number_format($_SESSION["paper_items"],0)." sheets</td>
    		  <td class=lineitem colspan=2 >&nbsp;</td>
          <td class=lineitem>&nbsp;</td>
          <td class=lineitem>&nbsp</td>
          <td class=subtotal style='text-align:right'>\$".number_format($_SESSION["paper_price"],2)."</td>
        </tr>";
  // display save change button
  if($change == true)
  {
    // Next button (Recalculate)was "Save Changes"
    echo "<tr>
              <td class=lineitem colspan = ". (6+$images) ."  align = left>
              <input type = hidden name = save value = true>
			     <input type = hidden name = catid value =". $catid.">
              <input type = submit value = 'Recalculate' STYLE='font:8pt ARIAL;background:#c6eff7'>
              <font color=red>(Change paper quantities, or set to zero to delete an item, then press 'Recalculate'.</font>)
            </td>
            </tr>";
  }
  echo "</form></table>";
  return true;
}

function get_pulp_shipping()
{

	//fetch the row
    $conn = db_connect();
    $query = "select state,pulpshipcharge from shipping where state!= '==' order by state";   
	//echo $query; 
    $result = @mysqli_query($conn, $query);
    if (!$result)
     return false;
    $num_recs = @mysqli_num_rows($result);
	$value = db_result_to_array($result);
	
	return $value;
}

function encryptx($data,$subkey = ""){
$key = $GLOBALS['cryptkey'] . $subkey;
$iv = substr(md5($key), 0, mcrypt_get_iv_size (MCRYPT_3DES,
MCRYPT_MODE_ECB));
$output = mcrypt_encrypt (MCRYPT_3DES, $key, $data, MCRYPT_MODE_ECB, $iv);
$output = trim(chop(base64_encode($output)));
return $output;
}

/*
function decryptx($data,$subkey = ""){
$key = $GLOBALS['cryptkey'] . $subkey;
$data = base64_decode($data);
$iv = substr(md5($key), 0, mcrypt_get_iv_size(MCRYPT_3DES,MCRYPT_MODE_ECB));
$output = mcrypt_decrypt (MCRYPT_3DES, $key, $data, MCRYPT_MODE_ECB, $iv);
$output = trim($output);
return $output;
}
*/


// 7/10/2014  -ref- fns below relocated from book_fns.php 

function get_invt_details($item)
{
  // query database for all details for a particular invt item
  if (!$item || $item=="")
     return false;

   $conn = db_connect();
   $query = "select * from invt where item='$item'";
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   $result = @mysqli_fetch_array($result);
   return $result;
}

function get_papercolorcodes()
{
	
   // query database for a list 
   $conn = db_connect();
   //echo "conn=".$conn;
   $query = "select DISTINCT colorgroup
             from PAPER
			 WHERE colorgroup<>''
			 order by colorgroup"; 
   $result = @mysqli_query($conn, $query);
   if (!$result)
      return false;
   $num_cats = @mysqli_num_rows($result);
   if ($num_cats ==0)
      return false;  
   $result = db_result_to_array($result);
   
   $arraysize = sizeof($result);
      
   return $result; 
}

function get_stationerycolorcodes()
{
	
   // query database for a list 
   $conn = db_connect();
   //echo "conn=".$conn;
   $query = "select DISTINCT colorgroup
             from stationery
			 WHERE colorgroup<>''
			 order by colorgroup"; 
   $result = @mysqli_query($conn, $query);
   if (!$result)
      return false;
   $num_cats = @mysqli_num_rows($result);
   if ($num_cats ==0)
      return false;  
   $result = db_result_to_array($result);
   
   $arraysize = sizeof($result);
      
   return $result; 
}
function get_paperitems($mfilter)
{
   // query database for a list 
   $conn = db_connect();
   //echo "conn=".$conn;
   $query = "select ITEM,DESCRIP,PRICE,QOH,COLORGROUP,YTDSLS
             from PAPER
			 WHERE ( $mfilter ) 
			 order by ITEM"; 
	
	//echo $query;
	//OR (MID(DESCRIP,0,4)=UCASE(MID(DESCRIP,0,4)) ) to show headers...
	
   $result = @mysqli_query($conn, $query);
   if (!$result)
      return false;
   $num_cats = @mysqli_num_rows($result);
   if ($num_cats ==0)
      return false;  
   $result = db_result_to_array($result);
         
   return $result; 
}

function get_bestpaperitems()
{
   // query database for a list of the 40 best selling papers
   $conn = db_connect();
   //echo "conn=".$conn;
   $query = "select ITEM,DESCRIP,PRICE,QOH,COLORGROUP,YTDSLS
             from PAPER
			 WHERE QOH > 0  
			 order by ITEM  LIMIT 0,40"; 
	
	//echo $query;
	
   $result = @mysqli_query($conn, $query);
   if (!$result)
      return false;
   $num_cats = @mysqli_num_rows($result);
   if ($num_cats ==0)
      return false;  
   $result = db_result_to_array($result);
         
   return $result; 
}



?>
