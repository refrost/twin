<?php  

function get_cc_info($cust,$message='',$pay_phone='the office')    //Payment info.
 {

 Echo "
    <form ACTION='payments.php' method='post' name='form'>
    <input type='hidden' name='ordertoken'  value='".$cust['ordertoken']."'>
    <input type='hidden' name='billtoname'  value='".$cust['billtoname']."'>
     <table width='100%' border='0' cellspacing='0' cellpadding='5' height='22' align='center'>
      <tr>
       <td colspan='4' width='100%'>
       <table border='0' cellspacing='1' cellpadding='2' width='100%' valign='TOP' >
       <tr valign='top' bgcolor='red'>
       <td height='25' width='82%' NOWRAP valign=middle><font color='#FFFFFF' size='2' face='Verdana, Arial, Helvetica, sans-serif'><strong>
           Enter Payment Information</strong></font>
           <td  align=right valign=middle>
                   <img src='graphics/visa.gif' height='20' width='100' alt='[Visa, Mastercard, etc.]'>
               </td> </tr>";
    if (isset($message) && $message) {
	     echo "<tr><td colspan=4><div align='left'><font face='Arial,Verdana' color=red size='2'><b>The following problems occurred:</b><br><br>\n";
	     foreach ($message as $key => $value)
       {
		     echo "* $value <br>\n";
	     }
	     echo "<hr><td></tr>";
    }
               

  echo "<tr>
        <td NOWRAP height='25' colspan='4'><strong><font face='verdana, Arial' size='2' color=red> Billing
            Information:</font></strong><font size='2' face='Arial, Helvetica, sans-serif'> &nbsp;&nbsp;Required fields".red_star()."  -- Press tab or mouse to move between fields.</td></tr>
      </table>

      <table border='0' cellspacing='1' cellpadding='2' width='100%' valign='TOP' >

      <tr> ";

  Echo "
       <td width='10%' height='25' NOWRAP > <div align='right'><font face='Verdana, Arial, Helvetica, sans-serif' size='2'>Customer
            Name: </font>".red_star()."</div></td>
            
        <td height='25' colspan='3' width=90% NOWRAP> <input name='name' type='text' value='".$cust['name']."' >
            <font size='2' face='Arial, Helvetica, sans-serif'>Exact
             name appearing on the credit card</font> </td></tr>
      <tr>
        <td width='10%' height='22' align='right' NOWRAP><font size='2' face='Verdana,Arial, Helvetica, sans-serif'>Email
             Address: </font>".red_star()."</td>
        <td height='22' colspan='3'> <input name='email' type='text' value='".$cust['email']."'></td></tr>
      <tr>
        <td width='10%' height='25' NOWRAP> <div align='right'><font size='2' face='Verdana,Arial, Helvetica, sans-serif'>Billing
             Address: </font>".red_star()." </div></td>
        <td height='25' colspan='3' NOWRAP> <input name='address1' type='text' value='".$cust['address1']."'>
            <font size='2' face='Arial, Helvetica, sans-serif'>Where credit card statement is sent. </font></td></tr>
      <tr>
        <td width='10%' height='25' NOWRAP> <div align='right'><font size='2' face='Verdana,Arial, Helvetica, sans-serif'>City: </font>".red_star()."</div></td>
        <td width='10%' height='25' colspan='3'>
        <input name='city' type='text' value='".$cust['city']."' >
           </td></tr>
     <tr>
        <td height='25' align='right' NOWRAP><font face='verdana, Arial' size='2'>State/Province: </font>".red_star()."</td>
          <td height='25' colspan='3'><input name='state' type='text' value='".$cust['state']."' ></td></tr>
     <tr>
        <td width='10%' height='25' NOWRAP> <div align='right'><font face='verdana, Arial' size='2' >Postal/Zipcode: </font>".red_star()."</div></td>
        <td height='25' colspan='3'> <input name='zip' size='10' maxlength='10' value='".$cust['zip']."'></td></tr>
     <tr>
        <td height='25' align='right' NOWRAP><font face='verdana, Arial' size='2'>Country: </font></td>

        <td height='25' colspan='3'> <select name='country' size='1' maxlength='2' value=''>
         ".get_country2()."
        </select>

        </td></tr>

     <tr>
        <td  height='25' align='right' NOWRAP><font face='verdana, Arial' size='2'>Country Phone Code: </font></td>

        <td height='25' colspan='3' >
        <select name='country_code' size='1' maxlength='10' value=''>
        ".get_country_areacode()."
        </select></td></tr>
     <tr>
        <td width='10%' height='25' align='right' NOWRAP><font face='verdana, Arial' size='2'>Telephone
           Number: </font>".red_star()."</td>
        <td height='25' colspan='3' >";
        //<input name='phone' size='28' maxlength='30' value='".$cust['phone']."'>
    echo "
        <input class=pay_smtxt type=text size=3 maxlength=3 name=phn_area value='".stripslashes($cust["phn_area"])."'>
				<input class=pay_smtxt type=text size=3 maxlength=3 name=phn_prefix value='".stripslashes($cust["phn_prefix"])."'>
				<input class=pay_smtxt type=text size=4 maxlength=4 name=phn_suffix value='".stripslashes($cust["phn_suffix"])."'  >

     </td></tr></table>

     <table border=0 > <tr>


        <td width='10%' height='40' NOWRAP><font  face='verdana, Arial' size='2' color=red><b>Payment Method
           : </b></font>".red_star()."</td>
        <td class=pay_smtxt height='40'  align=center>
           <a href=  class=cartmenu>Info</a>
           <input type=radio class=pay_smtxt name='method' value='cconline'";
           if ($cust['method']=='cconline')
            echo ' CHECKED';
           echo ">Pay On-line Now<br>Fill in below.</td>
        <td class=pay_smtxt height='40'  align=center>
           <input type=radio class=pay_smtxt name='method' value='ccphone'";
           if ($cust['method']=='ccphone')
            echo ' CHECKED';
           echo " >Institutional PO (see below) <br>or pay by phone (call $pay_phone) <br>Skip to order name field
           </td>
        <td class=pay_smtxt height='40'  align=center>
          &nbsp;";
          
         // <!--   Only have two options: pay on line or phone it in.
         //<input type=radio class=pay_smtxt name=method value=ccfile  >Prior Agreement<br>Skip to Commit Button  -->
 Echo "</td></tr></table>

     <table border=0 >
     <tr>
        <td height='25' align='right' NOWRAP><font face='verdana, Arial' size='2' >&nbsp</font></td>
        <td height='25' colspan='3'><font face='verdana, Arial' size='2' color='red'> We Accept Visa, Mastercard, Discover and American Express.</font>";

        /* Credit card type :

           <select name='cctype' value=''>
           <option value='mcd'>Mastercard</option>
           <option value='vis'>Visa</option>
           <option value='dis'>Discover</option>
           <option value='amx'>American Express</option>
          </select>
        */
 echo " </td></tr>
     <tr>
        <td height='25' align='right' NOWRAP><font face='verdana, Arial' size='2' >Credit
          Card number: </font>".red_star()."</td>
        <td height='25' colspan='3'><input name='ccnum' size='23' maxlength='20' value='".$cust['ccnum']."'>
        <font size='2' face='Arial, Helvetica, sans-serif'>(Enter number with or without spaces)</font></td></tr>
     <tr>
         <td height='25' align='right' NOWRAP><font face='verdana, Arial' size='2' >Expiration
             date: </font>".red_star()."</td>
         <td height='25' colspan='3'><select class=pay_smtxt name='ccmonth' size='1'>";
                          for ($n=1; $n < 13; $n++)
                          {
                            if ($n < 10)
                              $k = '0'.$n;
                            else
                              $k = ''.$n;
                            echo "<option value='$k'";
                            if ($cust['ccmonth']==$k)
                              echo ' SELECTED ';
                            echo ">$k</option>";
                            
                       }
                    echo "  </select>
                            /
                            <select class=pay_smtxt name='ccyear' size='1'>";
         
                        // ccyear : Start at prior year, show 10 years...  6/2010 -ref-
                        // Generalized to work next century
                        $y= strftime("%y");              //Year (two-digit)
                        $c=substr(strftime("%Y"),0,2);   //Century
                        $start = intval($y);    
                        $stop = intval($y)+10;
                        for ($n=$start; $n < $stop; $n++)
                        {
                            if ($n < 10)
                              $k = '0'.$n;
                            else
                              $k = ''.$n;
                            echo "<option value='$k'";
                            if ($cust['ccyear']==$k)
                              echo ' SELECTED ';
                            echo ">$c".$k."</option>";
                           //   <option value='04'>2004</option>
                           
                        }
                     echo "</select></td>
                        </tr>
  <tr>
     <td height='25' align='right' NOWRAP><font face='verdana, Arial' size='2' >CVV2: &nbsp;</font></td>
     <td height='25' colspan='3'><input type=text name='cvv2' size='6'  maxlength='4' value='".$cust['cvv2']."'>
        <font size='2' face='Arial, Helvetica, sans-serif'>If available, please include this 3 or 4 digit code. (Optional)
        </font></td></tr>
  <tr>
     <td class=lineitem>&nbsp;&nbsp <a class=cartmenu href='showprod.php?dept=MyMenu&grp=Company&prod=Terms'>Terms & Conditions</a>
        </td>
     <td align=right colspan='3' valign=top> <p class=greyboxright>
       <img src='graphics/visacvv.gif' width=145 height=60>Last 3 digits&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;

       <img src='graphics/amexcvv.gif' width=122 height=65>Four digits</td>
       </p>
  </tr>   </table>

     <table border=0 >

  <tr>


        <td width='10%' height='40' colspan=2 NOWRAP><font  face='verdana, Arial' size='2'><b>Order name, description or Purchase Order (PO) number:
            </b> </font></td></tr>
  <tr>

        <td valign=top> <input name='order_name' type='text' value='".$cust['order_name']."' size='30'>
        </td>
        <td height='25'  >
            <font size='2'  face='Arial, Helvetica, sans-serif'>

                   Enter an institutional purchase number and/or a project name,
                   class, or other descriptive phrase that relates to this specific order.
                   Let this phrase serve as a helpful reminder when when you re-call your orders in the future.
                   You can view your prior orders by accessing <em><b>MyMenu</b></em> and then <em><b>MyOrderHistory</b></em> (Optional)
                   <br><br>

            </font>
            </td></tr>

  <tr>


        <td width='10%' height='40' colspan=2 NOWRAP><font  face='verdana, Arial' size='2'><b>Comments or Special Instructions for this order (Optional; upto 250 characters):
            </b> </font></td></tr>
  <tr>

        <td valign=top colspan=4>
          <input type=text name='order_comment' value='".trim($cust['order_comment'])."' size=100 maxlength=250><br><br>
        </td>
        </tr>



            <tr>
            <td class=lineitem height='27' colspan='4' align='right'>";
            
       echo " <div align='center'>
               <input  name='submit' type='submit'
                 value='Commit order now via this secure connection' >
              </div>

              	        </td>
                        </tr>
                      </table>
              </form>
  ";

	return true;
	}




//Load these functions only with screens that get customer information
Function get_country()
{
//Giant list of all countries returns two char code
$mret = "
<option value='US' selected>United States</option> <option value='AF'>Afghanistan</option><option value='AL'>Albania</option> <option value='DZ'>Algeria</option>  <option value='AS'>American Somoa</option> <option value='AD'>Andorra</option>
<option value='AO'>Angola</option> <option value='AI'>Anguilla</option> <option value='AQ'>Antartica</option> <option value='AG'>Antigua and Barbuda</option> <option value='AR'>Argentina</option>   <option value='AM'>Armenia</option>
<option value='AW'>Aruba</option><option value='AU'>Australia</option> <option value='AT'>Austria</option><option value='AZ'>Azerbaijan</option><option value='BS'>Bahamas</option><option value='BH'>Bahrain</option>
<option value='BD'>Bangladesh</option><option value='BB'>Barbados</option><option value='BY'>Belarus</option><option value='BE'>Belgium</option><option value='BZ'>Belize</option><option value='BJ'>Benin</option><option value='BM'>Bermuda</option>
<option value='BT'>Bhutan</option><option value='BO'>Bolivia</option><option value='BA'>Bosnia and Herzegovina</option><option value='BW'>Botswana</option><option value='BV'>Bouvet Islands</option><option value='BR'>Brazil</option>
<option value='IO'>British Indian Ocean Territory</option><option value='BN'>Brunei</option><option value='BG'>Bulgaria</option><option value='BF'>Burkina Faso</option><option value='BI'>Burundi</option><option value='KH'>Cambodia</option>
<option value='CM'>Cameroon</option><option value='CA'>Canada</option><option value='CV'>Cape Verde</option><option value='KY'>Cayman Islands</option><option value='CF'>Central African Republic</option><option value='TD'>Chad</option>
<option value='CL'>Chile</option><option value='CN'>China</option><option value='CX'>Christmas Islands</option><option value='CC'>Cocos (Keeling) Islands</option><option value='CO'>Colombia</option><option value='KM'>Comoros</option>
<option value='CG'>Congo</option><option value='CD'>Congo, Democratic Republic of</option><option value='CK'>Cook Island</option><option value='CR'>Costa Rica</option><option value='CI'>Cote Divoire</option><option value='HR'>Croatia</option>
<option value='CY'>Cyprus</option><option value='CZ'>Czech Republic</option><option value='DK'>Denmark</option><option value='DJ'>Djibouti</option><option value='DM'>Dominica</option><option value='DO'>Dominican Republic</option>
<option value='TP'>East Timor</option><option value='EG'>Egypt</option><option value='SV'>El Salvador</option><option value='EC'>Equador</option><option value='GC'>Equatorial Guinea</option><option value='ER'>Eritrea</option>
<option value='EE'>Estonia</option><option value='ET'>Ethiopia</option><option value='FK'>Falkland Islands</option><option value='FO'>Faroe Islands</option><option value='FM'>Federated States of Micronesia</option><option value='FJ'>Fiji</option>
<option value='FI'>Finland</option><option value='FR'>France</option><option value='GF'>French Guiana</option><option value='PF'>French Polynesia</option><option value='TF'>French Southern Territories</option><option value='GA'>Gabon</option>
<option value='GM'>Gambia</option><option value='GE'>Georgia</option><option value='DE'>Germany</option><option value='GH'>Ghana</option><option value='GI'>Gibraltar</option><option value='GR'>Greece</option><option value='GL'>Greenland</option>
<option value='GD'>Grenada</option><option value='GP'>Guadeloupe</option><option value='GU'>Guam</option><option value='GT'>Guatemala</option><option value='GN'>Guinea</option><option value='GW'>Guinea-Bissau</option>
<option value='GY'>Guyana</option><option value='HT'>Haiti</option><option value='HM'>Heard and Macdonald Islands</option><option value='HN'>Honduras</option><option value='HK'>Hong Kong</option><option value='HU'>Hungary</option>
<option value='IS'>Iceland</option><option value='IN'>India</option><option value='ID'>Indonesia</option><option value='IR'>Iran</option><option value='IE'>Ireland</option><option value='IL'>Israel</option><option value='IT'>Italy</option>
<option value='JM'>Jamaica</option><option value='JP'>Japan</option><option value='JO'>Jordan</option><option value='KZ'>Kazakhstan</option><option value='KE'>Kenya</option><option value='KI'>Kiribati</option>
<option value='KP'>Korea, North</option><option value='KR'>Korea, South</option><option value='KW'>Kuwait</option><option value='KG'>Kyrgyzstan</option><option value='LA'>Laos</option><option value='LV'>Latvia</option>
<option value='LB'>Lebanon</option><option value='LS'>Lesotho</option><option value='LR'>Liberia</option><option value='LI'>Liechtenstein</option><option value='LT'>Lithuania</option><option value='LU'>Luxembourg</option>
<option value='MO'>Macau</option><option value='MK'>Macedonia (Rep. of Fmr Yugoslav)</option><option value='MG'>Madagascar</option><option value='MW'>Malawi</option><option value='MY'>Malaysia</option><option value='MV'>Maldives</option>
<option value='ML'>Mali</option><option value='MT'>Malta</option><option value='MH'>Marshall Islands</option><option value='MQ'>Martinique</option><option value='MR'>Mauritania</option><option value='MU'>Mauritius</option>
<option value='YT'>Mayotte</option><option value='FX'>Metropolitan France</option><option value='MX'>Mexico</option><option value='MD'>Moldova</option><option value='MC'>Monaco</option><option value='MN'>Mongolia</option>
<option value='MS'>Montserrat</option><option value='MA'>Morocco</option><option value='MZ'>Mozambique</option><option value='MM'>Myanmar</option><option value='NA'>Namibia</option><option value='NR'>Nauru</option>
<option value='NP'>Nepal</option><option value='NL'>Netherlands</option><option value='AN'>Netherlands Antilles</option><option value='NC'>New Caledonia</option><option value='NZ'>New Zealand</option><option value='NI'>Nicaragua</option>
<option value='NU'>Nieu</option><option value='NE'>Niger</option><option value='NG'>Nigeria</option><option value='NF'>Norfolk Island</option><option value='MP'>Northern Mariana Islands</option><option value='NO'>Norway</option>
<option value='OM'>Oman</option><option value='PK'>Pakistan</option><option value='PW'>Palau</option><option value='PS'>Palestinian Territory, Occupied</option><option value='PA'>Panama</option><option value='PG'>Papua New Guinea</option>
<option value='PY'>Paraguay</option><option value='PE'>Peru</option><option value='PH'>Philippines</option><option value='PN'>Pitcairn</option><option value='PL'>Poland</option><option value='PT'>Portugal</option><option value='PR'>Puerto Rico</option>
<option value='QA'>Qatar</option><option value='RE'>Reunion</option><option value='RO'>Romania</option><option value='RU'>Russia</option><option value='RW'>Rwanda</option><option value='GS'>S. Georgia and S. Sandwich Islands</option>
<option value='WS'>Samoa</option><option value='SM'>San Marino</option><option value='ST'>Sao Tome and Principe</option><option value='SA'>Saudi Arabia</option><option value='SN'>Senegal</option><option value='SC'>Seychelles</option>
<option value='SL'>Sierra Leone</option><option value='SG'>Singapore</option><option value='SK'>Slovakia</option><option value='SI'>Slovenia</option><option value='SB'>Solomon Islands</option><option value='SO'>Somalia</option>
<option value='ZA'>South Africa</option><option value='ES'>Spain</option><option value='LK'>Sri Lanka</option><option value='SH'>St Helena</option><option value='KN'>St Kitts and Nevis</option><option value='LC'>St Lucia</option>
<option value='PM'>St Pierre and Miquelon</option><option value='VC'>St Vincent and the Grenadines</option><option value='SD'>Sudan</option><option value='SR'>Suriname</option><option value='SJ'>Svalbard and Jan Mayen Islands</option>
<option value='SZ'>Swaziland</option><option value='SE'>Sweden</option><option value='CH'>Switzerland</option><option value='SY'>Syria</option><option value='TW'>Taiwan</option><option value='TJ'>Tajikistan</option><option value='TZ'>Tanzania</option>
<option value='TH'>Thailand</option><option value='TG'>Togo</option><option value='TK'>Tokelau</option><option value='TO'>Tonga</option><option value='TT'>Trinidad and Tobago</option><option value='TN'>Tunisia</option><option value='TR'>Turkey</option>
<option value='TM'>Turkmenistan</option><option value='TC'>Turks and Caicos Islands</option><option value='TV'>Tuvalu</option><option value='UG'>Uganda</option><option value='UA'>Ukraine</option><option value='AE'>United Arab Emirates</option>
<option value='GB'>United Kingdom</option><option value='UM'>United States Minor Outlying Islands</option><option value='UY'>Uruguay</option><option value='UZ'>Uzbekistan</option><option value='VU'>Vanuatu</option><option value='VA'>Vatican City</option>
<option value='VE'>Venezuela</option><option value='VN'>Vietnam</option><option value='VG'>Virgin Islands - British</option><option value='VI'>Virgin Islands - US</option><option value='WF'>Wallis and Futuna Islands</option>
<option value='EH'>Western Sahura</option><option value='YE'>Yemen</option><option value='YU'>Yugoslavia</option><option value='ZR'>Zaire</option><option value='ZM'>Zambia</option><option value='ZW'>Zimbabwe</option>
";
return $mret;
}

function get_country_areacode()
{
//Giant list of all country area codes: returns a number from 1 to 3 char long
$mret = "
<option value='1' selected>United States (+ 001)</option><option value='93'>Afghanistan (+ 093)</option><option value='355'>Albania (+ 355)</option><option value='213'>Algeria (+ 213)</option><option value='684'>American Somoa (+ 684)</option>
<option value='376'>Andorra (+ 376)</option><option value='244'>Angola (+ 244)</option><option value='1'>Anguilla (+ 001)</option><option value='672'>Antartica (+ 672)</option><option value='1'>Antigua and Barbuda (+ 001)</option>
<option value='54'>Argentina (+ 054)</option><option value='374'>Armenia (+ 374)</option><option value='297'>Aruba (+ 297)</option><option value='61'>Australia (+ 061)</option><option value='43'>Austria (+ 043)</option>
<option value='994'>Azerbaijan (+ 994)</option><option value='1'>Bahamas (+ 001)</option><option value='973'>Bahrain (+ 973)</option><option value='880'>Bangladesh (+ 880)</option><option value='1'>Barbados (+ 001)</option>
<option value='375'>Belarus (+ 375)</option><option value='32'>Belgium (+ 032)</option><option value='501'>Belize (+ 501)</option><option value='229'>Benin (+ 229)</option><option value='1'>Bermuda (+ 001)</option>
<option value='975'>Bhutan (+ 975)</option><option value='591'>Bolivia (+ 591)</option><option value='387'>Bosnia and Herzegovina (+ 387)</option><option value='267'>Botswana (+ 267)</option><option value='47'>Bouvet Islands (+ 047)</option>
<option value='55'>Brazil (+ 055)</option><option value='246'>British Indian Ocean Territory (+ 246)</option><option value='673'>Brunei (+ 673)</option><option value='359'>Bulgaria (+ 359)</option><option value='226'>Burkina Faso (+ 226)</option>
<option value='257'>Burundi (+ 257)</option><option value='855'>Cambodia (+ 855)</option><option value='237'>Cameroon (+ 237)</option><option value='1'>Canada (+ 001)</option><option value='238'>Cape Verde (+ 238)</option>
<option value='1'>Cayman Islands (+ 001)</option><option value='236'>Central African Republic (+ 236)</option><option value='235'>Chad (+ 235)</option><option value='56'>Chile (+ 056)</option><option value='86'>China (+ 086)</option>
<option value='618'>Christmas Islands (+ 618)</option><option value='61'>Cocos (Keeling) Islands (+ 061)</option><option value='57'>Colombia (+ 057)</option><option value='269'>Comoros (+ 269)</option><option value='242'>Congo (+ 242)</option>
<option value='243'>Congo, Democratic Republic of (+ 243)</option><option value='682'>Cook Island (+ 682)</option><option value='506'>Costa Rica (+ 506)</option><option value='225'>Cote Divoire (+ 225)</option><option value='385'>Croatia (+ 385)</option>
<option value='357'>Cyprus (+ 357)</option><option value='420'>Czech Republic (+ 420)</option><option value='45'>Denmark (+ 045)</option><option value='253'>Djibouti (+ 253)</option><option value='1'>Dominica (+ 001)</option>
<option value='1'>Dominican Republic (+ 001)</option><option value='670'>East Timor (+ 670)</option><option value='20'>Egypt (+ 020)</option><option value='503'>El Salvador (+ 503)</option><option value='593'>Equador (+ 593)</option>
<option value='240'>Equatorial Guinea (+ 240)</option><option value='291'>Eritrea (+ 291)</option><option value='372'>Estonia (+ 372)</option><option value='251'>Ethiopia (+ 251)</option><option value='500'>Falkland Islands (+ 500)</option>
<option value='298'>Faroe Islands (+ 298)</option><option value='691'>Federated States of Micronesia (+ 691)</option><option value='679'>Fiji (+ 679)</option><option value='358'>Finland (+ 358)</option><option value='33'>France (+ 033)</option>
<option value='594'>French Guiana (+ 594)</option><option value='689'>French Polynesia (+ 689)</option><option value='596'>French Southern Territories (+ 596)</option><option value='241'>Gabon (+ 241)</option>
<option value='220'>Gambia (+ 220)</option><option value='995'>Georgia (+ 995)</option><option value='49'>Germany (+ 049)</option><option value='233'>Ghana (+ 233)</option><option value='350'>Gibraltar (+ 350)</option>
<option value='30'>Greece (+ 030)</option><option value='299'>Greenland (+ 299)</option><option value='1'>Grenada (+ 001)</option><option value='590'>Guadeloupe (+ 590)</option><option value='1'>Guam (+ 001)</option>
<option value='502'>Guatemala (+ 502)</option><option value='224'>Guinea (+ 224)</option><option value='245'>Guinea-Bissau (+ 245)</option><option value='592'>Guyana (+ 592)</option><option value='509'>Haiti (+ 509)</option>
<option value='61'>Heard and Macdonald Islands (+ 061)</option><option value='504'>Honduras (+ 504)</option><option value='852'>Hong Kong (+ 852)</option><option value='36'>Hungary (+ 036)</option><option value='354'>Iceland (+ 354)</option>
<option value='91'>India (+ 091)</option><option value='62'>Indonesia (+ 062)</option><option value='98'>Iran (+ 098)</option><option value='353'>Ireland (+ 353)</option><option value='972'>Israel (+ 972)</option>
<option value='39'>Italy (+ 039)</option><option value='1'>Jamaica (+ 001)</option><option value='81'>Japan (+ 081)</option><option value='962'>Jordan (+ 962)</option><option value='7'>Kazakhstan (+ 007)</option>
<option value='254'>Kenya (+ 254)</option><option value='686'>Kiribati (+ 686)</option><option value='850'>Korea, North (+ 850)</option><option value='82'>Korea, South (+ 082)</option><option value='965'>Kuwait (+ 965)</option>
<option value='996'>Kyrgyzstan (+ 996)</option><option value='856'>Laos (+ 856)</option><option value='371'>Latvia (+ 371)</option><option value='961'>Lebanon (+ 961)</option><option value='266'>Lesotho (+ 266)</option>
<option value='231'>Liberia (+ 231)</option><option value='423'>Liechtenstein (+ 423)</option><option value='370'>Lithuania (+ 370)</option><option value='352'>Luxembourg (+ 352)</option><option value='853'>Macau (+ 853)</option>
<option value='381'>Macedonia (Rep. of Fmr Yugoslav) (+ 381)</option><option value='261'>Madagascar (+ 261)</option><option value='265'>Malawi (+ 265)</option><option value='60'>Malaysia (+ 060)</option>
<option value='960'>Maldives (+ 960)</option><option value='269'>Mali (+ 269)</option><option value='356'>Malta (+ 356)</option><option value='692'>Marshall Islands (+ 692)</option><option value='596'>Martinique (+ 596)</option>
<option value='222'>Mauritania (+ 222)</option><option value='230'>Mauritius (+ 230)</option><option value='269'>Mayotte (+ 269)</option><option value='33'>Metropolitan France (+ 033)</option><option value='52'>Mexico (+ 052)</option>
<option value='373'>Moldova (+ 373)</option><option value='377'>Monaco (+ 377)</option><option value='976'>Mongolia (+ 976)</option><option value='1'>Montserrat (+ 001)</option><option value='212'>Morocco (+ 212)</option>
<option value='258'>Mozambique (+ 258)</option><option value='95'>Myanmar (+ 095)</option><option value='264'>Namibia (+ 264)</option><option value='674'>Nauru (+ 674)</option><option value='977'>Nepal (+ 977)</option>
<option value='31'>Netherlands (+ 031)</option><option value='599'>Netherlands Antilles (+ 599)</option><option value='687'>New Caledonia (+ 687)</option><option value='64'>New Zealand (+ 064)</option><option value='505'>Nicaragua (+ 505)</option>
<option value='683'>Nieu (+ 683)</option><option value='227'>Niger (+ 227)</option><option value='234'>Nigeria (+ 234)</option><option value='672'>Norfolk Island (+ 672)</option><option value='1'>Northern Mariana Islands (+ 001)</option>
<option value='47'>Norway (+ 047)</option><option value='968'>Oman (+ 968)</option><option value='92'>Pakistan (+ 092)</option><option value='680'>Palau (+ 680)</option><option value='970'>Palestinian Territory, Occupied (+ 970)</option>
<option value='507'>Panama (+ 507)</option><option value='675'>Papua New Guinea (+ 675)</option><option value='595'>Paraguay (+ 595)</option><option value='51'>Peru (+ 051)</option><option value='63'>Philippines (+ 063)</option>
<option value='872'>Pitcairn (+ 872)</option><option value='48'>Poland (+ 048)</option><option value='351'>Portugal (+ 351)</option><option value='1'>Puerto Rico (+ 001)</option><option value='974'>Qatar (+ 974)</option>
<option value='262'>Reunion (+ 262)</option><option value='40'>Romania (+ 040)</option><option value='7'>Russia (+ 007)</option><option value='250'>Rwanda (+ 250)</option><option value='995'>S. Georgia and S. Sandwich Islands (+ 995)</option>
<option value='684'>Samoa (+ 684)</option><option value='378'>San Marino (+ 378)</option><option value='239'>Sao Tome and Principe (+ 239)</option><option value='966'>Saudi Arabia (+ 966)</option><option value='221'>Senegal (+ 221)</option>
<option value='248'>Seychelles (+ 248)</option><option value='232'>Sierra Leone (+ 232)</option><option value='65'>Singapore (+ 065)</option><option value='421'>Slovakia (+ 421)</option><option value='386'>Slovenia (+ 386)</option>
<option value='677'>Solomon Islands (+ 677)</option><option value='252'>Somalia (+ 252)</option><option value='27'>South Africa (+ 027)</option><option value='34'>Spain (+ 034)</option><option value='94'>Sri Lanka (+ 094)</option>
<option value='290'>St Helena (+ 290)</option><option value='1'>St Kitts and Nevis (+ 001)</option><option value='1'>St Lucia (+ 001)</option><option value='508'>St Pierre and Miquelon (+ 508)</option>
<option value='1'>St Vincent and the Grenadines (+ 001)</option><option value='249'>Sudan (+ 249)</option><option value='597'>Suriname (+ 597)</option><option value='47'>Svalbard and Jan Mayen Islands (+ 047)</option>
<option value='268'>Swaziland (+ 268)</option><option value='46'>Sweden (+ 046)</option><option value='41'>Switzerland (+ 041)</option><option value='963'>Syria (+ 963)</option><option value='886'>Taiwan (+ 886)</option>
<option value='992'>Tajikistan (+ 992)</option><option value='255'>Tanzania (+ 255)</option><option value='66'>Thailand (+ 066)</option><option value='228'>Togo (+ 228)</option><option value='690'>Tokelau (+ 690)</option>
<option value='676'>Tonga (+ 676)</option><option value='1'>Trinidad and Tobago (+ 001)</option><option value='216'>Tunisia (+ 216)</option><option value='90'>Turkey (+ 090)</option><option value='993'>Turkmenistan (+ 993)</option>
<option value='1'>Turks and Caicos Islands (+ 001)</option><option value='688'>Tuvalu (+ 688)</option><option value='256'>Uganda (+ 256)</option><option value='380'>Ukraine (+ 380)</option><option value='971'>United Arab Emirates (+ 971)</option>
<option value='44'>United Kingdom (+ 044)</option><option value='1'>United States Minor Outlying Islands (+ 001)</option><option value='598'>Uruguay (+ 598)</option><option value='998'>Uzbekistan (+ 998)</option>
<option value='678'>Vanuatu (+ 678)</option><option value='39'>Vatican City (+ 039)</option><option value='58'>Venezuela (+ 058)</option><option value='84'>Vietnam (+ 084)</option><option value='1'>Virgin Islands - British (+ 001)</option>
<option value='340'>Virgin Islands - US (+ 340)</option><option value='681'>Wallis and Futuna Islands (+ 681)</option><option value='212'>Western Sahura (+ 212)</option><option value='967'>Yemen (+ 967)</option>
<option value='381'>Yugoslavia (+ 381)</option><option value='243'>Zaire (+ 243)</option><option value='260'>Zambia (+ 260)</option>
<option value='263'>Zimbabwe (+ 263)</option>
";
return $mret;
}

function get_country2()
{
$mret = "
<option value='us'>United States
<option value='australia'>Australia
<option value='afghanistan'>Afghanistan
<option value='albania'>Albania
<option value='algeria'>Algeria
<option value='argentina'>Argentina

<option value='austria'>Austria
<option value='belgium'>Belgium
<option value='brazil'>Brazil
<option value='bulgaria'>Bulgaria
<option value='canada'>Canada
<option value='chile'>Chile
<option value='china'>China
<option value='columbia'>Colombia
<option value='costa_rica'>Costa Rica
<option value='croatia'>Croatia
<option value='denmark'>Denmark
<option value='ecuador'>Ecuador
<option value='egypt'>Egypt
<option value='finland'>Finland
<option value='france'>France
<option value='germany'>Germany
<option value='greece'>Greece

<option value='hong_kong'>Hong Kong
<option value='hungary'>Hungary
<option value='india'>India
<option value='indonesia'>Indonesia
<option value='ireland'>Ireland
<option value='israel'>Israel
<option value='italy'>Italy
<option value='japan'>Japan
<option value='korea'>Korea
<option value='lebanon'>Lebanon
<option value='malaysia'>Malaysia
<option value='mexico'>Mexico
<option value='netherlands'>Netherlands
<option value='new_zealand'>New Zealand
<option value='norway'>Norway
<option value='pakistan'>Pakistan
<option value='peru'>Peru

<option value='philippines'>Philippines
<option value='poland'>Poland
<option value='portugal'>Portugal
<option value='puerto_rico'>Puerto Rico
<option value='romania'>Romania
<option value='russia'>Russia
<option value='saudi_arabia'>Saudi Arabia
<option value='singapore'>Singapore
<option value='south_africa'>South Africa
<option value='spain'>Spain
<option value='sweden'>Sweden
<option value='switzerland'>Switzerland
<option value='taiwan'>Taiwan
<option value='thailand'>Thailand
<option value='turkey'>Turkey
<option value='ukraine'>Ukraine
<option value='united_arab_emirates'>United Arab Emirates

<option value='united_kingdom'>United Kingdom
<option value='us_minor_is'>United States--Minor Outlying Islands
<option value='venezuela'>Venezuela
<option value='yugoslavia'>Yugoslavia
<option value='other'>Other
";
return $mret;
}

function confirm_order()
{
			$message = '
			<html>
			<head>
			 <title>Products Purchased:</title>
			</head>
			<body>
			<p>Customer Information:</p>
			<table>
			 <tr>
			  <td>Name</td>
			  <td>{$name}</td>
			 </tr>
			 <tr>
			  <td>Title</td>
			  <td>.$title.</td>
			 </tr>
			 <tr>
			  <td>Email</td>
			  <td>.$email.</td>
			 </tr>
			 <tr>
			  <td>Company</td>
			  <td>.$company.</td>
			 </tr>
			 <tr>
			  <td>Phone</td>
			  <td>.$phone.</td>
			 </tr>
			 <tr>
			  <td>Fax</td>
			  <td>.$fax.</td>
			 </tr>
			 <tr>
			  <td>Address</td>
			  <td>.$address.</td>
			 </tr>
			 <tr>
			  <td>City</td>
			  <td>.$city.</td>
			 </tr>
			 <tr>
			  <td>State</td>
			  <td>.$state.</td>
			 </tr>
			 <tr>
			  <td>Postal Code</td>
			  <td>.$postalcode.</td>
			 </tr>
			 <tr>
			  <td>Country</td>
			  <td>.$country.</td>
			 </tr>
			 <tr>
			  <td colspan=2>&nbsp;</td>
			 </tr>
			 <tr>
			  <td colspan=2>Billing Information</td>
			 </tr>
			 <tr>
			  <td>Card Type</td>
			  <td>.$methodofpay.</td>
			 </tr>
			 <tr>
			  <td>Card Name</td>
			  <td>.$cardname.</td>
			 </tr>
			 <tr>
			  <td>Exp Date</td>
			  <td>.$cardexp.</td>
			 </tr>
			</table>
			</body>
			</html>
			';
			echo $message;
}