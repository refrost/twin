<?php
//require_once("db_fns.php");

function login($username, $password)
// check username and password with db
// if match, return true
// else return false
{
  $conn = db_connect();
  if (!$conn)
    return false;                 

  // check username then verify password
  $query =  "select id,password from customers where username='$username' ";
  $result = mysqli_query( $conn, $query);

  if ($result) {
      
      $row = mysqli_fetch_array($result);

      if ($row && password_verify($password,$row['password'])) 
      { 
          //echo 'Here '.$row['password'];
          //exit;    
          return true;  //username and password match
      } else
          return false;
  }
  else
     return false;
}

function check_admin_user()
// 
{
  if (isset($_SESSION["SESSION"]) 	&& 	($_SESSION["SESSION"]== 'Admin'))
    return true;
  else
    return false;
}

function check_web_user()
//
{
  if (isset($_SESSION["SESSION"]) 	&& 	($_SESSION["SESSION"]== 'User'))
    return true;
  else
    return false;
}


function change_password($username, $old_password, $new_password)
// change password for username/old_password to new_password
// return true or false
{
  // if the old password is right 
  // change their password to new_password and return true
  // else return false
  if (login($username, $old_password))
  {
    if (!($conn = db_connect()))
      return false;
      
    // 11/1/2020 User's pswd hash already saved via pdo 
    // (w/out mysqli_real_ewcape) so don't use it here
    // since only comparing hash. 
    //$new_password = mysqli_real_escape_string($conn, $new_password);  // added 2/4/2011  5.3 upgrd 7/2014 real $conn
    $new_password = $new_password;  
        
    $hash = password_hash($new_password,PASSWORD_DEFAULT);
    $query =  "update customers set password = '$hash' where username = '$username'";
    
    $result = mysqli_query( $conn, $query);    
    if (!$result)
      return false;  // not changed
    else
      return true;  // changed successfully
  }
  else
  {
    return false; // old password was wrong
  }
}

function reset_password($username)
// set password for username to a random value
// return the new password or false on failure
//5/6/2019 now uses password_hash()
{
  // get a readable password
  $new_password = gen_readable_password();

  // set user's password to this in database or return false
  if (!($conn = db_connect()))
      return false;
      
  $hash =  password_hash($new_password,PASSWORD_DEFAULT);
  $query = "update customers set password = '$hash' where username = '$username'";
  $result = @mysqli_query( $conn, $query);
  // 1/29/2010 Changed to OLD_PASSWORD() 5/6/19 to password_hash()
  if (!$result)
    return false;  // not changed
  else
    return $new_password;  // changed successfully
}


function notify_password($username, $password,$companyurl)
// notify the user that their password has been changed
{
    global $g_emaildomain,$g_payment_phone;
    if (!($conn = db_connect()))
      return false;
    $query = "select email from customers where username='$username'";
    $result = @mysqli_query($conn, $query);     
    
    if (!$result)
      return false;  // not changed
    else if (mysqli_num_rows($result)==0)
      return false; // username not in db
    else
    {
        // Send the email...
    	$row = mysqli_fetch_array($result);
      $email = $row["email"];
      $from = "admin@$g_emaildomain";
      $fromname = 'Webadmin';
      $subject = 'Login information';
      $msg = "Your  password has been changed to $password \r\n"
              ."Please change your password the next time you log in. \r\n"
              ."(Log in -> MyMenu -> MyProfile -> Change password) \r\n\r\n"
                 ."--Web Admin and Support \r\n"
                 .$from."\r\n"
                 .$companyurl."\r\n"
                 .$g_payment_phone;

       // phpmailer 8/29/2015
       include("mail_fns.php");
       $mail = new mailer;

       $mail->SMTPDebug = 0;
       $mail->addReplyTo($from,$fromname);
       //Set -> addReplyTo before ->setFrom
       $mail->setFrom($from,$fromname);
       $mail->Subject = $subject;
       $mail->Body    = $msg;
       $mail->addAddress($email);
      
      //echo "$password  $from  $subject  $msg";
      //exit;  
      if($mail->Send())
        return true;
      else
        return false;
    }
}

