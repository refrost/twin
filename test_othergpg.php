<?php
   /*
   The entire PHP script, to invoke PGP and send encrypted e-mail, will look something like this: 

 
//build the message string
$msg = "Sender's Full Name:\t$sender_name\n";
$msg .= "Sender's E-Mail:\t$sender_email\n";
$msg .= "Secret Message?\t$secret_msg\n\n";
//set the environment variable for PGPPATH
putenv("PGPPATH=/home/www/.pgp");

//generate token for unique filenames
$tmpToken = md5(uniqid(rand()));

//create vars to hold paths and filenames
$plainTxt = "/home/www/" . "$tmpToken" . "data";
$crypted = "/home/www/" . "$tmpToken" . "pgpdata";

//open file and dump in plaintext contents
$fp = fopen($plainTxt, "w+"); 
fputs($fp, $msg); 
fclose($fp);

//invoke PGP to encrypt file contents
system("/usr/local/bin/pgpe -r 'Julie Meloni <julie@thickbook.com>' -o $crypted -a $plainTxt");

//open file and read encrypted contents into var
$fd = fopen($crypted, "r");
$mail_cont = fread($fd, filesize($crypted));
fclose($fd);

//delete files!
unlink($plainTxt); 
unlink($crypted); 

// Build mail message and send it to target recipient.
$recipient = "julie@thickbook.com";
$subject = "Secret Message";

$mailheaders = "From: My Web Site\n";
$mailheaders .= "Reply-To: $sender_email\n\n";

mail("$recipient", "$subject", $mail_cont, $mailheaders);

// Print confirmation to screen.
echo "
<H1 align=center>Thank You, $sender_name</h1>
<p align=center>Your secret message has been sent.</p>
"; 


 
The target recipient should receive an email that looks something like this: 

-----BEGIN PGP MESSAGE-----
Version: PGP for Personal Privacy 5.0
MessageID: ruVxzGiwNH9hJMbhAt5mEfUmWtozI3/4
qANQR1DBwk4DJoa8eforpr0QDAC2QHWlBYRTGKepMDcFXqyO1vrXlTh1p7yB0Wo4
lqYoZ9FippFCJddmLbZkvBRpEBceODLg+gEf5hrtXl3b5NO7Q6xUyMiPnF/71M9v
hdUvhJD/2gPnVZmq5qm0HhrjYLwQv9/2+z3sRN70NqohaWMjMR7kTcCAus/eZGS0
7ZWbyWc8x1c6qWU8EyDIw1nqfF62s4WTixx6BCve5m0A4xUXHncWZhDLvC/a47x5
C6oX5C7Dv+KpyROl++1aPOfFfZ+38fEZC5+E4IgPVYvxyLRVgDoeJbZ7QlDyxkVh
6Qe/bTI9CpP5kAb6uxCywgpaecj6P8ABg4ONR6xu0uYC1lG9UhNg5a/KzNyJNpTe
SYPx+1jS1q5285v3kF4ptDFQdLML/i4LTx9oE68WksAzVaqBE/zVRmGQaaczf+gx
fWjQZGbU4l11TImfpO16vZ21CkbobD6AZOaG0B3a1df4GqQwIK3Jf+hWWnXNQ514
DUWzW5puY6VHiEb60cztvds0/KsHNVZUAQ4VYm4R+Ahyb0M44MS4UpzILbH3HbV9
GpvfxeIZ/aUNOOSpJjhn8hzjEALZP1habVZKFJV6sVRtnCadoSV2gSQNQg0kfEY1
vxmbvxBAQRDga6CS8oHaFb45LJo2gVlLhiShDdp2eDR8X4KtUA6MdtO66w56qAy0
UWNKKwsqv0UP3jMHvQl1eb0rHWayxMmSfYw69zo56CjnUWNJN6Rh0y1g54Bl0afA
bt2D7FkQwGNPSm7e5HGcMVLocQ/XV5NTUOmX+s2DqvFT4h9/bAoBkxmR8hf2C7el
v8AJ4Ya/6D139cogzfDQtCy4Bo07vqz79lXfQYSGmk5f2c4LqvtKPhmDAWmSgUsW
-----END PGP MESSAGE----- 
Recipients will then be able to decrypt the message using the PGP tools installed on their own machines.

Using GnuPG 

If you don't want to use PGP encryption, or if you are in a country to which PGP cannot be exported, you can use the open source GnuPG software and substitute the following in the generic script above:

Substitute the environment variable: 

putenv("GNUPGHOME=/home/www/.gnupg"); 
Substitute the system() call: 

system("/path/to/gpg --encrypt -ao $crypted -r 'Julie Meloni <julie@thickbook.com>' $plainTxt"); 
The encrypted message will look quite similar, something like: 

*/
   //----------------
  
      $oldhome = getenv("GNUPGHOME");
      //echo $oldhome;
  
      putenv("GNUPGHOME=/home/twinrocker/.gnupg");
 

  $gpg = '/usr/bin/gpg';                                             # path to gpg - yours may differ
  $recipient = 'Brookston';
  $order = 'this is our big order';                                                   # make a composite of your order information
  $tmpfile = '/home/twinrocker/tmp/2345'; //xx_' . md5(uniqid(time()));  # temp file to encrypt to
  $cmd = "$gpg --homedir /home/twinrocker/.gnupg --no-default-keyring
        --always-trust --no-secmem-warning -e -a --batch -t -r $recipient -o $tmpfile";

  $fp = popen($cmd, 'w');                       # open a new process to gpg
  fwrite($fp, $order); 
  //fflush($fp);       
  pclose($fp);                # write the command to encrypt our order to the gpg process
                                   # close it

  $fp1 = fopen($tmpfile, 'r');                   # open the temp file
  $enc_order = fread($fp1, filesize($tmpfile));  # read the encrypted contents
  fclose($fp1);                                  # close it
  
  pclose($fp);
  unlink($tmpfile);                             # delete the temp file

  mail('refrost@isp.com', 'New order!', $enc_order);   # mail it to your account
   
   
   
?> 
