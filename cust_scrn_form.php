<?php
//php5.3  5/9/2015 some changes to init shipid  and ship_state
if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost" )
  {
  	// working locally so don't do anything
  }
else
  {
	// 8/5/21 cmtted too many redirects
    //if ($_SERVER["SERVER_PORT"] != "443")
  	//	header("location:https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
  }

require_once("book_sc_fns.php"); 
session_start();

// This routine is called from the customer.php script [lookup by zip, lastname, 
// acct #, etc]with POST, and otherwise from forms created here which lists multiple 
// records where the user selects ONE from the list.

$goto = '';  //12/13/03 redirect back to final review...

if (isset($_POST["cust_action"]))
	$cust_action = $_POST["cust_action"];

if (isset($_GET["cust_action"]))
	$cust_action = $_GET["cust_action"];

if (isset($_GET["goto"]))
	$goto = $_GET["goto"];


if (isset($_POST["this_id"]))
	$cust_id = $_POST["this_id"];
else
{
	//was not passed from form so get id from session var
	// 8/25/03 changed from UID to UACCT   (webid)
	if (isset($_GET["this_id"]))
			$cust_id = $_GET["this_id"];
	else
	{
		if (isset($_SESSION["SESSION_UACCT"]))
   		$cust_id = $_SESSION["SESSION_UACCT"];
	}

}

// $cust_id must be webid...

// Added: isset($cust_id)  &&  5/27/18...
if (isset($cust_id) && ($cust = get_custarray($cust_id)))   //8.25.03 Add 'id' to force getting by id and not webid
{
    //If logged in as Admin
    //OR if the webid is the same as one for this user..
    //then we have some extra steps
	if (check_admin_user() || $cust_id == $_SESSION["SESSION_UACCT"])
	{	
	  if (check_admin_user())	
	  {
	  
	  // If Admin is working multiple orders  then we want to shift the User session vars 
	  // over to the selected person.  Following code is also in authenticate.php  4/12/02
	  // If current (web)customer gets here, this info is already set.
	  
		// State the Firstname+lastname
		$this_name = stripslashes($cust["firstname"]).'&nbsp;'.stripslashes($cust["lastname"]);
      $_SESSION["SESSION_UNAME"] = $this_name;
    
		// Fix user record ID
		$_SESSION["SESSION_UID"] = $cust["id"];
				
		// include the webid  //was: custno//
		$webid = stripslashes($cust["webid"]);  // 8/25/03 was custno
      $_SESSION["SESSION_UACCT"]   = $webid;

     
     }  //6/17/2010 new endblock re: ship_state change of just the user
	
     // get ship_state, too
	  if (isset($cust["state"]))
	  {
		 if (isset($_SESSION["SESSION_SHIPID"]) &&$_SESSION["SESSION_SHIPID"]== -1) // 6/10/2010 fixing ship_state
			$ship_state= stripslashes($cust["state"]);
        else  // 5/9/2015 added init to elimin warning
            $ship_state= '==';
	  }
     else
			$ship_state= '==';

   // } prior endblock for check_admin 6/17/2010 problem in ship_state change
   
	$_SESSION["ship_state"] = $ship_state;
			
	//now go do the selected operation: 
	
	switch ($cust_action)
		{
			case 'display' :
			{
				//do_html_header("Profile&nbsp;","home");
                $jscript ='moveonmax'; // 06/24/2015
				include('tr_header.php');
				display_cust_form($cust,$g_table1_width,$goto);
                silk_footer($g_table1_align,$g_table1_width);
				exit;
			}
			case 'continue' :
			{
				// vars are set now go display the message.
				pass_msg('Working with user:  '.$this_name.' : '.stripslashes($cust["name"]) );
				exit;
			}
			case 'checkout' :
			{
				// vars are set now go to checkout page...
				
				header("location:tr_checkout.php");
				exit;
			}
			case 'edit' :
			{
				//vars are set, now call this page again with action = display
				header("location:cust_scrn_form.php?cust_action=display&this_id=".$cust_id);
				exit;
			}
			case 'history' :
			{
				//show browse of all orders with detail and checkbox to select as current order...
			}
				
		}
  	}
	else
		echo "Unable to access record in Logon";
		
 
}
else
{
    echo "Could not retrieve profile.<br>";
    if (isset($cust_id)) //prevent undefined var warning
        echo 'cust_id='.$cust_id;
}
  
//do_html_header("View/Edit Profile","home");

// echo "<br><br><br>You are not authorized to enter the administration area.<br><br><br>";

//do_html_footer();

?>
