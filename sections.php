<?php
//sections.php  sets sessions variable for the current product section
// supply | stationery | paper | ...   and then switches to that section's main page
// php 5.3
session_start();
include("book_sc_fns.php");

if (isset($_GET['next']))
  $section = $_GET['next'];
else
  $section = 'Supply';
  
if (isset($_GET['anchor']))
  $anchor = $_GET['anchor'];
else
  $anchor = '';

if (!isset($_SESSION['ship_state']))  //No Session vars set, so..
  include('set_vars.php');

//_unregister("SECTION_NAME");
//_unregister("SECTION_HEADER");  //Defined in TR in tr_fns.php - display_header()
//_unregister("SECTION_MAINFILE");

switch ($section)
{
  case 'supply' :
    $name = $section;
    $header = 'Twinrocker Papermaking Supplies';
    $mainfile = 'showprod.php';
    break;
  case 'stationery' :
    $name = $section;
    $header = 'Twinrocker Handmade Stationery';
    $mainfile = 'st_main.php';
    break;
  case 'paper' :
    $name = $section;
    $header = 'Twinrocker Handmade Paper';
    $mainfile = 'pa_main.php';
   break;
}
if ($anchor)
  $mainfile .= "#$anchor";
  
$_SESSION['SECTION_NAME'] = $name;
$_SESSION['SECTION_HEADER'] = $header;
$_SESSION['SECTION_MAINFILE'] = $mainfile;

//echo $g_companyurl.'/'.$mainfile;exit;

header("location:$mainfile");
exit;
?>
