<?php
  // 5/6/2019 twin converted to use pdo w/ prep statements and to password_hash()...
  // changed password hash field to 255 vchar and if < 60, is first verified via old_pswd() first and then hashed() and saved
  // Other changes in register.php, etc...
  include ('book_sc_fns.php');
  $pdo = pdo_connect();  // create pdo connect object in db_fns.php
  
  	//called from ...weblogon() in action.fns
	session_start();
    /* Migrate admin to new has  7/18/2012*/
	if (isset($_POST['f_user']))
		$f_user = $_POST['f_user'];
	else
		$f_user = '';
	if (isset($_POST['f_pass']))
		$f_pass = $_POST['f_pass'];
	else
		$f_pass = '';
	
	if (isset($_POST['gotouri']))
		$gotouri = $_POST['gotouri'];
	else
		$gotouri = ''; 
	
	$submit = trim($_POST['submit']);

	switch ($submit)
	{
		case "New User";
        // 4/25/2017 for New User, changed from
        // authenticate.php to register.php directly.
        // changed in weblogin() in action_fns.php
      		header("location:register.php");
			exit();
			break;
		
		case "Forgot password?";
			display_forgot_password_form();
			break;
		
		case "Log In";
		{
			// 5/6/2019 authenticate using form variables, return staus and rec_id
			list($status,$rec_id) = authenticate($f_user, $f_pass);

			if ($status == 1)	// ie, found in adminusers
			{
				// register some session variables
				$_SESSION["SESSION"] = "Admin";   // user with regular users.. 

				// including the username
				$_SESSION["SESSION_ADNAME"] = $f_user;
	
					$message = urlencode ("Welcome, $f_user - Admin login successful.".msgcontinue());
					header ("Location: message.php?cntrl=1&message=$message"); // Send them on their way.
					exit;
		
			} // Endif $status==1
			
			if ($status == 2)	// found in users
			{
				// This ~same code is replicated in  Register.php  to  auto log in New Uers 7/9/03 
				// (Avoids passing via _GET...)
				
				// register some session variables
				$_SESSION["SESSION"] = "User";   // user with regular users.. 

				$conn = db_connect();
                // retrieve by rec_id  5/6/2019
				$query = "SELECT id,firstname,lastname,custno,state,webid,logins from customers WHERE username = '$f_user' AND id = $rec_id";
				//OLD $query = "SELECT id,firstname,lastname,custno,state,webid,logins from customers WHERE username = '$f_user' AND password = OLD_PASSWORD('$f_pass')";
                $result = mysqli_query( $conn, $query) or die ("Error in query. " );
				$row = mysqli_fetch_array($result);
		
				// State the Firstname+lastname
				$_SESSION["SESSION_UNAME"] = $row["firstname"].'&nbsp;'.$row["lastname"];
		
				// Fix user record ID
				$_SESSION["SESSION_UID"] = $row["id"];
				
				// include the custno
				$_SESSION["SESSION_UACCT"] = $row["webid"];

				// get ship_state, too
				$_SESSION["ship_state"] = $row["state"];
				
				//update_login_count($row['logins'],$row['webid']);

	            $message = urlencode ("Welcome, $f_user - login successful. ".msgcontinue());
					header ("Location: message.php?cntrl=1&message=$message"); // Send them on their way.
					exit;
			}	
	
			if ($status == 0)	// user/pass check failed
			{
				// redirect to error page
				header("Location:error.php?e=$status");
				exit();
			}
			
			
		} // log in
	}//switch	

function authenticate($user, $pass)
{
  // $pdo dsn set above before function call
  global $pdo; 
  //Truncate to 16 characters: 2/12/2011 if user entered more than 16 char: (Twin only)
  $user = trim(substr($user,0,16));
  $pass = trim(substr($pass,0,16));
		
    // admin table password field manually  changed to password_hash() (NO old_password to check/deal with in that table)
		
	// see if user is in admin table..		
    $stmt = $pdo->prepare('SELECT `id`,`password` from `admin` WHERE `username` = :user');
    $stmt->execute(['user' =>$user]);
	$row = $stmt->fetch();
    
	    if ($row && password_verify($pass,$row['password'])) {
			// user is found in admin table
			return array(1,$row['id']);
		}
    
		// Otherwise, check in the customers table  
		$stmt = $pdo->prepare('SELECT `id`,`password` from `customers` WHERE `username` = :user');
        $stmt->execute(['user' =>$user]);
	    $row = $stmt->fetch();

        // Check strlen($row['password'] is 60 and valid)
		if ($row && (strlen($row['password'])>=60)  && password_verify($pass,$row['password'])) {
			// valid user is found in customers table and already password_hash()ed
			return array(2,$row['id']);
		
		}  else {
			    if ($row && (strlen($row['password'])==16) && ($row['password'] == old_pswd($pass)) ) {
						// valid user using mysqli old_password(): update hash to password_hash() and save then authenticate this user
						$valid_return = array(2,$row['id']);
						$hash = password_hash($pass,PASSWORD_DEFAULT);
						$stmt = $pdo->prepare('update customers SET password = :newhash where id = :id');
						$stmt->execute(['newhash'=>$hash,'id'=>$row['id'] ]);
						return $valid_return;
					}  else {

						// the user/pswd not found in admin or customer.. 
						return array(0,1);
					}
					
				}
}
	
function authenticate_old($user, $pass)
{
  // Original function,relied on mysql old_password()
  // and password field varchar(16))  Replaced 5/6/2019
  //Truncate to 16 characters: 2/12/2011 if user entered more than 16 char: 
  $user = trim(substr($user,0,16));
  $pass = trim(substr($pass,0,16));
  
  $conn = db_connect();	  // 3/22/2015 
  $user = mysqli_real_escape_string($conn, $user);
  $pass = mysqli_real_escape_string($conn, $pass);

    $query = "SELECT `id` from `admin` WHERE `username` = '$user' AND `password`=OLD_PASSWORD('$pass')"; 
    
    //echo $query;
    //exit;
    
  
	$result = @mysqli_query( $conn, $query) or die ("Error in login query: " .  mysqli_error($conn));
	// if row exists -> user/pass combination is correct
	if (mysqli_num_rows($result) == 1)
	{
		return 1;
	}
	// user/pass combination is wrong for admin so check for a user in the customer file	else
	{
    	$query = "SELECT id from customers WHERE `username` = '$user' AND `password` = OLD_PASSWORD('$pass')";

		$result = mysqli_query( $conn, $query) or die ("Error in login query: " .mysqli_error($conn));
		// if row exists -> user/pass combination is correct
		if (mysqli_num_rows($result) == 1)
		{
			return 2;
		}
		else
			return 0;
	}
}

function adduser($user, $pass)
{
	$conn = db_connect();
  $query = "SELECT id from users WHERE username = '$user' ";
	$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " .mysqli_error($conn));
	if (mysqli_num_rows($result) == 1 )
	{
		//username in use
		return -3;
	}
	else
	{
		
	    $query = "SELECT id from users WHERE username = '$user' AND password = OLD_PASSWORD('$pass')";
		$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " .  mysqli_error($conn));
		if (mysqli_num_rows($result) == 0)
		{
			// Can add new record
			$query = "INSERT into users values (NULL,'$user',OLD_PASSWORD('$pass'), '', '1')";
			$result = mysqli_query( $conn, $query) or die ("Error in inserting new user: $query. " .mysqli_error($conn));
			echo '<br>result'.$result;
		}
		return 4;
	}
}

?>
