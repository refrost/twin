<?php
// weblogoff.php - destroys session and returns to login form

// destroy all session variables
session_start();
$_SESSION = array();  // 6/21/2010 Added to end Admin carryover 
session_destroy();

// redirect browser back to login page

header("Location:weblogin.php");  //2/19/03  6/26/03 -ref-
exit;
?>
