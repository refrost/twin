<?php
//mergeid.php 12/16/2010 - 2/6/2011
//Consolidates order and shipaddr records under one login/webid
// for customer records sharing same email
//$user = 'carynbopp';
//$email = 'shebopp80@yahoo.com';	
/* 
Situation: user may have created more than 1 usernames which link to same email, before app
was fitted w/ 'forgot username' lookup'. If user wishes to consolidate orders and 
shipaddresses under ONE username, then they specify the username and run this program
(first by admin; later after logging in and running from the profile page).
Starts with the username to retain and the email. This routine then:
1. finds the customer record/webid for that pair. (Exit if not 1 record)
2. selects the customer records for this email
3. find the earlier date entered
4. get the arrays of customer rec id's, and webid to process
5. update the orders.webid (and email) to the desired values
6. update the shipaddress file to the desired webid
7. copy the customer records to mergedrecs file
8. delete the customer records
*/
// Admin can run directly 
// Later, may be called either from profile by user or admin. (If changed)
 // php 5.3
include ('book_sc_fns.php');
session_start();
//get vars:
if (!check_admin_user()) { //User must first run TR webapp and login as admin.
   exit('Accessed denied');
}
else {
   if (!isset($_POST['username']) || empty($_POST['username'])) { 
      //Show form if no data.
      $form = <<<EOB
         
         <br />
         Consolidate the multiple records of a Twinrocker customer under:<br /><br />
         <form action="merge_id.php" method="post">
         <table border="0"><tr>
            <td>Username: </td><td> <input type="text" name="username" /></td></tr><tr>
            <td>Email: </td><td><input type="text" name="email" /></td></tr><tr>
            <td></td><td><input type="submit" name="submit" value="Consolidate" /></td></tr>
         </table>
         </form>
EOB;
      echo $form;
      exit;  //Stop to get data.
   }
   else {
      $username = trim($_POST['username']);
      $email = trim($_POST['email']); 
   }
}

//Clean & check  data...
//$email = 'shebopp80@yahoo.com';	

if (!valid_email($email))
{
   exit('Problem with email data.');
}
if (!($conn = db_connect()))  // Connect to store
   exit('Data connection failed');
   
$username = @mysqli_real_escape_string($conn, $username);  // clean the user-enetered data 
$email    = @mysqli_real_escape_string($conn, $email);

//Make sure the main record exists and and get the webid
$query =  "SELECT id,webid,username,entered,email FROM `customers` WHERE username = '".$username."' AND email='".$email."'";
$result = @mysqli_query($conn, $query);
if (!$result || mysqli_num_rows($result)<>1) 
   exit('Problem with input data #2. Main user record does not exist.');  
else {
   $row = mysqli_fetch_array($result);
   $webid = $row['webid'];
   echo 'Main record found. The following webid: <br />';
   echo $row['id'].' '.$row['webid'].' '.$row['username'].' '.$row['entered'].' '.$row['email'].'<br /><br />';
   
   //Now find the other records and get those webid's
   $query = "SELECT id,webid,username,entered,email FROM `customers` WHERE  email='".$email."' AND webid != '".$webid."'";
   $result = @mysqli_query( $conn, $query);
   if (!$result || mysqli_num_rows($result)<1) //Makes sure there are other records
      exit('Problem with input data #3. Only the one record exists.');  
   else { 
      echo 'will consolidate the following records:<br/>';  
      while ($row = mysqli_fetch_array($result,  MYSQLI_ASSOC)) { 
         echo $row['id'].' '.$row['webid'].' '.$row['username'].' '.$row['entered'].' '.$row['email'].'<br />';
         $dup_webid[] = $row['webid'];
      }
   }
   //Save data in session and run a form calling consolidate.php
   $_SESSION['mrg_user'] = $username;
   $_SESSION['mrg_email'] = $email;
   $_SESSION['mrg_webid'] = $webid;
   $_SESSION['mrg_dup_webid'] = $dup_webid;

      $form = <<<EOB
         
         <br />
         Are you sure you want to consolidate these records?<br /><br />
         <form action="consolidate.php" method="post">
         <table border="0"><tr>
            <td><input type="submit" name="submit" value="Cancel" /></td>
            <td><input type="submit" name="submit" value="Consolidate" /></td></tr>
         </table>
         </form>
EOB;
      echo $form;

   
} 

// copy cust recs to merged_cust  (email)
// copy orders and shipaddr to merged_* (array of webid's))

//begin transaction
//  update webid in shipaddr and in orders
//  delete cust record with old webid's
//end transaction
//print summary & check on completeions
//close

//adjust to run from profile screen
exit;

/*
Copying to backup then deleting
INSERT INTO settings_b SELECT * FROM settings_a WHERE account = 'bishop';
3. Remove the old record from the old table.
DELETE FROM settings_a WHERE account = 'bishop';
*/

if (!isset($_SESSION["SESSION_UACCT"]) && (empty($user) || empty($email)))
   pass_msg("To merge multiple accounts sharing one email, login in with the username you wish to retain.  Try again or email Webmaster for assistance.
                    <br /><a href=weblogin.php?function=user>Please try again.</a>",1);

// User runs from profile screen; or admin runs from admin page..
// 
//SELECT * FROM `customers` WHERE email='shebopp80@yahoo.com'

// Should be logged in as the webid person is consolidating to
$custarray = get_custarray($_SESSION["SESSION_UACCT"]);

$merge_email = $custarray["email"];
$merge_webid = $_SESSION["SESSION_UACCT"];
$merge_username = $custarray["username"];

//SELECT min( entered ) FROM `customers` WHERE email = 'shebopp80@yahoo.com'
//$merge_entered = 

echo $_SESSION["SESSION_UACCT"].' '.$custarray["email"].' '.$custarray["entered"];






if (isset($_GET['action']))
	$action = $_GET['action'];
else
	$action = '';

if ($action == 'add')
{
   //8/25/03 UID to UACCT
	$custarray = get_custarray($_SESSION["SESSION_UACCT"]);
	display_shipto($custarray);
}
?>
