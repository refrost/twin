<?php
// include function files for this application
require_once("book_sc_fns.php"); 
session_start();
	
$goto = '';
if (isset($_POST["goto"]))
  $goto = $_POST["goto"];

if (isset($_POST["cancel"]))		// CANCEL and go to home page
{
		if ($goto=='final')            // If called from Final Review
      header("location:tr_checkout.php");
    else
    	header("location:showprod.php");
	exit;
}



//  if (cust_validfilled_out($_POST)) {redo with  error msg}

   if(cust_update($_POST,'customers'))  //function is in data_fns.php
	{
	   //$msg = urlencode("Profile was updated");
		// Send to a new page so 'refresh' won't add again...
		//header("location:task_done.php?task=$msg");
		if ($goto=='final')            // If called from Final Review
         header("location:tr_checkout.php");
      else
		  header("location:showprod.php");

   	exit;
	  
	}
    else
	{
	  //include('tr_header.php'); //do_html_header("Updating Customer profile","home");

    pass_msg("<br>Profile could not be updated.<br>");
      //do_html_url("showprod.php", "Continue");
	}
?>
