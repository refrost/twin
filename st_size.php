<?php
 // st_size.php
 
 /* 
 
 Part 1. 
 
 Editing st_pricelist.php to change dimensions
 1. Download filezilla the st_pricelist.php file  
    to the code directory
 2. Copy the file to the backup sub-directory
 3. Edit the file with notepad++
 4. Search (ctrl-f) for the stylecode  you 
    need to edit. Example to find item1, search for 001_
    To find item 20a search for 020A
 5. The dimensions to edit are in the 2 to 4 lines 
    of code  above the line found and looks like:
    
    <td><font size="2">4-1/2&quot; x<br>
						3-1/4&quot;</font></td>
	<td><font size="2">{2-1/4&quot;x<br>
						3-1/4}</font></td> 
 6. Edit only the dimension numbers. The 2nd set, in curly braces
  are the 'Folded' dimensions. if any. 
  
 7. When edited, press ctrl-f to find next stylwcode to edit.
 8. When done editing, ctrl-s to save the file in the code directory
 9. Upload the edited file with Filezilla and then test in browser.
 10. If website page is flawed, upload the file from backup sub-directory
     and then re-do your editing -- try again.                     
 
 11. Send email to Ralph saying you edited te file. 
 
 Part 2.
 
 Editing website pages which show the Stationery items.
 Login to the website as Admin   joni/friday25
 Then edit Stationery pages.
 
 1. Click into The field labled Price Table HTML
 2. Ctrl-A then ctrl-c to copy contents to clipboard
 3. In  gmail, Compose and then click into body and ctrl-v
    to make a temporary backup.
 4. Back in the editing field, ctrl-f to find the stylecode
 5. Again, to file 47 search for 047_
 6. Scroll down until you find the dimension
    you need to edit. 
 7. Make  all changes and then 
    scroll up to top of page and 
    click theSave Record  button.
    
  8. if (the page does not display right, 
     copy and paste the backup copy back into the field
     and try editing it again.     	
*/   
 
 
 
 */
 
 $size = array ('001_' => array('5-1/2" x 2-3/4" '
            ,'{2-3/4"x 2-3/4"}'),
                '002_' => array('5-1/2" x 2-3/4" '
            ,'{2-3/4"x 2-3/4"}'),
                '003_' => array('5-1/2" x 2-3/4" '
            ,'{2-3/4"x 2-3/4"}'),

);

echo $size['001_'][0];
?>