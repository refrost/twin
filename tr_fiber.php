<?php
/*
  tr_fiber.php lets users add items of fiber to 
  the "fibercart". The top part of the code
  takes POST vars and displays the current items
  in a table. This includes a call to 
  ../inc/action_fns.php .. load_fiber_array()
  which actually calculates the pricebreak 
  from data passed to it.
  Below that, the form is displayed for 
  "active" fiber items in store.fiber table.
  User enters qty in lbs or as bales (b,b2)
  in the form and then POSTs the form to 
  this script.  The display_fibercart() 
  can show a clear or edit link which is 
  used when showing fibercart on this page
  or on show_cart.php.  Database contains 
  fiber_old table which had 2 more 
  pricebreaks  (14,49,99,>99,bale)
  rather than current (14,>14,bales).
*/
// Rev. 10/08/2009 Refined Bale ordering in load_fiber_array() (action_fns.php (ref)
// Rev. 09/08/2011 Sorted list order by `type` (ref)
// Rev. 10/18/2011 Added displayorder: 
//                 $query = "select * from fiber where active = 'Y' order by `displayorder`";
// Rev. 1/15/2013 Wheat Straw price/volume comment
// 06/27/2015 php 5.3
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 
// rev 5/24/2018 saved fiber_old table; edited fiber ( removed 2 middle fields))

  include ('book_sc_fns.php');
  // tr_fiber.php 2/18/03 -ref-
  session_start();
  if (!isset($_SESSION['ship_state']))
      include('set_vars.php');

  //if recalled from 'clear fiber button', then unset fibercart
  $clearfiber = $_GET['clearfiber'];
  if (isset($_SESSION["fibercart"])){
  	$fibercart = $_SESSION["fibercart"];
  }
  if ($clearfiber=='Y') {
  	unset($fibercart);
    $_SESSION["fibercart"] = array();
  }
	
  //if called from tr_fiber.php, this send buttonname = 'clear' to display_fibercart()
  if (strchr($_SERVER["PHP_SELF"],'tr_fiber') == 'tr_fiber.php')
   	$disp_button = 'clear';
  else
  	$disp_button = 'none';
	
  // Load process and load HTTP_POST_VARS into fibercart array
  if (!empty($_POST))
  {
     //10/21/2005 Revised load_ function in action_fns.php 
     //to round up qty ..if javascript is off
	 $fibercart = load_fiber_array($_POST,'FI');  //../inc/action_fns.php
  }

 
  if (isset($fibercart) && count($fibercart)>0)	
  {
    // isset($fibercart) && if the fiber array is not empty, show the summary	
	// this resets totals which display in the html_header
  		// next section resets header vars to new value  or zero of clearfiber=yes
	
  		$wt = 0;
  		$cnt = 0;
  		$val = 0.00;		
		reset($fibercart);
        foreach ($fibercart as $key => $row)
    	{
    	 	if ($fibercart[$key]['totalweight']>0)
    	 	{
    	 		$wt = $wt + $fibercart[$key]['totalweight'];
    	 		$cnt= $cnt + 1;
    			$val= $val + $fibercart[$key]['totalamount'];
    		}
    	}
    	//Store into session_vars
      	$fiber_weight = $wt;
      	$fiber_items = $cnt;
      	$fiber_price = $val;
      	$_SESSION["fiber_weight"]= $wt;
      	$_SESSION["fiber_items"]= $cnt;
      	$_SESSION["fiber_price"]= $val;		
    
    //do_html_header("",$catid);
    $dept= 'Fibers';
    $jscript='validateFiber';
    include('tr_header.php');
    
    echo "<table class=fiber width=$g_table1_width border=1 bgcolor=white align=$g_table1_align><tr><td valign=top>";
    
    echo "<h3>Review Current Fiber Order:</h3>";
    echo "(Scroll down to view/change fiber order)";
    
    display_tr_fibercart($fibercart,$disp_button);
    echo "<br>";
  } 
  else
  {
        // fibercart is empty so initialize
  		$fiber_weight = 0;
  		$fiber_items = 0;
  		$fiber_price = 0.00;
  		$_SESSION["fiber_weight"]= 0;
  		$_SESSION["fiber_items"]= 0;
  		$_SESSION["fiber_price"]= 0;		
	 
	    //do_html_header("",$catid);
		$dept= 'Fibers';
		$jscript='validateInteger';
		include('tr_header.php');
		echo "<table class=fiber width=$g_table1_width border=1 bgcolor=white align=$g_table1_align><tr>
			<td valign=top>";

   }

   // end of showing the summary table at top of page +++++++++++++
		
   //display items from a custom pricebreak file and get qty
   //then lookup price and insert into cart when form is processed.
   //This will mean inserting item,qty,
   // unit price, unit weight, and catid 
   // into fibercart array
   // so that calculate_pb_totals can work without going back to file...

   echo "<br /><a name='FIBER SAMPLER:'><b>FIBER SAMPLER:</b></a> Try them all!<br />
		<table class=fiber width=$g_table1_width border=0><tr>
		<td valign=top><a href=images/fiber/fsampler.jpg><img border=0  src=images/fiber/xfsample.jpg></a></td>
		<td>Fiber Sampler:  Labeled 1/8 lb. samples of each of the fibers we carry. These larger samples contain enough fiber to test and still leave enough to keep as a permanent reference.
		<br ><br >
		Fiber Sampler -- ".silk_buy('SF')."
		</td>
		</tr></table> ";
	echo "<br><font size=3><a class=fiberdes   href=fiberdes.html >View Fiber Descriptions</a></font><br>";
		

   //Load price break data table from store.fiber table
   $conn = db_connect();
   // 10/18/2011 Added displayorder
   $query = "select * from fiber where active = 'Y' order by `displayorder`";   //$filename
   $result = @mysqli_query($conn, $query);
   if (!$result)
     return false;
   $num_res = @mysqli_num_rows($result);
   if ($num_res ==0)
      return false;
  	 
   $fiberdata = db_result_to_array($result);
       		
	echo "<br><h3>Fibers Selection and Price Break Form:</h3>";
    echo "<b>Note: </b>
	      Enter or edit the quantities (in pounds) for each fiber type desired,
          <br />or, <b>for full Bales, enter 'b'</b> followed by a number.
          <br />Press the 'Add Fiber to Cart' button to preview your fiber order on this page.
          <br />
          Prices shown are $ per pound for the different weight ranges.
          <br /><br />";
		  
    echo "<FORM method=post action=tr_fiber.php?clearfiber=N>";
	display_pb_add_button();

	//Create table and write header:
    echo "<table class=fiber width=$g_table1_width border = 0 >";  
    //echo "<thead>"; 
    echo "<tr><td class=fiber>Qty</td>";
    
    // Then print first 6 column names
	for ($n =0 ;$n<6;$n++)
	{
	    // 5/24/2018 ref fiber_old: WAS: for($n =0 ;$n<8;$n++)
        // 10/19/2011 Replace '_'  with space in field names --REF
	    $columnname = trim(str_replace("_"," ",((($___mysqli_tmp = mysqli_fetch_field_direct($result, $n)->name) && (!is_null($___mysqli_tmp))) ? $___mysqli_tmp : false)));
	  	echo "<td class=fiber>".$columnname."</td>";
	} 
    
	//echo "</thead>";
	echo "</tr><tbody>";
	
  //Write each line in the table from the data file..
  foreach ($fiberdata as $key => $row)
  {
	  //echo $key."  ".$row; //fibercartqty[$key];
	  echo "<tr>";
	
	  //Create a qty text box and name it with  the item and appended vars 
	  //so can pass--look up price in "pricebreak form handler"
	  // Create input field for the user to enter quantity
	  
	  // if qty's have been defined, write them back into the table
	  
	  if (isset($fibercart[$key]['item']) && !empty($fibercart[$key]['item'])) 
	  {
		 if ($fibercart[$key]['qty']=="0")
		 	$fibercart[$key]['qty'] = "";//show blanks 
			
		  // If itemno ends w/-B (bale), change the numberic qty back to bn
		  if (strtoupper(substr($fibercart[$key]['item'],-2)) == '-B' and strtoupper(substr($fibercart[$key]['qty'], 0, 1)) != 'B')   
	  		 $fibercart[$key]['qty'] = 'b'.$fibercart[$key]['qty'];
			 
          //Create the qty input field: 
          //name passes ALL the info for the row, in the name... 
               
    
          // 5/24/2018 for 2 fewer proce breaks     
		  echo "<td ><input type='text' name=".$row['item'].'|'.$key.'|'.$row[2].'|'.$row[3].'|'.$row[4].'|'.$row[5].'|'.$row[6].'|'.$row[7].'|'.str_replace(' ','+',$row[1])." value ='". $fibercart[$key]['qty'] ."'  size='4' maxsize='4' ".'onchange="return validateFiber(this.value);"'."> </td>";
	    
        //This was for original fiber_old pricebreaks
	    //echo "<td ><input type='text' name=".$row['item'].'|'.$key.'|'.$row[2].'|'.$row[3].'|'.$row[4].'|'.$row[5].'|'.$row[6].'|'.$row[7].'|'.str_replace(' ','+',$row[1])." value ='". $fibercart[$key]['qty'] ."'  size='4' maxsize='4' ".'onchange="return validateFiber(this.value);"'."> </td>";
	  }
	  else
	  {	
	      echo "<td><input type='text' name=".$row['item'].'|'.$key.'|'.$row[2].'|'.$row[3].'|'.$row[4].'|'.$row[5].'|'.$row[6].'|'.$row[7].'|'.str_replace(' ','+',$row[1])." 	           value =''  size='4' maxsize='4' ".'onchange="return validateInteger(this.value);"'."> </td>";
  	}
				 
	  //Write out each of the field values for each row:
	  $n = 0;
	  while($row[$n]) 
	  {
	    //Quits when encounters the first 0.00
	    if ($row[$n]== '0.00')
		      break;
		  //Show the pricebreaks as $
          // 5/24/2018 fiber_old: if ($n > 1 and $n < 7)
		  if ($n > 1 and $n < 5)
			  echo "<td>$".number_format($row[$n], 2)."</td>";
		  else
		    echo "<td>".trim($row[$n])."</td>";
		  $n = $n + 1;
		  //Don't show any fields after #8  --  
		  //This means structure of data file can't change except after field #8'       // 5/24/2018  changed structure; reduced from 8 to 6
          
          if ($n==6)  // was 8 originally w/ fiber_old struc
		   break;
	  }
	  echo "</tr>";
    }
    echo "</table>";
	  display_pb_add_button();
    echo "<hr>";
	  echo "</form>";

  if (isset($fibercart))
  {
	 $xcart = $fibercart;
	 $_SESSION["fibercart"] = $xcart;
  }
	// 1/15/2013 ref -- Wheat Straw note added
	echo 'Note: Wheat Straw unit  pricing is dictated  by its low density. 
          The larger the  order, the larger the package.';
          
	Echo '</td></tr> 
          </table>';  // overall display table
          
    
	
	silk_footer($g_table1_align,$g_table1_width);
   

function display_pb_add_button()
{   
	// show 'Add-to-Cart button on display_pricebreak() -- entry screen' 
	echo "<Button name='submit' style='font-family:Arial;
			font-weight:bold;
		  	font-size:10px;' value = 'submit' type='submit' >Add Fiber Order to Cart</button>";
}

?>
