<?php
 // Forgot  password:
 // Receives username from weblogin.php.display_forgot_password(),
 // then if username is found, present lostpassword question (back on the weblogin.php?function=question,
 // which posts to forgot_password2.php
 // If username & answer matches, change the password. . receive old and new password(s); compare, reset_password; Message
 // php5.3
 
 include ('book_sc_fns.php');
 session_start();

 if (!filled_out($_POST))
 {
   pass_msg("You have not filled out the form completely.
          <a href=weblogin.php?function=forgot>Please try again.</a>",1);
 }
 else
 {
    $userquestion = get_user_question(htmlspecialchars($_POST['username']));
    if ($userquestion)
    {
        //Stick the user name in a session var so it can be retreived on forgot_passwod2.php
        //(Don't pass username and question/answer in the hidden vars.)
  		  $_SESSION['tempuser'] = addslashes($_POST['username']);
        
        header("location:weblogin.php?function=ask&question=$userquestion");
        exit;
    }
    else
       pass_msg("Username not found.  (Or email Webmaster for assistance.)
          <a href=weblogin.php?function=forgot>Please try again.</a>",1);

    
 }
?>
