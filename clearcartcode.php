<?php

  //Code to clear cart: used in clear_cart.php AND in reorder.php
	$_SESSION['cart'] = array(); 			//$cart;
	$_SESSION['items'] = 0;
	$_SESSION['total_price'] = 0.00;
	$_SESSION['total_weight'] = 0.00;
  	$_SESSION['fibercart'] = array();	 	//fibercart
	$_SESSION['fiber_items'] = 0;
	$_SESSION['fiber_price'] = 0.00;
	$_SESSION['pulpcart'] = array();	 	//See pulper.php for definitions
	$_SESSION['pulp_items'] = 0;
	$_SESSION['pulp_shpitems'] = 0;
	$_SESSION['pulp_price'] = 0.00;
 	$_SESSION['papercart'] = array();	 	//papercart
	$_SESSION['paper_items'] = 0;
	$_SESSION['paper_price'] = 0.00;
  	$_SESSION['order_items'] = 0;
	$_SESSION['order_price'] = 0.00;
	$_SESSION['ship_special'] = '';
	$_SESSION['ship_rush'] = '';
	// 5/30/2017 uncmted next line
    // Was: 'Please pick a customer or a state.';
    $_SESSION['ship_string'] = 'No State Selected';
	$_SESSION['ship_charge'] = 0.00;
	$_SESSION['ship_cod'] = '';
    $_SESSION['discount_code'] =  '';    
    $_SESSION['discount_amt']  =  0.00;  
    $_SESSION['discount_string'] =  '';  
	unset($_SESSION["final"]);
?>
