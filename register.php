<?php # register.php
// Revised: 1/31/09 ref:  changed PASSWORD() to OLD_PASSWORD() encryption to match with 
//          phpwebhosting upgrade to mysql 5.0.45
//          1/24/2010 6/24/2015 ref: php 5.3 preg_match ... session_
// 11/2/2015 Rev. add addr2/3 city/st to register form
// 05/06/2019 rev. convert to password_hash() since mysql OLD_PASSWORD() deprecated

if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost")
{
  	// working locally so don't do ssl switch
}
else
{
	// 8/5/21 cmtted too many redirects
    //if ($_SERVER["SERVER_PORT"] != "443")
	//{
  	//	header("location:https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
	//	exit;
	//}
}
include ('book_sc_fns.php');
	
session_start();
if (!isset($_SESSION['searchstr']))
   	include('set_vars.php');


if (isset($_POST['cancel'])) 
{
	header("location:showprod.php");
	exit;			
}
	// get  the posted vars
	if (isset($_POST['username']))
		$username=$_POST['username'];
	else 
		$username='';
	if (isset($_POST['firstname']))
		$firstname=$_POST['firstname'];
	else 
		$firstname='';
	if (isset($_POST['lastname']))
		$lastname=$_POST['lastname'];
	else 
		$lastname='';
	if (isset($_POST['email']))
		$email=$_POST['email'];
	else 
		$email='';
	if (isset($_POST['pass1']))
		$pass1=$_POST['pass1'];
	else 
		$pass1='';
	if (isset($_POST['pass2']))
		$pass2=$_POST['pass2'];
	else 
		$pass2='';
	if (isset($_POST['company']))
		$company=$_POST['company'];
	else 
		$company='';

	if (isset($_POST['lostpswdquestion']))
		$lostpswdquestion=$_POST['lostpswdquestion'];
	else 
		$lostpswdquestion='';
	if (isset($_POST['lostpswdanswer']))
		$lostpswdanswer=$_POST['lostpswdanswer'];
	else 
		$lostpswdanswer='';
	if (isset($_POST['address1']))
		$address1=$_POST['address1'];
	else
		$address1='';
	if (isset($_POST['address2']))
		$address2=$_POST['address2'];
	else
		$address2='';
	if (isset($_POST['address3']))
		$address3=$_POST['address3'];
	else
		$address3='';        

	if (isset($_POST['zip']))
		$zip=$_POST['zip'];
	else
		$zip='';
	if (isset($_POST['city']))
		$city=$_POST['city'];
	else
		$city='';
		
	if (isset($_POST['state']))
		$state=$_POST['state'];
	else
		$state='';
	if (isset($_POST['country']))
		$country=$_POST['country'];
	else
		$country='USA';        

	if (isset($_POST['phn_area']))
		$phn_area=$_POST['phn_area'];
	else 
		$phn_area='';
	if (isset($_POST['phn_prefix']))
		$phn_prefix=$_POST['phn_prefix'];
	else
		$phn_prefix='';
	if (isset($_POST['phn_suffix']))
		$phn_suffix=$_POST['phn_suffix'];
	else
		$phn_suffix='';

    if (isset($_POST['cell_area']))
		$cell_area=$_POST['cell_area'];
	else 
		$cell_area='';
	if (isset($_POST['cell_prefix']))
		$cell_prefix=$_POST['cell_prefix'];
	else
		$cell_prefix='';
	if (isset($_POST['cell_suffix']))
		$cell_suffix=$_POST['cell_suffix'];
	else
		$cell_suffix='';

		
		  
if (isset($_POST['Submit'])) { // If the form was submitted, process it.
	

	// Check the username.	
    //  ("^[[:alnum:]]+$", $_POST['username']) -->> preg_match("/^[a-z0-9]*$/i", $str)
	if (preg_match("/^[a-z0-9]*$/i", $username)) {
		$a = TRUE;
	} else {
		$a = FALSE;
		$message[] = "Please enter a Username that consists only of letters and numbers.";
	}
	
	// Check to make sure the password is long enough and of the right format. 
	// Changed from 8 to 6 chr long 11/29/03 per HC
	if (preg_match("/^[a-z0-9]{6,16}+$/i", $pass1)) {
			$b = TRUE;
	} else {
			$b = FALSE;
			$message[] = "Please enter a password that consists only of letters and numbers, between 6 and 16 characters long.";
	}

	// Check to make sure the password matches the confirmed password. 
	if ($pass1 == $pass2) {
			$c = TRUE;
            // 5/6/2019 hash the password.
            $password = password_hash($pass1,PASSWORD_DEFAULT);
	} else {
			$c = FALSE;
			$message[] = "The password you entered did not match the confirmed password.";	
	}
	
	// Check to make sure they entered their first name and it's of the right format. 
	if (preg_match("/^[a-z\-']*$/i", $firstname)) { 
			$d = TRUE;
	} else {
			$d = FALSE;
			$message[] = "Please enter a valid first name.";
	}
	
	// Check to make sure they entered their last name and it's of the right format. a-z -'  (allows space 9/17/15)
	if (preg_match("/^[a-z\- ']*$/i", $lastname)) { 
			$e = TRUE;
	} else {
			$e = FALSE;
			$message[] = "Please enter a valid last name.";
	}
	
	// Check to make sure they entered a valid email address. 
    // ("^([[:alnum:]]|_|\.|-)+@([[:alnum:]]|\.|-)+(\.)([a-z]{2,4})$",
    $valid_email = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i'; 
    if (preg_match($valid_email, $email)) { 
			$f = TRUE;
	} else {
			$f = FALSE;
			$message[] = "Please enter a valid email address.";
	}

	// Check the Company.	
	if (empty($company))    {
		$g = TRUE;
	} else {
		if (preg_match("/^[a-z\-'& ]*$/i", $company)) {
			$g = TRUE;
		} else {
			$g = FALSE;
			$message[] = "Please enter a Company name.";
		}
	}
	
	// Check the pswd question.	
	if (preg_match("/^[a-z0-9 \?']{3,60}+$/i", $lostpswdquestion)) {
		$h = TRUE;
	} else {
		$h = FALSE;
		$message[] = "Please enter a question to use to reissue a password.";
	}
	// Check the pswd answer.	
	if (preg_match("/^[a-z0-9 ']{3,20}+$/i", $lostpswdanswer)) {
		$i = TRUE;
	} else {
		$i = FALSE;
		$message[] = "Please enter the  answer to your question (to reissue a lost password) - from 3 to 20 characters; letters and/or numbers ONLY.";
	}
	
	// Check the address.
	if (empty($address1))    {
		$g = TRUE;
	} else {
        
		if (preg_match("/^[a-z0-9 .,\-'&]+$/i", $address1)) {
			$g = TRUE;
		} else {
			$g = FALSE;
			$message[] = "Please enter billing address. (No special characters)";
		}
	}

	// Check the postal code.	
	$uszipregex = '/(^\d{5}$)|(^\d{5}-\d{4}$)|(^\d{9}$)/';
    //Allow ~any postal code
    $anycoderegex = "/(?i)^[a-z0-9][a-z0-9\- ]{0,10}[a-z0-9]$/"; 
    if( preg_match($anycoderegex, $_POST['zip'])) {
		$j = TRUE;
	} else {
		$j = FALSE;
		$message[] = "Please enter the  postal code  (as 47923-0433 if in US";
	}

  // Check the phone....
	if (preg_match("/[0-9][0-9][0-9]/", $phn_area)
       && preg_match("/[0-9][0-9][0-9]/", $phn_prefix)
       && preg_match("/[0-9][0-9][0-9][0-9]/", $phn_suffix) )
    {
		$k = TRUE;
	} else {
		$k = FALSE;
		$message[] = "Please enter the  phone number as 765 555 1212.";
	}

    if ("{$cell_area}{$cell_prefix}{$cell_suffix}") {
          // Check the cell phone....
    	if (preg_match("/[0-9][0-9][0-9]/", $cell_area)
           && preg_match("/[0-9][0-9][0-9]/", $cell_prefix)
           && preg_match("/[0-9][0-9][0-9][0-9]/", $cell_suffix) )
        {
    		$l = TRUE;
    	} else {
    		$l = FALSE;
    		$message[] = "Please enter the  cell phone number as 765 555 1212.";
    	}
    } else {
        // cell phn blank so allow as valid
        $l = TRUE;
    }
	//  If the data passes all the tests, check to ensure a unique member name, then register them. 
	if ($a AND $b AND $c AND $d AND $e AND $f AND $g AND $h AND $i AND $j AND $k AND $l)
	{
	
		$conn = db_connect();
        // 5/6/19 chk  if username is in eiter admin or customers  tables
        $rec_count = 0;
        $query = "SELECT id from admin WHERE username = '$username' ";
		$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " .       ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$rec_count += mysqli_num_rows($result);
        $query = "SELECT id from customers WHERE username = '$username' ";
		$result = mysqli_query( $conn, $query) or die ("Error in query: $query. " .       ((is_object($conn)) ? mysqli_error($conn) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)));
		$rec_count += mysqli_num_rows($result);
        if ($rec_count > 0 )
		{
			//username is in use
			$add_status = -3;
		}
		else
		{
    		// Insert the new username and record
            // w/ password_hash() as of 5/6/2019 	
    		// Webusers will add themselves.
            // If they  order, the custno will be set to match
            // the backend custno.
            // Webid will be unique string
    		
    		//The new user is adding a new record into the customer file
            //on the web.
				
   			$lostpswdquestion = addslashes($lostpswdquestion);
			//$lostpswdanswer = $lostpswdanswer;
			$firstname =addslashes(ucwords(strtolower($firstname)));
			$lastname =addslashes(ucwords(strtolower($lastname)));
			$company =addslashes(ucwords(strtolower($company)));
            $address1 =addslashes(ucwords(strtolower($address1)));
            $address2 =addslashes(ucwords(strtolower($address2)));
            $address3 =addslashes(ucwords(strtolower($address3)));
    
			//If blank find  city,state, zip from zip5 datafile, then initialize...
            // rev. ziplookupjson 11/4/2015
            if (empty($city) || empty($state)) {
				    $location = ziplookup($zip);
				    $city = $location['city'];
			        $state = $location['state'];
            }
            $city =addslashes(ucwords(strtolower($city)));
            $state =addslashes(strtoupper($state));
            $country  =addslashes($country);
            //create a 32-char unique id (fns in action_fns.php)
            //This will be the foreign key in child files on the web
    		$webid = get_uniqueid();     // 8/25/03

            // Insert the new record. Assign backend custno as 'NEW'
            $secretanswer = crypt(STRTOUPPER($lostpswdanswer),'si');
    		$query = "INSERT into customers
             (id,webid,username,password,lostpswdquestion,lostpswdanswer,email,
              entered,firstname,lastname,name,city,state,zip,country,custno,
              phn_area,phn_prefix,phn_suffix,
              cell_area,cell_prefix,cell_suffix,
              address1,address2,address3)
              values (NULL,'$webid','$username','$password', '$lostpswdquestion',
              '$secretanswer','$email',now(),'$firstname','$lastname','$company',
              '$city','$state','$zip','$country','NEW',
              '$phn_area','$phn_prefix','$phn_suffix',
              '$cell_area','$cell_prefix','$cell_suffix',
              '$address1','$address2','$address3')";
               //echo $query;
      			$result = @mysqli_query( $conn, $query) or die ("Error in inserting new user");  
				//echo '<br>result'.$result;
				if ($result)
				{

					 	// LOG the new user into the system. 
						// Get data and register some session variables
						// ~Similar CODE in Authenticate.php  7/9/03

						$conn = db_connect();
    				    $query = "SELECT id,firstname,lastname,state,webid,logins from customers WHERE username = '$username' ";  // retrieve just on username is unique 
                        $result = mysqli_query( $conn, $query) or die ("Error in registration query. " );
						if ($result)
						{
							$row = mysqli_fetch_array($result);
							
		
							
							$_SESSION['SESSION'] = "User";   // user with regular users.. 

							//echo $row["firstname"];
							// State the Firstname+lastname
							$_SESSION['SESSION_UNAME'] = stripslashes($row["firstname"]).'&nbsp;'.stripslashes($row["lastname"]);
		

							// Fix user record ID  -- this is auto increment value
                            // 8/25/03 switch from this to webid in the application
							$_SESSION['SESSION_UID'] = $row["id"];
					
							// include the webid which will be the foreign key in child files
							// this WAS sbt custno -- backend account number --
							$_SESSION['SESSION_UACCT'] = $row["webid"];

							// get ship_state, too
							$_SESSION['ship_state'] = $row["state"];
					
							$add_status = 1;

                     //update_login_count($row['logins'],$row['webid']);
						}
						else
							$add_status = -4;	//Logging in the new usered failed...
							
					}
					else
						$add_status = -2;	// Inserting the new data failed
		} // Inserting the new username and record

        // send cntrl=1 to keep message under SSL control during register or login
		switch ($add_status)
		{
			case 1:
				
				// Send 
				$message = urlencode ("You have been successfully registered. Welcome to $g_storename.<br>
                                <br>Click on a menu choice to begin shopping or <a href=showprod.php>Here</a> to return to the Supplies main page.");
				header ("Location: message.php?cntrl=1&message=$message"); // Send them on their way.
				exit;
			case -2:
				$message[] = "Could not save record. Please try again or contact the Webmaster for more information.<br />";			
				break;
			case -3:
				$message[] = "That username is already taken. Please select another.";			
				break;
			case -4:
				$message = urlencode ("You have been registered, but please login again using your user name and password.");
				header ("Location: message.php?cntrl=1&message=$message"); // Send them on their way.
				exit;
				break;
				
					
		}
		
	} // end of if ($a and $b ...and $l) (is data valid?)..

 

} // End of Submit if.

// 11/6/2015 First instance of passing array of scripts to load into tr_header.php.
// Also implemented ziplookup in js
$jscript = ['moveonmax','jqziplookup'];   // 6/24/2015 (phn fields)

include('tr_header.php');
echo "<table class=descrip border=1 align=center width=$g_table1_width>
  <tr><td><p class=info><b>New User Registration Form</b><br /><br />";

echo redmessage('<u>Note:</u> We do not sell or exchange personal information with any third parties. <br />You can add Fax# and other info, or edit data later from your profile page.')."  <small><a href=showprod.php?dept=MyMenu&grp=Company&prod=Privacy> Privacy Policy </a>
     <br></small></td></tr><tr height=200><td class=descrip>";

if (isset($message)) {
	echo "<div align=\"left\"><font color=red><b>The following problems occurred:</b><br />\n";	
	foreach ($message as $key => $value) {
		echo "$value <br />\n";	
	}
	echo "<p></p><b>Be sure to reenter your passwords!</b></font></div><br />\n";	
}



?>
<form action="register.php" method="post">
<table class=login border="0"  cellspacing="2" cellpadding="2" align="center">
	<tr>
		<td align="right">Your Abbreviated Login Name</td>
		<td align="left"><input type="text" name="username" size="25" maxsize="16" value="<?php  echo $username;?>" autofocus></td>
		<td align="left"><small>Maximum of 16 characters, letters and numbers, no spaces, underscores, hyphens, etc.</small></td>
	</tr>
	<tr>
		<td align="right">Password</td>
		<td align="left"><input type="password" name="pass1" size="25"></td>
		<td align="left"><small>Minimum of 6 characters, maximum of 16, letters and numbers, no spaces, underscores, hyphens, etc.</small></td>
	</tr>
	<tr>
		<td align="right">Confirm Password</td>
		<td align="left"><input type="password" name="pass2" size="25"></td>
		<td align="left"><small>Should be the same as the password.</small></td>
	</tr>
	<tr>
		<td align="right">First Name</td>
		<td align="left"><input type="text" name="firstname" size="25" maxsize="20" value="<?php  echo $firstname ?>"></td>
		<td align="left">&nbsp;</td>
	</tr>
	<tr>
		<td align="right">Last Name</td>
		<td align="left"><input type="text" name="lastname" size="25" maxsize="20" value="<?php  echo $lastname ?>"></td>
		<td align="left">&nbsp;</td>
	</tr>
	<tr>
		<td align="right">Email Address</td>
		<td align="left"><input type="text" name="email" size="25" maxsize="60" value="<?php  echo $email ?>"></td>
		<td align="left"><small></small></td>
	</tr>
	<tr>
		<td align="right">Company</td>
		<td align="left"><input type="text" name="company" size="25" maxsize="40" value="<?php  echo $company;?>"></td>
		<td align="left"><small>Please fill in if you have a company or school affiliation. (Optional)</small></td>
	</tr>
	<tr>
		<td align="right">Postal Code</td>
		<td align="left"><input type="text" name="zip" size="25" maxsize="10" value="<?php  echo $zip;?>"></td>
		<td align="left"><small> Billing postal code (Program should seek/fill in your US city/state.)</small></td>
	</tr>
	<tr>
		<td align="right">Address 1</td>
		<td align="left"><input type="text" name="address1" size="25" maxsize="45" value="<?php  echo $address1;?>"></td>
		<td align="left"><small>Enter billing address line (as shown on credit card statements).</small></td>
	</tr>
    
	<tr>
		<td align="right">Address 2</td>
		<td align="left"><input type="text" name="address2" size="25" maxsize="45" value="<?php  echo $address2;?>"></td>
		<td align="left"><small> Second billing address line.</small></td>
	</tr>
	<tr>
		<td align="right">Address 3</td>
		<td align="left"><input type="text" name="address3" size="25" maxsize="45" value="<?php  echo $address3;?>"></td>
		<td align="left"><small> Third billing address line, if any.</small></td>
	</tr>
    
    <tr>
    <td align="right">City</td>
    <td align="left" colspan=2>
      <input type=text size=20 maxlength=20 name=city value="<?php  echo $city; ?>">
      State&nbsp;&nbsp;
      <input type=text size=20 maxlength=20 name=state value="<?php  echo $state; ?>">
      Country&nbsp;&nbsp;
      <input type=text size=20 maxlength=20 name=country value="<?php  echo $country; ?>">
    </td>
  </tr>
   
  <tr>
    <td align='right'> Phone</td>
    <td align=left>
      <input type="text" name="phn_area"  onkeyup="moveOnMax(this,'phn_prefix')"  size="3" maxlength="3" value="<?php  echo $phn_area;?>"/>
			<input type="text" size="3" maxlength="3" id="phn_prefix" onkeyup="moveOnMax(this,'phn_suffix')" name="phn_prefix" value="<?php  echo $phn_prefix;?>"/>
			<input type="text" size="4"
            maxlength="4" id="phn_suffix" name="phn_suffix" value="<?php  echo $phn_suffix;?>"/>
     </td>
		<td align="left"><small>Enter phone as area code,exchange and suffix like 765 563 3119.</small></td>
	</tr>

  <tr>
    <td align='right'>Cell Phone</td>
    <td align=left>
      <input type="text" name="cell_area"  onkeyup="moveOnMax(this,'cell_prefix')"  size="3" maxlength="3" value="<?php  echo $cell_area;?>"/>
			<input type="text" size="3" maxlength="3" id="cell_prefix" onkeyup="moveOnMax(this,'cell_suffix')" name="cell_prefix" value="<?php  echo $cell_prefix;?>"/>
			<input type="text" size="4" maxlength="4" id="cell_suffix" name="cell_suffix" value="<?php  echo $cell_suffix;?>"/>
     </td>
		<td align="left"><small>Enter cell phone as area code,exchange and suffix like 765 563 3119.</small></td>
	</tr>


	<tr>
		<td align="right">Question</td>
		<td align="left"><input type="text" name="lostpswdquestion" size="25" maxsize="50" value="<?php  echo $lostpswdquestion;?>"></td>
		<td align="left"><small>Enter question to  ask to confirm your ID in event of a lost password.</small></td>
	</tr>
	<tr>
		<td align="right">Answer</td>
		<td align="left"><input type="text" name="lostpswdanswer" size="20" maxsize="20" value="<?php  echo $lostpswdanswer;?>"></td>
		<td align="left"><small>Enter answer to the  question to confirm your ID in event of a lost password.</small></td>
	</tr>
	
	

	<tr>
		<td align="center" colspan="3"><input class=login type="submit" name="Submit" value="Register"> &nbsp; &nbsp; &nbsp; <input class=login type="submit" name="cancel" value="Cancel"></td>
	</tr>
	<tr>
	<td align="right" colspan="3"><div align=left>
      &nbsp;&nbsp;Secure web communication
      <br>&nbsp;&nbsp;<?php echo $g_ssl_img; ?></div>
  </td></tr>

</table>
</form>
</table>
<?php
silk_footer($g_table1_align,$g_table1_width);
?>
