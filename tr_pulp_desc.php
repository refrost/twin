<?php 
//<!-- tr_pulp_desc.html  2/25/03 revised description; 
//     called from show_pulp()  -->
	 
Echo "<table width=448><tr><td>

<table><tr>

<td valign='top'>
<a href='images/pulp/ppulp.jpg'><image border='1' src='images/pulp/xppulp.jpg'></a>

<a name='MAKING PULP FROM FIBER:'><strong>MAKING PULP FROM FIBER: 'BEATING'</strong></a><br>

<p>The first step in making paper from cooked fiber is to literally beat the fiber to a pulp. The plant fibers

are mixed with pure water and a pounding action crushes and abrades the fibers. This process

works water molecules deep into the structure of the fiber, causing them to attach to bonding

sights on the cellulose. After the paper is formed and water evaporates from the pulp, the fibers

come together even closer than they were in the plant and form new bonds, called 'hydrogen

bonds'. This holds the paper together. The beating process is crucial in determining the physical

characteristics of the finished paper, those relating to hardness or softness, translucency or

opacity, along with tear and bursting strength. The more water which is pounded into the plant

fibers, the more bonds created.

<br>

<a href='images/pulp/ptjbigb4.jpg'><img border='1' src='images/pulp/xptjbig4.jpg' width='154'></a>

<strong>THE HOLLANDER BEATER</strong><br>

A Hollander Beater was first used in the Netherlands in the 1600s, it is still the machine of choice for dealing 

with long, strong fibers like cotton and flax. In the photograph, Travis Becker (Twinrocker's Shop Director) has 

raised the roll cover of our largest beater and is pointing out the bars on the roll, the major component of a 

Hollander Beater. The 

roll is driven by a powerful electric motor and works by squeezing and releasing the wet fiber between the moving 

bars of the roll and the fixed bars of the bedplate that is attached to the bottom of the tub, directly under the 

roll. As the roll spins, the bars also act as a paddle wheel to move the wet pulp around the beater. The fibers 

are pounded under the roll, circulate around the tub and are pounded again, time after time, until the pulp is ready. 

The longer the fiber is in the beater, the greater number of bonding sites that are developed on the fiber. These 

bonding sites are where the fibers stick together after the paper has been formed and dried. 

<p>Well-beaten fiber forms many bonds and makes paper that is strong, hard, 'rattly', translucent,

and  shrinks a great deal during drying. Currency (money) paper is made from well-beaten fiber.

Paper made from fiber with little beating (such as blotter paper) has few bonds, is weak, soft,

opaque, and shrinks little during drying. These are the extremes. The pulp for most papers falls in

between.</td></tr></table>


<table><tr><td valign='top'><a href='images/pulp/psmall3.jpg'><img border='1' src='images/pulp/xpsmall3.jpg' width='154'></a></td>

<td valign='top'><strong>MAKING PULP FROM SHEETS OF HALF STUFF</strong><br>

In the photo, Travis is tearing strips off of a sheet of wet cotton linters and is adding it to one of our small 

beaters. The fiber will be beaten anywhere from 20 minutes to several hours depending on the kind of paper it 

will be used for. We make all pulp to order. 

<p><a name='SPECIFYING BEATING:'><strong>SPECIFYING BEATING:</strong></a>

<br>We use the term 'Very Fine' to describe the shortest, most highly beaten fiber, and 'Very

Coarse' to describe the longest, most lightly beaten fiber, with 'Coarse', 'Medium Coarse',

'Medium', 'Medium Fine', and 'Fine' to describe the range between. <em>Very Fine</em> pulp is

generally used for drawing with colored pulps, thin laminated  imagery and other decorative

effects, particularly when it is to be applied with squeeze bottles and sprayers. <em>Very Coarse</em> is

normally used for sculptural techniques like building pulp over an armature and is usually too

coarse for most castings, depending on one's imagery. 

</td>

</tr></table>
		<h3>MAKING PULP FROM RAW, UNCOOKED FIBER</h3>
		<table><tr>

<td colspan='3'><strong>COOKING THE FIBER</strong></td>

</tr><tr>

<td valign='top' width='30%'><a href='images/pulp/fhemp.jpg'><img border='1' src='images/pulp/xfhemp.jpg'></a><br>

	<em>Raw Hemp Fiber</em></td>

<td valign='top' width='30%'><a href='images/pulp/pcookhmp.jpg'><img border='1' src='images/pulp/xpcookhm.jpg'></a><br>

	<em>Hemp Fiber at the Beginning of the Cook</em></td>

<td valign='top'>The photographs show us cooking and beating raw hemp fiber for a customer. Raw, uncooked fiber, such as 

flax and hemp, should be cooked in a caustic to remove the non-cellulose material before it is used to make paper (for  

detailed descriptions of the raw fibers that we carry, see the <em>FIBER</em> 

section.) 

Uncooked fiber can make interesting paper, it may even be long lasting, but it will definitely not be archival. 

To cook fiber, big stainless steel pot is filled with fiber, water and about 10% caustic (soda ash or lye depending 

on the fiber)and placed on a stove to simmer.</td>

</tr><tr>

<td valign='top' width='30%'><a href='images/pulp/phempck3.jpg'><img border='1' src='images/pulp/xphempc3.jpg'></a><br>

	<em>Cooked Hemp Fiber</em></td>

<td valign='top' width='30%'><a href='images/pulp/phemprns.jpg'><img border='1' src='images/pulp/xphemprn.jpg'></a><br>

	<em>Rinsed Hemp Fiber</em></td>

<td valign='top'>After 4 to 6 hours on the stove, the water has turned to 'black liquor' i.e. the non-cellulose impurities

	have been dissolved in the water making it black in color and ph nuetral. The fiber is then rinsed until the water runs clear</td>

</tr></table>
		<table><tr>

<td valign='top' width='30%'><a href='images/pulp/ptjhemp.jpg'><img border='1' src='images/pulp/xptjhemp.jpg'></a><br>

	<em>Travis Becker Adding Cooked Hemp to the Beater</em></td>

<td valign='top' width='30%'><a href='images/pulp/phempbtr.jpg'><img border='1' src='images/pulp/xphempbt.jpg'></a><br>

	<em>Separating the Fiber as it is Added to the Beater</em></td>

<td valign='top'>The cooked hemp is slowly added to the beater. Hemp and flax are really tough fibers, so they must be added 

	slowly to the beater. Separating lumps and clumps speeds the process.</td>

</tr><tr>

<td valign='top' width='30%'><a href='images/pulp/phmpbtr2.jpg'><img border='1' src='images/pulp/xphmpbt2.jpg'></a><br>

	<em>After an Hour, the Fiber Begins to Look Like Pulp</em></td>

<td valign='top' width='30%'><a href='images/pulp/ppailgry.jpg'><img border='1' src='images/pulp/xppailgr.jpg'></a><br>

	<em>Drained and Beaten Ready-To-Use Hemp Pulp in a 5 Gallon Pail Ready for Shipment</em></td>

<td valign='top'>After an hour or so, the hemp looks less stringy and more like pulp. After several hours in the beater, 

the fiber is drained out and placed in 5 gallon pails for shipment.</td>

</tr></table>



<p><strong><a name='BLENDER OR BEATER?:'>BLENDER OR BEATER?</strong>

<p>Blenders are very useful in papermaking, but they are very limited in their ability to beat pulp.

The action of a blender is to cut and chop rather than to pound and beat. They work well for

recycling paper and even for preparing fiber in sheet form, but a blender does not add strength as

beating does. If blender prepared pulp works for your requirements, then by all means use it. If

you find that your paper is not strong enough, or doesn't take pigments well, then we recommend

using our Ready-to-Use Pulp. The hydrogen bonds that are created in the beater not only hold the

paper together, but the bonding sites also attract pigments, sizing, and other additives.    Fiber

that doesn't take color evenly will usually perform much better after beating.

<p><hr>



<p><strong><a name='PULP MADE ESPECIALLY FOR YOU:'>PULP MADE ESPECIALLY FOR YOU</strong>

<p>
		<ul><li><strong>PULP IN A 5 GALLON PAIL: </strong>

<br>We ship pulp in a 5 gallon plastic pail. Each pail contains 3 full pounds of fiber which will make

3 full pounds of dry paper. This is 1/3 MORE pulp in the 5 gallon pail than is available anywhere

else. You can order wet, Ready-to-Use beaten pulps from almost all of the many Twinrocker

fibers, beaten to your specifications, with sizing and/or methylcellulose added depending on your

requirements. This pulp can be made from one fiber or a combination of two different fibers. For

instance, half bleached abaca for strength and half cotton linters for softness and bulk. There is

no extra charge for combining fibers, but the pulp is priced according to the more expensive of

the two pulps. 





<li><strong>BEATING RECOMMENDATIONS:</strong>

<br>As a rule of thumb, we usually recommend Medium Coarse beating for paper castings and other

3-dimensional, sculptural uses, Medium for forming sheets of art paper and some 3D techniques,

and Medium Fine for calligraphy and book papers.

</ul>
		<hr>
		<p><a name='ADDITIVES:'><strong>ADDITIVES </strong></a>

<p>There is a whole section on <em>ADDITIVES</em>, but the two 

additives most often used by papermakers are sizing and CMC (carboxymethylcellulose), both can be added to Ready-to-Use 

Pulp when we prepare it for you. 
		<ul>

<li><strong>SIZING:</strong><br>

Paper is naturally absorbent, like paper towels. Sizing makes paper less absorbent and easier to write or draw on. 

Twinrocker Internal Sizing is an archival material that is easy to use. We recommend that you always use sizing as it 

protects your work. Use unsized (waterleaf) paper only if bleeding or wicking is an important aesthetic effect in your work. 

<br>

<li><strong>CARBOXYMETHYLCELLULOSE (CMC):</strong><br>

Is an archival material made from cellulose that promotes fiber-to-fiber bonding and strength. As a consequence, it also 

provides some additional sizing effect. We recommend the use of CMC for sculptural casting of paper pulp. We also recommend 

its use with other techniques whenever the vital pressing step is not used, or with fiber that has received little beating.  

We do not recommend that you use CMC with pulp beaten more than medium as it reduces drainage so much that it is difficult 

to form sheets, although it may work well for other types of papermaking. We always recommend that you use CMC for paper 

castings and other sculptural techniques.

</ul>
		<hr>
		<A NAME='SUMMARY OF READY-TO-USE PULP RECOMMENDATIONS:'></a>
		<table BORDER='1' BGCOLOR='#dbdbdb'>

<tr>

<td align='center' colspan='4'><strong>SUMMARY OF READY-TO-USE PULP RECOMMENDATIONS</strong></td>

</tr>

<tr>

<td align='center'>Paper making process</td>

<td align='center'>Fiber</td>

<td align='center'>Beating</td>

<td align='center'>Additives</td>

</tr>

<tr>

<td align='center'>Casting and Other 3D Techniques</td>

<td align='center'>Cotton Linters #27</td>

<td align='center'>Medium Coarse</td>

<td align='center'>Sizing & CMC</td>

</tr>

<tr>

<td align='center'>Forming Sheets of Art Paper</td>

<td align='center'>Cotton Linters #29</td>

<td align='center'>Medium</td>

<td align='center'>Sizing</td>

</tr>

<tr>

<td align='center'>Forming Thin Sheets of Book or Calligraphy Paper</td>

<td align='center'>Cotton Rag #89</td>

<td align='center'>Medium Fine</td>

<td align='center'>Sizing</td>

</tr>

<tr>

<td align='center' colspan='4'><em><small>Note: These are the most popular fibers and additives. 

For other Fibers, see <a href='sfiber.htm' target='_top'>FIBERS</a>. For other additives, see the 

<a href='sadditiv.htm' target='_top'>ADDITIVES</a> section.</small></em></td>	

</tr>

</table>
		<hr>
		<p><a name='1 GALLON COTTON PULP SAMPLES:'><strong>1 GALLON COTTON PULP SAMPLES</strong></a>

<p>One gallon samples of the three white cotton fibers; cotton rag # 89, cotton linters #29 and #27. 

Cotton rag #89 and cotton linters #29 are beaten to MEDIUM (for sheet forming) and come with sizing 

added. Cotton linters #27 is beaten to MEDIUM COARSE (for casting) and sizing and CMC are added.

<br>These samples can help determine which cotton pulp you need, or if you 

need a combination of two fibers, for example, part cotton rag #89 for

strength and rattle and part linters for softness and bulk. 

<p></p>

<table border='1' bgcolor='#dbdbdb'>

<tr>

<td align='left' colspan='3'><strong>PULP SAMPLES</strong></td>

</tr>

<tr>

<td align='left' colspan='3'>One gallon pails of ready-to-use pulp in our 

3 most popular fibers:<br>

Cotton Linters #27 (medium coarse beating, sizing & CMC added)<br>

Cotton Linters #29 (medium beating & sizing added)<br>

or Cotton Rag #89 (medium beating & sizing added)

</td>

</tr>

<tr>

<td align='left'>1 sample:</td>

<td align='left'>(free shipping)</td>

<td align='left'>$15.00</td>

</tr>

<tr>

<td align='left'>All 3 samples</td>

<td align='left'>(free shipping)</td>

<td align='left'>$40.00</td>

</tr>

</table>

<p><hr>



<p><a name='PULP PRICES:'><strong>PULP PRICES</strong></a>



<table border='1' bgcolor='#dbdbdb'>

<tr>

<td align='center' colspan='3'><strong>PULP MADE FROM HALF-STUFF</strong></td>

</tr>

<tr>

<td align='left'><strong>Fiber</strong></td>

<td align='center'><strong>Price per Single Pail</strong></td>

<td align='center'><strong>Price per pail; Groups of 5</strong></td>

</tr>

<tr>

<td align='left'>Abaca, Bleached</td>

<td align='center'>$40.00</td>

<td align='center'>$31.00</td>

</tr>

<tr>

<td align='left'>Abaca, Unbleached</td>

<td align='center'>$40.00</td>

<td align='center'>$31.00</td>

</tr>

<tr>

<td align='left'>Cotton Linters #27</td>

<td align='center'>$37.00</td>

<td align='center'>$27.00</td>

</tr>

<tr>

<td align='left'>Cotton Linters #29</td>

<td align='center'>$37.00</td>

<td align='center'>$27.00</td>

</tr>

<tr>

<td align='left'>Cotton Rag #89</td>

<td align='center'>$37.00</td>

<td align='center'>$27.00</td> 

</tr>

<tr>

<td align='left'>Black Cotton Rag</td>

<td align='center'>$42.00</td>

<td align='center'>$32.00</td>

</tr>

<tr>

<td align='left'>Blue Cotton Rag</td>

<td align='center'>$42.00</td>

<td align='center'>$32.00</td>  

</tr>

<tr>

<td align='left'>Flax</td>

<td align='center'>$40.00</td>

<td align='center'>$31.00</td>

</tr>
			<tr>

<td align='left'>Hemp</td>

<td align='center'>$40.00</td>

<td align='center'>$31.00</td>

</tr>
			<tr>

<td align='center' colspan='3'><strong>PULP MADE FROM RAW FIBER</strong><BR>

(the fiber must be cooked before beating)</td>

</tr>
			<tr>

<td align='left'><strong>Fiber</strong></td>
				<td align='center'><strong>Price per Single Pail</strong></td>
				<td align='center'></td>
			</tr>
			<tr>

<td align='left'>Raw Cotton</td>
				<td align='center'>$75.00</td>
				<td align='center'></td>
			</tr>
			<tr>

<td align='left'>Raw Line Flax</td>
				<td align='center'>$75.00</td>
				<td align='center'></td>
			</tr>
			<tr>

<td align='left'>Raw Hemp</td>
				<td align='center'>$75.00</td>
				<td align='center'></td>
			</tr>

<tr>

<td align='left'>Raw Wheat Straw</td>
				<td align='center'>$75.00</td>
				<td align='center'></td>
			</tr>

</table>

<p><hr>



<p><a name='SHIPPING COSTS:'><strong>SHIPPING PULP </strong></a>

<p>A pail of pulp is heavy, 42 lbs. We must add a shipping surcharge, which depends on distance. It

is much less than the actual freight charges, however. 

<p>LARGE QUANTITIES OF PULP ( called 5 in 4 ):

<p>Our large beater beats five pails of pulp at once, so the price is reduced for pulp in 5 pail batches.

In addition, we can take a little more water out of this pulp and ship it in only 4 pails.



<table border='1' bgcolor='#dbdbdb'>

<tr>

<td align='center' colspan='6'><strong>PULP SHIPPING CHARGE PER PAIL</strong></td>

</tr>

<tr>

<td align='left'>AL: $8.00</td>

<td align='left'>AR: $7.00</td>

<td align='left'>AZ: $14.00</td>

<td align='left'>CA: $18.00</td>

<td align='left'>CO: $11.00</td>

<td align='left'>CT: $9.00</td>

</tr>

<tr>

<td align='left'>DC: $7.00</td>

<td align='left'>DE: $7.00</td>

<td align='left'>FL: $9.00</td>

<td align='left'>GA: $8.00</td>

<td align='left'>IA: $7.00</td>

<td align='left'>ID: $16.00</td>

</tr>

<tr>

<td align='left'>IL: $5.00</td>

<td align='left'>IN: $4.00</td>

<td align='left'>KS: $9.00</td>

<td align='left'>KY: $5.00</td>

<td align='left'>LA: $9.00</td>

<td align='left'>MA: $9.00</td>

</tr>

<tr>

<td align='left'>MD: $7.00</td>

<td align='left'>ME: $9.00</td>

<td align='left'>MI: $5.00</td>	

<td align='left'>MN: $8.00</td>

<td align='left'>MO: $7.00</td>

<td align='left'>MS: $7.00</td>

</tr>

<tr>

<td align='left'>MT: $13.00</td>

<td align='left'>NC: $8.00</td>

<td align='left'>ND: $9.00</td>

<td align='left'>NE: $8.00</td>

<td align='left'>NH: $9.00</td>

<td align='left'>NJ: $9.00</td>

</tr>

<tr>

<td align='left'>NM: $13.00</td>

<td align='left'>NV: $16.00</td>

<td align='left'>NY: $9.00</td>

<td align='left'>OH: $5.00</td>	

<td align='left'>OK: $9.00</td>

<td align='left'>OR: $19.00</td>

</tr>

<tr>

<td align='left'>PA: $7.00</td>

<td align='left'>RI: $9.00</td>

<td align='left'>SC: $7.00</td>

<td align='left'>SD: $8.00</td>

<td align='left'>TN: $6.00</td>

<td align='left'>TX: $11.00</td>

</tr>

<tr>

<td align='left'>UT: $13.00</td>

<td align='left'>VA: $7.00</td>

<td align='left'>VT: $9.00</td>

<td align='left'>WA: $19.00</td>

<td align='left'>WI: $6.00</td>

<td align='left'>WV: $6.00</td>

</tr>

<tr>
				<td align='left'>WY: $13.00</td>
			</tr>

</table>



<p><hr>

<p><a name='COLORING PULP WITH PIGMENT:'><strong>COLORING PULP WITH PIGMENT</strong></a>
		<UL><LI>INTRODUCTION:

Pigments are like tiny, inert, colored rocks. Because pigments do not react chemically with anything, they tend to be very 

light fast, resisting fading in sunlight. Pigments can become �paint� when suspended in a binder such gum arabic for 

watercolor paint or acrylic medium for acrylic paint. Pigments are also an excellent material to color paper pulp. When 

pigment is used to color paper pulp, however, it is suspended in water with no adhesive-like binder as in paint.

Pigments are very easy to use and they mix together well in order to create subtly colored pulps. Primary pigments, such 

as yellow and blue, can be combined to create secondary colors, like green, before OR after the pigment is added to the 

pulp. For example, yellow pulp can be mixed with red pulp to create orange pulp. Several pigments or colored pulps can 

be mixed together to create taupe or mauve.<BR>

<LI>AN IN-DEPTH EXPLANATION OF THE PRINCIPLES INVOLVED IN COLORING WITH PIGMENT: Pigment sticks to paper pulp through a '+' and '-' attraction, a little like a magnet. The cellulose plant fiber (pulp) is anionic; which means that it has a negative charge which increases from the pounding or beating of the fiber during pulp preparation in the hollander beater. Dry, unbeaten, fiber has very little or no charge. As the fiber is beaten into a pulp, the '-' charge increases. Pigments have little attraction to the pulp, so a material which has a '+' or positive charge must be added. Retention agent is such a material, and its only purpose is to provide the '+' charge. Internal Sizing is a mild waterproofing agent which also happens to be cationic; it has a positive '+' charge. Consequently, Internal Sizing adds both waterproofing properties AND retention properties. The positive charge of the fiber depends on how much the fiber has been beaten in the pulping process, the longer it is beaten the greater the charge. Cationic materials (with a '+' positive charge) add to the charge. If you pour in too much retention agent,chowever, you can add too much '+' charge, the poles on the little magnets will reverse, and all the pigment will instantly come off the pulp. If this happens, just rinse the pulp, washing away the pigment and excess retention agent. Then add more pigment. You may not need anymore retention agent as there may still be enough on the fiber. If you feel like you need more, just add a tiny amount and dilute it a lot before adding it.

<BR><LI>DIRECTIONS FOR COLORING PULP WITH SIZING AND RETENTION AGENT: At Twinrocker, we color with pigments by first sizing pulp made from 3 pounds of dry fiber (1 beater load which is one 5 ga1. pail of ready-to-use pulp) with 50 ml. or 5 tbsp. of our Internal Sizing. We dilute the measured sizing in plenty of water (about a quart) and slowly add it to the pulp while stirring to mix it in evenly. This sizes the pulp and adds some '+' charge. We then add the pigment to the pulp. A tiny amount of red will create a pink pulp, more red, a more intense color. The liquid pigments are very concentrated, so begin by adding just a little. If you want an intense color, add a little pigment and mix it into the pulp well before you add more. Gradually build the intensity of the color . If you find you've added too much pigment for the color you want, just add more white pulp to make the color more pastel. After stirring the pigment into the pulp thoroughly, it should automatically stick to the pulp, and the water should be clear or almost so. If you still have pigment in the water after you have stirred the pulp thoroughly by hand, the pulp is saturated. The pigment has attached to most of the bonding sites on the fiber, and little if any more pigment will stick to the pulp. It is that simple. No rinsing is needed after coloring. Using this method, the pigment normally attaches to the pulp quite well without the addition of any retention agent. However, if the pulp has not been beaten very much and does not have a strong '-' charge, we then add a little, very dilute liquid retention agent (perhaps a tsp. of in a quart of water). We add it slowly while stirring so that the '+' charge is mixed in the pulp evenly and stop adding it when the color begins to attach to the pulp. If the color is too intense, you can lighten it or make it more pastel by adding white pulp. A couple of our customers, who are not pulping cotton linters in a hollander beater, but in a mixer or blender instead, color their pulp using retention agent in a slightly different manner. They dilute the pigment they want to use with water, then add a little retention agent to that pigment/water mixture, and then add that mixture to the pulp. If you are having trouble coloring fiber that has not been beaten in a hollander, try using our dry, powdered retention agent as it is stronger than the liquid material. We have found that the dry material tends to 'overcharge' fiber prepared in a hollander, however.
<BR><LI>COLORING PAPER PULP WITH PIGMENTS IS EASY AND A LOT OF FUN. This is a fairly detailed explanation of how pigments stick to paper pulp, but I could have said, 'It's magic.' That's how spontaneous and easy it is to do. Mixing colored pulps together to create art work is as easy as mixing colored paints together. If you should have a problem, just give us a call at Twinrocker, and we'll be happy to help you.
</ul>
		<hr>
		<p><a name='CUSTOM-COLORED PULP:'<strong>CUSTOM-COLORED PULP </strong></a>

<p>Your Ready-to-Use pulp can arrive already colored for you so all you need to do is take it out of

the pail and use it. If you want to order pulp in a certain color on an on-going basis and want it to

always be the same, we will create a personal formula for you which we can repeat again and

again. Talk to Kathryn Clark or Travis Becker about your needs. A single batch of 5 gallons of

pulp in a relatively simple color like dark grey, cream or red is priced at $15 per pail. Color

matching is charged by the hour or portion. 

<p><hr>



<p><hr>

<p><strong><a name='STORING PULP:'>STORING PULP:</strong></a>

<ul>

<li><strong>KEEP COOL: </strong>

<br>If you cannot use your pulp at one time, it is possible to store it for a while. An unopened pail of

cotton linters pulp will last up to several months, if kept cool. 



<li><strong>KEEP CLOSED: </strong>

<br>Opened pails won't last as long, due to molds & spores from the atmosphere. 



<li><strong>KEEP CLEAN: </strong>

<br>While you are using it, make sure your hands and your tools are clean. Reseal the lid as soon as

possible. 



<li><strong>ADD BLEACH:</strong>

<br>If you must store your opened pulp for a long time, add a capful of chlorine bleach to a gallon or

two of water. Add to the pulp and stir thoroughly. Let it sit for awhile, then rinse it thoroughly in

clean water. Any un-rinsed, residual chlorine bleach can attack the fiber, however. If you are truly 

concerned about the archival qualities of your paper, don't use bleach, order the amount of pulp you 

need when you need it.





<li><strong>ABACA WON'T LAST: </strong>

<br>Cotton pulps are the only ones that will last for more than a week or two. Fibers such as flax,

abaca, hemp, etc. just won't last...use immediately! 



<li><strong>FREEZING: </strong>

<br>You can freeze pulp. After defrosting you must use a mechanical stirring device such as a paint

stirrer chucked into an electric drill to get the lumps out. 









<p><a name='MAKING A PAPER CASTING:'><strong>MAKING A PAPER CASTING</strong></a>



<p>BRIEF INSTRUCTIONS FOR CASTING LOW RELIEF SCULPTURES WITH PAPER PULP<br>

I assume that you are using Twinrocker casting pulp, Ready-To-Use, cotton linters #27, beaten MC (medium coarse) with 

sizing and methyl cellulose.

<UL>

1. ADD WATER: Take some pulp out of the 5 gallon pail and add water, about three times as much water.<br>

2. STIR the dilute mixture of cotton pulp and water vigorously with your hands until it's as smooth as it can be, 

considering it is plant fibers and water.<br>

3. THE MOLD: If your piece is a medium to large bas relief, you will need sides on the mold about 1' high in order to hold 

enough pulp and water for your casting. Be sure that your mold is not absorbent to water.  If it is made of plaster, seal 

it with shellac or varnish.  Twinrocker sells an excellent urethane mold making material. It is a two part material which, 

when mixed together, will harden into a permanent mold for casting.<br>  

4. PARTING AGENT: Be sure to coat any mold with a parting agent, either the wax spray Twinrocker sells or a silicone spray 

for door locks.  Let it dry after spraying.<br>

5. POUR THE PULP & WATER MIXTURE INTO THE MOLD:  Be sure there's plenty of water in the mixture you pour.<br>

6. 'PATTY CAKE' (PAT, PAT, PAT) THE PULP WITH YOUR HAND:  This may be the most important step in the process.  By patting 

the pulp directly with your hand, you jiggle the pulp fibers down into the detail at the bottom of the mold.  This is also 

why it is so important to have enough water in the pulp mixture.  When you are learning, pat the pulp three times as much 

as you think you should.<br>

7. TAKE THE WATER OUT OF THE PULP MIXTURE WITH A SPONGE:  Begin by laying a 'real' cellulose sponge on top of the pulp and 

water mixture, when it has absorbed as much water as it can, squeeze it out and continue working  the  sponge  slowly  

around  the sculpture, absorbing water and squeezing out the sponge.  Slowly begin to PRESS as you absorb the water. 

Continue to do this until you are PRESSING AS HARD AS YOU CAN WITH THE SPONGE ON THE PAPER PULP AND TAKING AS MUCH WATER 

OUT AS YOU CAN.<br>

8. DRY THE CASTING in the air.  Bear in  mind  that  warping  is  caused  by  one  area  (or areas) drying before the rest 

of the piece.  If you place it in the. sun or use fans be sure to cover the surface of the casting with something absorbent 

like a terrycloth towel or blotters.  A warm, dry chamber with little air movement is ideal, like a closet with a 

dehumidifier in it.  With a medium to large mold, you may need to weight the edges of the casting to keep them from warping 

up. DON'T TRY TO TAKE THE SCULPTURE OUT OF THE MOLD UNTIL IT IS COMPLETELY DRY!!<br>

</ul>

</td></tr></table>";	// end of php echo cmd.


?>
                               