<?php
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 


  include ('book_sc_fns.php');
  // add_stat_2_cart.php: 12/21/03 ref&a (converted from add_codes.php. Data entry form to allow user
  // to add (1) items codes and qty (with lookup), and (2) notes/comments lines
  // into the order. 10/17/02
  session_start();

  if (isset($_POST["submit"])) // Data was entered and this adds VALID items to cart
  {
    $stylecode = $_POST['stylecode'];
    $price = $_POST['price'];
    $color = $_POST['color'];
    $qty = $_POST['qty'];

    $itemno = 'Y'.strtoupper($color).$stylecode;   //Format of stationery itemno
    
	   $xcart = $_SESSION["cart"];

/*
	   for ($n=0; $n<count($items); $n++)
	   {
   	    if (strlen(trim($items[$n])) > 0)
		    {
			     //validate, is it a real item?
			     $items[$n] =strtoupper($items[$n]);
			     $detail = get_invt_details($items[$n]);
			     if ($detail)
			     {
				      $descrip[$n] = $detail['descrip'];
				      $price[$n] = $detail['price'];
				      $cat = $detail['catid'];
		       }
			     else
			     {
				      //not valid code...
				      $descrip[$n] = "** Invalid Item Code **";
				      $price[$n] = '0.00';
				      $qty[$n] = 0;
			     }
			
			     if ($qty[$n] <= 0)
				      $codemsg .= $items[$n].'   '.$descrip[$n].'<br>';
			     else
			     {
*/
				      //  add qty to cart..
				      for ($i=0; $i<$qty; $i++)
				      {
					       // Pass the itemno. If item is in array, increment qty,
					       // else add item and qty=1 to array.
					       $itemno = $items[$n];
    				     if(@$xcart[$itemno])
      					   $xcart[$itemno]++;
    				     else
      					   $xcart[$itemno] = 1;
				      }
/*
			     } //endif adding this item to cart
		    } //endif itemcode not blank
				
	   } //endfor
	   
	   if ($codemsg <> '')
	   	 $codemsg = '**** NOT PROCESSED ****<br>'.$codemsg.'<br>***********************';
*/

	$cart = $xcart;
	$_SESSION['total_price'] = calculate_price($cart);  
    $_SESSION['total_weight'] = calculate_weight($cart);
    $_SESSION['items'] = calculate_items($cart);
	
	$_SESSION['cart'] = $cart;
	

  }  //endif SUBMIT

header("location:st_pricelist.php");
exit;

?>
