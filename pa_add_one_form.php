<?php
// pa_add_one_form.php ref&a 8/23/04
// Intermediate processing page: receives an itemno clicked on the paper.php table: presents form
// to get qty and surface finish;  calls pa_add2_cart.php
// Pulled form/process code from pa_main.php
// php 5.30
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

  include ('book_sc_fns.php');
  session_start();

  $itemno = $_GET['itemno'];   //7/30/04 Pass the itemno, then arrange for added cp/hp price add-on elsewhere
  $descrip = $_GET['descrip']; // Descrip from invt; may need to be modified for \CP \HP
  $price = $_GET['price'];
  $price = $_GET['onhand'];
  //$surface = $_POST['surface']; // Can be  /CP, /HP, /BP, hp, r
  $qty = 0.00; //$_POST['qty'];

  $anchor = $_GET['anchor'];   // Use to return to page/item


  $thisitem = $itemno;
  $thisdescrip = $descrip;
  $thisprice = $price;
  $thisonhand = $onhand;

  echo $itemno.' '.$descrip.'  '.$price.'  '.$onhand;
  

  if (preg_match('%/CP%',$thisdescrip))
      $thiscp = 'cp';
  if (preg_match('%/HP%',$thisdescrip))
      $thiscp = 'hp';
  if (preg_match('%/R%',$thisdescrip))
      $thiscp = 'r';

  echo '<table><tr>
                <td class="pa_detail_line">'.
                $thisdescrip.
                '</td>
                <td class="pa_detail_line" align="right" >'.number_format($thisonhand,0).'
                </td> <td class="pa_detail_line">$ '.
                number_format($thisprice,2).
                '</td><td align="right" class="pa_detail_line">
                 <form method=post action=pa_add2_cart.php>
                 <input type=hidden name=itemno value="'.$thisitem.'">
                 <input type=hidden name=descrip value="'.$thisdescrip.'">
                 <input type=hidden name=anchor value="'.$thistype.'">
                 <input type=hidden name=price value="'.$thisprice.'">
                 ';

                 // If paper is marked r or hp that is the way it goes
                 // If /CP then can be changed  to HP or BP  with extra charge
                 switch ($thiscp)
                 {
                    case 'cp':
                      echo "<select style='font:8pt Arial' name=surface size=1>
                          <option selected value='/CP'>Cold Pressed</option>
                          <option  value='/HP'>Hot Pressed</option>
                          <option  value='/B'>Both</option>
                          </select>";
                          break;
                    case 'hp':
                      echo 'Hot Pressed
                      <input type=hidden name=surface value="'.$thiscp.'">';
                      break;
                    case 'r':
                      echo 'Rough
                           <input type=hidden name=surface value="'.$thiscp.'">';
                      break;

                 }
                 echo "
                      </td><td class=st_order_form valign=Middle><input style='font:8pt Arial' type=text name=qty value=0 size=6 maxsize=4>
                      </td><td class=st_order_form valign=Middle><input style='font:8pt Arial' type=submit name=submit value='Add'>
                      </form>
                      </td></tr><table>";
                      
  //header("location:paper.php?anchor=yes#$anchor");
  exit;


                      
?>

