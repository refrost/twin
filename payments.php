<?php  

// Search on : ****  Text of messages in emails goes here
// to edit email content. 

// In production, session[UNAME] not searchstr to force login;
//                activate ccval on ccnum
// 06/29/2012 rev. added billtoname  to handle case 
//            where Name on Card is different from Bill to/Web Customer. 
//            We append these vars to shipto address  string.   Also
//            revised hidden var in form created in get_cc_info() in cust_fns.php
// php 5.30

session_start();

// 5/30/2017 prevent No ship state, $0 charge 
if ($_SESSION['ship_string'] == 'No State Selected')
{
  // Shipping must be set
  echo 'Shipping must be set. Click link to continue.';
  echo '<br /><a href="shipaddr.php">Select Ship Address</a>';
} 
//echo '<br />String: '.$_SESSION['ship_string']; 
//exit;

if ($_SERVER["SERVER_NAME"]=="127.0.0.1"  || $_SERVER["SERVER_NAME"]=="localhost")
{
  	// working locally and no session or cart is empty
  	if (!isset($_SESSION['searchstr']) || !isset($_SESSION['final']))      // UNAME
  	{
  	 header("location:showprod.php"); //don't come here directly
    exit;
   }
   else
   {
        echo 'Local - no payments.';
        //exit;    // exit during production
   }
}
else
{
 	if (!isset($_SESSION['SESSION_UACCT']) || !isset($_SESSION['final']) )  // Customer has an order $_SESSION['searchstr']
 	{
     header("location:showprod.php"); //don't come here if no order
     exit;
  }
// 8/5/21 cmtted too many redirects
//	if ($_SERVER["SERVER_PORT"] != "443")
//  {
//  		header("location:https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
//      exit;
//  }
}


include('book_sc_fns.php');  
include('ccval.php');

/*
if form is submitted, put  vars into the array.
  if all fields are valid with no errors
    then process the order
    (
     Process: check_datetime_of_last_order()
              get_order_number()
              make_order_header()
              make_order_detail()
              encrypt and send order & payment info
    )
  else
    send the error messages back to the payment form

  if the form is not submitted then initialize the vars from the cust record
  display the form the first time.
*/

$message = array();
 
if (isset($_POST['submit']))   // Vars submitted from form...
{
  // Get vars passed from form and put them in array
	$cust['name']=strtoupper($_POST['name']);
    $cust['billtoname']=$_POST['billtoname'];
	$cust['email']=$_POST['email'];
	$cust['address1']=strtoupper($_POST['address1']);
	$cust['city']=strtoupper($_POST['city']);
	$cust['state']=strtoupper($_POST['state']);
	$cust['zip']=strtoupper($_POST['zip']);
	$cust['country']=strtoupper($_POST['country']);
	$cust['country_code']=strtoupper($_POST['country_code']);
	$cust['phn_area']=$_POST['phn_area'];
	$cust['phn_prefix']=$_POST['phn_prefix'];
	$cust['phn_suffix']=$_POST['phn_suffix'];
	//CC values
	$cust['method']=$_POST['method'];
	//$cust['cctype']=$_POST['cctype'];
	$cust['ccnum']=$_POST['ccnum'];
	$cust['ccmonth']=$_POST['ccmonth'];
	$cust['ccyear']=$_POST['ccyear'];
	$cust['cvv2']=$_POST['cvv2'];
	$cust['ordertoken']=$_POST['ordertoken'];
    $cust['order_name']=$_POST['order_name'];
    $cust['order_comment']=$_POST['order_comment'];
    $cust['order_comment'].=' ['.$cust['billtoname'].']';

    // Now validate fields and create error messages
	// Check the name.  Was: eregi ("^([[:alpha:]]|-|'| )+$"
	if (preg_match("%^[a-z\-\'\ ]*$%i", $cust['name'])) {
		$a = TRUE;
	} else {
		$a = FALSE;
		$message[] = "Please enter the name exactly as it appears on the credit card. Letters or space.";
	}

    // /^[a-z0-9_-]{3,16}$/     3-16 char alphanum with hypen or dash rexex pattern
  	// Check to make sure they entered a valid email address.
	$preg_valid_email = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i'; 
    if (preg_match($preg_valid_email, $cust['email'])) {
			$f = TRUE;
	} else {
			$f = FALSE;
			$message[] = "Please enter a valid email address.";
	}

	// Check for an address line.
	if (preg_match("%^([[:alnum:]]|.|#|,|-|'| )+$%i", $cust['address1']))  {
		$g = TRUE;
	} else {
		$g = FALSE;
		$message[] = "Please enter your billing address using letters and numbers.";
  }

  	// Check for a city.
	if (preg_match("%^([[:alnum:]]|-|'| )+$%i", $cust['city']))   {
		$g = TRUE;
	} else {
		$g = FALSE;
		$message[] = "Please enter your city. Letters only.";
  }

	// Check for a state.
	if (preg_match("%^([[:alnum:]]|-|'| ){2,}$%", $cust['state']))    {
		$g = TRUE;
	} else {
		$g = FALSE; 
		$message[] = "Please enter your state.";
  }

	// Check the zipcode.
	if (preg_match("%(^([[:digit:](4,12)]|-)+$)|(^[A-Z].*)%S", $cust['zip'])) {
		$j = TRUE;
	} else {
		$j = FALSE;
		$message[] = "Please enter the  zipcode  as 47923 or 47923-0433, or Euro formats.";
	}
  // Check the phone....
	if (preg_match("%[0-9][0-9][0-9]%", $cust['phn_area'])
       && preg_match("%[0-9][0-9][0-9]%", $cust['phn_prefix'])
       && preg_match("%[0-9][0-9][0-9][0-9]%", $cust['phn_suffix']) )
  {
		$k = TRUE;
	} else {
		$k = FALSE;
		$message[] = "Please enter the  phone number as 765 555 1212.";
	}

  if ($cust['method']=='ccphone')
  {
      //customer intends to pay by phone
      // Reset certain vars

      $cust['ccnum']='';
	  $cust['ccmonth']='';
	  $cust['ccyear']='';
	  $cust['cvv2']='';
  }
  else
  {
      // Customer entered CC online  -- validate the CC info
	    if (IsCCNumberValid($cust['ccnum']))
        $a = true;
      else
		    $message[] = "Problem with Credit card number. Please re-enter";

      // Check expiration date
      $expdate = $cust['ccmonth'].'/01/'.$cust['ccyear'];
      if (strtotime($expdate)<strtotime('next week'))
        $message[] = "Card has or is about to expire. Please use another card."; //.strftime('%c',strtotime($expdate ))." < ".strftime('%c',strtotime('next week'));

	    // Check on cvv2
      if (preg_match("%[0-9]{3,4}%", $cust['cvv2']) || empty($cust['cvv2']) )
        $a = true;
      else
        $message[] = "Please enter numbers or spaces in the Cvv2 code.";
  }
  
  $cust['order_name']=addslashes($cust['order_name']);
  $cust['order_comment']=addslashes($cust['order_comment']);
  
  ///////**************************************************\\\\\\\\
	// DONE VALIDATING:  if no error messages then PROCESS this order

	if (!$message)  //Submitted valid data. Save order then commit
   {
     // WE EXIT OUT THROUGH THIS HOLE, otherwise user uses menu or
     //    browser and does not complete the order.
     //
     // 1. Make sure this order has not already been added to orders

     // disable while developing...
     
     $mval = is_order_saved($cust['ordertoken']); //data_fns returns 1 if token has been saved..
     if ($mval== 1)  //check to see if token is in order file.
     {
        // Order has already been saved. We should clearcartcode here
        // and then msg...
        pass_msg($cust['ordertoken']." This order and payment has already been recorded. Please Clear your Cart.");
     }

     // 2. Get next order number:
     $sono = get_next_number('ai_orders');  //data_fns
     if (!$sono)
        pass_msg("Paymt.001 - System failed to save your order.  Please try again.");

     // 8/28/03 This next section was initially to create strings from the arrays
     // We use most of it below when saving to the orders file (3...)
     // Put all BILLING values from the payment form into a
     // comma-separated string that is not easy to read
     $billing='';
     foreach ($cust as $value)
     {
      $billing.= ",'".$value."'";
     }

     $billing = substr($billing,1); //Removes leading comma

     //Now get the customer record
     	$custarray = get_custarray($_SESSION["SESSION_UACCT"]);
     	// Set vars used in calculating totals (below...)
     	$bill_state = $custarray['state'];
  		$tax_id = $custarray['taxexemptid'];
		$discount_type = $custarray['terr'];

     //Next get the SHIPTO  RECORD
 			if (isset($_SESSION["SESSION_SHIPID"]) && $_SESSION["SESSION_SHIPID"] <> '0')
			{
				// If the shipid is set to -1 display the billto as the shipto..
				if ($_SESSION["SESSION_SHIPID"]== -1)
				{
					$shiparray = $custarray;
				}
				else
				{
				    //Otherwise go get the ship address
					$shiparray = get_shiparray($_SESSION["SESSION_SHIPID"]);
				}
			}
			else
			{
				$shiparray = $custarray;
			}

  	 if (!isset($shiparray) || $shiparray==false)
				$shiparray = $custarray;

     // Next, get items in carts
     // initialize strings and puts itemno,qty, etc into strings
     $items='';
     $fiber='';
     $pulp='';
     $paper='';  // Added on 8/20/04
     $amounts='';
     
     
     if (isset($_SESSION['cart']))
     {
        $cart = $_SESSION['cart']; //$items;
        foreach ($cart as $key=>$value)
        {
          $items.= "$key, $value<br>";
          //if item is paper or stationery  10/08/04 prepare to update onhand qty in invt
          if (left($key,1)=='Z' || left($key,1)=='Y') 
          {
  		      $invt_updt_list[$key] = $value;
          }
        }
     }
     if (isset($_SESSION['fibercart']))
     {
        $fibercart = $_SESSION['fibercart'];
        foreach ($fibercart as $key=>$value)
        {
          $fiber.= join(',',$value).'<br>';
          //'FI'.$value['item'].','.$value['description'].','.$value['price']$value['unitwt'].','..','.$value['qty'].','.$value['price'].','.$value['totalamount'].'<br>';
        }
     }
     

     if (isset($_SESSION['pulpcart']))
     {
        $pulpcart = $_SESSION['pulpcart'];
        foreach ($pulpcart as $key=>$value)
        {
          $pulp .= join('|',$value).'<br>';
        }
     }
     
     if (isset($_SESSION['papercart']))
     {
        $papercart = $_SESSION['papercart'];
        foreach ($papercart as $key=>$value)
        {
          $paper.= join('|',$value).'<br>';
          //Add items to list to update onhand qty
          $invt_updt_list[$value['item']] = $value['qty'];
        }
     }

    //INVENTORY UPDATE: Update aloc in INVT file [only for Z and Y items]
    //  invt values are uploaded via  routines in \\graphic\ecommerce\vfp_code\invtload.prg
    //  That routine gets invt items from sbt and initializes them.
    if (isset($invt_updt_list))
    {
        reset($invt_updt_list);
        // open the invt file
        //loop through array and update onhand and aloc
        $conn = db_connect();
        
        
        foreach($invt_updt_list as $key => $value)
  	    {
  	      // In invt file we just increase aloc. Displays of paper, stationery, etc. show as onhand-aloc
  		    $query = "update invt set aloc = aloc + $value 
                            where item = '$key' ";
          $result = @mysqli_query($conn, $query);
          
          //echo 'invt '.$result.'  '.$query.'<br>';
          
          if (left($key,1)=='Y')
          {
              //If item is stationery, reduce the right value in the stationery table
              //only if one of the four stocked colors...  whap,whsc,srec,simo 
              $mstyle = right(trim($key),4); //style  ex.  002_
              $mcolor = substr($key,1,4);       //color ex.  WHAP,WHSC,CREC,SIMO, or other
              $stocked_colors = array('WHAP','WHSC','CREC','SIMO');
              if (in_array($mcolor,$stocked_colors))
              {
                  //Reduce the qty in the stationery table for the stocked colors
                  $mcolor = strtolower($mcolor);  //Field name is lower case
          		    $query = "update stationery set `$mcolor` = `$mcolor` - $value 
                            where stylecode = '$mstyle' ";
                  $result = @mysqli_query($conn, $query);
              }
              //echo 'stat '.$result.'  '.$query.'<br>';
              //echo $result.'  '.$query;
              //exit;
           }
   	    }
     }
     // Get order totals...
     // OLD before customer discounts:  $totals = get_order_totals($_SESSION["total_price"],$_SESSION["fiber_price"],$_SESSION["ship_charge"],$_SESSION["pulp_price"],$_SESSION["paper_price"],$bill_state,$tax_id,$discount_type);
     $totals = get_order_totals($_SESSION["total_price"],$_SESSION["total_discount"],$_SESSION["fiber_price"],$_SESSION["ship_charge"],$_SESSION["pulp_price"],$_SESSION["paper_price"],$bill_state,$tax_id,$discount_type);


     $amounts.=$totals['subtotal'].','.$totals['disc_text'].','.$totals['discount'].','.$totals['s_charge'].','.$totals['tax_text'].','.$totals['tax'].','.$totals['total_order'].','.$totals['tax_id'].'<br>';

     $frtstring = trim($_SESSION['ship_string']);

     // --- Done making text strings of the order data
     // --- Now we are going to save the info and get the web order number

     //$webno = save_order($billing,$custarray,$shiparray,$items,$fiber,$pulp,$frtstring,$amounts,$totals);

     // 3. Save order info into orders table

     // Need the webid of the customer saved in the order file
     $webno = $_SESSION['SESSION_UACCT'];
     // Need the shipid
     if (isset($_SESSION["SESSION_SHIPID"]))
        $shipid = $_SESSION["SESSION_SHIPID"];
     else
        $shipid = -1;

     //We will pass the ccnumber to the encrypted email routine soon
     $ccnumber = $cust['ccnum'];
     //But we will save only the "safe" partial ccnumber in the file.
     $ccsafe_number = substr($cust['ccnum'],0,1).'-'.substr($cust['ccnum'],-4);

     $conn = db_connect();

     // NOW SAVE THE ORDER DISCOUNTS, if any:
     // Added 6/1/2013 to record actual discounts and ease reporting
     if (isset($_SESSION['dc']) && $_SESSION['dc'])  {
        //print_r($_SESSION['dc']);
        $disc =  $_SESSION['dc'];
        //Create array for each record and then do multiple records in one insert  
        //also my imploding the array of values
        //
        $dsql = array();
        foreach ($disc AS $dc) {
            $dsql[] = "(null,".$sono.",NOW(),'".$webno."','".$dc['code'].
                        "','".$dc['type']."','".$dc['item']."','".$dc['expires']."','".$dc['cart_item'].
                        "',".$dc['cart_qty'].",".$dc['amt'].")";
                        
        }

//         $query ="INSERT INTO `orders_discounts` (`id`, `sono`, `date`, `webid`, `code`, `type`,
//                    `item`, `expires`, `cart_item`, `cart_qty`, `amt`) 
//                     VALUES ".implode(',', $dsql);
//         echo $query;
//         exit;

        $query =  "INSERT INTO `orders_discounts` (`id`, `sono`, `date`, `webid`, `code`, `type`, `item`, `expires`, `cart_item`, `cart_qty`, `amt`) VALUES ".implode(',', $dsql);           
        $result = @mysqli_query($conn, $query);
        //echo $query;
        //exit;

	    if (!$result)  //Insert failed...
	    {
          pass_msg("Paymt.002A - System failed to save your discounts/order.  Please try again.");
        }
     
     }


     // NOW SAVE THE ORDER TO FILE AS READABLE 'Cart-Strings'
     $query = "insert into orders set sono=$sono,
        date = NOW(),order_status = 'WebStat-1',
        custno = '".$custarray['custno']."',
        order_name ='".addslashes($cust['order_name'])."',
        order_token = '".$cust['ordertoken']."',
        company = '".addslashes($custarray['name'])."',
        name='".addslashes($cust['name'])."',
        email='".addslashes($cust['email'])."',
        address1 ='".addslashes($cust['address1'])."',
        city='".addslashes(ucwords(strtolower($cust['city'])))."',
        state='".addslashes($cust['state'])."',
        zip='".addslashes($cust['zip'])."',
        country='".addslashes($cust['country'])."',
        country_code='".addslashes($cust['country_code'])."',
        phn_area='".addslashes($cust['phn_area'])."',
        phn_prefix='".addslashes($cust['phn_prefix'])."',
        phn_suffix='".addslashes($cust['phn_suffix'])."',
        ccmethod = '".addslashes($cust['method'])."',
        ccnumber = '".$ccsafe_number."',
        ccmonth = '".$cust['ccmonth']."',
        ccyear = '".$cust['ccyear']."',
        cvv2 = '".addslashes($cust['cvv2'])."',
        webid = '".$webno."',
        shipaddrid  = '".$shipid."',
        subtotal = '".$totals['subtotal']."',
        disc_text = '".$totals['disc_text']."',
        discount = '".$totals['discount']."',
        s_charge = '".$totals['s_charge']."',
        tax_text = '".$totals['tax_text']."',
        tax = '".$totals['tax']."',
        total_order = '".$totals['total_order']."',
        tax_id = '".$totals['tax_id']."',
        items1 = '".addslashes($items)."',
        items2 = '".addslashes($fiber)."',
        items3 = '".addslashes($pulp)."',
        items4 = '".addslashes($paper)."',
        ship_string = '".addslashes($frtstring)."',
        order_comment='".addslashes($cust['order_comment'])."'";

       // 6/4/2010 not removed yet:  cvv2 = '".addslashes($cust['cvv2'])."',

   	   $result = @mysqli_query($conn, $query);
	   if (!$result)  //Insert failed...
	   {
          //echo $query;
          //exit;
          pass_msg("Paymt.002 - System failed to save your order.  Please try again.");
     }
 
     // 4.  Encode strings before sending

     //$billing = base64_encode(serialize($billing));
     //$custinfo = base64_encode(serialize(get_final_cust_info($custarray)));
     //$shipinfo = base64_encode(serialize(get_final_ship_info($shiparray)));
     //$items = base64_encode(serialize($items));
     //$fiber = base64_encode(serialize($fiber));
     //$pulp  = base64_encode(serialize($pulp));
     //$shipping = base64_encode(serialize($frtstring));
     //$amounts = base64_encode(serialize($amounts));
     
     //TESTING: print to screen  (<br> s/b \n)
     /*
     echo unserialize(base64_decode($billing)).'<br>';
     echo unserialize(base64_decode($custinfo)).'<br>';
     echo unserialize(base64_decode($shipinfo)).'<br>';

     echo unserialize(base64_decode($items)).'<br>';
     echo unserialize(base64_decode($fiber)).'<br>';
     echo unserialize(base64_decode($pulp)).'<br>';
     echo unserialize(base64_decode($shipping)).'<br>';
     echo unserialize(base64_decode($amounts)).'<br>';
     */

     // Now we pass the order, customer, ...tokens/keys and ccnum, encoded,
     // to the encrpytion/email routine
     $a  = $cust["ordertoken"];
     $b  = $webno;
     $c = base64_encode(serialize($ccnumber));
     $d = base64_encode(serialize(get_final_ship_info($shiparray)));
     
     // ****  Text of messages in emails goes here
     if ($cust['method']=='cconline')
        $methodstr = "  This amount will be charged to the card beginning and ending like: $ccsafe_number when the order ships.
                        <br><strong>Thank you for your online order!</strong>";
     else
        $methodstr = ". Unless you are paying by institutional purchase order or your payment info is already on file,
                        please print this screen and then call $g_payment_phone  with your payment information.
                        No action will be taken on your order until payment information is called or mailed in.
                        Include the Reference #$sono and your name. <br>
                        <strong>Thank you for your online order!</strong>";
        
     $e = base64_encode(serialize('Order Ref# '.$sono. " totals $".number_format($totals['total_order'],2)." $methodstr"));
     $f = $sono;
     
     
     // Calling sequence is payments->commitcgi->order_conf->showprod

     // 5.  NOW call commitcgi.php to send encrypted data to Office
     if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost")
     {
        //If dev, send to local copy for test
        header("location:commitcgi.php?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f");
        exit;
     }
     else
     {
        // On Server, so execute in cgi-bin (as user) to get gpg and create/delete file.
        header("location:https://www.twinrockerhandmadepaper.com/cgi-bin/commitcgi.php?a=$a&b=$b&c=$c&d=$d&e=$e&f=$f");
        exit;
     }

     /*pass_msg("**TEST** Transaction # $sono completed successfully.<br>
               $".$totals['total_order']."charged to ".$ccsafe_number."<br><br>
                If you wish to browse more, please <a href=clear_cart.php?catid=home>Clear Cart</a> now.
                <br><br> For security reasons, before exiting the site, please <a href=weblogoff.php>Logout</a>");
     */
  }
  else
  {
      //Will redisplay payments.php form. Originally show only the first and the last four digits of the ccnum
      //$cust['ccnum'] = substr($cust['ccnum'],0,1).'-'.substr($cust['ccnum'],-4);
      // 11/14/03 -- We will redisplay the cc number. per HClark
  }
}  // if form was submitted      
else
{
  // Form was not submitted...  Initialize vars and display Payments.php form.
	if (isset($_SESSION["SESSION_UACCT"]))
	{
	   //get billto for the current customer..
		  $custarray = get_custarray($_SESSION["SESSION_UACCT"]);
      $cust['name']=strtoupper($custarray['firstname'].' '.$custarray['lastname']);  //show name per HC
      $cust['billtoname']=ucwords(strtolower($cust['name']));
	    $cust['email']=$custarray['email'];
	    $cust['address1']=strtoupper($custarray['address1']);
	    $cust['city']=strtoupper($custarray['city']);
	    $cust['state']=strtoupper($custarray['state']);
	    $cust['zip']=strtoupper($custarray['zip']);
		  $cust['phn_area']=$custarray['phn_area'];
		  $cust['phn_prefix']=$custarray['phn_prefix'];
		  $cust['phn_suffix']=$custarray['phn_suffix'];
      //$billtophone=$cust['phn_area'].'/'.$cust['phn_prefix'].'-'.$cust['phn_suffix'];
	    //CC values
	    $cust['method']='cconline';
	    //$cust['cctype']='';
	    $cust['ccnum']='';
	    $cust['ccmonth']='';
	    $cust['ccyear']='';
	    $cust['cvv2']='';
	    // Insert a unique token in the form that will prevent
       // the order from being inserted more than once (on resubmit)  action_fns.php
	    $cust['ordertoken']=get_uniqueid();
	    $cust['order_name']='MyOrderMemory';
	    $cust['order_comment']='';
  }
  else
 	{
		//Should never get  here:  get billto as blanks..
      $cust['name']='';
      $cust['billtoname']='';
      //$billtophone='';
	    $cust['email']='';
	    $cust['address1']='';
	    $cust['city']='';
	    $cust['state']='';
	    $cust['zip']='';
		  $cust['phn_area']='';
		  $cust['phn_prefix']='';
		  $cust['phn_suffix']='';
	    //CC values
	    $cust['method']='';
	    //$cust['cctype']='';
	    $cust['ccnum']='';
	    $cust['ccmonth']='';
	    $cust['ccyear']='';
	    $cust['cvv2']='';
	    $cust['ordertoken']=get_uniqueid();
	    $cust['order_name']='MyOrderMemory';
	    $cust['order_comment']='';
  }

}

//Display the HTML form
include ('tr_header.php');
include ('cust_fns.php'); //Country/phone codes lookups and get_cc_info()
echo "<table align=$g_table1_align width=$g_table1_width border=1 bgcolor=$g_table1_bgcolor> <tr><td>";
get_cc_info($cust,$message,$g_payment_phone);  //cust_fns.php:  displays the form.
echo "</td></tr>
      <tr><td>
      <div align=left>&nbsp;&nbsp;Secure web communication
                <br />&nbsp;&nbsp;$g_ssl_img</div>
      </td></tr></table>";
//
// <a href=s://rapidssl.com target=_blank><img src=graphics/rapidssl.gif border=0></a>
echo "</td></tr></table>";
silk_footer($g_table1_align,$g_table1_width);
?>
