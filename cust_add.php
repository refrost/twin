<?php
// include function files for this application
require_once("book_sc_fns.php"); 
session_start();
// Called from get_customer() in action_fns.php:   get_addcustzip($this_zip);

//do_html_header("","home"); //New customer profile
	include('tr_header.php');
	if (isset($_GET["this_zip"]))  
  	{	
  		$this_zip = $_GET["this_zip"];
  	}
	else
		$this_zip = '';
	
	//find  city,state, zip from zip5 datafile, then initialize...	
	$location = ziplookup($this_zip);  	
	$city = $location['city'];
	$state = $location['state'];
	$zip = $location['zip'];
	
	//if ($location)
	//		echo "Found >> &nbsp;&nbsp;&nbsp; $city   $state   $zip  ";

if (isset($_POST["cancel"]))		// CANCEL and go to home page
{
	header("location:showprod.php");
	exit;
}		
			
	if (isset($_POST["addcust"]))	
	{
		// if the addcust form has been submitted
		// then validate and process
		
		
		
		$cust = $_POST;
		// validate... NEED!! 10/16/02 -ref-
		// name, first, last, phone#, zip filled in
		// check for dups in customers, if present, display that profile
		// if valid, insert
		
		// DISABLED 8/25/03: creates custno from initials of f/l name + phone or an incrementing number
		// Add new record into file ; this fns insert new uniqueid for webid
		
		$this_id = cust_insert($cust,'customers');//function is in data_fns.php

    echo 'cust_add '.$this_id;
		
		if($this_id != 0)    // Query didn't work
		{
			$cust = get_custarray($this_id);
			// set this customer as the current customer in session vars..
					
		  // State the Firstname+lastname
		  $this_name = stripslashes($cust["firstname"]).'&nbsp;'.stripslashes($cust["lastname"]);
        $_SESSION["SESSION_UNAME"] = $this_name;
    
		  // Fix user record ID
	     $_SESSION["SESSION_UID"] = $cust["id"];
				
		  // include the custno
	     $_SESSION["SESSION_UACCT"] = stripslashes($cust["webid"]);

		  // get ship_state, too
		  if (isset($cust["state"]))
			   $ship_state= stripslashes($cust["state"]);
		  else
			   $ship_state= '==';
		
        $_SESSION["ship_state"] = $ship_state;

			// -ref- 10/16/02
			
			// Send to a new page so 'refresh' won't add again...
			$msg = "New customer: $this_name, was added";
      pass_msg($msg);
		}
    else
		{
			// There was a problem. try again.

     	//$msg = "PROBLEM!!  Profile could not be updated.<br>";
     // pass_msg($msg);

      echo "<br>PROBLEM!!  Profile could not be updated.<br>";
			display_cust_form($cust);
		}
	
	//	br();
	//	do_html_url("showprod.php", "Continue");
	}
	else
	{
		//initialize blank profile array
		$cust = initialize_blank_profile_array();
		//Set values to those from the ziplookup			   
		if ($city)
		{
			$cust['city'] = $city;
			$cust['state'] = $state;
			$cust['zip'] = $zip;
		}

		display_cust_form($cust);
	}
	
//do_html_footer();

/*function val_cust()
{
name sdssdsfsdf 
custno ^new^
firstname wewev
lastname fdfyhb
email dssdsb 
webaddress sdsdsdf 
webdescription 
phn_area 345
phn_prefix 554
phn_suffix 3423
besttime 
cell_area 494
cell_prefix 785
cell_suffix 3459
fax_area 
fax_prefix 
fax_suffix 
address1 Left street
address2 Box 23
address3 
city Pungoteague
state VA
zip 23422
country USA
taxexemptid sdfsdfsdeee543
enews on
specials on
type inet
source inet
code ar
pterms Net 
entered 2002-08-23
ldate 
lsale 0
notes 
}
*/
?>
