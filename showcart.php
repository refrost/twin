<?php
// php 5.3 6/26/2015   (6/5/10)
// 6/3/2010 added papercart for tube (action_fns) in calc_shipping()
// 6/3/2010 rev $_SESSION{} -> []
// 12/05/2010  has_envelopes() - disable rush if envelope order
// 5/9/2011 has_pulp() -- in action_fns -  disable rush if pulp
// 4/12/2013  
// 5/10/2013 Implemented discounts: tr_fns; showcart; tr_checkout
//  html tooltip icon:  <span title="Enter discount codes separated by commas"><img  src="graphics/info.png" height="20px" align="top" /></span>
// rev 8/30/2017: Major edit removed all
//     checks/switches for/to https/443. 

  // showcart.php is the web version of show_cart.php
  // created 2/13/03 to match look/feel of showprod
  // called to (new & addonly) from main lists ,
  // and from recalculate button on show_cart (save).

  include ('book_sc_fns.php');
  session_start();

  if (!isset($_SESSION['searchstr']))
  	include('set_vars.php');

    
  if (isset($_GET['catid']))
  	    $catid = $_GET['catid'];
  else
  {
  	    if (isset($_POST['catid']))
			$catid = $_POST['catid'];
		else
				$catid = $_SESSION["catid"];
  }

  //  "View cart".
  $jscript='validateInteger';
  //$tooltip=true; //4/30/2013 discountcode  tr_header..  Didn't need/use -- screwed up title styling..
  include ('tr_header.php');		// This is Web version of cart
 
  // echo $_SESSION['SESSION'].' '.$_SESSION["ship_state"].'  '.$_SESSION["SESSION_UACCT"]; 	//xxx		
  
   echo "<table class=cart border=1 align=right width='$g_table1_width'><tr>
   		 	<td >";
   echo '<center>View/Edit Shopping Cart Contents</center>';
	
  $cart = $_SESSION['cart'];
  $fibercart = $_SESSION['fibercart'];
  $pulpcart = $_SESSION['pulpcart'];
  $papercart = $_SESSION['papercart'];
  
  $dcodes =   $_SESSION['discount_codes'];      // 5/10/2013 discount codes set in set_discount_codes.php

//12/5/2010 Set var and session_var to exclude Rush if order includes envelopes (need time to fold)
//     fns in action_fns.php
has_envelopes();
// 05/09/2011 Set vars -- disable if pulp is ordered -- no rush on pulp orders
// Must be set before calc_shipping - fns in action_fns.php
has_pulp();
	
// If the cart have items, display each cart in separate band (items and fiber)
if (isset($cart)&& count($cart) || isset($fibercart)&& count($fibercart) || isset($pulpcart)&& count($pulpcart) || isset($papercart)&& count($papercart))
{
	if ( isset($_SESSION["ship_state"])  && $_SESSION["ship_state"] != '=='  )
	{
       
        // Next thing also calcs shp for paper in deeper function 7/30/04
        // 5/7/2013 Also extend to calc discounts...
		calc_shipping($_SESSION["ship_state"],$_SESSION["ship_type"],$_SESSION["fibercart"],$_SESSION["cart"],$_SESSION["papercart"],$_SESSION["pulp_shpitems"],$catid,$_SESSION["ship_special"],
        $_SESSION["ship_rush"],$_SESSION["ship_cod"],'showcart');  // 6/3/2010 added papercart for tube (action_fns)
   } 
   /*else {   echo "ship_state: ".$_SESSION["ship_state"]." <br />";
  //phpinfo();
  exit;	}  */
	    
	//Show carts that have items...
	if(isset($cart) && count($cart))
	{
		display_tr_cart($cart,$catid);
	}
	if (isset($fibercart)&& count($fibercart)) 
	{
      display_tr_fibercart($fibercart,'edit');
	}
	if (isset($pulpcart)&& count($pulpcart)) 
	{
		display_tr_pulpcart($pulpcart,'edit');
	}
	if (isset($papercart)&& count($papercart))
	{
		display_tr_papercart($papercart,'edit');
	}
    /*
    br(1);
    print_r($cart);   //xxx
    br(1);
    print_r($papercart);  //xxx
    br(1);
    print_r($fibercart);  //xxx
    br(1);
    print_r($pulpcart);  //xxx
    br(1);
    exit;
    */   
   // Carts are displayed, now have to show ship_string 
   // followed by order_totals.  Then, have a few conditions:
   //   1. No one logged in: prompt for login
   //   2. User logged in directly
   //   3. Admin in:  

   // Show ship_string, order totals and get_shipping changes
	echo "<table class=cart border=1 width='$g_table1_width'><tr>
		  	<td valign=top align=right>"; 
	if ($_SESSION["ship_string"])
   		echo '<p class=text9px>'.$_SESSION["ship_string"].'</p>';
        
	else
		  	echo "&nbsp;";

   //Show get_ship_state() action_fns.php - shows statelist if NO UACCT
   $state_list = get_statelist(); 
   get_ship_state($state_list,$_SESSION["ship_state"],$_SESSION["ship_type"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"],$_SESSION["ship_cod"],'showcart');  //display state dropdown, which figures S&H and TOTAL

   // 5/10/2013 Calc; display discount detail
   // calc_discounts($dcodes);  //$cart,$fibercart,$pulpcart,$papercart, 5/10/2013)
                             // See tr_fns.php/calc_discounts for output   
   // show_discount_form($dcodes,'showcart.php');  // Get discount codes   5/10/2013
              
   echo "</td><td width=160 align=right valign=top >";
		
   // 7/7/04 -- add tax on SO if ship_state= 'IN'
   if ($_SESSION["ship_state"]==$g_taxstate)
         $this_state = $g_taxstate;
   else
         $this_state = '==';
    
   // Calc and display the S&H and Order Totals:
   // 12/05/2010 Move calc_shipping here... from above
   
   // 5/10/2013 Revise next to calc and display total_discount   & 'Customer Discount'
   $thesetotals=get_order_totals($_SESSION["total_price"],$_SESSION["total_discount"],$_SESSION["fiber_price"],$_SESSION["ship_charge"],$_SESSION["pulp_price"],$_SESSION["paper_price"],$this_state,'','');  // shows table _SESSION
   display_order_totals($thesetotals);

	echo "</td></tr></table>";

/// ---
// 6/18/2010 --Editing width and format of get_totals display back
//             to  table layout (not div). Retained table for admin features
// <a name=shipping></a>

	echo "<table width='$g_table1_width' border=0 ><tr>
	  	  <td class=cartwarning valign=top align=left>";


// Now , have 3 conditions"
//    -no one logged in   -- prmpt msg to login and box to check frt
//    -user logged in directly -- have ship_state 
//    -admin w/ or w/o user -- get(customer)
  
	if (!isset($_SESSION["SESSION"]) ) // -no one logged in
	{
		echo '&nbsp;To complete this order, please <a class=cartmenu HREF=weblogin.php>&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;&nbsp;&nbsp;</a><br>
		      &nbsp;or to estimate shipping, select your state and press <b>Go</b>:<br>';
		// $state_list = get_statelist();
		// get_ship_state($state_list,$_SESSION["ship_state"],$_SESSION["ship_type"],$catid,$_SESSION["ship_special"],$_SESSION["ship_rush"],$_SESSION["ship_cod"],'showcart');  //display state dropdown, which figures S&H and TOTAL
    }
    else
    {
		// if logged in as admin, then have access to bottom admin menu so as to get new customer..
		if (check_admin_user())
		{
         if (!isset($_SESSION["SESSION_UACCT"]))
         get_customer('showcart.php');
		}
	   else 
	   {
	     // - user logged in; have ship_state
   	}
		
	}

	echo "</td></tr></table>";
}
else
{
 	echo "<p class=text9px>No items in your cart</p>";
}

//echo $_SESSION['SESSION'].' '.$_SESSION["ship_state"].'  '.$_SESSION["SESSION_UACCT"]; 			

//echo "</td></tr></table>"; 
//silk_footer($g_table1_align,$g_table1_width);


?>
