<?php
// 6/5/2010 Edited
// In production, session[UNAME] not searchstr to force login;
//                activate ccval on ccnum
// php 5.30

include('book_sc_fns.php');
session_start();
//error_reporting(E_ALL ^ E_NOTICE);

// This is called from cgi-bin/commitcgi.php after the order is processed -- shouldn't be in SSL
// Calling sequence is payments->commitcgi->order_conf->

if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost")
{
  	// working locally so don't do anything
  	//include ('clearcartcode.php'); 
  	//pass_msg("Local Order ~submitted. [order_conf.php]");
}
else
{
 	if (!isset($_SESSION['searchstr']))      //
 	   header("location:showprod.php"); //don't ever come here if no order

    // 8/5/21 cmtted too many redirects
	//if ($_SERVER["SERVER_PORT"] != "443")
  	//	header("location:https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]);
}



if (isset($_GET['res']))   // Passed result from commitcgi...
{
  $result=$_GET['res'];
  $contents= unserialize(base64_decode($_GET['str']));  //6/5/10 req'd at site!!
  $rush = $_GET['rush'];  //01/14/05 when result='99', require callback
 
  //Save vars before clear cart for syslog() if problem
  $loginfo = $result.' '.$_SESSION['SESSION_UNAME'].' UID='.$_SESSION['SESSION_UID'].' ACCT='.$_SESSION['SESSION_UACCT'];

  include ('clearcartcode.php');  //Erase Cart/order 

  if ($result=='0')
  {
    $message = "<br>Your order has been successfully processed.";

    // Added the '1' to signal staying in SSL mode
    pass_msg($contents.'<br>'.$message."<br>
               <br><br>
                To view or reorder these items in the future, click on MyOrderHistory on the <b>MyMenu</b> link above.
                <br><br> For security reasons, before exiting the site, please <a href=weblogoff.php>Logout</a>"
                ,1);
  }
  else //if ($result=='97/98)  Not successful; during encryption
  {
    //gpg/email failed. Send person to new weberror page 
    //so they can call the office with CC#
    if ($_SERVER["SERVER_NAME"]=="127.0.0.1" || $_SERVER["SERVER_NAME"]=="localhost")  //Avoid on dev box
    {
      sys_log("Failed: Order Encrption: $contents $loginfo ");
      header("location:weberror.php?message=LocalEncryptionFails");
      exit;
    }
    else
    {
      //Bad situation: log the occurence:
      sys_log("Failed: Order Encrption: $contents $loginfo ");
      header("location:weberror.php?cntrl=1");
      exit;
    }
    
  }
}

?>
