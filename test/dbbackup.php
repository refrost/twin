<?php

/*
Current version runs in cron daily at 1:10am to backup the 
"store" db to the /home/twinrocker/dbbackup directory
Each Monday morning the compressed file is emailed to gail;
Every other day compressed file is emailed  to twinrocker@gmail.com
(pswd ser....) for archived storage until manually deleted...

Quickly and easily backup your MySQL database and have the tgz emailed to you.  You need PEAR installed with the Mail and Mail_Mime packages installed. Read more about PEAR here: http://pear.php.net. This will work in any *nix enviornment. Make sure you have write access to your /tmp directory.
http://www.sematopia.com/?p=61
Adapted to trhmp  4/28/2009 -Ralph Frost
*/
// NO -> Does NOT Depends on Zend Mail/Mime components loaded at /twinrocker/inc/zend

ini_set("include_path",".:/home/twinrocker/inc");
//require_once('Zend/Mail.php');
//require_once('Zend/Mime.php');

// mysql details..
$tmpDir = "/home/twinrocker/dbbackup/";
require_once('dbvars.php');  //load database vars from include dir
$prefix = "db_";

// Email settings...
if (date("l")!=='Monday')
   $to = "twinrocker@gmail.com"; //"ralph.frost@gmail.com";
else
   $to = "gail@twinrockerhandmadepaper.com"; //For Monday mailing
   
$toname = 'trhmp archive';
$from = "cron@trhmp.com";
$fromname = 'Cron Data backup';
$subject = 'Routine db Backup '.date('Y_m_d_h:i');
$sqlFile = $tmpDir.'store'.".sql";  //$prefix.date('Y_m_d')
$attachment = $tmpDir.'store'.".tgz";
//$zipname = $tmpDir.$prefix.date('Y_m_d').".zip";

$creatBackup = "mysqldump -u ".$user." --password=".$password." ".$dbName." > ".$sqlFile;
$createZip = "tar cvzf $attachment $sqlFile";
//echo $creatBackup.'<br />'.$createZip.'<br />';
echo "Starting data backup...";
exec($creatBackup);
exec($createZip);

// Pear approach -- http://www.theblog.ca/mysql-email-backup#comment-9819


/* //Create Zend_Mail with attachment...  DIDN'T WORK RIGHT !!
$mail = new Zend_Mail();
$mail->setBodyText('TRHMP db_Backup.');
$mail->setFrom($from, $fromname);
$mail->addTo($to, $toname);
$mail->setSubject($subject);
$myFile = file_get_contents($attachment); // $sqlFile);  // Read the file
$at = $mail->createAttachment(myFile);
$at->type = 'text/plain';
$at->disposition = Zend_Mime::DISPOSITION_INLINE;
$at->encoding    = Zend_Mime::ENCODING_8BIT;
$at->filename = 'store'.'.tgz';
$result = $mail->send();
*/

// print_r($result);

// -------------------- Alternate code FYI -------------------

//Send array of attached files No Objects...

// array with filenames to be sent as attachment
$files = array($attachment);  //$sqlFile); 
 
// email fields: to, from, subject, and so on
//$to = "mail@mail.com";
//$from = "mail@mail.com"; 
//$subject ="My subject"; 
$message = "Routine db Backup";
$headers = "From: $from";
 
// boundary 
$semi_rand = md5(time()); 
$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
 
// headers for attachment 
$headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 
 
// multipart boundary 
$message = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/plain; charset=\"iso-8859-1\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n"; 
$message .= "--{$mime_boundary}\n";
 
// preparing attachments
for($x=0;$x<count($files);$x++){
    $file = fopen($files[$x],"rb");
    $data = @fread($file,filesize($files[$x]));
    fclose($file);
    $data = chunk_split(base64_encode($data));
    $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$files[$x]\"\n" . 
    "Content-Disposition: attachment;\n" . " filename=\"$files[$x]\"\n" . 
    "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
    $message .= "--{$mime_boundary}\n";
}
 
// send
 
$ok = @mail($to, $subject, $message, $headers); 
if ($ok) { 
    echo "<p>Backup sent to $to!</p>"; 
} else { 
    echo "<p>Email could not be sent!</p>"; 
} 
// Uncomment to delete the temp files from server: ... Now save on server
//unlink($sqlFile);
//unlink($attachment);

echo "  DONE! ".date('Y_m_d_h:i')."<br /><br />";

?>
