<?php
  // php 5.3  6/24/2015
  include ('book_sc_fns.php');
  // recalcpaper.php 7/30/04 made from recalccart.php  5/20/03  (from recalc_cart.php) redirects to showcart for tr/css/web version
  
  session_start();
  $rows   =  $_POST["rownum"];
  $newqty =  $_POST["newqty"];

	$xcart = $_SESSION["papercart"];	//this is the paper lineitems
    /*
    echo '<pre>';
    print_r($_POST);
    echo '<br><br>';
    print_r($xcart);
    echo '</pre>';
    */
    //exit;
    
  // Now go down through the lineitems and compare/fix the newqty vs the prior qty
  $numrows = count($rows);
  reset($xcart);
  
  for ($i=0;$i<$numrows;$i++)
  {
    if ($newqty[$i] <= 0)
      unset($xcart[$i]);
    else
      if ( $newqty[$i] != $xcart[$i]["qty"])
      {
          // QTY is different so must change two things
          $xcart[$i]["qty"] =  $newqty[$i];
          $xcart[$i]["totalamount"] = $xcart[$i]["qty"]*($xcart[$i]["price"]+$xcart[$i]["surfaceprice"]);
      }
  }
  
  //Save the edited and reindexed  cart array.
  // 09/15/2019 added array_values() to reindex
  $xcart = array_values($xcart);
    
	//Calculate paper items...
   	$count = 0;
  	$val = 0.00;
	reset($xcart);
    
    foreach ($xcart as $key => $row)
		  {
	 	  if ($xcart[$key]['qty']>0)
	 		  {
  	 		   $count = $count + $xcart[$key]['qty'];
	   		$val= $val + $xcart[$key]['totalamount'];
		  	}
		  }
  //Store into session_vars
  $_SESSION["papercart"] = $xcart;
  $_SESSION["paper_items"] = $count;
  $_SESSION["paper_price"] = $val;

  header("location:showcart.php");
  exit;

?>
